<?php
/**
 * Loads parent and child theme scripts.
 */
function shoptimizer_child_enqueue_scripts() {
	$parent_style    = 'shoptimizer-style';
	$parent_base_dir = 'shoptimizer';
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css', array(), wp_get_theme( $parent_base_dir ) ? wp_get_theme( $parent_base_dir )->get( 'Version' ) : '' );

	if ( is_rtl() ) {
		wp_enqueue_style( 'shoptimizer-rtl', get_template_directory_uri() . '/rtl.css', array(), wp_get_theme( $parent_base_dir ) ? wp_get_theme( $parent_base_dir )->get( 'Version' ) : '' );
	}

    wp_enqueue_style( 'shoptimizer-child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version') );
}

add_action( 'wp_enqueue_scripts', 'shoptimizer_child_enqueue_scripts', 99 );

// Ship to a different address closed by default
 
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );

/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

function shoptimizer_custom_fonts( $standard_fonts ) {
	$my_custom_fonts = array();
	$my_custom_fonts['semplicitapro'] = array(
		'label' => 'semplicitapro',
		'variants' => array('regular','italic','700','700italic', 300, 400, 700),
		'stack' => 'semplicitapro, sans-serif',
	);
return array_merge_recursive( $my_custom_fonts, $standard_fonts );
}
add_filter( 'kirki/fonts/standard_fonts', 'shoptimizer_custom_fonts', 20 );

add_filter( 'woocommerce_loop_add_to_cart_link', 'add_to_cart_nofollow', 10, 2 );
function add_to_cart_nofollow($html, $product){
	$html = sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( isset( $quantity ) ? $quantity : 1 ),
		esc_attr( $product->get_id() ),
		esc_attr( $product->get_sku() ),
		esc_attr( isset( $class ) ? $class : 'button' ),
		esc_html( $product->add_to_cart_text() )
	);
	return $html;
}
add_action('plugins_loaded','ao_defer_inline_init');

function ao_defer_inline_init() {
if ( get_option('autoptimize_js_include_inline') != 'on' ) {
add_filter('autoptimize_html_after_minify','ao_defer_inline_jquery',10,1);
}
}

function ao_defer_inline_jquery( $in ) {
if ( preg_match_all( '#<script.*>(.*)</script>#Usmi', $in, $matches, PREG_SET_ORDER ) ) {
foreach( $matches as $match ) {
if ( $match[1] !== '' && ( strpos( $match[1], 'jQuery' ) !== false || strpos( $match[1], '$' ) !== false ) ) {
// inline js that requires jquery, wrap deferring JS around it to defer it.
$new_match = 'var aoDeferInlineJQuery=function(){'.$match[1].'}; if (document.readyState === "loading") {document.addEventListener("DOMContentLoaded", aoDeferInlineJQuery);} else {aoDeferInlineJQuery();}';
$in = str_replace( $match[1], $new_match, $in );
} else if ( $match[1] === '' && strpos( $match[0], 'src=' ) !== false && strpos( $match[0], 'defer' ) === false ) {
// linked non-aggregated JS, defer it.
$new_match = str_replace( '<script ', '<script defer ', $match[0] );
$in = str_replace( $match[0], $new_match, $in );
}
}
}
return $in;
}

add_filter( 'jetpack_implode_frontend_css', '__return_false', 99 );

/**
 * Disable WooCommerce block styles (front-end).
 */
function slug_disable_woocommerce_block_styles() {
  wp_dequeue_style( 'wc-block-style' );
}
add_action( 'wp_enqueue_scripts', 'slug_disable_woocommerce_block_styles' );

remove_filter('template_redirect', 'redirect_canonical');  

// functions.php
add_filter( 'wpseo_schema_graph_pieces', 'remove_breadcrumbs_from_schema', 11, 2 );

/**
 * Removes the breadcrumb graph pieces from the schema collector.
 *
 * @param array  $pieces  The current graph pieces.
 * @param string $context The current context.
 *
 * @return array The remaining graph pieces.
 */
function remove_breadcrumbs_from_schema( $pieces, $context ) {
    return \array_filter( $pieces, function( $piece ) {
        return ! $piece instanceof \Yoast\WP\SEO\Generators\Schema\Breadcrumb;
    } );
}

/**
 * Change "Out Of Stock" text.
 *
 */
function mydomain_change_out_stock_string() {
	return __( 'Informeer naar levertijd', 'shoptimizer' );
}
add_filter( 'shoptimizer_shop_out_of_stock_string', 'mydomain_change_out_stock_string' );

add_action('woocommerce_shop_loop_item_title', 'DHLBrand', 1);
function DHLBRand()
{
    global $product;
    $attribute  = 'pa_merk';
    $abv        = $product->get_attribute($attribute);
    if(!empty($abv))
    {
        echo '<h6 class="merk">' . $abv . '</h6>';
    }
}


add_action('woocommerce_single_product_summary', 'DHLBrand', 1);


