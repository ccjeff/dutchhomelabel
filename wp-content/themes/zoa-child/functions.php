<?php
/**
 * Theme functions file
 */

/**
 * Enqueue parent theme styles first
 * Replaces previous method using @import
 * <http://codex.wordpress.org/Child_Themes>
 */

add_action( 'wp_enqueue_scripts', 'zoa_enqueue_parent_theme_style', 99 );

function zoa_enqueue_parent_theme_style() {
	wp_enqueue_style( 'zoa-parent-style', get_template_directory_uri().'/style.css' );
}

/**
 * Add your custom functions below
 */

/**
* Change Proceed To Checkout Text in WooCommerce
* Place this in your Functions.php file
**/

function woocommerce_button_proceed_to_checkout() {
       $checkout_url = WC()->cart->get_checkout_url();
       ?>
       <a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Naar de kassa', 'woocommerce' ); ?></a>
       <?php
     }

// Ship to a different address closed by default
 
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );

/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

add_filter( 'woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2);
function wcs_custom_get_availability( $availability, $_product ) {
    
    $sync = get_post_meta($_product->get_id(), '_sync_a4s', true);

    if($sync == 'yes') {
        if ( $_product->is_in_stock() ) {
            $availability['availability'] = __('Binnen 2 werkdagen in huis!', 'woocommerce');
            // - Indien een product vanaf de koppeling op voorraad is, staat hier: leverbaar binnen 3-5 werkdagen;
        } else {
            $availability['availability'] = __('Niet op voorraad. Weten wanneer dit product weer op voorraad is? Neem hierover contact met ons op!', 'woocommerce');            
            // - Indien een product niet op voorraad is, staat hier: niet op voorraad;
        }
    } else {
        // - Indien een product in het schap ligt op locatie bij Dutch Home Label (Dus dat het vinkje Sync met App 4 sales uit staat), staat hier; voor 15.00 besteld, zelfde dag verzonden.
        if ( $_product->is_in_stock() ) {
            $availability['availability'] = __('Voor 15.00 uur besteld, zelfde dag verzonden', 'woocommerce');
        }
        if ( ! $_product->is_in_stock() ) {
            $availability['availability'] = __('Niet op voorraad. ', 'woocommerce');
        }
    }

    return $availability;
}


