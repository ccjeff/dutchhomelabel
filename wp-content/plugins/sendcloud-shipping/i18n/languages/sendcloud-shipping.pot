# Copyright (C) 2021 SendCloud B.V.
# This file is distributed under the same license as the SendCloud | Smart Shipping Service plugin.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: SendCloud | Smart Shipping Service 2.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/sendcloud-"
"shipping\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-09-28 15:40+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Domain: sendcloud-shipping\n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;"

#: includes/ServicePoint/Shipping/class-sc-service-point-shipping-method.php:62
msgid ""
"A comma separated list of your Sendcloud enabled carrier codes (e.g. ups, "
"dpd, dhl), an empty value here will display all your Sendcloud enabled "
"carriers"
msgstr ""

#: resources/views/wc-settings/sendcloud-page.php:18
msgid "Allows you to see your WooCommerce orders in the Sendcloud Panel."
msgstr ""

#: includes/ServicePoint/Shipping/class-sc-service-point-shipping-method.php:58
msgid "Carrier Selection"
msgstr ""

#: includes/Checkout/Api/class-checkout-api-service.php:105
#: includes/Integration/Api/class-integration-api-service.php:78
msgid "Configuration deleted"
msgstr ""

#: includes/Checkout/Api/class-checkout-api-service.php:81
msgid "Configuration updated"
msgstr ""

#: resources/views/wc-settings/sendcloud-page.php:16
msgid "Connect with Sendcloud"
msgstr ""

#: includes/Checkout/Shipping/NominatedDay/class-checkout-handler.php:85
msgid "Delivered by {carrier}"
msgstr ""

#: includes/Checkout/Shipping/class-abstract-free-shipping-shipping-method.php:102
msgid "Enable Free Shipping"
msgstr ""

#: resources/views/mail/delivery-date-mail-widget.php:12
#: resources/views/checkout/delivery-date-thank-you-widget.php:14
#: resources/views/checkout/delivery-date-admin-widget.php:12
msgid "Expected delivery date"
msgstr ""

#: includes/Checkout/Api/class-checkout-api-service.php:100
#: includes/Integration/Api/class-integration-api-service.php:73
msgid "Failed to delete checkout configuration."
msgstr ""

#: resources/views/wc-settings/sendcloud-page.php:28
msgid "Go to Sendcloud"
msgstr ""

#. URI of the plugin
#. Author URI of the plugin
msgid "http://sendcloud.sc"
msgstr ""

#: includes/Checkout/Shipping/class-abstract-free-shipping-shipping-method.php:115
msgid "If enabled, users will need to spend this amount to get free shipping."
msgstr ""

#: includes/Checkout/Api/class-checkout-api-service.php:76
msgid "Invalid checkout payload."
msgstr ""

#: includes/Checkout/Shipping/NominatedDay/class-nominated-day-shipping-method.php:27
msgid ""
"Let your buyer choose the delivery date. Configure the method in the "
"Sendcloud Checkout panel."
msgstr ""

#: includes/Checkout/Shipping/class-abstract-free-shipping-shipping-method.php:112
msgid "Minimum Order Amount for Free Shipping"
msgstr ""

#: includes/Checkout/Shipping/class-abstract-free-shipping-shipping-method.php:107
msgid "No"
msgstr ""

#: includes/ServicePoint/Api/class-sc-api-service-point.php:59
msgid "No data specified to enable the plugin"
msgstr ""

#: includes/Checkout/Shipping/NominatedDay/class-nominated-day-shipping-method.php:26
msgid "Nominated Day Delivery"
msgstr ""

#: resources/views/checkout/extend-order-details.php:13
#: resources/views/checkout/delivery-date-admin-widget.php:16
msgid "Non editable"
msgstr ""

#: includes/Checkout/Shipping/SameDay/class-same-day-shipping-method.php:23
msgid ""
"Offer same day delivery before a chosen cut-off time. Configure the method "
"in the Sendcloud Checkout panel."
msgstr ""

#: resources/views/wc-settings/sendcloud-page.php:30
msgid "Open the Sendcloud panel and start shipping."
msgstr ""

#: includes/Checkout/Shipping/NominatedDay/class-checkout-handler.php:124
msgid "Please choose a delivery date."
msgstr ""

#: includes/ServicePoint/class-checkout-handler.php:111
msgid "Please choose a service point."
msgstr ""

#: includes/ServicePoint/Api/class-sc-api-service-point.php:75
msgid "Plugin disabled"
msgstr ""

#: includes/ServicePoint/Api/class-sc-api-service-point.php:63
msgid "Plugin enabled"
msgstr ""

#: includes/Checkout/Shipping/SameDay/class-same-day-shipping-method.php:22
msgid "Same Day Delivery"
msgstr ""

#: includes/ServicePoint/class-checkout-handler.php:74
msgid "Select Service Point"
msgstr ""

#: includes/class-sc-settings-sendcloud.php:27
msgid "Sendcloud"
msgstr ""

#. Author of the plugin
msgid "Sendcloud B.V."
msgstr ""

#. Description of the plugin
msgid "Sendcloud plugin."
msgstr ""

#. Name of the plugin
msgid "Sendcloud | Smart Shipping Service"
msgstr ""

#: resources/views/plugin/deactivation-notice.php:2
msgid ""
"Sendcloud | Smart Shipping Service plugin deactivated itself because "
"WooCommerce is not available."
msgstr ""

#: resources/views/mail/extend-order-created-mail.php:10
#: resources/views/checkout/extend-order-details.php:9
#: resources/views/checkout/service-point-address-thank-you-widget.php:11
msgid "Service Point Address"
msgstr ""

#: includes/ServicePoint/Shipping/class-sc-service-point-shipping-method.php:27
msgid "Service Point Delivery"
msgstr ""

#: includes/ServicePoint/Shipping/class-sc-service-point-shipping-method.php:28
msgid ""
"Service Point Delivery lets the customer select a servicepoint in the "
"checkout."
msgstr ""

#: includes/Checkout/Shipping/Standard/class-standard-shipping-method.php:27
msgid ""
"Set up next day, standard, international depending on carrier. Configure the "
"method in the Sendcloud Checkout panel."
msgstr ""

#: includes/Checkout/Shipping/Standard/class--checkout-handler.php:56
#: includes/Checkout/Shipping/SameDay/class-checkout-handler.php:59
msgid "Shipping method not available."
msgstr ""

#: includes/Checkout/Shipping/Standard/class-standard-shipping-method.php:26
msgid "Standard Delivery"
msgstr ""

#: includes/Checkout/Shipping/class-abstract-free-shipping-shipping-method.php:108
msgid "Yes"
msgstr ""

#: resources/views/checkout/delivery-date-admin-widget.php:15
msgid "You can't change the selected delivery date"
msgstr ""

#: resources/views/checkout/extend-order-details.php:12
msgid "You can't change the selected Service Point"
msgstr ""

#: resources/views/wc-settings/sendcloud-page.php:22
msgid "You need to enable permalinks. Go to Settings -> Permalinks."
msgstr ""
