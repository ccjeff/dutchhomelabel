=== Sendcloud | The all-in-one shipping platform for WooCommerce ===
Version: 2.1.0
Developer: SendCloud Global B.V.
Developer URI: http://sendcloud.com
Tags: shipping, carriers, service point, delivery, webshop, woocommerce, dpd, dhl, ups, postnl, bpost, colissimo, fadello
Requires at least: 4.5.0
Tested up to: 5.8
Stable tag: 2.1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Contributors: sendcloudbv

SendCloud helps to grow your online store by optimizing the shipping process.
Shipping packages has never been that easy!

== Description ==

[youtube https://www.youtube.com/watch?v=0GUV5W0bNi0 ]

= Sendcloud, the all-in-one shipping platform that accelerates your growth =

“Shipping Sucks.”

We feel you.

Orders are trickling in, before having to be picked, labeled and shipped in time. Meanwhile your customers receive boring tracking emails and you’re kept busy handling returns. It takes a ton of time (and money) to do all of this properly.

Shipping should be easy, no matter what, where and how much you ship, so you can focus on growing your ecommerce business!

That’s why we built Sendcloud. A ridiculously easy to use shipping platform _that scales with you_.

* Increase checkout conversion by offering a wide choice of delivery options
* Let customers choose if they want to receive their parcel today, tomorrow, or on another specific date.
* Easily activate and ship with multiple carriers through a single platform
* Save a lot of time with automated shipping label generation process
* Provide the best post-purchase experience with branded tracking emails and pages that drive customer loyalty
* Avoid negative reviews by keeping customers in the loop, even during shipping delays
* Create a smooth and efficient returns process for both you and your customers with your own branded returns portal
* Get actionable insights from your shipping dashboard to drive business growth

The time is now to turn shipping into your competitive advantage. Join 23,000+ online
businesses that grow with Sendcloud.

= Integrate your WooCommerce with +35 international carriers =
Your business is unique. That’s why we’ve built integrations with the world’s leading shipping carriers. Connect up to +35 international carriers (and counting!) and switch in one click whenever needed. Upload your own carrier contract or ship directly via Sendcloud’s pre-negotiated shipping rates.

= Supported carriers =
DHL, DHL Express, DPD, UPS, FedEx, Hermes, Budbee, GLS, Royal Mail, PostNL, bpost, SEUR, Correos, Correos Express, Colissimo, Mondial Relay, Colis Prive, Lettre Suivie, Chronopost, Austrian Post, Deutsche Post, Red Je Pakketje, Trunkrs, Post Italiane, MRW, Homerr, BRT, ViaTim, Hurby, Fietskoeriers, StoreShippers, Delivengo, Quicargo,...Stay tuned! We’re integrating new carriers at a fast pace, so don’t miss out on any [developments](http://releaselog.sendcloud.com).

= How does Sendcloud work? =
Pretty darn smoothly! Connect your WooCommerce to Sendcloud in under 2 minutes (yep, literally).
Step 1: Choose the carriers that fit your store best.
Step 2: Optimise your checkout with your customers’ preferred shipping methods
Step 3: Start saving minutes on every order with picking lists, packing slips and shipping labels that can be printed simultaneously or separately.
Step 4: Build out your brand. The last mile is no longer just in the hands of carriers. Send branded track & trace notifications and use them to increase customer retention.
Step 5: Tackle returns and drive growth. Offer a seamless return process and handle returns without hassle. It will make both you and your clients happy!

== Installation ==

= General instructions =

1. Upload the plugin files to the `/wp-content/plugins/sendcloud` directory,
1. Activate the plugin through the 'Plugins' screen in WordPress
   or install the plugin through the WordPress plugins screen directly (recommended).
1. Activate your WooCommerce integration _with service point support_ in the
   Sendcloud Panel. Please, refer to _Integrations_ section in the
   [support page](https://support.sendcloud.sc) for more information about this.

= WooCommerce 2.5.x =

1. Go to WooCommerce->Settings->Shipping->Service Point Delivery
1. Check the _Enable this shipping method_
1. You may change your shipping costs by setting a new value for the _Cost_ field.
1. Click _Save changes_

= WooCommerce 2.6.x, 3.x.x =

1. Go to WooCommerce->Settings->Shipping
1. Use the _Add shipping method_ button in the _Shipping Zones_ listing and
   select _Service Point Delivery_ from the drop down menu.
1. You should see _Service Point Delivery_ in the _Shipping Method(s)_ column of the
   _Shipping Zones_ list.
1. You may change your shipping costs by clicking in the _Service Point Delivery_
   and setting a new value for the _Cost_ field.
1. Click _Save changes_

= Checking the installation =

Your customers should be able to select _Service Point Delivery_ in the checkout
page, alongside with a button labeled _Select Service Point_.


== Frequently Asked Questions ==

= Do I need a Sendcloud account to use this plugin? =

Yes. In order to connect, you must register for an account and then, follow the installation
instructions.

= What is a service point? =

Service Points are places that accept packages to be retrieved later by the customer.
e.g. A grocery store near your house or work may accept those packages.


== Screenshots ==

1. Process orders faster with on-screen packing slips
2. Let customers choose a specific delivery date in your checkout
3. Offer all shipping methods
4. Print shipping labels in one click
5. Automatically select the right shipping method
6. See shipment statuses in one overview
7. Automatically send tracking notifications
8. Automate your returns process
9. Save time and provide better return experience with your own return portal
10. Keep track of number with our analytics dashboard

== Changelog ==

= 2.1.0 =
* Added: Support for 2 new delivery methods in the Checkout feature for subscription users.
* New delivery method for small shop and above: standard delivery (international and domestic).
* New delivery method for large shop and above: same day delivery (domestic, dependent on carrier).
* Added: Holiday support (for same day/nominated day methods, configurable in Sendcloud platform).
* Added: Translations for all Sendcloud supported languages (English, Dutch, German, Spanish, French, Italian).
* WordPress compatibility check for 5.8.

= 2.0.3 =
* Fix - Updated translations.
* Fix - Service points delivery (with same free shipping threshold) are now all visible at checkout.

= 2.0.2 =
* Fix - Fixed rendering Nominated day.

= 2.0.1 =
* Fix - Enabled compatibility with Woo Funnels plugin to fix Service points Sendcloud.

= 2.0.0 =
* Add - Introduced compatibility with the Sendcloud Checkout module giving users the best delivery options.
  Sendcloud Checkout module allows for the following:
  Set up delivery options and customize them.
  Set the pricing for the delivery options based on weight classes.
  Offer time slots for delivery which are sensitive to cut-off times.
* Fix - Bug fixes related to compatibility Sendcloud Servicepoints for Woocommerce version 3.

= 1.1.2 =
* Added support for Wordpress 5.3

= 1.1.1 =
* Added support for Wordpress 5.2.3

= 1.1.0 =
* Rebranding

= 1.0.17 =
* Add support to WooCommerce 3.5.5

= 1.0.16 =
* Fix translations

= 1.0.15 =
* Add carrier selection for service point shipping method
* Fix translations

= 1.0.14 =
* Improve WooCommerce 3.x.x compatibility

= 1.0.13 =
* Made permalink checks less strict

= 1.0.12 =
* Add support for custom site url's

= 1.0.11 =
* Made this plugin compatible with CloudFlare Rocket

= 1.0.10 =
* This plugin is now compatible with WooCommerce 3.0.x

= 1.0.9 =
* Added support for preventing the customer to ship to a service point when one of the items is too big.

= 1.0.8 =
* Service Point Shipping method can be set as free after certain amount of the order
* Add translation to select service point button

= 1.0.7 =
* Compatible with PHP 5.3
* Connect with SendCloud on new tab

= 1.0.6 =
* Added mo translation files

= 1.0.5 =
* Fix redirection to SendCloud Panel

= 1.0.4 =
* Fix headers on autoconnect

= 1.0.3 =
* Improved readme, documentation and screenshots

= 1.0.2 =
* Add auto connect button

= 1.0.1 =
* Add service point address on email

= 1.0.0 =
* Integrate WooCommerce with an existing SendCloud account enabling service point delivery
  locations to be selected at the checkout.
