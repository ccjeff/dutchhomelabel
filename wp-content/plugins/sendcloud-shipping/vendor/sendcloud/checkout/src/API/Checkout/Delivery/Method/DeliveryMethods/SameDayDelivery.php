<?php

namespace SendCloud\Checkout\API\Checkout\Delivery\Method\DeliveryMethods;

use SendCloud\Checkout\API\Checkout\Delivery\Method\DeliveryMethod;
use SendCloud\Checkout\API\Checkout\Delivery\Method\HandoverDay;
use SendCloud\Checkout\DTO\DataTransferObject;

class SameDayDelivery extends DeliveryMethod
{
    /**
     * @var HandoverDay[]
     */
    private $handoverDays;

    /**
     * @return \SendCloud\Checkout\API\Checkout\Delivery\Method\HandoverDay[]
     */
    public function getHandoverDays()
    {
        return $this->handoverDays;
    }

    /**
     * @param  \SendCloud\Checkout\API\Checkout\Delivery\Method\HandoverDay[]  $handoverDays
     */
    public function setHandoverDays($handoverDays)
    {
        $this->handoverDays = $handoverDays;
    }

    /**
     * Provides array representation of a dto.
     *
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();
        $array['parcel_handover_days'] = DataTransferObject::toArrayBatch($this->getHandoverDays());

        return $array;
    }

    /**
     * Set entity attributes from array
     *
     * @param  array  $rawData
     */
    protected function setEntityAttributes(array $rawData)
    {
        parent::setEntityAttributes($rawData);
        $this->setHandoverDays(HandoverDay::fromArrayBatch(static::getValue($rawData, 'parcel_handover_days',
            array())));
    }
}