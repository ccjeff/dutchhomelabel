<?php

namespace SendCloud\Checkout\API\Checkout\Delivery\Method\DeliveryMethods;

use SendCloud\Checkout\API\Checkout\Delivery\Method\DeliveryMethod;
use SendCloud\Checkout\API\Checkout\Delivery\Method\OrderPlacementDay;
use SendCloud\Checkout\DTO\DataTransferObject;

class StandardDelivery extends DeliveryMethod
{
    /**
     * @var \SendCloud\Checkout\API\Checkout\Delivery\Method\OrderPlacementDay[]
     */
    private $orderPlacementDays;

    /**
     * @return \SendCloud\Checkout\API\Checkout\Delivery\Method\OrderPlacementDay[]
     */
    public function getOrderPlacementDays()
    {
        return $this->orderPlacementDays;
    }

    /**
     * @param \SendCloud\Checkout\API\Checkout\Delivery\Method\OrderPlacementDay[] $orderPlacementDays
     */
    public function setOrderPlacementDays($orderPlacementDays)
    {
        $this->orderPlacementDays = $orderPlacementDays;
    }

    /**
     * Provides array representation of a dto.
     *
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();
        $array['order_placement_days'] = DataTransferObject::toArrayBatch($this->getOrderPlacementDays());

        return $array;
    }

    /**
     * Set entity attributes from array
     *
     * @param array $rawData
     */
    protected function setEntityAttributes(array $rawData)
    {
        parent::setEntityAttributes($rawData);
        $this->setOrderPlacementDays(OrderPlacementDay::fromArrayBatch(static::getValue($rawData,
            'order_placement_days',
            array())));
    }
}