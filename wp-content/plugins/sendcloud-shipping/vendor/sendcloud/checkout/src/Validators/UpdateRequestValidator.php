<?php

namespace SendCloud\Checkout\Validators;

use SendCloud\Checkout\Contracts\Validators\RequestValidator;
use SendCloud\Checkout\DTO\Validator;
use SendCloud\Checkout\Exceptions\ValidationException;
use SendCloud\Checkout\HTTP\Request;

/**
 * Class UpdateRequestValidator
 *
 * @package SendCloud\Checkout\Validators
 */
class UpdateRequestValidator implements RequestValidator
{
    /**
     * @var array[] Payload schema.
     */
    private static $schema = array(
        array(
            'field' => 'checkout_configuration',
            'type' => 'complex',
            'child' => array(
                array(
                    'field' => 'id',
                    'type' => 'string'
                ),
                array(
                    'field' => 'version',
                    'type' => 'int'
                ),
                array(
                    'field' => 'updated_at',
                    'type' => 'string'
                ),
                array(
                    'field' => 'minimal_plugin_version',
                    'type' => 'string'
                ),
                array(
                    'field' => 'delivery_zones',
                    'type' => 'collection',
                    'empty' => false,
                    'child' => array(
                        array(
                            'field' => 'id',
                            'type' => 'string'
                        ),
                        array(
                            'field' => 'location',
                            'type' => 'complex',
                            'child' => array(
                                array(
                                    'field' => 'country',
                                    'type' => 'complex',
                                    'child' => array(
                                        array(
                                            'field' => 'iso_2',
                                            'type' => 'string'
                                        ),
                                        array(
                                            'field' => 'name',
                                            'type' => 'string'
                                        ),
                                    )
                                )
                            )
                        ),
                        array(
                            'field' => 'delivery_methods',
                            'type' => 'collection',
                            'empty' => false,
                            'child' => array(
                                array(
                                    'field' => 'id',
                                    'type' => 'string'
                                ),
                                array(
                                    'field' => 'delivery_method_type',
                                    'type' => 'string'
                                ),
                                array(
                                    'field' => 'external_title',
                                    'type' => 'string'
                                ),
                                array(
                                    'field' => 'internal_title',
                                    'type' => 'string'
                                ),
                                array(
                                    'field' => 'carrier',
                                    'type' => 'complex',
                                    'child' => array(
                                        array(
                                            'field' => 'name',
                                            'type' => 'string'
                                        ),
                                        array(
                                            'field' => 'code',
                                            'type' => 'string'
                                        ),
                                        array(
                                            'field' => 'logo_url',
                                            'type' => 'string'
                                        ),
                                    )
                                ),
                                array(
                                    'field' => 'sender_address_id',
                                    'type' => 'int'
                                ),
                                array(
                                    'field' => 'time_zone_name',
                                    'type' => 'string'
                                ),
                                array(
                                    'field' => 'show_carrier_information_in_checkout',
                                    'type' => 'bool'
                                ),
                                array(
                                    'field' => 'shipping_product',
                                    'type' => 'complex',
                                    'child' => array(
                                        array(
                                            'field' => 'code',
                                            'type' => 'string'
                                        ),
                                        array(
                                            'field' => 'name',
                                            'type' => 'string'
                                        ),
                                        array(
                                            'field' => 'lead_time_hours',
                                            'type' => 'int'
                                        ),
                                        array(
                                            'field' => 'selected_functionalities',
                                            'type' => 'array'
                                        ),
                                        array(
                                            'field' => 'carrier_delivery_days',
                                            'type' => 'complex',
                                            'child' => array(
                                                array(
                                                    'field' => 'monday',
                                                    'type' => 'complex',
                                                    'nullable' => true,
                                                    'child' => array(
                                                        array(
                                                            'field' => 'enabled',
                                                            'type' => 'bool',
                                                        ), array(
                                                            'field' => 'start_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'start_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ),
                                                    )
                                                ),
                                                array(
                                                    'field' => 'tuesday',
                                                    'type' => 'complex',
                                                    'nullable' => true,
                                                    'child' => array(
                                                        array(
                                                            'field' => 'enabled',
                                                            'type' => 'bool',
                                                        ), array(
                                                            'field' => 'start_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'start_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ),
                                                    )
                                                ),
                                                array(
                                                    'field' => 'wednesday',
                                                    'type' => 'complex',
                                                    'nullable' => true,
                                                    'child' => array(
                                                        array(
                                                            'field' => 'enabled',
                                                            'type' => 'bool',
                                                        ), array(
                                                            'field' => 'start_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'start_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ),
                                                    )
                                                ),
                                                array(
                                                    'field' => 'thursday',
                                                    'type' => 'complex',
                                                    'nullable' => true,
                                                    'child' => array(
                                                        array(
                                                            'field' => 'enabled',
                                                            'type' => 'bool',
                                                        ), array(
                                                            'field' => 'start_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'start_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ),
                                                    )
                                                ),
                                                array(
                                                    'field' => 'friday',
                                                    'type' => 'complex',
                                                    'nullable' => true,
                                                    'child' => array(
                                                        array(
                                                            'field' => 'enabled',
                                                            'type' => 'bool',
                                                        ), array(
                                                            'field' => 'start_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'start_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ),
                                                    )
                                                ),
                                                array(
                                                    'field' => 'saturday',
                                                    'type' => 'complex',
                                                    'nullable' => true,
                                                    'child' => array(
                                                        array(
                                                            'field' => 'enabled',
                                                            'type' => 'bool',
                                                        ), array(
                                                            'field' => 'start_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'start_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ),
                                                    )
                                                ),
                                                array(
                                                    'field' => 'sunday',
                                                    'type' => 'complex',
                                                    'nullable' => true,
                                                    'child' => array(
                                                        array(
                                                            'field' => 'enabled',
                                                            'type' => 'bool',
                                                        ), array(
                                                            'field' => 'start_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'start_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_hours',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ), array(
                                                            'field' => 'end_time_minutes',
                                                            'type' => 'int',
                                                            'nullable' => true,
                                                        ),
                                                    )
                                                )
                                            )
                                        ),
                                    )
                                ),
                                array(
                                    'field' => 'parcel_handover_days',
                                    'type' => 'complex',
                                    'required' => false,
                                    'child' => array(
                                        array(
                                            'field' => 'monday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'tuesday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'wednesday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'thursday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'friday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'saturday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'sunday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        )
                                    )
                                ),
                                array(
                                    'field' => 'order_placement_days',
                                    'type' => 'complex',
                                    'required' => false,
                                    'child' => array(
                                        array(
                                            'field' => 'monday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'tuesday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'wednesday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'thursday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'friday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'saturday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        ),
                                        array(
                                            'field' => 'sunday',
                                            'type' => 'complex',
                                            'nullable' => true,
                                            'child' => array(
                                                array(
                                                    'field' => 'enabled',
                                                    'type' => 'bool'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_hours',
                                                    'type' => 'int'
                                                ),
                                                array(
                                                    'field' => 'cut_off_time_minutes',
                                                    'type' => 'int'
                                                ),
                                            )
                                        )
                                    )
                                ),
                                array(
                                    'field' => 'holidays',
                                    'type' => 'collection',
                                    'empty' => true,
                                    'required' => false,
                                    'child' => array(
                                        array(
                                            'field' => 'frequency',
                                            'type' => 'string',
                                            'nullable' => true,

                                        ),
                                        array(
                                            'field' => 'from_date',
                                            'type' => 'string'
                                        ),
                                        array(
                                            'field' => 'recurring',
                                            'type' => 'bool'
                                        ),
                                        array(
                                            'field' => 'title',
                                            'type' => 'string'
                                        ),
                                        array(
                                            'field' => 'to_date',
                                            'type' => 'string'
                                        ),
                                    )
                                ),
                            )
                        ),
                    )
                ),
            )
        )
    );

    /**
     * @var string Plugin version.
     */
    private $plugin_version;

    public function __construct($plugin_version)
    {
        $this->plugin_version = $plugin_version;
    }

    /**
     * Validates array.
     *
     * @param Request $request
     *
     * @return void
     * @throws ValidationException
     */
    public function validate(Request $request)
    {
        $body = $request->getBody();
        if (empty($body)) {
            throw new ValidationException(array(
                array(
                    'path' => array('body'), 'message' => 'Request payload is empty.'
                )
            ));
        }

        if (!array_key_exists('checkout_configuration', $body)) {
            throw new ValidationException(array(
                array(
                    'path' => array('checkout_configuration'),
                    'message' => "Field is required but missing."
                )
            ));
        }

        if (!array_key_exists('minimal_plugin_version', $body['checkout_configuration'])) {
            throw new ValidationException(array(
                array(
                    'path' => array('checkout_configuration', 'minimal_plugin_version'),
                    'message' => "Field is required but missing."
                )
            ));
        }

        if (!$this->checkIfVersionFormatIsValid($body['checkout_configuration']['minimal_plugin_version'])) {
            throw new ValidationException(array(
                array(
                    'path' => array('checkout_configuration', 'minimal_plugin_version'),
                    'message' => "Format is not valid."
                )
            ));
        }

        if (version_compare($body['checkout_configuration']['minimal_plugin_version'], $this->plugin_version, '>')) {
            throw new ValidationException(array(
                array(
                    'path' => array('checkout_configuration', 'minimal_plugin_version'),
                    'message' => "Plugin version mismatch detected. Requested to publish a checkout configuration for plugin version {$body['checkout_configuration']['minimal_plugin_version']}, but the plugin version in use is {$this->plugin_version}."
                )
            ));
        }

        Validator::validate(static::$schema, $request->getBody());
    }

    /**
     * Check if the version format is valid
     *
     * @param $version
     *
     * @return bool
     */
    private function checkIfVersionFormatIsValid($version)
    {
        $split = explode(".", $version);
        foreach ($split as $s) {
            if (!is_numeric($s)) {
                return false;
            }
        }

        return true;
    }
}