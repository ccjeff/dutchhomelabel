<?php

namespace SendCloud\Checkout\DTO;

use SendCloud\Checkout\Exceptions\DTO\DTOValidationException;

/**
 * Class Validator
 *
 * @package SendCloud\Checkout\DTO
 */
class Validator
{
    /**
     * Validates data against schema.
     *
     * @param array $schema
     * @param array $data
     * @throws DTOValidationException
     */
    public static function validate(array $schema, array $data)
    {
        $errors = array();
        static::doValidate($schema, $data, $errors, []);
        if (!empty($errors)) {
            throw new DTOValidationException($errors);
        }
    }

    /**
     * Performs validation.
     *
     * @param array $schema
     * @param array $data
     * @param array $errors
     * @param array $path
     */
    private static function doValidate(array $schema, array $data, array &$errors, array $path)
    {
        foreach ($schema as $field) {
            // Validate field's existence.
            if (!array_key_exists($field['field'], $data)) {
                if (!(array_key_exists('required', $field) && $field['required'] === false)){
                    $errors[] = array(
                        'path' => static::formatPath($path, $field['field']),
                        'message' => 'Field is required.',
                    );
                }

                continue;
            }

            // Validate nullable field.
            if ($data[$field['field']] === null) {
                if (empty($field['nullable'])) {
                    $errors[] = array(
                        'path' => static::formatPath($path, $field['field']),
                        'message' => 'Field cannot be null.',
                    );
                }

                continue;
            }

            // Validate non-scalar fields.
            if ($field['type'] === 'complex') {
                self::doValidate($field['child'], $data[$field['field']], $errors, array_merge($path, [$field['field']]));
            } elseif ($field['type'] === 'collection') {
                if (empty($data[$field['field']]) && !$field['empty']) {
                    $errors[] = array(
                        'path' => static::formatPath($path, $field['field']),
                        'message' => 'Collection is empty',
                    );

                    continue;
                }

                foreach ($data[$field['field']] as $index => $item) {
                    if ($item === null) {
                        if (empty($field['contains_null'])) {
                            $errors[] = array(
                                'path' => static::formatPath($path, $field['field']),
                                'message' => 'Collection cannot contain null values.',
                            );
                        }

                        continue;
                    }

                    self::doValidate($field['child'], $item, $errors, array_merge($path, [$field['field'], $index]));
                }
            }
        }
    }

    /**
     * Formats path
     *
     * @param array $parent
     * @param string $field
     * @return array
     */
    static private function formatPath($parent, $field)
    {
        $parent[] = $field;

        return $parent;
    }
}