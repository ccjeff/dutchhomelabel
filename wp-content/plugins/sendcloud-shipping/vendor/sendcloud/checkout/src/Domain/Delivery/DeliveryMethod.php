<?php

namespace SendCloud\Checkout\Domain\Delivery;

use SendCloud\Checkout\API\Checkout\Delivery\Method\DeliveryMethods\NominatedDayDelivery;
use SendCloud\Checkout\API\Checkout\Delivery\Method\DeliveryMethods\SameDayDelivery;
use SendCloud\Checkout\API\Checkout\Delivery\Method\DeliveryMethods\StandardDelivery;
use SendCloud\Checkout\Domain\Delivery\Availability\AvailabilityPolicyFactory;
use SendCloud\Checkout\Domain\Interfaces\DTOInstantiable;
use SendCloud\Checkout\Domain\Interfaces\Identifiable;
use SendCloud\Checkout\Domain\Interfaces\Updateable;
use SendCloud\Checkout\Utility\CollectionComparator;

/**
 * Class DeliveryMethod
 *
 * @package SendCloud\Checkout\Domain\Delivery
 */
class DeliveryMethod implements DTOInstantiable, Updateable, Identifiable
{
    /**
     * Assigned by the API.
     *
     * @var string
     */
    protected $id;
    /**
     * Id assigned by the system when internal delivery method representation has been created.
     *
     * @var int | string | null
     */
    protected $systemId;
    /**
     * Reference to a delivery zone. Null if unknown.
     *
     * @var string | null
     */
    protected $deliveryZoneId;
    /**
     * @var string
     */
    protected $type;
    /**
     * @var string
     */
    protected $externalTitle;
    /**
     * @var string
     */
    protected $internalTitle;
    /**
     * @var Carrier
     */
    protected $carrier;
    /**
     * @var int
     */
    protected $senderAddressId;
    /**
     * @var bool
     */
    protected $showCarrierInfoOnCheckout;
    /**
     * @var ShippingProduct
     */
    protected $shippingProduct;
    /**
     * @var HandoverDay[] | OrderPlacementDay[]
     */
    protected $processingDays;
    /**
     * @var string
     */
    protected $timeZoneName;
    /**
     * @var Holiday[]
     */
    protected $holidays;
    /**
     * @var string
     */
    protected $rawConfig;

    /**
     * DeliveryMethod constructor.
     *
     * @param string $id
     * @param int|string|null $systemId
     * @param string|null $deliveryZoneId
     * @param string $type
     * @param string $externalTitle
     * @param string $internalTitle
     * @param Carrier $carrier
     * @param int $senderAddressId
     * @param bool $showCarrierInfoOnCheckout
     * @param ShippingProduct $shippingProduct
     * @param HandoverDay[] | OrderPlacementDay[] $processingDays
     * @param string $timeZoneName
     * @param Holiday[] $holidays
     * @param $rawConfig
     */
    public function __construct(
        $id,
        $systemId,
        $deliveryZoneId,
        $type,
        $externalTitle,
        $internalTitle,
        Carrier $carrier,
        $senderAddressId,
        $showCarrierInfoOnCheckout,
        ShippingProduct $shippingProduct,
        array $processingDays,
        $timeZoneName,
        array $holidays,
        $rawConfig
    ) {
        $this->id = $id;
        $this->systemId = $systemId;
        $this->deliveryZoneId = $deliveryZoneId;
        $this->type = $type;
        $this->externalTitle = $externalTitle;
        $this->internalTitle = $internalTitle;
        $this->carrier = $carrier;
        $this->showCarrierInfoOnCheckout = $showCarrierInfoOnCheckout;
        $this->shippingProduct = $shippingProduct;
        $this->processingDays = $processingDays;
        $this->timeZoneName = $timeZoneName;
        $this->holidays = $holidays;
        $this->rawConfig = $rawConfig;
        $this->senderAddressId = $senderAddressId;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int|string|null
     */
    public function getSystemId()
    {
        return $this->systemId;
    }

    /**
     * @param int|string|null $systemId
     */
    public function setSystemId($systemId)
    {
        $this->systemId = $systemId;
    }

    /**
     * @return string|null
     */
    public function getDeliveryZoneId()
    {
        return $this->deliveryZoneId;
    }

    /**
     * @param string|null $deliveryZoneId
     */
    public function setDeliveryZoneId($deliveryZoneId)
    {
        $this->deliveryZoneId = $deliveryZoneId;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getExternalTitle()
    {
        return $this->externalTitle;
    }

    /**
     * @param string $externalTitle
     */
    public function setExternalTitle($externalTitle)
    {
        $this->externalTitle = $externalTitle;
    }

    /**
     * @return string
     */
    public function getInternalTitle()
    {
        return $this->internalTitle;
    }

    /**
     * @param string $internalTitle
     */
    public function setInternalTitle($internalTitle)
    {
        $this->internalTitle = $internalTitle;
    }

    /**
     * @return Carrier
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param Carrier $carrier
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
    }

    /**
     * @return int
     */
    public function getSenderAddressId()
    {
        return $this->senderAddressId;
    }

    /**
     * @param int $senderAddressId
     */
    public function setSenderAddressId($senderAddressId)
    {
        $this->senderAddressId = $senderAddressId;
    }

    /**
     * @return bool
     */
    public function isShowCarrierInfoOnCheckout()
    {
        return $this->showCarrierInfoOnCheckout;
    }

    /**
     * @param bool $showCarrierInfoOnCheckout
     */
    public function setShowCarrierInfoOnCheckout($showCarrierInfoOnCheckout)
    {
        $this->showCarrierInfoOnCheckout = $showCarrierInfoOnCheckout;
    }

    /**
     * @return ShippingProduct
     */
    public function getShippingProduct()
    {
        return $this->shippingProduct;
    }

    /**
     * @param ShippingProduct $shippingProduct
     */
    public function setShippingProduct(ShippingProduct $shippingProduct)
    {
        $this->shippingProduct = $shippingProduct;
    }

    /**
     * @return HandoverDay[] | OrderPlacementDay[]
     */
    public function getProcessingDays()
    {
        return $this->processingDays;
    }

    /**
     * @param HandoverDay[] | OrderPlacementDay[] $handoverDays
     */
    public function setProcessingDays($handoverDays)
    {
        $this->processingDays = $handoverDays;
    }

    /**
     * @return string
     */
    public function getTimeZoneName()
    {
        return $this->timeZoneName;
    }

    /**
     * @param string $timeZoneName
     */
    public function setTimeZoneName($timeZoneName)
    {
        $this->timeZoneName = $timeZoneName;
    }

    /**
     * @return Holiday[]
     */
    public function getHolidays()
    {
        return $this->holidays;
    }

    /**
     * @param Holiday[] $holidays
     */
    public function setHolidays($holidays)
    {
        $this->holidays = $holidays;
    }

    /**
     * @return string
     */
    public function getRawConfig()
    {
        return $this->rawConfig;
    }

	/**
	 * Checks if delivery method is available
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function isAvailable()
	{
		$availabilityPolicy = AvailabilityPolicyFactory::create($this);

		return $availabilityPolicy->isAvailable();
	}

    /**
     * Makes an instance from dto.
     *
     * @param \SendCloud\Checkout\API\Checkout\Delivery\Method\DeliveryMethod $object
     *
     * @return DeliveryMethod
     */
    public static function fromDTO($object)
    {
        $processingDays = array();
        if ($object instanceof NominatedDayDelivery || $object instanceof SameDayDelivery) {
            foreach ($object->getHandoverDays() as $index => $handoverDay) {
                $processingDays[$index] = $handoverDay !== null ? HandoverDay::fromDTO($handoverDay) : null;
            }
        } elseif ($object instanceof StandardDelivery) {
            foreach ($object->getOrderPlacementDays() as $index => $handoverDay) {
                $processingDays[$index] = $handoverDay !== null ? OrderPlacementDay::fromDTO($handoverDay) : null;
            }
        }

        $holidays = array();
        foreach ($object->getHolidays() as $index => $holiday) {
            $holidays[$index] = $holiday !== null ? Holiday::fromDTO($holiday) : null;
        }

        $shippingProduct = ShippingProduct::fromDTO($object->getShippingProduct());
        $carrier = Carrier::fromDTO($object->getCarrier());

        return new DeliveryMethod(
            $object->getId(),
            null,
            null,
            $object->getType(),
            $object->getExternalTitle(),
            $object->getInternalTitle(),
            $carrier,
            $object->getSenderAddressId(),
            $object->isShowCarrierInformationInCheckout(),
            $shippingProduct,
            $processingDays,
            $object->getTimeZoneName(),
            $holidays,
            json_encode($object->toArray())
        );
    }

    /**
     * Checks whether the instance is different enough from target to require an update.
     *
     * @param DeliveryMethod $target

     * @return boolean
     */
    public function canBeUpdated($target)
    {
        return $this->getDeliveryZoneId() !== $target->getDeliveryZoneId()
               || $this->getType() !== $target->getType()
               || $this->getExternalTitle() !== $target->getExternalTitle()
               || $this->getInternalTitle() !== $target->getInternalTitle()
               || !$this->getCarrier()->isEqual($target->getCarrier())
               || $this->getSenderAddressId() !== $target->getSenderAddressId()
               || $this->isShowCarrierInfoOnCheckout() !== $target->isShowCarrierInfoOnCheckout()
               || !$this->getShippingProduct()->isEqual($target->getShippingProduct())
               || !CollectionComparator::isEqual($this->getProcessingDays(), $target->getProcessingDays())
               || $this->getTimeZoneName() !== $target->getTimeZoneName()
               || !CollectionComparator::isEqual($this->getHolidays(), $target->getHolidays());
    }
}