<?php

namespace SendCloud\Checkout\Domain\Delivery\Availability;

use SendCloud\Checkout\Domain\Delivery\DeliveryMethod;

abstract class AvailabilityPolicy
{
    /**
     * @var DeliveryMethod
     */
    protected $deliveryMethod;

    /**
     * AvailabilityPolicy constructor.
     *
     * @param  DeliveryMethod  $deliveryMethod
     */
    public function __construct(DeliveryMethod $deliveryMethod)
    {
        $this->deliveryMethod = $deliveryMethod;
    }

    /**
     * Checks if delivery method is available
     *
     * @return bool
     */
    abstract public function isAvailable();
}