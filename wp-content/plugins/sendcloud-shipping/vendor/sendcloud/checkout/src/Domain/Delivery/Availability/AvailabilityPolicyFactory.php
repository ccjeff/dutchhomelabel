<?php

namespace SendCloud\Checkout\Domain\Delivery\Availability;

use SendCloud\Checkout\Domain\Delivery\Availability\AvailabilityPolicy\NullAvailabilityPolicy;
use SendCloud\Checkout\Domain\Delivery\Availability\AvailabilityPolicy\SameDayAvailabilityPolicy;
use SendCloud\Checkout\Domain\Delivery\Availability\AvailabilityPolicy\StandardAvailabilityPolicy;
use SendCloud\Checkout\Domain\Delivery\DeliveryMethod;

class AvailabilityPolicyFactory
{
    /**
     * Creates instance of the AvailabilityPolicy based on the DeliveryMethod instance type
     *
     * @param  DeliveryMethod  $deliveryMethod
     *
     * @return AvailabilityPolicy
     */
    public static function create(DeliveryMethod $deliveryMethod)
    {
        if ($deliveryMethod->getType() === 'same_day_delivery') {
            $instance = new SameDayAvailabilityPolicy($deliveryMethod);
        } elseif ($deliveryMethod->getType() === 'standard_delivery') {
            $instance = new StandardAvailabilityPolicy($deliveryMethod);
        } else {
            $instance = new NullAvailabilityPolicy($deliveryMethod);
        }

        return $instance;
    }
}