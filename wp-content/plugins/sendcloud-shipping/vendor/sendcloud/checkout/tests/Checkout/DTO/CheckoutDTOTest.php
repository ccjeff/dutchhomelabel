<?php


namespace SendCloud\Tests\Checkout\DTO;


use SendCloud\Checkout\API\Checkout\Checkout;
use SendCloud\Checkout\Exceptions\DTO\DTOValidationException;
use SendCloud\Tests\Common\BaseTest;

class CheckoutDTOTest extends BaseTest
{
    public $payload;

    protected function setUp()
    {
        $this->payload = json_decode(file_get_contents(__DIR__ . '/../../Common/APIDataExamples/checkout.json'), true);
    }

    /**
     * @throws DTOValidationException
     */
    public function testSerialization()
    {
        // arrange
        $checkout = Checkout::fromArray($this->payload['checkout_configuration']);

        // act
        $array = $checkout->toArray();

        // assert
        $this->assertEquals($this->payload['checkout_configuration'], $array);
    }
}