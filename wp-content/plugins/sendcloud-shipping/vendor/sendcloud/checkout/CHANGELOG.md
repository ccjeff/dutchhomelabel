# Changelog
All notable changes to this project will be documented in this file.
Procedure for releasing new version is [here](https://logeecom.atlassian.net/wiki/spaces/SC/overview).

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## Unreleased

## [1.0.1]
- Update Validators\UpdateRequestValidator::$schema field to reflect detected changes in the checkout payload.
- Reformat validation errors

## [1.0.0]
- Initial version.