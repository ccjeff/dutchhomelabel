<?php

namespace Sendcloud\Shipping\Checkout\Shipping\NominatedDay;

use Sendcloud\Shipping\Repositories\Checkout_Payload_Meta_Repository;
use Sendcloud\Shipping\Repositories\Delivery_Methods_Repository;
use Sendcloud\Shipping\Sendcloud;
use Sendcloud\Shipping\Utility\Logging_Callable;
use Sendcloud\Shipping\Utility\View;
use WC_Shipping_Rate;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Checkout_Handler {
	const CHECKOUT_PLUGIN_UI_JS = 'https://cdn.jsdelivr.net/npm/@sendcloud/checkout-plugin-ui@1/dist/checkout-plugin-ui.js';
	const CHECKOUT_PLUGIN_UI_CSS = 'https://cdn.jsdelivr.net/npm/@sendcloud/checkout-plugin-ui@1/dist/checkout-plugin-ui.css';

	const NOMINATED_DAY_WIDGET_JS_CONTROLLER_HANDLE = 'sendcloud-nominated-day-widget-controller';
	const NOMINATED_DAY_WIDGET_JS_HANDLE = 'sendcloud-nominated-day-widget-js';
	const NOMINATED_DAY_WIDGET_CSS_HANDLE = 'sendcloud-nominated-day-widget-css';

	/**
	 * Hooks all checkout functions
	 */
	public function init() {
		add_action( 'wp_enqueue_scripts', new Logging_Callable( array( $this, 'register_scripts' ) ) );
		add_filter( 'clean_url', new Logging_Callable( array( $this, 'bypass_widget_src_cleanup' ) ), 10, 2 );
		add_filter( 'script_loader_tag', array( $this, 'defer_widget_scrip_tag' ), 10, 2 );

		add_action( 'woocommerce_checkout_after_order_review',
			new Logging_Callable( array( $this, 'initialize_nominated_day_widget' ) ) );
		add_action( 'woocommerce_after_shipping_rate', new Logging_Callable( array( $this, 'render_nominated_day_widget' ) ), 10,
			2 );
		add_action( 'woocommerce_checkout_process', new Logging_Callable( array( $this, 'validate_nominated_day_selection' ) ) );
		add_action( 'woocommerce_checkout_update_order_meta', new Logging_Callable( array( $this, 'save_nominated_day_meta' ) ) );
	}

	/**
	 * Register all required JS and CSS scripts for the nominated day widget rendering on the checkout page
	 */
	public function register_scripts() {
		wp_register_script( static::NOMINATED_DAY_WIDGET_JS_HANDLE, static::CHECKOUT_PLUGIN_UI_JS, array(), null, true );
		wp_register_script( static::NOMINATED_DAY_WIDGET_JS_CONTROLLER_HANDLE,
			Sendcloud::get_plugin_url( 'resources/js/sendcloud.nominated-day-widget-controller.js' ),
			array( 'jquery', static::NOMINATED_DAY_WIDGET_JS_HANDLE ), Sendcloud::VERSION, true );

		wp_register_style( static::NOMINATED_DAY_WIDGET_CSS_HANDLE, static::CHECKOUT_PLUGIN_UI_CSS, array(), null );
	}

	public function bypass_widget_src_cleanup( $cleaned_url, $original_url ) {
		if ( in_array( $original_url, array( self::CHECKOUT_PLUGIN_UI_JS, self::CHECKOUT_PLUGIN_UI_CSS ), true ) ) {
			return $original_url;
		}

		return $cleaned_url;
	}

	/**
	 * Add defer attribute during rendering of the widget script tag
	 *
	 * @param string $tag
	 * @param string $handle
	 *
	 * @return string
	 */
	public function defer_widget_scrip_tag( $tag, $handle ) {
		if ( $handle === static::NOMINATED_DAY_WIDGET_JS_HANDLE ) {
			return (string) str_replace( ' src', ' defer src', $tag );
		}

		return $tag;
	}

	/**
	 * Initializes nominated day widget for usage in checkout page
	 */
	public function initialize_nominated_day_widget() {
		wp_enqueue_script( static::NOMINATED_DAY_WIDGET_JS_HANDLE );
		wp_enqueue_style( static::NOMINATED_DAY_WIDGET_CSS_HANDLE );
		wp_enqueue_script( static::NOMINATED_DAY_WIDGET_JS_CONTROLLER_HANDLE );
		echo View::file( '/checkout/nominated-day-widget-locale-messages.php' )->render( array(
			'locale_messages' => array(
				'Delivered by {carrier}' => __( 'Delivered by {carrier}', 'sendcloud-shipping' ),
			),
		) );
	}

	/**
	 * Initializes nominated day shipping method widget
	 *
	 * @param WC_Shipping_Rate $method
	 */
	public function render_nominated_day_widget( $method, $index ) {
		if ( $method->method_id !== Nominated_Day_Shipping_Method::ID ) {
			return;
		}

		$delivery_method_config = $this->get_delivery_method_config( $this->get_method_instance_id( $method ) );
		if ( empty( $delivery_method_config ) ) {
			return;
		}

		echo View::file( '/checkout/nominated-day-widget.php' )->render( array(
			'index'                  => $index,
			'shipping_method_id'     => $method->id,
			'delivery_method_config' => $delivery_method_config,
			'locale'                 => $this->get_locale(),
		) );
	}

	/**
	 * Validates submitted nominated day data (if any) before order is created
	 */
	public function validate_nominated_day_selection() {
		$selected_shipping_method = $this->extract_shipping_method_id( $_POST );
		if ( $selected_shipping_method !== Nominated_Day_Shipping_Method::ID ) {
			return;
		}

		$nominated_day_data = $this->extract_nominated_day_submit_data( $_POST );
		if ( empty( $nominated_day_data['nominated_day_delivery'] ) ) {
			wc_add_notice( __( 'Please choose a delivery date.', 'sendcloud-shipping' ), 'error' );
		}
	}

	public function save_nominated_day_meta( $order_id ) {
		$nominated_day_data = $this->extract_nominated_day_submit_data( $_POST );
		if ( ! empty( $nominated_day_data ) ) {
			$checkout_payload_meta_repo = new Checkout_Payload_Meta_Repository();
			$checkout_payload_meta_repo->save_raw( $order_id, $nominated_day_data );
		}
	}

	private function get_locale() {
        if ( strpos( get_locale(), '_' ) ) {
            $localeParts = explode( "_", get_locale() );

            return implode( '-', array( strtolower( $localeParts[0] ), strtoupper( $localeParts[1] ) ) );
        }

        return get_locale();
	}

	/**
	 * Extracts nominated day data from posted data
	 *
	 * @param array $data Posted data
	 *
	 * @return array Submitted nominated day widget data or empty array
	 */
	private function extract_nominated_day_submit_data( $data ) {
		$selected_shipping_method = $this->extract_shipping_method_id( $data );
		if ( $selected_shipping_method !== Nominated_Day_Shipping_Method::ID ) {
			return array();
		}

		$selected_shipping_method_id_key = '';
		if ( isset( $data['shipping_method'][0] ) ) {
			$selected_shipping_method_id_key = esc_attr( sanitize_title( $data['shipping_method'][0] ) );
		}

		if ( empty( $data['sendcloudshipping_nominated_day_data'][ $selected_shipping_method_id_key ] ) ) {
			return array();
		}

		return json_decode( html_entity_decode( stripslashes( $data['sendcloudshipping_nominated_day_data'][ $selected_shipping_method_id_key ] ) ),
			true );
	}

	/**
	 * Extracts selected shipping method id form posted data
	 *
	 * @param array $data Posted data
	 *
	 * @return string
	 */
	private function extract_shipping_method_id( $data ) {
		$selected_shipping_method = '';

		if ( isset( $data['shipping_method'][0] ) ) {
			$shipping_method_parts    = explode( ':', $data['shipping_method'][0] );
			$selected_shipping_method = (string) $shipping_method_parts[0];
		}

		return $selected_shipping_method;
	}

	/**
	 * Provides configuration for delivery method.
	 *
	 * @param int
	 * $method_id
	 *
	 * @return array
	 */
	private function get_delivery_method_config( $method_id ) {
		global $wpdb;
		$repo   = new Delivery_Methods_Repository( $wpdb );
		$method = $repo->find_by_system_id( $method_id );
		if ( $method === null ) {
			return array();
		}

		return json_decode( $method->getRawConfig(), true );
	}

	private function get_method_instance_id( $shipping_method ) {
		if ( ! empty( $shipping_method->instance_id ) ) {
			return $shipping_method->instance_id;
		}

		$id = $shipping_method->id;
		$id = explode( ':', $id );

		return ! empty( $id[1] ) ? $id[1] : null;
	}
}
