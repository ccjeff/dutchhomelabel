<?php

namespace Sendcloud\Shipping\Checkout\Shipping;

use Exception;
use Sendcloud\Shipping\Repositories\Delivery_Methods_Repository;
use Sendcloud\Shipping\Utility\Logger;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class Free_Shipping_Delivery_Method extends Free_Shipping_Shipping_Method {

	/**
	 * Init user set variables.
	 */
	public function init() {
		parent::init();

		$this->sc_delivery_method_id = $this->get_option( 'sc_delivery_method_id' );
	}

	/**
	 * Is this method available?
	 *
	 * @param array $package
	 *
	 * @return bool
	 */
	public function is_available( $package ) {
		$is_available = parent::is_available( $package );

		if ( $is_available ) {
			try {
				$delivery_method = $this->get_delivery_method();
				if ( $delivery_method === null ) {
					return false;
				}

				$is_available = $delivery_method->isAvailable();
			} catch ( Exception $exception ) {
				Logger::error( 'Error while checking method availability. ' . $exception->getMessage() );

				return false;
			}
		}

		return $is_available;
	}

	/**
	 * Retrieves delivery method from db
	 *
	 * @return \SendCloud\Checkout\Domain\Delivery\DeliveryMethod|null
	 */
	protected function get_delivery_method() {
		global $wpdb;
		$repo = new Delivery_Methods_Repository( $wpdb );

		return $repo->find_by_system_id( $this->instance_id );
	}
}