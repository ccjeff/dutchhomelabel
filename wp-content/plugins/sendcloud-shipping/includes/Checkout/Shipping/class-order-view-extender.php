<?php

namespace Sendcloud\Shipping\Checkout\Shipping;

use Sendcloud\Shipping\Repositories\Checkout_Payload_Meta_Repository;
use Sendcloud\Shipping\Utility\Logging_Callable;
use Sendcloud\Shipping\Utility\View;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Order_View_Extender {

	const DELIVERY_METHODS = [ 'nominated_day_delivery', 'same_day_delivery' ];

	/**
	 * @var checkout_payload_meta_repository
	 */
	private $checkout_payload_meta_repository;

	/**
	 * Hooks all checkout functions
	 */
	public function init() {
		add_action( 'woocommerce_thankyou', new Logging_Callable( array( $this, 'add_delivery_date_info' ) ), 11 );
		add_action( 'woocommerce_view_order', new Logging_Callable( array( $this, 'add_delivery_date_info' ) ), 11 );
		add_action( 'woocommerce_email_after_order_table',
			new Logging_Callable( array( $this, 'add_delivery_date_info_in_mail' ) ), 15 );
		add_action( 'woocommerce_admin_order_data_after_shipping_address',
			new Logging_Callable( array( $this, 'add_delivery_date_info_in_admin_order' ) ), 11 );
	}

	/**
	 * Adds delivery date information in the order thank you, order view and email template pages
	 *
	 * @param $order_id
	 */
	public function add_delivery_date_info( $order_id ) {
		$this->render_widget_template( $order_id, '/checkout/delivery-date-thank-you-widget.php' );
	}

	/**
	 * Adds delivery date information in the order emails
	 *
	 * @param $order
	 */
	public function add_delivery_date_info_in_mail( $order ) {
		$this->render_widget( $order, '/mail/delivery-date-mail-widget.php' );
	}

	/**
	 * Adds delivery date information in the order details page
	 *
	 * @param $order
	 */
	public function add_delivery_date_info_in_admin_order( $order ) {
		$this->render_widget( $order, '/checkout/delivery-date-admin-widget.php' );
	}

	/**
	 * Renders the widget template for a give order
	 *
	 * @param $order
	 * @param string $template
	 */
	private function render_widget( $order, $template ) {
		if ( version_compare( WC()->version, '3.0', ">=" ) ) {
			$order_id = $order->get_id();
		} else {
			$order_id = $order->id;
		}

		$this->render_widget_template( $order_id, $template );
	}

	/**
	 * Renders the widget template for a give order id
	 *
	 * @param $order_id
	 * @param string $template
	 */
	private function render_widget_template( $order_id, $template ) {
		$delivery_method = $this->get_checkout_payload_meta_repository()->get_delivery_method_type( $order_id );
		if ( ! in_array( $delivery_method, self::DELIVERY_METHODS, true ) ) {
			return;
		}
		$delivery_method_data = $this->get_checkout_payload_meta_repository()->get_delivery_method_data( $order_id );
		if ( $delivery_method_data ) {
			echo View::file( $template )->render( array( 'delivery_method_data_payload' => $delivery_method_data ) );
		}
	}

	/**
	 * Returns checkout payload meta repository
	 *
	 * @return Checkout_Payload_Meta_Repository
	 */
	private function get_checkout_payload_meta_repository() {
		if ( ! $this->checkout_payload_meta_repository ) {
			$this->checkout_payload_meta_repository = new Checkout_Payload_Meta_Repository();
		}

		return $this->checkout_payload_meta_repository;
	}
}
