<?php

namespace Sendcloud\Shipping\Checkout\Shipping;

use Sendcloud\Shipping\Utility\Logging_Callable;
use WC_Shipping_Flat_Rate;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class Free_Shipping_Shipping_Method extends WC_Shipping_Flat_Rate {
	public $plugin_id = 'sendcloudshipping_';

	/**
	 * @var string
	 */
	protected $free_shipping_enabled;
	/**
	 * @var float
	 */
	protected $free_shipping_min_amount;

	/**
	 * Init user set variables.
	 */
	public function init() {
		add_filter( "woocommerce_shipping_instance_form_fields_{$this->id}",
			new Logging_Callable( array( $this, 'override_form_fields_config' ) ) );

		parent::init();

		$this->free_shipping_enabled    = $this->get_option( 'free_shipping_enabled' );
		$this->free_shipping_min_amount = $this->get_option( 'free_shipping_min_amount' );
	}

	/**
	 * Overrides title default value and extends form fields
	 *
	 * @param $form_fields
	 *
	 * @return mixed
	 */
	public function override_form_fields_config( $form_fields ) {
		$form_fields['title']['default'] = $this->method_title;
		$this->add_extra_fields( $form_fields );

		return $form_fields;
	}

	/**
	 * Initialize form fields
	 */
	public function init_form_fields() {
		parent::init_form_fields();
		$this->add_extra_fields( $this->form_fields );
	}

	/**
	 * Calculates shipping costs
	 *
	 * @param array $package
	 */
	public function calculate_shipping( $package = array() ) {
		if ( $this->check_free_shipping() ) {
			$this->add_rate( array(
				'label'   => $this->title,
				'cost'    => 0,
				'taxes'   => false,
				'package' => $package,
			) );
		} else {
			parent::calculate_shipping( $package );
		}
	}

	/**
	 * Checks whether or not shipping is free
	 *
	 * @return bool
	 */
	protected function check_free_shipping() {
		if ( $this->free_shipping_enabled !== 'yes' || ! isset( WC()->cart->cart_contents_total ) ) {
			return false;
		}

		$total = WC()->cart->get_displayed_subtotal() - WC()->cart->get_cart_discount_total();
		if ( 'incl' === WC()->cart->tax_display_cart ) {
			$total -= WC()->cart->get_cart_discount_tax_total();
		}

		return $total >= $this->free_shipping_min_amount;
	}

	/**
	 * Add extra fields
	 *
	 * @param $form_fields
	 */
	protected function add_extra_fields( &$form_fields ) {
		$form_fields['free_shipping_enabled']    = array(
			'title'   => __( 'Enable Free Shipping', 'sendcloud-shipping' ),
			'type'    => 'select',
			'class'   => 'wc-enhanced-select',
			'default' => '',
			'options' => array(
				'no'  => __( 'No', 'sendcloud-shipping' ),
				'yes' => __( 'Yes', 'sendcloud-shipping' ),
			),
		);
		$form_fields['free_shipping_min_amount'] = array(
			'title'       => __( 'Minimum Order Amount for Free Shipping', 'sendcloud-shipping' ),
			'type'        => 'price',
			'placeholder' => wc_format_localized_price( 0 ),
			'description' => __( 'If enabled, users will need to spend this amount to get free shipping.', 'sendcloud-shipping' ),
			'default'     => '0',
			'desc_tip'    => true,
		);
	}
}
