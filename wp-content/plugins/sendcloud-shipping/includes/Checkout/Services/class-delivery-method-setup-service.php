<?php

namespace Sendcloud\Shipping\Checkout\Services;

use InvalidArgumentException;
use SendCloud\Checkout\Contracts\Services\DeliveryMethodSetupService;
use SendCloud\Checkout\Contracts\Storage\CheckoutStorage;
use SendCloud\Checkout\Domain\Delivery\DeliveryMethod;
use Sendcloud\Shipping\Checkout\Shipping\Free_Shipping_Shipping_Method;
use Sendcloud\Shipping\Checkout\Shipping\NominatedDay\Nominated_Day_Shipping_Method;
use Sendcloud\Shipping\Checkout\Shipping\SameDay\Same_Day_Shipping_Method;
use Sendcloud\Shipping\Checkout\Shipping\Standard\Standard_Shipping_Method;
use Sendcloud\Shipping\Repositories\Delivery_Zones_Repository;

/**
 * Class Delivery_Method_Setup_Service
 *
 * @package Sendcloud\Shipping\Checkout\Services
 */
class Delivery_Method_Setup_Service implements DeliveryMethodSetupService {

	private static $delivery_method_types = array(
		'standard_delivery'      => Standard_Shipping_Method::CLASS_NAME,
		'nominated_day_delivery' => Nominated_Day_Shipping_Method::CLASS_NAME,
		'same_day_delivery'      => Same_Day_Shipping_Method::CLASS_NAME
	);
	/**
	 * @var CheckoutStorage
	 */
	private $storage;
	/**
	 * @var Delivery_Zones_Repository
	 */
	private $repository;

	/**
	 * Delivery_Method_Setup_Service constructor.
	 *
	 * @param CheckoutStorage $storage
	 * @param Delivery_Zones_Repository $repository
	 */
	public function __construct( CheckoutStorage $storage, Delivery_Zones_Repository $repository ) {
		$this->storage    = $storage;
		$this->repository = $repository;
	}

	/**
	 * Deletes delivery methods specified in the provided batch.
	 *
	 * @param DeliveryMethod[] $methods
	 *
	 * @return void
	 */
	public function deleteSpecific( array $methods ) {
		foreach ( $methods as $method ) {
			$this->do_delete( $method );
		}
	}

	/**
	 * Deletes all delivery methods.
	 *
	 * @return void
	 */
	public function deleteAll() {
		$methods = $this->storage->findAllMethodConfigs();
		foreach ( $methods as $method ) {
			$this->do_delete( $method );
		}
	}

	/**
	 * Updates delivery methods.
	 *
	 * @param DeliveryMethod[] $methods
	 *
	 * @return void
	 */
	public function update( array $methods ) {
		foreach ( $methods as $method ) {
			$this->do_update( $method );
		}
	}

	/**
	 * Creates delivery methods.
	 *
	 * @param DeliveryMethod[] $methods
	 *
	 * @return void
	 */
	public function create( array $methods ) {
		foreach ( $methods as $method ) {
			$this->do_create( $method );
		}
	}

	/**
	 * Deletes wc shipping method.
	 *
	 * @param DeliveryMethod $method
	 */
	private function do_delete( DeliveryMethod $method ) {
		$zone = $this->storage->findZoneConfigs( array( $method->getDeliveryZoneId() ) );
		if ( empty( $zone[0] ) ) {
			// Nothing to delete. User already deleted zone.

			return;
		}

		$zone = $zone[0];

		$this->repository->remove_wc_delivery_method( $zone->getSystemId(), $method->getSystemId() );
	}

	/**
	 * Updates shipping method.
	 *
	 * @param DeliveryMethod $method
	 */
	private function do_update( DeliveryMethod $method ) {
		$zones = $this->storage->findZoneConfigs( array( $method->getDeliveryZoneId() ) );
		if ( empty( $zones[0] ) ) {
			throw new InvalidArgumentException( "Delivery zone not found." );
		}

		if ( ! array_key_exists( $method->getType(), self::$delivery_method_types ) ) {
			throw new InvalidArgumentException( "Unknown delivery method." );
		}

		$zone_config = $zones[0];
		if ( ! $this->repository->has_method( $zone_config->getSystemId(), $method->getSystemId() ) ) {
			$this->do_create( $method );

			return;
		}

		$instance = new self::$delivery_method_types[ $method->getType() ]( $method->getSystemId() );
		$instance->init_instance_settings();
		$this->set_data( $method, $instance );
	}

	/**
	 * Creates shipping method by adding it to a shipping zone.
	 *
	 * @param DeliveryMethod $method
	 */
	private function do_create( DeliveryMethod $method ) {
		$zones = $this->storage->findZoneConfigs( array( $method->getDeliveryZoneId() ) );
		if ( empty( $zones[0] ) ) {
			throw new InvalidArgumentException( "Delivery zone not found." );
		}

		if ( ! array_key_exists( $method->getType(), self::$delivery_method_types ) ) {
			throw new InvalidArgumentException( "Unknown delivery method." );
		}

		$zone_config = $zones[0];
		$count       = $this->repository->get_method_count( $zone_config->getSystemId() );
		$instance_id = $this->repository->add_wc_delivery_method_to_wc_delivery_zone(
			self::$delivery_method_types[ $method->getType() ]::ID,
			$zone_config->getSystemId(),
			$count + 1
		);
		$instance    = new self::$delivery_method_types[ $method->getType() ]( $instance_id );

		$this->set_data( $method, $instance );

		$method->setSystemId( $instance_id );
	}

	/**
	 * Sets shipping method data.
	 *
	 * @param DeliveryMethod $method
	 * @param Nominated_Day_Shipping_Method $instance
	 */
	private function set_data( DeliveryMethod $method, Free_Shipping_Shipping_Method $instance ) {
		$instance->instance_settings['title']                 = $method->getExternalTitle();
		$instance->instance_settings['sc_delivery_method_id'] = $method->getId();
		update_option(
			$instance->get_instance_option_key(),
			apply_filters(
				'woocommerce_shipping_' . $instance->id . '_instance_settings_values',
				$instance->instance_settings,
				$instance
			)
		);
	}
}