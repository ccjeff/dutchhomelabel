<?php

namespace Sendcloud\Shipping\ServicePoint;

use Sendcloud\Shipping\Models\Service_Point_Meta;
use Sendcloud\Shipping\Repositories\Service_Point_Configuration_Repository;
use Sendcloud\Shipping\Repositories\Service_Point_Meta_Repository;
use Sendcloud\Shipping\Sendcloud;
use Sendcloud\Shipping\ServicePoint\Shipping\SendCloudShipping_Service_Point_Shipping_Method;
use Sendcloud\Shipping\Utility\Logging_Callable;
use Sendcloud\Shipping\Utility\View;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Checkout_Handler {
	const CLASS_NAME = __CLASS__;

	const SERVICE_POINT_ID = 'sendcloudshipping_service_point_selected';
	const SERVICE_POINT_EXTRA_FIELD_NAME = 'sendcloudshipping_service_point_extra';

	/**
	 * @var Service_Point_Configuration_Repository
	 */
	private $service_point_config_repository;

	/**
	 * @var Service_Point_Meta_Repository
	 */
	private $service_point_meta_repository;

	/**
	 * Hooks all checkout functions
	 */
	public function init() {
		add_action( 'woocommerce_checkout_after_order_review', new Logging_Callable( array(
			$this,
			'add_service_point_to_checkout',
		) ) );
        add_action( 'wfacp_checkout_after_order_review', new Logging_Callable( array(
            $this,
            'add_service_point_to_checkout',
        ) ) );
		add_action( 'woocommerce_after_shipping_rate', new Logging_Callable( array( $this, 'add_extra_shipping_method' ) ) );
		add_action( 'woocommerce_checkout_process',
			new Logging_Callable( array( $this, 'add_notice_if_service_point_not_chosen' ) ) );
		add_action( 'woocommerce_checkout_update_order_meta', new Logging_Callable( array( $this, 'update_order_meta' ) ) );
		add_action( 'woocommerce_thankyou', new Logging_Callable( array( $this, 'add_service_point_info' ) ), 11 );
		add_action( 'woocommerce_view_order', new Logging_Callable( array( $this, 'add_service_point_info' ) ), 11 );
		add_action( 'woocommerce_admin_order_data_after_shipping_address',
			new Logging_Callable( array( $this, 'add_service_point_data_in_admin_order' ) ), 11 );
	}

	/**
	 * Adds service point to checkout
	 */
	public function add_service_point_to_checkout() {
		$script = $this->get_service_point_config_repository()->get()->get_script();

		if ( empty( $script ) ) {
			return;
		}

		$parts = explode( "_", get_locale() );

		wp_enqueue_script( 'sendcloud-service-point-js', $script, array(), Sendcloud::VERSION, true );

		echo View::file( '/checkout/add-service-point-data.php' )->render( array(
			'language'             => $parts[0],
			// Double encode dimensions because we want to pass the string as one GET param
			'cart_dimensions'      => base64_encode( json_encode( $this->cart_max_dimensions() ) ),
			'cart_dimensions_unit' => json_encode( get_option( 'woocommerce_dimension_unit' ) ),
			'select_spp_label'     => __( 'Select Service Point', 'sendcloud-shipping' ),
		) );
	}

	/**
	 * Adds service point shipping method
	 *
	 * @param $method
	 */
	public function add_extra_shipping_method( $method ) {
		if ( $method->method_id === SendCloudShipping_Service_Point_Shipping_Method::ID ) {
			if ( ! $instance_id = $method->instance_id ) {
				$id          = explode( ':', $method->id );
				$instance_id = ! empty( $id[1] ) ? $id[1] : null;
			}
			$carriers = $this->get_service_point_config_repository()->get( $instance_id )->get_carriers();

			echo View::file( '/checkout/extra-shipping-method.php' )->render( array(
				'field_id'       => $method->id . ':carrier_select',
				'carrier_select' => isset( $carriers ) ? $carriers : ''
			) );
		}
	}

	/**
	 * Checks whether or not user chose service point before creating an order
	 */
	public function add_notice_if_service_point_not_chosen() {
		$servicePointSM = '';

		if ( isset( $_POST['shipping_method'][0] ) ) {
			$parts          = explode( ':', $_POST['shipping_method'][0] );
			$servicePointSM = $parts[0];
		}

		$servicePointSelected = isset( $_POST[ self::SERVICE_POINT_ID ] ) && ! empty( $_POST[ self::SERVICE_POINT_ID ] );
		if ( $servicePointSM === SendCloudShipping_Service_Point_Shipping_Method::ID && ! $servicePointSelected ) {
			wc_add_notice( __( 'Please choose a service point.', 'sendcloud-shipping' ), 'error' );
		}
	}

	/**
	 * Updates post meta field if service point is selected
	 *
	 * @param $order_id
	 */
	public function update_order_meta( $order_id ) {
		if ( isset( $_POST[ self::SERVICE_POINT_ID ] ) ) {
			$service_point = new Service_Point_Meta();
			$service_point->set_id( $_POST[ self::SERVICE_POINT_ID ] );
			$service_point->set_extra( $_POST[ self::SERVICE_POINT_EXTRA_FIELD_NAME ] );

			$this->get_service_point_meta_repository()->save( $order_id, $service_point );
		}
	}

	/**
	 * Adds service point information in the order details page
	 *
	 * @param $order
	 */
	public function add_service_point_data_in_admin_order( $order ) {
		if ( version_compare( WC()->version, '3.0', ">=" ) ) {
			$order_id = $order->get_id();
		} else {
			$order_id = $order->id;
		}

		$service_point = $this->get_service_point_meta_repository()->get( $order_id );
		if ( $service_point ) {
			echo View::file( '/checkout/extend-order-details.php' )->render( array(
				'address' => $service_point->get_extra()
			) );
		}
	}

	/**
	 * Adds service point information in the order thank you page
	 *
	 * @param $order_id
	 */
	public function add_service_point_info( $order_id ) {
		$service_point = $this->get_service_point_meta_repository()->get( $order_id );
		if ( $service_point ) {
			echo View::file( '/checkout/service-point-address-thank-you-widget.php' )->render( array(
				'address' => $service_point->get_extra(),
			) );
		}
	}

	/**
	 * Gets product dimensions
	 *
	 * @return array
	 */
	private function cart_max_dimensions() {
		$dimensions = array();
		$cart       = WC()->cart;

		foreach ( $cart->get_cart() as $cart_item_key => $values ) {
			$product = $values['data'];
			if ( $product->has_dimensions() ) {
				$dimensions[] = array(
					$product->get_length(),
					$product->get_width(),
					$product->get_height()
				);
			}
		}

		return $dimensions;
	}

	/**
	 * Returns service point configuration repository
	 *
	 * @return Service_Point_Configuration_Repository
	 */
	private function get_service_point_config_repository() {
		if ( ! $this->service_point_config_repository ) {
			$this->service_point_config_repository = new Service_Point_Configuration_Repository();
		}

		return $this->service_point_config_repository;
	}

	/**
	 * Returns service point meta repository
	 *
	 * @return Service_Point_Meta_Repository
	 */
	private function get_service_point_meta_repository() {
		if ( ! $this->service_point_meta_repository ) {
			$this->service_point_meta_repository = new Service_Point_Meta_Repository();
		}

		return $this->service_point_meta_repository;
	}
}