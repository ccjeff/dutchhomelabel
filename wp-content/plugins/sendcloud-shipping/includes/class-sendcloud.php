<?php

namespace Sendcloud\Shipping;

require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

use Sendcloud\Shipping\Checkout\Api\Checkout_Api_Service;
use Sendcloud\Shipping\Checkout\Shipping\NominatedDay\Checkout_Handler as Nominated_Day_Checkout_Handler;
use Sendcloud\Shipping\Checkout\Shipping\NominatedDay\Nominated_Day_Shipping_Method;
use Sendcloud\Shipping\Checkout\Shipping\Order_View_Extender as Delivery_Method_Order_View_Extender;
use Sendcloud\Shipping\Checkout\Shipping\SameDay\Checkout_Handler as Same_Day_Checkout_Handler;
use Sendcloud\Shipping\Checkout\Shipping\SameDay\Same_Day_Shipping_Method;
use Sendcloud\Shipping\Checkout\Shipping\Standard\Checkout_Handler as Standard_Checkout_Handler;
use Sendcloud\Shipping\Checkout\Shipping\Standard\Standard_Shipping_Method;
use SendCloud\Shipping\Database\Exceptions\Migration_Exception;
use Sendcloud\Shipping\Integration\Api\Integration_Api_Service;
use Sendcloud\Shipping\Repositories\Plugin_Options_Repository;
use Sendcloud\Shipping\Repositories\Service_Point_Configuration_Repository;
use Sendcloud\Shipping\ServicePoint\Api\SendCloudShipping_API_ServicePoint;
use Sendcloud\Shipping\ServicePoint\Checkout_Handler as Service_Point_Checkout_Handler;
use Sendcloud\Shipping\ServicePoint\Email_Handler;
use Sendcloud\Shipping\ServicePoint\Shipping\SendCloudShipping_Service_Point_Shipping_Method;
use Sendcloud\Shipping\Utility\Database;
use Sendcloud\Shipping\Utility\Logger;
use Sendcloud\Shipping\Utility\Logging_Callable;
use Sendcloud\Shipping\Utility\View;

class Sendcloud {

	const VERSION = '2.0.0';

	const INTEGRATION_NAME = 'sendcloudshipping';
	const BASE_API_URI = '/sendcloudshipping';

	/**
	 * @var Sendcloud
	 */
	protected static $instance;

	/**
	 * @var string
	 */
	private $sendcloud_plugin_file;

	/**
	 * @var Service_Point_Checkout_Handler
	 */
	private $service_point_checkout_handler;

	/**
	 * @var Email_Handler
	 */
	private $email_handler;

	/**
	 * @var Service_Point_Configuration_Repository
	 */
	private $service_point_config_repository;

	/**
	 * @var Nominated_Day_Checkout_Handler
	 */
	private $nominated_day_checkout_handler;

	/**
	 * @var Delivery_Method_Order_View_Extender
	 */
	private $deivery_method_order_view_extender;

	/**
	 * @var Standard_Checkout_Handler
	 */
	private $standard_checkout_handler;

	/**
	 * @var Same_Day_Checkout_Handler
	 */
	private $same_day_checkout_handler;
	/**
	 * @var Database
	 */

	private $database;
	/**
	 * Flag that signifies that the plugin is initialized.
	 *
	 * @var bool
	 */
	private $is_initialized = false;

	/**
	 * Sendcloud_Plugin constructor.
	 *
	 * @param string $sendcloud_plugin_file
	 */
	private function __construct( $sendcloud_plugin_file ) {
		$this->sendcloud_plugin_file              = $sendcloud_plugin_file;
		$this->service_point_checkout_handler     = new Service_Point_Checkout_Handler();
		$this->nominated_day_checkout_handler     = new Nominated_Day_Checkout_Handler();
		$this->deivery_method_order_view_extender = new Delivery_Method_Order_View_Extender();
		$this->email_handler                      = new Email_Handler();
		$this->service_point_config_repository    = new Service_Point_Configuration_Repository();
		$this->standard_checkout_handler          = new Standard_Checkout_Handler();
		$this->same_day_checkout_handler          = new Same_Day_Checkout_Handler();
		$this->database                           = new Database( new Plugin_Options_Repository() );
	}

	/**
	 * Initialize the plugin and returns instance of the plugin
	 *
	 * @param $sendcloud_plugin_file
	 *
	 * @return Sendcloud
	 */
	public static function init( $sendcloud_plugin_file ) {
		if ( self::$instance === null ) {
			self::$instance = new self( $sendcloud_plugin_file );
		}

		self::$instance->initialize();

		return self::$instance;
	}

	/**
	 * Defines global constants and hooks actions to appropriate events
	 */
	private function initialize() {
		if ( $this->is_initialized ) {
			return;
		}

		register_activation_hook( $this->sendcloud_plugin_file, array( $this, 'sendcloudshipping_activate' ) );
		add_action( 'init', new Logging_Callable( array( $this, 'sendcloudshipping_init' ) ) );
		add_action( 'plugins_loaded', new Logging_Callable( array( $this, 'sendcloudshipping_bootstrap' ) ) );

		try {
			$this->database->update( is_multisite() );
		} catch ( Migration_Exception $e ) {
			Logger::error( 'Unable to migrate database:' . $e->getMessage(), array( 'trace' => $e->getTraceAsString() ) );
		}

		$this->is_initialized = true;
	}

	/**
	 * SendCloud activate action
	 */
	public function sendcloudshipping_activate() {
		if ( ! class_exists( 'WooCommerce' ) ) {
			return;
		}
	}

	/**
	 * Loads translations
	 */
	public function sendcloudshipping_init() {
		if ( ! class_exists( 'WooCommerce' ) ) {
			return;
		}
		load_plugin_textdomain( 'sendcloud-shipping', false, basename( dirname( $this->sendcloud_plugin_file ) ) . '/i18n/languages/' );
	}

	/**
	 * Action on plugin loaded
	 */
	public function sendcloudshipping_bootstrap() {
		if ( ! class_exists( 'WooCommerce' ) ) {
			deactivate_plugins( plugin_basename( $this->sendcloud_plugin_file ) );
			add_action( 'admin_notices', new Logging_Callable( array(
				$this,
				'sendcloudshipping_deactivate_notice'
			) ) );
		} else {
			$this->service_point_checkout_handler->init();
			$this->nominated_day_checkout_handler->init();
			$this->deivery_method_order_view_extender->init();
			$this->email_handler->init();
			$this->standard_checkout_handler->init();
			$this->same_day_checkout_handler->init();

			add_filter( 'woocommerce_api_classes', new Logging_Callable( array(
				$this,
				'sendcloudshipping_add_api'
			) ) );
			add_filter( 'woocommerce_shipping_methods', new Logging_Callable( array(
				$this,
				'sendcloudshipping_register_shipping_methods',
			) ) );
			add_filter( 'woocommerce_get_settings_pages',
				new Logging_Callable( array( $this, 'sendcloudshipping_settings_page' ) ) );
			register_shutdown_function( array( $this, 'log_errors' ) );
		}
	}

	/**
	 * Adds service point shipping method
	 *
	 * @param $methods
	 *
	 * @return mixed
	 */
	public function sendcloudshipping_register_shipping_methods( $methods ) {
		$script = $this->service_point_config_repository->get()->get_script();
		if ( ! empty( $script ) ) {
			$methods[ SendCloudShipping_Service_Point_Shipping_Method::ID ] = SendCloudShipping_Service_Point_Shipping_Method::CLASS_NAME;
		}

		$methods[ Nominated_Day_Shipping_Method::ID ] = Nominated_Day_Shipping_Method::CLASS_NAME;
		$methods[ Standard_Shipping_Method::ID ]      = Standard_Shipping_Method::CLASS_NAME;
		$methods[ Same_Day_Shipping_Method::ID ]      = Same_Day_Shipping_Method::CLASS_NAME;

		return $methods;
	}

	/**
	 * Adds Api Service Point in WC Api classes
	 *
	 * @param $apis
	 *
	 * @return mixed
	 */
	public function sendcloudshipping_add_api( $apis ) {
		$apis[] = SendCloudShipping_API_ServicePoint::CLASS_NAME;
		$apis[] = Checkout_Api_Service::CLASS_NAME;
		$apis[] = Integration_Api_Service::CLASS_NAME;

		return $apis;
	}

	/**
	 * Renders message about WooCommerce being deactivated
	 */
	public function sendcloudshipping_deactivate_notice() {
		echo View::file( '/plugin/deactivation-notice.php' )->render();
	}

	/**
	 * Init SendCloud settings page
	 *
	 * @param $settings
	 *
	 * @return mixed
	 */
	public function sendcloudshipping_settings_page( $settings ) {
		$settings[] = new SendCloudShipping_Settings();

		return $settings;
	}

	/**
	 * Returns base directory path
	 *
	 * @return string
	 */
	public static function get_plugin_dir_path() {
		return rtrim( plugin_dir_path( __DIR__ ), '/' );
	}

	/**
	 * Returns url for the provided directory
	 *
	 * @param $path
	 *
	 * @return string
	 */
	public static function get_plugin_url( $path ) {
		return rtrim( plugins_url( "/{$path}/", __DIR__ ), '/' );
	}

	/**
	 * Logs errors
	 */
	public function log_errors() {
		$error = error_get_last();
		if ( $error && in_array( $error['type'], array(
				E_ERROR,
				E_PARSE,
				E_COMPILE_ERROR,
				E_USER_ERROR,
				E_RECOVERABLE_ERROR
			), true ) ) {
			Logger::critical( sprintf( '%1$s in %2$s on line %3$s', $error['message'], $error['file'], $error['line'] ) .
			                  PHP_EOL );
		}
	}
}