<?php

namespace Sendcloud\Shipping;

use Sendcloud\Shipping\Repositories\Api_Key_Repository;
use Sendcloud\Shipping\Utility\Logger;
use Sendcloud\Shipping\Utility\View;
use WC_Settings_Page;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class SendCloudShipping_Settings extends WC_Settings_Page {
	const CLASS_NAME = __CLASS__;

	/**
	 * @var Api_Key_Repository
	 */
	private $api_key_repository;

	/**
	 * SendCloudShipping_Settings constructor.
	 */
	public function __construct() {
		$this->id                 = 'sendcloud';
		$this->label              = __( 'Sendcloud', 'sendcloud-shipping' );
		$this->api_key_repository = new Api_Key_Repository();
		parent::__construct();
	}

	/**
	 * Render SendCloud page in WC settings
	 */
	public function output() {
		global $hide_save_button;
		$hide_save_button = true;

		wp_enqueue_script( 'sendcloud-js', Sendcloud::get_plugin_url( 'resources/js/sendcloud.page.js' ), array( 'jquery' ),
			Sendcloud::VERSION, true );
		wp_enqueue_style( 'sendcloud-css', Sendcloud::get_plugin_url( 'resources/css/sendcloud.css' ), array(),
			Sendcloud::VERSION );
		echo View::file( '/wc-settings/sendcloud-page.php' )->render( array(
			'panel_url'          => $this->get_panel_url(),
			'permalinks_enabled' => get_option( 'permalink_structure' ),
		) );
	}

	/**
	 * Save settings
	 */
	public function save() {
		$permalinks_enabled = get_option( 'permalink_structure' );
		if ( ! $permalinks_enabled ) {
			$url = admin_url( 'admin.php?page=wc-settings&tab=' . $this->id );
		} else {
			Logger::info( "Connecting to Sendcloud." );
			$site_url = get_option( 'home' );

			if ( defined( 'SC_NGROK_URL' ) ) {
				$ngrok_url = parse_url( SC_NGROK_URL, PHP_URL_HOST );
				$site_url  = str_replace( parse_url( $site_url, PHP_URL_HOST ), $ngrok_url, $site_url );
			}

			$site_url = urlencode( $site_url );

			update_option( 'woocommerce_api_enabled', 'yes' );

			$api_key = $this->api_key_repository->get_fresh_credentials();

			$url = sprintf( '%s/shops/woocommerce/connect/?url_webshop=%s&api_key=%s&api_secret=%s', $this->get_panel_url(),
				$site_url, $api_key->get_consumer_key(), $api_key->get_consumer_secret() );
		}

		if ( ! headers_sent() ) {
			header( 'Location: ' . $url );
			exit;
		}

		echo View::file( '/wc-settings/save-action.php' )->render( array( 'url' => $url ) );
	}

	/**
	 * Gets SendCloud panel url
	 *
	 * @return array|false|string
	 */
	private function get_panel_url() {
		$panel_url = getenv( 'SENDCLOUDSHIPPING_PANEL_URL' );
		if ( empty( $panel_url ) ) {
			$panel_url = 'https://panel.sendcloud.sc';
		}

		return $panel_url;
	}
}
