<?php

namespace Sendcloud\Shipping\Repositories;

use Sendcloud\Shipping\Models\Delivery_Method_Meta_Data;

class Checkout_Payload_Meta_Repository {
	const CHECKOUT_PAYLOAD_META_FIELD_NAME = 'sendcloudshipping_checkout_payload_meta';

	/**
	 * Get service point based on order id
	 *
	 * @param $order_id
	 *
	 * @return Delivery_Method_Meta_Data|null
	 */
	public function get_delivery_method_data( $order_id ) {
		$data = get_post_meta( $order_id, self::CHECKOUT_PAYLOAD_META_FIELD_NAME );
		if ( empty( $data ) ) {
			return null;
		}

		// Compatibility with old payload
		$delivery_method_data = array_key_exists( 'nominated_day_delivery', $data[0] ) ? $data[0]['nominated_day_delivery'] : array();
		$delivery_method_data = array_key_exists( 'delivery_method_data', $data[0] ) ? $data[0]['delivery_method_data'] : $delivery_method_data;

		if ( empty( $delivery_method_data ) ) {
			return null;
		}

		return Delivery_Method_Meta_Data::from_array( $delivery_method_data );
	}

	/**
	 * Get delivery method type
	 *
	 * @param $order_id
	 *
	 * @return string
	 */
	public function get_delivery_method_type( $order_id ) {
		$data = get_post_meta( $order_id, self::CHECKOUT_PAYLOAD_META_FIELD_NAME );
		if ( empty( $data ) ) {
			return null;
		}

		return array_key_exists( 'delivery_method_type', $data[0] ) ? $data[0]['delivery_method_type'] : '';
	}

	/**
	 * Update service point
	 *
	 * @param $order_id
	 * @param array $raw_payload_data Raw checkout payload data array
	 *
	 */
	public function save_raw( $order_id, $raw_payload_data ) {
		update_post_meta( $order_id, self::CHECKOUT_PAYLOAD_META_FIELD_NAME, $raw_payload_data );
	}
}