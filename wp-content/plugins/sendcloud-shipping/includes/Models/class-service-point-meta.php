<?php

namespace Sendcloud\Shipping\Models;

class Service_Point_Meta {
	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var string
	 */
	private $extra;

	/**
	 * @return string
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function set_id( $id ) {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function get_extra() {
		return $this->extra;
	}

	/**
	 * @param string $extra
	 */
	public function set_extra( $extra ) {
		$this->extra = $extra;
	}

	/**
	 * Return object as array
	 *
	 * @return array
	 */
	public function to_array() {
		return array(
			'id'    => $this->id,
			'extra' => $this->extra
		);
	}

	/**
	 * Creates object from array
	 *
	 * @param $data
	 *
	 * @return Service_Point_Meta
	 */
	public static function from_array( $data ) {
		$service_point = new self();

		$service_point->id    = array_key_exists( 'id', $data ) ? $data['id'] : '';
		$service_point->extra = array_key_exists( 'extra', $data ) ? $data['extra'] : '';

		return $service_point;
	}
}