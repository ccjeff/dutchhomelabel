<?php

namespace Sendcloud\Shipping\Models;

class Api_Key {

	/**
	 * @var int
	 */
	private $key_id;

	/**
	 * @var int
	 */
	private $user_id;

	/**
	 * @var string
	 */
	private $consumer_key;

	/**
	 * @var string
	 */
	private $consumer_secret;

	/**
	 * @return int
	 */
	public function get_key_id() {
		return $this->key_id;
	}

	/**
	 * @param int $key_id
	 */
	public function set_key_id( $key_id ) {
		$this->key_id = $key_id;
	}

	/**
	 * @return int
	 */
	public function get_user_id() {
		return $this->user_id;
	}

	/**
	 * @param int $user_id
	 */
	public function set_user_id( $user_id ) {
		$this->user_id = $user_id;
	}

	/**
	 * @return string
	 */
	public function get_consumer_key() {
		return $this->consumer_key;
	}

	/**
	 * @param string $consumer_key
	 */
	public function set_consumer_key( $consumer_key ) {
		$this->consumer_key = $consumer_key;
	}

	/**
	 * @return string
	 */
	public function get_consumer_secret() {
		return $this->consumer_secret;
	}

	/**
	 * @param string $consumer_secret
	 */
	public function set_consumer_secret( $consumer_secret ) {
		$this->consumer_secret = $consumer_secret;
	}

	/**
	 * Transforms array into an object
	 *
	 * @param $data
	 *
	 * @return Api_Key
	 */
	public static function from_array( $data ) {
		$api_key                  = new self();
		$api_key->key_id          = $data['key_id'];
		$api_key->user_id         = $data['user_id'];
		$api_key->consumer_secret = $data['consumer_secret'];

		return $api_key;
	}
}