(function ($) {
    'use strict';
    $(document).ready(function () {
        var form = $('#sendcloud_shipping_connect').parents('form');
        form.attr({target: '_blank', rel: 'noopener noreferrer'});
        form.on('submit', function () {
            form.find('input.button').prop('disabled', true);
            setTimeout(function () {
                window.location.reload(true);
            }, 5000);
        })
    });
})(jQuery);