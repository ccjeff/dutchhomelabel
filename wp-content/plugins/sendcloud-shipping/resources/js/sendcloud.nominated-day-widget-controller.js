/* global jQuery, SendcloudLocaleMessages */
jQuery(function ($) {
    let widgetStates = {};

    /**
     * @type {jQuery|HTMLElement|*}
     */
    let $checkoutForm = $('form.checkout');

    if (0 === $checkoutForm.length || !window.renderScShippingOption) {
        return;
    }

    let $selectedShippingMethod,
        shippingMethodSelectorPrefix,
        mountElement,
        deliveryMethod,
        locale,
        state,
        renderedWidgetDestructor;

    $(document.body).on('updated_checkout', function () {
        if (renderedWidgetDestructor) {
            renderedWidgetDestructor.call();
        }

        $selectedShippingMethod = findSelectedShippingMethod();
        if (0 === $selectedShippingMethod.length) {
            return;
        }

        initShippingMethodSelectorPrefix();

        let deliveryMethodConfig = $(`#${shippingMethodSelectorPrefix}_delivery_method`).html() || null;
        mountElement = $(`#${shippingMethodSelectorPrefix}_mount_point`).get(0);
        deliveryMethod = JSON.parse(deliveryMethodConfig);
        locale = $(`#${shippingMethodSelectorPrefix}_locale`).val() || 'en-US';
        state = widgetStates[shippingMethodSelectorPrefix];

        renderWidget();
    });

    function initShippingMethodSelectorPrefix() {
        shippingMethodSelectorPrefix = $selectedShippingMethod.attr('id');

        // If default prefix does not work try to adopt for the Woo 2.6.X versions where single shipping method id is
        // not generated uniformly as when there are multiple shipping methods available on the checkout (instance id
        // is committed from id attribute for versions 2.6.X)
        if (!$(`#${shippingMethodSelectorPrefix}_delivery_method`).html()) {
            shippingMethodSelectorPrefix += '_' + $selectedShippingMethod.val().replace(/:/g, '');
        }
    }

    /**
     * @return {jQuery|HTMLElement|*}
     */
    function findSelectedShippingMethod() {
        let name = 'shipping_method';
        return $checkoutForm.find(
            `select.${name}, input[name^="${name}"][type="radio"]:checked, input[name^="${name}"][type="hidden"]`
        );
    }

    function renderWidget() {
        if (!mountElement || !deliveryMethod) {
            return;
        }
        $(mountElement).on('scShippingOptionChange', onNominatedDateSelectionChange);

        renderedWidgetDestructor = window.renderScShippingOption({
            mountElement,
            deliveryMethod,
            locale,
            state,
            localeMessages: SendcloudLocaleMessages
        });

    }

    function onNominatedDateSelectionChange(event) {

        widgetStates[shippingMethodSelectorPrefix] = event.detail.state;

        let $selectionDataElement = $(`#${shippingMethodSelectorPrefix}_submit_data`);
        if (0 !== $selectionDataElement.length) {
            $selectionDataElement.val(JSON.stringify({...deliveryMethod, ...event.detail.data}));
        }
    }
});