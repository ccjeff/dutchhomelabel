<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * @var array $data
 */

?>
<!-- Required by Woocommerce 3.5.5 onwards -->
<input type="hidden" name="save" value="yes"/>
<!-- End required -->
<div id="sendcloud_shipping_connect" class="submit">
    <div class="connect">
        <input type="submit" name="connect" class="button button-primary" <?php if ( ! $data['permalinks_enabled'] )
			echo 'disabled="true"' ?>value="<?php _e( 'Connect with Sendcloud', 'sendcloud-shipping' ); ?>"/>
        <p class="description">
			<?php _e( 'Allows you to see your WooCommerce orders in the Sendcloud Panel.', 'sendcloud-shipping' ); ?>
        </p>
		<?php if ( ! $data['permalinks_enabled'] ): ?>
            <p class="sendcloud-enable-permalinks">
				<?php _e( 'You need to enable permalinks. Go to Settings -> Permalinks.', 'sendcloud-shipping' ); ?>
            </p>
		<?php endif ?>
    </div>
    <div class="goto-panel">
        <a class="button button-primary" href="<?php echo $data['panel_url']; ?>" target="_blank"
           rel="noopener noreferrer"><?php _e( 'Go to Sendcloud', 'sendcloud-shipping' ); ?></a>
        <p class="description">
			<?php _e( 'Open the Sendcloud panel and start shipping.', 'sendcloud-shipping' ); ?>
        </p>
    </div>
</div>
