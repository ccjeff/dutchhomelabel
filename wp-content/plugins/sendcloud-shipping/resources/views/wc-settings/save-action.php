<?php
/**
 * @var array $data
 */
?>
<script>
    window.location.href = '<?php echo $data['url'];?>';
</script>
<noscript>
    <meta http-equiv="refresh" content="0;url=<?php echo $data['url']; ?>"/>
</noscript>