<?php
/**
 * @var array $data
 */

$address = implode( '<br>', explode( '|', $data['address'] ) )
?>
<div class="col2-set addresses">
    <div class="col1">
        <header class="title">
            <h3><?php echo __( 'Service Point Address', 'sendcloud-shipping' ); ?></h3>
        </header>
        <address>
			<?php echo $address; ?>
        </address>
    </div>
</div>
