<?php
/**
 * @var array $data
 */

/**
 * @var \Sendcloud\Shipping\Models\Delivery_Method_Meta_Data $delivery_method_data_payload
 */
$delivery_method_data_payload = $data['delivery_method_data_payload'];
?>
<div class="address">
    <h3><?php echo __( 'Expected delivery date', 'sendcloud-shipping' ); ?></h3>
	<?php echo $delivery_method_data_payload->get_delivery_date()->format( get_option( 'date_format' ) ); ?>
    <br>
    <span class="description"><?php echo wc_help_tip( __( "You can't change the selected delivery date",
				'sendcloud-shipping' ) ) . ' ' . __( 'Non editable', 'sendcloud-shipping' ); ?></span>
</div>
