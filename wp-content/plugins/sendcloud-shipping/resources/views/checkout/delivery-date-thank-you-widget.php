<?php
/**
 * @var array $data
 */

/**
 * @var \Sendcloud\Shipping\Models\Delivery_Method_Meta_Data $delivery_method_data_payload
 */
$delivery_method_data_payload = $data['delivery_method_data_payload'];
?>
<div class="col2-set addresses">
    <div class="col1">
        <header class="title">
            <h3><?php echo __( 'Expected delivery date', 'sendcloud-shipping' ); ?></h3>
        </header>
        <address>
			<?php echo $delivery_method_data_payload->get_formatted_delivery_date(); ?>
        </address>
    </div>
</div>
