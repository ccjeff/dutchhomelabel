<?php
/**
 * @var array $data
 */

$address = join( '<br>', explode( '|', $data['address'] ) )
?>
<div class="address">
    <h3><?php echo __( 'Service Point Address', 'sendcloud-shipping' ); ?></h3>
	<?php echo $address; ?>
    <br>
    <span class="description"><?php echo wc_help_tip( __( "You can't change the selected Service Point", 'sendcloud-shipping' ) ) . ' '
	                                     . __( 'Non editable', 'sendcloud-shipping' ); ?></span>
</div>
