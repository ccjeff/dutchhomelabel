<?php
/**
 * @var array $data
 */
?>
<script type='text/javascript'>
    var SENDCLOUDSHIPPING_LANGUAGE = '<?php echo $data['language'];?>';
    var SENDCLOUDSHIPPING_SELECT_SPP_LABEL = '<?php echo $data['select_spp_label'];?>';
    var SENDCLOUDSHIPPING_DIMENSIONS = '<?php echo $data['cart_dimensions'];?>';
    var SENDCLOUDSHIPPING_DIMENSIONS_UNIT = <?php echo $data['cart_dimensions_unit'];?>;
</script>

