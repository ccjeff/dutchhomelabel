<?php
/**
 * @var array $data
 */

$clean_shipping_method_id = esc_attr( sanitize_title( $data['shipping_method_id'] ) );
$id_prefix                = sprintf( 'shipping_method_%1$d_%2$s', $data['index'], $clean_shipping_method_id );
?>
<div id="<?php echo $id_prefix ?>_mount_point"></div>
<input type="hidden" id="<?php echo $id_prefix ?>_locale" value="<?php echo $data['locale'] ?>"/>
<input type="hidden" id="<?php echo $id_prefix ?>_submit_data"
       name="sendcloudshipping_nominated_day_data[<?php echo $clean_shipping_method_id ?>]"/>
<script
        type="application/json"
        id="<?php echo $id_prefix ?>_delivery_method"
><?php echo json_encode( $data['delivery_method_config'] ) ?></script>