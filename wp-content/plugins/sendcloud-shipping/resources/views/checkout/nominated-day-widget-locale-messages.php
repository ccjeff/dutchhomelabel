<?php
/**
 * @var array $data
 */
?>
<script type="text/javascript">
    const SendcloudLocaleMessages = <?php echo json_encode( $data['locale_messages'] ) ?>;
</script>