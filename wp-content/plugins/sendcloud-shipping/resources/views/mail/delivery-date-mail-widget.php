<?php

/**
 * @var array $data
 */

/**
 * @var \Sendcloud\Shipping\Models\Delivery_Method_Meta_Data $delivery_method_data_payload
 */
$delivery_method_data_payload = $data['delivery_method_data_payload'];
?>
<h3><?php echo __( 'Expected delivery date', 'sendcloud-shipping' ); ?></h3>
<p><?php echo $delivery_method_data_payload->get_formatted_delivery_date(); ?></p>