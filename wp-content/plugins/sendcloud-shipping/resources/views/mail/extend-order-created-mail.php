<?php

/**
 * @var array $data
 */

$address = join( '<br>', explode( '|', $data['address'] ) );

?>
<h3><?php echo __( 'Service Point Address', 'sendcloud-shipping' ); ?></h3>
<p><?php echo $address ?></p>