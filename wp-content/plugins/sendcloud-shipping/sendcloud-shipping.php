<?php
/**
 * Plugin Name: Sendcloud | Smart Shipping Service
 * Plugin URI: http://sendcloud.sc
 * Description: Sendcloud plugin.
 * Version: 2.1.0
 * Author: Sendcloud B.V.
 * Author URI: http://sendcloud.sc
 * Requires at least: 4.5.0
 * Tested up to: 5.8
 *
 * Text Domain: sendcloud-shipping
 * Domain Path: /i18n/languages/
 * WC requires at least: 2.6.0
 * WC tested up to: 5.7
 *
 * @package sendcloud-shipping
 * @category Core
 * @author Sendcloud B.V.
 */

use Sendcloud\Shipping\Sendcloud;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

require_once plugin_dir_path( __FILE__ ) . '/vendor/autoload.php';

if ( file_exists( __DIR__ . '/dev_env.php' ) ) {
	require_once __DIR__ . '/dev_env.php';
}

Sendcloud::init( __FILE__ );

//This is for backward compatibility with WooFunnels plugin
function sendcloudshipping_add_service_point_to_checkout() {
    //Don't do nothing, real method is used.
}