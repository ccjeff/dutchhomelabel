<?php
/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.daisycon.com
 * @since      1.0.0
 *
 * @package    Daisycon_Woocommerce
 * @subpackage Daisycon_Woocommerce/public/partials
 */
?>
<?php do_action( 'daisycon_woocommerce_pixel_before_display' ); ?>

<img src="<?php echo $this->_get_daisycon_pixel_url(); ?>" style="border: 0; height: 1px; width: 1px;" alt="<?php echo __( 'Advertising through Daisycon', $this->text_domain ); ?>" />
<script type="text/javascript">
    window.onload = function () {
        setTimeout(function () {
            im = document.getElementsByTagName('img');
            for (i = 0; i < im.length; i++) {
                o = im[i];
                if (/[s|c]i=/i.exec(o.src) && (!o.offsetHeight || o.offsetHeight < 1)) {
                    i = document.createElement('img');
                    i.height = '1';
                    i.width = '1';
                    i.id = 'news';
                    i.className = 'net';
                    i.src = '//' + Math.round(+new Date() / 83000) + '.' + i.id + 'tat.' + i.className + '/ab/' + o.src.substring(o.src.indexOf('?'), o.src.length);
                    document.body.appendChild(i);
                }
            }
        }, 100);
    }
</script>

<?php do_action( 'daisycon_woocommerce_pixel_after_display' ); ?>