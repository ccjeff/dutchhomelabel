<?php

/**
 * The plugin bootstrap file
 *
 * @link              https://www.daisycon.com
 * @since             1.0.0
 * @package           Daisycon_Woocommerce
 *
 * @wordpress-plugin
 * Plugin Name:       Daisycon WooCommerce pixel
 * Plugin URI:        https://www.daisycon.com/nl/tools/woocommerce-conversie-pixel/
 * Description:       This plugin will automatically add the Daisycon Pixel to the WooCommmerce succespage
 * Version:           1.5
 * Author:            daisycon
 * Author URI:        https://www.daisycon.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       daisycon-woocommerce
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'DAISYCON_PLUGIN_VERSION', '1.5' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-daisycon-woocommerce-activator.php
 */
function activate_daisycon_woocommerce() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-daisycon-woocommerce-activator.php';
	Daisycon_Woocommerce_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-daisycon-woocommerce-deactivator.php
 */
function deactivate_daisycon_woocommerce() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-daisycon-woocommerce-deactivator.php';
	Daisycon_Woocommerce_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_daisycon_woocommerce' );
register_deactivation_hook( __FILE__, 'deactivate_daisycon_woocommerce' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-daisycon-woocommerce.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_daisycon_woocommerce() {

	$plugin = new Daisycon_Woocommerce();
	$plugin->run();

}
run_daisycon_woocommerce();

/**
 * Get a setting value
 *
 * @param bool $setting
 * @param bool $first
 *
 * @return mixed
 */
function daisycon_get_setting_value( $setting = false, $first = false ){
	if( empty( $setting ) ){
		return false;
	}

	if( $options = get_option( 'daisycon_woocommerce_options' ) ){
		if( $first ){
			return isset( $options[$setting][0] ) ? $options[$setting][0] : false;
		}

		return isset( $options[$setting] ) ? $options[$setting] : false;
	}

	return false;
}
