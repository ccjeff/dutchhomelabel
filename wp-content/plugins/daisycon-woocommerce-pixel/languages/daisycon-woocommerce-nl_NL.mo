��          �   %   �      0  I   1     {     �     �     �     �  M   �     B  #   ]  0   �  .   �     �     �     �               '     5     C     S  Q   V     �     �  �  �  m   X  +   �     �     
     "     0  T   N     �  '   �  >   �  -   %     S     a     n     {     �     �     �     �     �  R   �     	     !	                                          
                                                  	                                   <p>You can add product extra fields or attributes by adding the name.</p> Add shipping in totals Advertising through Daisycon Campaign ID (required) Commission VAT Daisycon Pixel Commission Code Daisycon WooCommerce Pixel: No order ID found in the "dc_add_pixel" function. Daisycon WooCommerce pixel Daisycon WooCommerce pixel settings Daisycon WooCommerce pixel settings explanation. Enter the Daisycon Pixel Commission Code here. Excluding VAT Extra field 1 Extra field 2 Extra field 3 Extra field 4 Extra field 5 Including VAT Matching Domain No Not all required settings for the Daisycon WooCommerce pixel plugin have been set Yes lt45.net Project-Id-Version: Daisycon WooCommerce pixel
POT-Creation-Date: 2018-06-25 12:29+0200
PO-Revision-Date: 2018-06-25 12:29+0200
Last-Translator: 
Language-Team: Daisycon
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 <p>U kunt hieronder een custom attribuut of custom veld van een product toevoegen doormiddel van de naam.</p> Voeg verzendkosten toe aan het totaalbedrag Adverteren via Daisycon Campagne ID (verplicht) Commissie BTW Daisycon Pixel Commissie Code Daisycon WooCommerce Pixel: Geen order ID gevonden in de “dc_add_pixel” functie. Daisycon WooCommerce pixel Daisycon WooCommerce pixel instellingen Hier vind u de instellingen van de Daisycon WooCommerce pixel. Vul de Daisycon Pixel Commissie Code hier in. Exclusief BTW Extra veld 1 Extra veld 2 Extra veld 3 Extra veld 4 Extra veld 5 Inclusief BTW Overeenkomstige domein Nee Niet alle verplichte velden van de Daisycon WooCommerce pixel plugin zijn ingevuld Ja lt45.net 