﻿=== Daisycon WooCommerce Pixel ===
Contributors: Daisycon
Donate link: https://www.daisycon.com
Tags: Daisycon, Daisycon WooCommerce Pixel, WooCommerce, Pixel, Conversion Pixel
Requires at least: 4.8
Tested up to: 5.7.2
Stable tag: 5.7.2
Requires PHP: 5.6
License: Daisycon

Adding Daisycon conversion pixel to WooCommerce

== Description ==
Daisycon offers a plugin to install the conversion pixel to advertisers who use WordPress with WooCommerce.
This plugin adds the conversion pixel to WooCommerce without changing any code.
General information about the Daisycon conversion pixel: [https://faq-advertiser.daisycon.com/hc/en-us/articles/206905459-Implement-the-conversion-pixel](https://faq-advertiser.daisycon.com/hc/en-us/articles/206905459-Implement-the-conversion-pixel)

== Installation ==
**Installation using the WordPress plugin page**
Navigate in the WordPress menu to "Plugins -> Add New". In the search field, in the upper right corner, type "Daisycon WooCommerce" and wait until the plugin shows up in the list below. You can now install and activate the plugin.

**Manual installation**
To install the plugin manually, you have to download it first.  Navigate in the WordPress menu to "Plugins -> Add New". In the upper left corner, press the "Upload Plugin" button and select the downloaded plugin.
Press the "Install Now" button and finish it up by pressing the "Activate Plugin" button.

== Frequently Asked Questions ==
To change the settings of the plugin, navigate in the WordPress menu to "Settings -> Daisycon WooCommerce Pixel". Below you can find a more detailed description of the fields and values on the settings page.

= Campaign ID (required) =
In this field, you can add the Campaign ID of your campaign/program with Daisycon. You can find this ID in the Daisycon interface. Read more about how to do this, over here: [https://faq-advertiser.daisycon.com/hc/en-us/articles/206902879-What-is-a-campaign-ID-and-where-do-I-find-mine-](https://faq-advertiser.daisycon.com/hc/en-us/articles/206902879-What-is-a-campaign-ID-and-where-do-I-find-mine-)

= Matching Domain =
Daisycon uses different domains to measure transactions. This domain may differ for each campaign. It is important that you set the correct matching domain for your campaign. The plugin shows all available matching domains.

= Use LCC cookie =
If you are using multiple affiliate networks for the same campaign you might want to consider turning this option on.
If this option is turned on, only the last affiliate link click is rewarded the commission for generated sales.
The plugin detects if the URL contains the "network" parameter, and sets a first-party cookie containing this value. When a sale is generated, this value will be read from the cookie to determine if the sale came from a Daisycon link. You can read more about it here: [https://faq-advertiser.daisycon.com/hc/en-us/articles/208742425-Network-filtering-implementing-an-LCC-script](https://faq-advertiser.daisycon.com/hc/en-us/articles/208742425-Network-filtering-implementing-an-LCC-script)

= LCC `network` url parameter =
Here you can configure which URL parameter to use for the LCC cookie. If "daisycon" is the value of the configured URL parameter, when someone visits the website, the sale will be measured by the Daisycon system. The `network` parameter in the URL is determined by the campaign links added in the Daisycon system. If you have any questions about this, ask your account manager at Daisycon. They will gladly help you.

= Commission VAT =
Do you want the commission to be paid based on the product prices including or excluding VAT?

= Extra field 1 to 5 =
You can add extra product data to the pixel, to enrich the statistics of your campaign within the Daisycon interface. These extra fields are E1 to E5. Enriching the statistics gives you the option to optimize your campaign. Using the extra fields is not required.

= Optional: Configuring product-specific commissions =
Every product will get an extra field, once the Daisycon WooCommerce plugin is installed. If you look at "Product data -> General" on the product page, you can find a new field called "Daisycon Pixel Commission Code". Only configure this field if you want the product to have a variable commission. The value of this field will be matched with the Commission Code configured in the Daisycon interface. If it matches the Commission Code in the Daisycon interface it will assign the commission matching the Commission Code to the product. If it doesn't match, it will fall back to the default commission value for the campaign.

= Questions & more information =
If you have any questions regarding the installation of this plugin, don't hesitate to contact us by submitting a support ticket from your Daisycon account.

== Screenshots ==
1. Settings overview

== Changelog ==
= 1.5 =
* Replaced deprecated get_used_coupons function
* Fixed 'include_once' issue
* Added matching domain
* Translated README to English

= 1.4 =
* Added new parameter

= 1.3 =
* Added matching domain

= 1.2 =
* Added all available matching domains

= 1.1 =
* Coupon codes are now shown correctly

= 1.0.5 =
* Added LCC method

= 1.0.4 =
* Extra values weren't correctly passed through to our system, this is fixed now

= 1.0.3 =
* Edited plugin manual

= 1.0.2 =
* Stability improvements

= 1.0.1 =
* Small changes

= 1.0 =
* First release of our plugin

== Upgrade Notice ==
= 1.5 =
* Replace deprecated get_used_coupons function
* Fixed 'include_once' issue
* Added matching domain
* Translated README to English

= 1.4 =
* Added new parameter

= 1.3 =
* Added matching domain

= 1.2 =
* Added all available matching domains

= 1.1 =
* Coupon codes are now shown correctly

= 1.0.5 =
* Added LCC method

= 1.0.4 =
* Extra values weren't correctly passed through to our system, this is fixed now

= 1.0.3 =
* Edited plugin manual

= 1.0.2 =
* Stability improvements

= 1.0.1 =
* Small changes

= 1.0 =
* First release of our plugin
