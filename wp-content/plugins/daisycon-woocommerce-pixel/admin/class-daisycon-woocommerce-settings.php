<?php

/**
 * The settings-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Daisycon_Woocommerce_Admin
 * @subpackage Daisycon_Woocommerce_Admin/admin
 */
class Daisycon_Woocommerce_Settings extends Daisycon_Woocommerce_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	protected $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	protected $version;

	/**
	 * The text domain
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $text -domain
	 */
	protected $text_domain = 'daisycon-woocommerce';

	/**
	 * The option name
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $option_name
	 */
	protected $option_name = 'daisycon_woocommerce_options';

	/**
	 * The settings of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $settings    The settings of this plugin.
	 */
	protected $settings = null;

	/**
	 * The setting fields of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $setting_fields    The setting fields of this plugin.
	 */
	protected $setting_fields = null;

	/**
	 * The required fields to make this plugin work correctly.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array    $setting_fields    The required setting fields of this plugin.
	 */
	protected $required_settings = array( 'campaign_id', 'daisycon_matching_domain', 'daisycon_commission_vat' );

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of this plugin.
	 * @param      string $version The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version     = $version;

		$this->get_settings();
	}

	/**
	 * Add an options page under the Settings submenu
	 *
	 * @since  1.0.0
	 */
	public function daisycon_add_options_page() {
		add_options_page(
			__( 'Daisycon WooCommerce pixel', $this->text_domain ),
			__( 'Daisycon WooCommerce pixel', $this->text_domain ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'daisycon_display_options_page' )
		);
	}

	/**
	 * Render the options page for plugin
	 *
	 * @since  1.0.0
	 */
	public function daisycon_display_options_page() {
		include_once plugin_dir_path( dirname( __FILE__ ) ) .  'admin/partials/daisycon-woocommerce-admin-display.php';
	}

	/**
	 * Register the settings
	 */
	public function daisycon_register_settings(){
		$settings = $this->_get_setting_fields();

		/**
		 * Add the title
		 */
		add_settings_section(
			$this->option_name . '_general',
			'',
			array( $this, 'general_description' ),
			$this->plugin_name
		);

		$this->add_settings( $settings );
	}

	/**
	 * Show a message in the WP Admin
	 *
	 * @param string $type
	 * @param bool $message
	 * @return bool
	 */
	public function show_admin_notice( $type = 'error', $message = false ){
		if( !$message ){
			return false;
		}

        printf( '<div class="notice notice-%1$s is-dismissible"><p>%2$s</p></div>', esc_attr( $type ), esc_html( $message ) );
	}

	/**
	 * Get and set the settings fields
	 *
	 * @return mixed
	 */
	protected function _get_setting_fields(){
		if( is_null( $this->setting_fields ) ){
			$this->setting_fields = array(
				'campaign_id' => array(
					'title' => __( 'Campaign ID (required)', $this->text_domain ),
					'type' 	=> 'text',
					'placeholder' => ''
				),
				'matching_domain' => array(
					'title' => __( 'Matching Domain', $this->text_domain ),
					'type' 	=> 'select',
					'group' => 'daisycon_matching_domain',
					'options' => array(
						'at19.net'  => __( 'at19.net', $this->text_domain ),
						'ds1.nl'    => __( 'ds1.nl', $this->text_domain ),
						'dt51.net'  => __( 'dt51.net', $this->text_domain ),
						'dt61.net'  => __( 'dt61.net', $this->text_domain ),
						'fr135.net' => __( 'fr135.net', $this->text_domain ),
						'hs82.net'  => __( 'hs82.net', $this->text_domain ),
						'jf79.net'  => __( 'jf79.net', $this->text_domain ),
						'lt45.net'  => __( 'lt45.net', $this->text_domain ),
						'mt74.net'  => __( 'mt74.net', $this->text_domain ),
						'ndt5.net'  => __( 'ndt5.net', $this->text_domain ),
					)
				),
				'daisycon_lcc_enabled' => array(
					'title' => __( 'Use LCC cookie', $this->text_domain ),
					'type' 	=> 'select',
					'group' => 'daisycon_lcc_enabled',
					'options' => array(
						'no' => __( 'No', $this->text_domain ),
						'yes' => __( 'Yes', $this->text_domain ),
					)
				),
				'daisycon_lcc_url_param' => array(
					'title' => __( 'LCC `network` url parameter (?the_configured_parameter_below=daisycon)', $this->text_domain ),
					'type' 	=> 'text',
					'default' => 'afnetwork',
				),
				'commission_vat' => array(
					'title' => __( 'Commission VAT', $this->text_domain ),
					'type' 	=> 'select',
					'group' => 'daisycon_commission_vat',
					'options' => array(
						'incl' => __( 'Including VAT', $this->text_domain ),
						'excl' => __( 'Excluding VAT', $this->text_domain ),
					)
				),
				'explain_custom_fields' => array(
				    'title' => '',
					'type' 	=> 'seperator',
					'text' => __( '<p>You can add product extra fields or attributes by adding the name.</p>', $this->text_domain ),
				),
				'cf_one' => array(
					'title' => __( 'Extra field 1', $this->text_domain ),
					'type' 	=> 'text',
					'placeholder' => ''
				),
				'cf_two' => array(
					'title' => __( 'Extra field 2', $this->text_domain ),
					'type' 	=> 'text',
					'placeholder' => ''
				),
				'cf_three' => array(
					'title' => __( 'Extra field 3', $this->text_domain ),
					'type' 	=> 'text',
					'placeholder' => ''
				),
				'cf_four' => array(
					'title' => __( 'Extra field 4', $this->text_domain ),
					'type' 	=> 'text',
					'placeholder' => ''
				),
				'cf_five' => array(
					'title' => __( 'Extra field 5', $this->text_domain ),
					'type' 	=> 'text',
					'placeholder' => ''
				),
			);
		}

		return apply_filters( 'daisycon_woocommerce_pixel_settings_fields', $this->setting_fields );
	}

	/**
	 * Check if all required settings have been set, otherwise show a message
	 */
	public function daisycon_check_required_settings(){
		$settings = $this->get_settings();
		$error_shown = false;

		if ( wp_doing_ajax() ) {
		    return true;
        }

		foreach( $this->required_settings as $required_setting ){
			if( $error_shown ){
				continue;
			}

			if( !isset( $settings[$required_setting] ) || ( isset( $settings[$required_setting] ) && empty( $settings[$required_setting] ) ) ){
				$this->show_admin_notice( 'error', __( 'Not all required settings for the Daisycon WooCommerce pixel plugin have been set', $this->text_domain ) );
				$error_shown = true;
			}
		}
	}

	/**
	 * Get and set the settings
	 *
	 * @return mixed|string
	 */
	protected function get_settings(){
		if( is_null( $this->settings ) ){
			$this->settings = get_option( $this->option_name );
		}

		return $this->settings;
	}

	/**
	 * Render the description for the general section
	 *
	 * @since  1.0.0
	 */
	public function general_description() {
		echo sprintf( '<p>%s</p>', __( 'Daisycon WooCommerce pixel settings explanation.', $this->text_domain ) );
	}

	/**
	 * Add the settings by sending a array with 'id' => 'description'
	 *
	 * @param bool $settings
	 * @param string $option_group
	 * @return bool
	 */
	protected function add_settings( $settings = false, $option_group = 'general' ){
		if( !$settings || !is_array($settings) ){
			return false;
		}

		foreach( $settings as $setting => $setting_info ){
			if( empty( $setting ) || empty( $setting_info ) ){
				continue;
			}

			$callback = $setting_info['type'] . '_field_callback';

			add_settings_field(
				$setting,
				__( $setting_info['title'], $this->text_domain ),
				array( $this, $callback ),
				$this->plugin_name,
				$this->option_name . '_' . $option_group,
				array(
					'setting' => $setting,
					'label_for' => $setting,
					'info' => $setting_info,
				)
			);
		}

		register_setting( $this->plugin_name, $this->option_name, array( $this, 'validate_options' ) );

		return true;
	}

	/**
	 * Render a seperator text
	 *
	 * @param array $args
	 *
	 * @return bool
	 */
	public function seperator_field_callback( array $args ){
		if( !isset( $args['setting'] ) ){
			return false;
		}

		if( isset( $args['info']['text'] ) ){
			echo $args['info']['text'];
		}
	}

	/**
	 * Render a text input for this plugin
	 *
	 * @param array $args
	 * @return bool
	 */
	public function text_field_callback( array $args ) {
		if( !isset( $args['setting'] ) ){
			return false;
		}

		$placeholder = ( isset( $args['info']['placeholder'] ) ) ? $args['info']['placeholder'] : '';

		$this->_show_text_field( $args['setting'], $placeholder );
	}

	/**
	 * Quick function to echo a text field
	 *
	 * @param $field_name
	 * @param $placeholder
	 */
	protected function _show_text_field( $field_name, $placeholder = '' ) {
		$name = sprintf( '%s[%s]', $this->option_name, $field_name );

		$val = ( isset( $this->settings[$field_name] ) ) ? esc_attr( $this->settings[$field_name] ) : '';

		echo '<input class="regular-text" type="text" id="' . $name . '" name="' . $name . '" value="' . $val . '" placeholder="' . $placeholder . '" />';
	}

	/**
	 * Render a text input for this plugin
	 *
	 * @param array $args
	 * @return bool
	 */
	public function password_field_callback( array $args ) {
		if( !isset( $args['setting'] ) ){
			return false;
		}

		$placeholder = ( isset( $args['info']['placeholder'] ) ) ? $args['info']['placeholder'] : '';

		$this->_show_password_field( $args['setting'], $placeholder );
	}

	/**
	 * Quick function to echo a password field
	 *
	 * @param $field_name
	 * @param $placeholder
	 */
	protected function _show_password_field( $field_name, $placeholder = '' ) {
		$name = sprintf( '%s[%s]', $this->option_name, $field_name );
		$val = ( isset( $this->settings[$field_name] ) ) ? esc_attr( $this->settings[$field_name] ) : '';
		echo '<input class="regular-text" type="password" id="' . $name . '" name="' . $name . '" value="' . $val . '" placeholder="' . $placeholder . '" />';
	}

	/**
	 * @param array $args
	 */
	public function button_field_callback( array $args ) {
		$onclick = ( isset( $args['info']['onclick'] ) ) ? sprintf( 'onclick="location.href=\'%s\'"', $args['info']['onclick'] ) : '';
		echo '<button id="' . $args['setting'] . '" ' . $onclick . '>' . $args['info']['title'] . '</button>';
	}

	/**
	 * @param array $args
	 */
	public function link_field_callback( array $args ) {
		echo '<a href="'. $args['info']['href'] .'" id="' . $args['setting'] .'">' . $args['info']['title'] . '</a>';
	}

	/**
	 * Quick function to show select input
	 *
	 * @param array $args
	 */
	public function select_field_callback( array $args ){
		$group = $args['info']['group'];
		$all_options = $args['info']['options'];

		$multiselect = ( isset( $args['info']['multiple'] ) ) ? 'multiple' : '';

		$options = '';
		$selected_options = ( isset( $this->settings[$group] ) ) ? $this->settings[$group] : array();

		foreach( $all_options as $option => $title ){
			$options .= $this->_show_select_option_field( $option, $selected_options, $title );
		}

		echo sprintf( '<select name="%s[%s][]" style="width: 25em;" %s>%s</select>', $this->option_name, $group, $multiselect, $options );
	}

	/**
	 * Quick function to echo a select option field
	 *
	 * @param $field_name
	 * @param $selected_options
	 * @param $title
	 *
	 * @return string
	 */
	protected function _show_select_option_field( $field_name, $selected_options, $title = false ) {
		$checked = ( in_array( $field_name, $selected_options ) ) ? 'selected="selected"' : false;
		$title = ( $title ) ?: ucfirst( $field_name );

		return sprintf( '<option id="%s" value="%s" %s /> %s', $field_name, $field_name, $checked, $title );
	}

}
