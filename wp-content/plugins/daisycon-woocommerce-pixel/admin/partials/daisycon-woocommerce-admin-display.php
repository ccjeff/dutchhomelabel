<div class="wrap" id="dc-core-container">
    <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . '../public/images/logo_small.png'; ?>" alt="<?php echo __( 'Daisycon WooCommerce pixel settings', 'daisycon-woocommerce' ); ?>" title="<?php echo __( 'Daisycon WooCommerce pixel settings', 'daisycon-woocommerce' ); ?>" />
    <h2><?php echo __( 'Daisycon WooCommerce pixel settings', 'daisycon-woocommerce' ); ?></h2>

    <form method="post" action="options.php" id="dc-woocommerce-pixel-form">

        <div id="is-general">
			<?php settings_fields( $this->plugin_name ); ?>
			<?php do_settings_sections( $this->plugin_name ); ?>
			<?php do_action( 'daisycon_woocommerce_pixel_general_tab' ); ?>
        </div>

		<?php $nonce = wp_create_nonce( 'daisycon-woocommerce' ); ?>
        <input type="hidden" name="nonce" value="<?php echo $nonce; ?>" />

		<?php submit_button(); ?>
    </form>
</div>
