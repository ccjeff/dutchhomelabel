(function( $ ) {
    'use strict';

    $(document).ready(function(){
        $('body').on('click', '#the-list .editinline', function(){
            var parent_tr = $(this).closest('tr'),
                post_id = parent_tr.attr('id');

            post_id = post_id.replace("post-", "");
            var $daisycon_inline_data = $('#daisycon_cc_inline_' + post_id),
                daisycon_cc_text = $daisycon_inline_data.text(),
                $daisycon_input = $('#edit-' + post_id).find('.daisycon_cc_quick_edit_input');

            $daisycon_input.val($daisycon_inline_data.text());
        });
    });

})( jQuery );