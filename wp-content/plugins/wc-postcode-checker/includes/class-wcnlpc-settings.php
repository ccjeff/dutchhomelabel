<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'WPO_WCNLPC_Settings' ) ) :

class WPO_WCNLPC_Settings {
	
	function __construct()	{
		add_filter( 'plugin_action_links_'.WPO_WCNLPC_BASENAME, array( $this, 'settings_link' ) );

		add_action( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_tab' ), 21 );
		add_action( 'woocommerce_settings_tabs_postcode_checker', array( $this, 'settings_tab' ) );
        add_action( 'woocommerce_update_options_postcode_checker', array( $this, 'update_settings' ) );
	}

    /**
     * Add a new settings tab to the WooCommerce settings tabs array.
     *
     * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding ours.
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including ours.
     */
    public function add_settings_tab( $settings_tabs ) {
        $settings_tabs['postcode_checker'] = __( 'Postcode Checker', 'wpo_wcnlpc' );
        return $settings_tabs;
    }
    /**
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     *
     * @uses woocommerce_admin_fields()
     * @uses $this->get_settings()
     */
    public function settings_tab() {
        woocommerce_admin_fields( $this->get_settings() );
    }
    /**
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses $this->get_settings()
     */
    public function update_settings() {
        woocommerce_update_options( $this->get_settings() );

 		if ( version_compare( PHP_VERSION, '7.1', '>=' ) && extension_loaded('curl') && $this->autocomplete_enabled() ) {
        	$_POST[ 'postcodenl_address_autocomplete_apiKey']    = get_option('woocommerce_wcnlpc_api_key');
        	$_POST[ 'postcodenl_address_autocomplete_apiSecret'] = get_option('woocommerce_wcnlpc_api_secret');
        	$options = WPO_WCNLPC()->autocomplete->getOptions();
        	$options->handleSubmit();
 		}
    }

	/**
	 * Add our API settings to the woocommerce Checkout settings tab
	 */
	public function api_settings ( $settings ) {
		$api_settings = $this->get_settings();

		// find end of checkout page options
		$checkout_page_options_end_key = array_search( array( 'type' => 'sectionend', 'id' => 'checkout_page_options' ), $settings );
		// $checkout_page_options_end_pos = $checkout_page_options_end_key+1;
		$checkout_page_options_end_pos = array_search($checkout_page_options_end_key, array_keys($settings)) + 1;

		// insert our api settings
		$new_settings = array_merge( array_slice($settings, 0, $checkout_page_options_end_pos, true), $api_settings, array_slice($settings, $checkout_page_options_end_pos, NULL, true));


		return $new_settings;
	}

	public function get_settings() {
		$settings = array(

			array(
				'title'		=> __( 'Postcode Checker', 'wpo_wcnlpc' ),
				'type'		=> 'title',
				'id'		=> 'wcnlpc_api_options',
			),

			array(
				'title'    => __( 'Enable', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_enable',
				'type'     => 'checkbox',
				'default'  => 'yes',
				'desc_tip' => true,
			),

			array(
				'title'    => __( 'Choose your API source', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_checkout_api_source',
				'default'  => 'kadaster',
				'type'     => 'select',
				'options'  => array(
					'postcode_tech'   => __( 'postcode.tech (free)', 'wpo_wcnlpc' ),
					'kadaster'        => __( 'Het Kadaster / BAG (free)', 'wpo_wcnlpc' ),
					'postcodeapi_nu'  => 'postcodeapi.nu',
					'postcode_nl'     => 'postcode.nl',
				),
			),

			array(
				'title'    => __( 'API Token', 'wpo_wcnlpc' ),
				'desc'     => sprintf( __( 'Get your API token at %s and enter it here', 'wpo_wcnlpc' ), '<a href="https://postcode.tech/register">postcode.tech</a>' ),
				'id'       => 'woocommerce_wcnlpc_api_token_postcode_tech',
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcode_tech' ),
			),

			array(
				'title'    => __( 'Key', 'wpo_wcnlpc' ),
				'desc'     => sprintf( __( 'Get your API key at %s and enter it here', 'wpo_wcnlpc' ), '<a href="https://account.postcode.nl/registreer">postcode.nl</a>' ),
				'id'       => 'woocommerce_wcnlpc_api_key',
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcode_nl' ),
			),

			array(
				'title'    => __( 'Secret', 'wpo_wcnlpc' ),
				'desc'     => __( 'Your API Secret from postcode.nl', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_api_secret',
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcode_nl' ),
			),

			array(
				'title'    => __( 'API Key', 'wpo_wcnlpc' ),
				'desc'     => sprintf( __( 'Get your API key at %s and enter it here', 'wpo_wcnlpc' ), '<a href="https://www.postcodeapi.nu/">postcodeapi.nu</a>' ),
				'id'       => 'woocommerce_wcnlpc_api_key_postcodeapi_nu',
				'type'     => 'text',
				'css'      => 'min-width:450px;',
				'default'  => '',
				'custom_attributes' => array( 'data-api_source_specific' => 'postcodeapi_nu' ),
			),

			array(
				'title'    => __( 'Enable auto-complete for Germany, Belgium, Luxemburg & Austria', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_enable_autocomplete',
				'type'     => 'checkbox',
				'default'  => 'no',
				'desc'     => version_compare( PHP_VERSION, '7.1', '<' ) ? __( 'requires PHP 7.1 or higher', 'wpo_wcnlpc' ) : '',
				'custom_attributes' => version_compare( PHP_VERSION, '7.1', '<' ) ?
					array( // autocomplete requires PHP 7.2+ 
						'disabled' => 'disabled',
						'data-api_source_specific' => 'postcode_nl'
					) : array(
						'data-api_source_specific' => 'postcode_nl'
					),
			),

			array(
				'title'    => __( 'Layout for postcode & number fields', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_checkout_layout',
				'default'  => 'one_line',
				'type'     => 'radio',
				'desc_tip' =>  __( 'Set your preference for the layout of the postcode checkout fields (postcode, house number, house number suffix)', 'wpo_wcnlpc' ),
				'options'  => array(
					'one_line'           => __( 'All on one line', 'wpo_wcnlpc' ),
					'postcode_separate'  => __( 'Postcode separate', 'wpo_wcnlpc' ),
					'all_separate'       => __( 'Each field on a new line', 'wpo_wcnlpc' ),
				),
			),

			array(
				'title'    => __( 'Show separate street & number fields for:', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_field_visibility',
				'default'  => 'nl',
				'type'     => 'select',
				'class'    => 'wc-enhanced-select',
				'css'      => 'min-width: 350px;',
				'options'  => array(
					'nl'      => __( 'Only the Netherlands', 'wpo_wcnlpc' ),
					'all'     => __( 'All countries', 'wpo_wcnlpc' ),
					'nl_plus' => __( 'The Netherlands plus these additional countries', 'wpo_wcnlpc' ),
				),
			),

			array(
				'title'   => '',
				'desc'    => '',
				'id'      => 'woocommerce_wcnlpc_field_countries',
				'css'     => 'min-width: 350px;',
				'default' => '',
				'type'    => 'multi_select_countries',
			),

			array(
				'title'    => __( 'Street & city fields (NL)', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_checkout_street_city_visibility',
				'default'  => 'show',
				'type'     => 'radio',
				'desc_tip' =>  __( 'Choose how/when you want to show the street & city fields', 'wpo_wcnlpc' ),
				'options'  => array(
					'show'     => __( 'Show (default: always editable)', 'wpo_wcnlpc' ),
					'readonly' => __( 'Read only (editable when postcode not found)', 'wpo_wcnlpc' ),
					'hide'     => __( 'Hidden (editable when postcode not found)', 'wpo_wcnlpc' ),
				),
			),

			array(
				'title'    => __( 'Full field names', 'wpo_wcnlpc' ),
				'desc'     => __( 'Show full names of the Nr. & Suffix fields in the checkout (normally abbreviated to save space)', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_full_field_names',
				'type'     => 'checkbox',
				'default'  => 'no',
				'desc_tip' => true,
			),

			array(
				'title'    => __( 'Show spinner', 'wpo_wcnlpc' ),
				'desc'     => __( 'Show a spinner when address is being validated (only for settings "Show" and "Read only")', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_show_spinner',
				'type'     => 'checkbox',
				'default'  => 'yes',
				'desc_tip' => true,
			),

			array(
				'title'    => __( 'Enable on My Account', 'wpo_wcnlpc' ),
				'id'       => 'woocommerce_wcnlpc_enable_my_account',
				'type'     => 'checkbox',
				'default'  => 'no',
			),

			array( 'type' => 'sectionend', 'id' => 'wcnlpc_api_options' ),
		);

        return apply_filters( 'wc_settings_tab_postcode_checker_settings', $settings );
	}

	public function settings_link ( $links ) {
		$url = admin_url( 'admin.php?page=wc-settings&tab=postcode_checker' );
		$settings_link = sprintf( '<a href="%s">%s</a>', $url, __( 'Settings', 'wpo_wcnlpc' ) );
		array_unshift( $links, $settings_link );
		return $links;
	}

	public function validation_enabled() {
		return WPO_WCNLPC()->validation_enabled();
	}

	public function api_keys_set( $api_source ) {
		return WPO_WCNLPC()->api_keys_set( $api_source );
	}

	public function autocomplete_enabled() {
		if ( get_option( 'woocommerce_wcnlpc_enable_autocomplete', 'no' ) !== 'yes' ) {
			return;
		}
		$api_source = get_option( 'woocommerce_wcnlpc_checkout_api_source', 'kadaster' );
		if ($api_source == 'postcode_nl' && WPO_WCNLPC()->api_keys_set($api_source)) {
			return true;
		} else {
			return false;
		}
	}

}

endif; // class_exists

return new WPO_WCNLPC_Settings();