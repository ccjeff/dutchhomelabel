<?php
namespace WPO\WC\Postcode_Checker\API\Exceptions;

use Exception;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( '\\WPO\\WC\\Postcode_Checker\\API\\Exceptions\\Postcode_Invalid' ) ) :

class Postcode_Invalid extends Exception {
    //
}

endif;