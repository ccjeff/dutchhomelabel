<?php
namespace WPO\WC\Postcode_Checker\API;

use WPO\WC\Postcode_Checker\API\Exceptions\Address_Not_Found;
use WPO\WC\Postcode_Checker\API\Exceptions\Postcode_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Number_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Connection_Error;

// use GuzzleHttp\Client;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( !class_exists( '\\WPO\\WC\\Postcode_Checker\\API\\Postcodeapi_NU' ) ) :

class Postcode_Tech extends Generic {

	public function get_address() {
		$token = get_option( 'woocommerce_wcnlpc_api_token_postcode_tech' );
		$url = "https://postcode.tech/api/v1/postcode?postcode={$this->postcode}&number={$this->housenumber}";

		$headers = array (
			"Authorization" => "Bearer {$token}",
		);
		$response = $this->get_json( $url, $headers );

		if ( !empty($response['street']) && !empty($response['city']) ) {
			$address = array(
				'postcode'				=> $this->postcode,
				'housenumber'			=> $this->housenumber,
				'housenumber_suffix'	=> $this->housenumber_addition,
				'street'				=> $response['street'],
				'city'					=> $response['city'],
				'data'					=> $response,
			);
			return $address;
		} elseif (!empty($response['message'])) {
			if ( stripos( $response['message'], 'no result' ) !== false ) {
				throw new Address_Not_Found( __('Address not found', 'wpo_wcnlpc') );
			} elseif ( stripos( $response['message'], 'unauthorized' ) !== false ) {
				throw new Connection_Error( $response['message'] );
			} else {
				throw new Connection_Error( $response['message'] );
			}
		} else {
			throw new Address_Not_Found( __('Address not found', 'wpo_wcnlpc') );
		}
	}
}

endif; // class_exists
