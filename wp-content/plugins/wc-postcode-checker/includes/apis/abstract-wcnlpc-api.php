<?php
namespace WPO\WC\Postcode_Checker\API;

use WPO\WC\Postcode_Checker\API\Exceptions\Address_Not_Found;
use WPO\WC\Postcode_Checker\API\Exceptions\Postcode_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Number_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Connection_Error;


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( '\\WPO\\WC\\Postcode_Checker\\API\\Generic' ) ) :

abstract class Generic {
	/**
	 * Postcode
	 * @var String
	 */
	public $postcode;

	/**
	 * Housenumber.
	 * @var String
	 */
	public $housenumber;

	/**
	 * Housenumber Addition
	 * @var string
	 */
	public $housenumber_addition;

	function __construct( $postcode = '', $housenumber = '', $housenumber_addition = '' ) {
		require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/exceptions/class-wcnlpc-api-exception-address-not-found.php' );
		require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/exceptions/class-wcnlpc-api-exception-postcode-invalid.php' );
		require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/exceptions/class-wcnlpc-api-exception-number-invalid.php' );
		require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/exceptions/class-wcnlpc-api-exception-connection-error.php' );

		$this->postcode = $this->clean_postcode( $postcode );
		$this->housenumber = $housenumber;
		$this->housenumber_addition = $housenumber_addition;
	}

	public function validate_input() {
		$postcode_check = $this->validate_postcode( $this->postcode );
		if ($postcode_check['valid'] === false ) {
			throw new Postcode_Invalid( $postcode_check['message'] );
		}
		if ( is_numeric($this->housenumber) === false ) {
			$message = __('Housenumber must contain numbers only', 'wpo_wcnlpc');
			throw new Number_Invalid( $message );
		}
	}

	public function get_address() {
		throw new Address_Not_Found( __('Address not found', 'wpo_wcnlpc') );
	}

	public function clean_postcode( $postcode ) {
		$postcode = strtoupper( preg_replace('/[^a-zA-Z0-9]/', '', $postcode) );
		return $postcode;
	}

	public function validate_postcode( $postcode = '' ) {
		if ( empty($postcode) ) {
			$valid = false;
			$message = __('No postcode entered', 'wpo_wcnlpc');
		} elseif ( strlen($postcode) < 6 ) {
			$valid = false;
			$message = __('The specified postcode is too short', 'wpo_wcnlpc');
		} elseif ( strlen($postcode) > 6 ) {
			$valid = false;
			$message = __('The specified postcode is too long', 'wpo_wcnlpc');
		} else {
			$valid = (bool) preg_match( '/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i', trim($postcode) );
			if ( $valid === true ) {
				$message = __( 'Postcode is valid', 'wpo_wcnlpc' );
			} else {
				$message = __('Postcode does not use format `1234AB`', 'wpo_wcnlpc');
			} 
		}

		return compact('valid','message');
	}

	public function get_json( $url, $headers = array(), $auth = false ) {
		if ( apply_filters( 'wpo_wcnlpc_use_curl', false ) ) {
			$response = $this->curl_get_json( $url, $headers, $auth );
		} else {
			$response = $this->wp_remote_get_json( $url, $headers, $auth );
		}
		return $response;
	}


	public function post_json( $url, $data, $headers = array() ) {
		if ( apply_filters( 'wpo_wcnlpc_use_curl', false ) ) {
			$response = $this->curl_post_json( $url, $data, $headers );
		} else {
			$response = $this->wp_remote_post_json( $url, $data, $headers );
		}
		return $response;
	}

	public function is_json( $string ) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	public function wp_remote_post_json( $url, $data, $headers = array() ) {
		$args = array(
			'headers'   => $headers,
			'body'      => json_encode( $data ),
			'timeout'   => 15,
			'sslverify' => true,
		);

		$response = wp_remote_post( $url, $args );
		return $this->wp_remote_process_json_response( $response );
	}

	public function wp_remote_get_json( $url, $headers = array(), $auth = false ) {
		$args = array(
			'headers'   => $headers,
			'timeout'   => 15,
			'sslverify' => true,
		);

		if (!empty($auth)) {
			$args['headers']['Authorization'] = 'Basic ' . base64_encode( $auth['user'] .':'. $auth['pwd'] );
		}
		$response = wp_remote_request( $url, $args );
		return $this->wp_remote_process_json_response( $response );
	}

	public function wp_remote_process_json_response( $response ) {
		if ( is_wp_error( $response ) ) {
			throw new Connection_Error( "Connection error: ".$response->get_error_message() );
		} else {
			$status = wp_remote_retrieve_response_code( $response );
			$header = wp_remote_retrieve_headers( $response );
			$body   = wp_remote_retrieve_body( $response );

			if ($status == 404 && ( empty($body) || !$this->is_json($body) ) ) {
				throw new Connection_Error( "404 page not found" );
			} else {
				return json_decode($body, true);
			}
		}
	}

	public function curl_get_json( $url, $_headers = array(), $auth = false ) {
		// flatten headers array
		$headers = array();
		foreach ($_headers as $key => $header) {
			$headers[] = "{$key}: {$header}";
		}
		$ch = curl_init( $url );

		$curl_options = apply_filters( 'wpo_wcnlpc_curl_options', array(
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_HTTPHEADER		=> $headers,
			// Retrieve HTTP response headers
			CURLOPT_HEADER			=> true,
		), 'GET' );
		curl_setopt_array($ch, $curl_options);

		if (!empty($auth)) {
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_USERPWD, $auth['user'] .':'. $auth['pwd']);
		}

		$response = $this->curl_get_json_response( $ch );

		return $response;
	}

	public function curl_post_json( $url, $data, $headers = array() ) {
		// Postman generated
		// $ch = curl_init();
		// curl_setopt_array($ch, array(
		//   CURLOPT_URL => $url,
		//   CURLOPT_RETURNTRANSFER => true,
		//   CURLOPT_ENCODING => "",
		//   CURLOPT_MAXREDIRS => 10,
		//   CURLOPT_TIMEOUT => 30,
		//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//   CURLOPT_CUSTOMREQUEST => "POST",
		//   CURLOPT_POSTFIELDS => json_encode( $data ),
		//   CURLOPT_HTTPHEADER => $headers,
		//   CURLOPT_HEADER => true,
		// ));
		// $response = curl_exec($ch);
		// flatten headers array
		$headers = array();
		foreach ($_headers as $key => $header) {
			$headers[] = "{$key}: {$header}";
		}

		$ch = curl_init($url);
		$curl_options = apply_filters( 'wpo_wcnlpc_curl_options', array(
			CURLOPT_CUSTOMREQUEST	=> "POST",
			CURLOPT_POSTFIELDS		=> json_encode( $data ) ,
			CURLOPT_RETURNTRANSFER	=> true,
			CURLOPT_HTTPHEADER		=> $headers,
			CURLOPT_HEADER			=> true,
		), 'POST' );
		curl_setopt_array($ch, $curl_options);

		$response = $this->curl_get_json_response( $ch );

		return $response;
	}

	public function curl_get_json_response($ch) {
		$response = curl_exec($ch);
		$info = curl_getinfo($ch);
		$curlError = curl_error($ch);

		if (!empty($curlError)) {
			throw new Connection_Error( "curl error: {$curlError}" );
		}

		$status = $info["http_code"];
		$header = substr($response, 0, $info["header_size"]);
		$body = substr( $response, $info["header_size"]);

		if ($status == 404 && ( empty($body) || !$this->is_json($body) ) ) {
			curl_close($ch);
			throw new Connection_Error( "404 page not found" );
		} else {
			curl_close($ch);
			return json_decode($body, true);
		}
	}
}

endif; // class_exists
