<?php
namespace WPO\WC\Postcode_Checker\API;

use WPO\WC\Postcode_Checker\API\Exceptions\Address_Not_Found;
use WPO\WC\Postcode_Checker\API\Exceptions\Postcode_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Number_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Connection_Error;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( !class_exists( '\\WPO\\WC\\Postcode_Checker\\API\\Postcode_NL' ) ) :

class Postcode_NL extends Generic {

	public function get_address() {
		$key = get_option( 'woocommerce_wcnlpc_api_key' );
		$secret = get_option( 'woocommerce_wcnlpc_api_secret' );

		$url = 'https://api.postcode.nl/rest/addresses/' . urlencode($this->postcode). '/'. urlencode($this->housenumber) . '/'. urlencode($this->housenumber_addition);

		$auth = array(
			'user'	=> $key,
			'pwd'	=> $secret,
		);
		// sleep(4); for debugging
		$response = $this->get_json( $url, array(), $auth );

		/* example data
		{
		    "street": "Julianastraat",
		    "houseNumber": 30,
		    "houseNumberAddition": "",
		    "postcode": "2012ES",
		    "city": "Haarlem",
		    "municipality": "Haarlem",
		    "province": "Noord-Holland",
		    "rdX": 103242,
		    "rdY": 487716,
		    "latitude": 52.37487801,
		    "longitude": 4.62714526,
		    "bagNumberDesignationId": "0392200000029398",
		    "bagAddressableObjectId": "0392010000029398",
		    "addressType": "building",
		    "purposes": [
		        "office"
		    ],
		    "surfaceArea": 643,
		    "houseNumberAdditions": [
		        ""
		    ]
		}
		*/

		if (isset($response['exceptionId'])) {
			$this->throw_error( $response['exceptionId'], $response['exception'] );
		} else {
			$address = array(
				'postcode'				=> $response['postcode'],
				'housenumber'			=> $response['houseNumber'],
				'housenumber_suffix'	=> $response['houseNumberAddition'],
				'street'				=> $response['street'],
				'city'					=> $response['city'],
				'data'					=> $response,
			);
			return $address;
		}
	}

	public function throw_error( $exceptionId, $exception ) {
		// TODO: base on ID instead of message
		switch ($exception) {
			case 'Specified postcode is too short.':
				throw new Postcode_Invalid( __('The specified postcode is too short', 'wpo_wcnlpc') );
				break;
			case 'Postcode does not use format `1234AB`.':
				throw new Postcode_Invalid( __('Postcode does not use format `1234AB`', 'wpo_wcnlpc') );
				break;
			case 'Housenumber must contain numbers only.':
				throw new Number_Invalid( __('Housenumber must contain numbers only', 'wpo_wcnlpc') );
				break;
			case 'Combination does not exist.':
			default:
				throw new Address_Not_Found( __('Combination does not exist', 'wpo_wcnlpc') );
				break;
		}
	}
}

endif; // class_exists
