<?php
namespace WPO\WC\Postcode_Checker\API;

use WPO\WC\Postcode_Checker\API\Exceptions\Address_Not_Found;
use WPO\WC\Postcode_Checker\API\Exceptions\Postcode_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Number_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Connection_Error;

// use GuzzleHttp\Client;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( !class_exists( '\\WPO\\WC\\Postcode_Checker\\API\\Kadaster_NL' ) ) :

class Kadaster_NL extends Generic {

	public function get_address() {
		$api_url = 'https://bagviewer.kadaster.nl/lvbag/bag-viewer/api/';

		$url = $api_url.'advancedSearch';

		// JSON POST body
		$data = array (
			'woonplaats' => '',
			'openbareruimte' => '',
			'postcode' => $this->postcode,
			'huisnummer' => $this->housenumber,
			'huisletter' => '',
			'huisnummertoevoeging' => $this->housenumber_addition,
		);

		$headers = array (
			"Accept"        => "application/json, text/plain, */*",
			"Cache-Control" => "no-cache",
			"Content-Type"  => "application/json;charset=UTF-8",
		);
		$response = $this->post_json( $url, $data, $headers );

		// Error:
		// '{"ExceptionResponse":{"message":"Er ging iets mis op de server. Wacht a.u.b. een moment en probeer het opnieuw."}}'
		if (isset($response['ExceptionResponse'])) {
			throw new Connection_Error( $response['ExceptionResponse']['message'] );
		}

		if (!empty($response['result'])) {
			foreach ($response['result'] as $result) {
				if (!isset( $result['objectId'] )) {
					throw new Address_Not_Found( __('Address not found', 'wpo_wcnlpc') );
				}
				$object_id = $result['objectId'];
				// if this result is historical, we keep looping, otherwise we use this directly
				if (empty($result['historisch'])) {
					break;
				}
			}
			$url = $api_url.'advancedSearch';
			$response = $this->get_json( $api_url . 'bag/details?objectId=' . $object_id );
			if (isset($response['ExceptionResponse'])) {
				throw new Connection_Error( $response['ExceptionResponse']['message'] );
			}
			$address = array(
				'postcode'				=> $this->postcode,
				'housenumber'			=> $this->housenumber,
				'housenumber_suffix'	=> $this->housenumber_addition,
				'street'				=> $response['openbareruimte']['naam'],
				'city'					=> $response['woonplaats']['naam'],
				'data'					=> $response,
			);
			return $address;
		} else {
			throw new Address_Not_Found( __('Address object not found', 'wpo_wcnlpc') );
		}

		/* using guzzle */
		// require_once( WPO_WCNLPC()->plugin_path().'/vendor/autoload.php' );

		// $client = new GuzzleHttp\Client(['base_uri' => $api_url]);

		// // $one = microtime(true);
		// $response = $client->request('POST', 'advancedSearch', [
		// 	'json' => array (
		// 		'woonplaats' => '',
		// 		'openbareruimte' => '',
		// 		'postcode' => $this->postcode,
		// 		'huisnummer' => $this->housenumber,
		// 		'huisletter' => '',
		// 		'huisnummertoevoeging' => $this->housenumber_addition,
		// 	),
		// 	'headers' => array (
		// 		'Accept'		=> 'application/json, text/plain, */*',
		// 		'Content-Type'	=> 'application/json;charset=UTF-8',
		// 		'Cache-Control'	=> 'no-cache',
		// 	)
		// ]);

		// $response = json_decode($response->getBody(), true);
		// // printf("<pre>%s</pre>", var_export($response,true));die();

		// if (isset($response['result'])) {
		// 	foreach ($response['result'] as $result) {
		// 		$response = $client->request('GET', 'bag/details', [
		// 			'query' => ['objectId' => $result['objectId']],
		// 			'headers' => array (
		// 				'Accept'		=> 'application/json, text/plain, */*',
		// 				'Content-Type'	=> 'application/json;charset=UTF-8',
		// 				'Cache-Control'	=> 'no-cache',
		// 			)
		// 		]);
		// 		// $two = microtime(true);
		// 		// echo 'Total Request time: '. ( $two - $one ).'<br>';
		// 		$response = json_decode($response->getBody(), true);
		// 		$address = array(
		// 			'postcode'				=> $this->postcode,
		// 			'housenumber'			=> $this->housenumber,
		// 			'housenumber_suffix'	=> $this->housenumber_addition,
		// 			'street'				=> $response['openbareruimte']['naam'],
		// 			'city'					=> $response['woonplaats']['naam'],
		// 			'data'					=> $response,
		// 		);
		// 		return $address;
		// 	}
		// }
	}
}

endif; // class_exists
