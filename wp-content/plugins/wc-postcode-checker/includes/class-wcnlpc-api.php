<?php
use WPO\WC\Postcode_Checker\API\Postcode_NL;
use WPO\WC\Postcode_Checker\API\Kadaster_NL;
use WPO\WC\Postcode_Checker\API\Postcodeapi_NU;
use WPO\WC\Postcode_Checker\API\Postcode_Tech;
use WPO\WC\Postcode_Checker\API\Exceptions\Address_Not_Found;
use WPO\WC\Postcode_Checker\API\Exceptions\Postcode_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Number_Invalid;
use WPO\WC\Postcode_Checker\API\Exceptions\Connection_Error;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'WPO_WCNLPC_Api' ) ) :

class WPO_WCNLPC_Api {
	
	function __construct()	{
		add_action( 'wp_ajax_wpo_wcnlpc_api_request', array($this, 'request_address' ));
		add_action( 'wp_ajax_nopriv_wpo_wcnlpc_api_request', array($this, 'request_address' ));
	}

	public function request_address () {
		if ( !check_ajax_referer( 'wpo_wcnlpc_api', 'security', false ) ) {
			$this->return_error( 'nonce check failed' );
		}

		// echo '<pre>';var_dump($_POST);echo '</pre>';die();
		extract($_POST);

		require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/abstract-wcnlpc-api.php' );

		$api_source = get_option( 'woocommerce_wcnlpc_checkout_api_source', 'kadaster' );
		switch ($api_source) {
			case 'postcode_nl':
				require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/class-wcnlpc-api-postcode-nl.php' );
				$api = new Postcode_NL( $postcode, $house_number );
				break;
			case 'postcodeapi_nu':
				require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/class-wcnlpc-api-postcodeapi-nu.php' );
				$api = new Postcodeapi_NU( $postcode, $house_number );
				break;
			case 'postcode_tech':
				require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/class-wcnlpc-api-postcode-tech.php' );
				$api = new Postcode_Tech( $postcode, $house_number );
				break;
			default:
			case 'kadaster':
				require_once( WPO_WCNLPC()->plugin_path().'/includes/apis/class-wcnlpc-api-kadaster-nl.php' );
				$api = new Kadaster_NL( $postcode, $house_number );
				break;
		}

		try {
			$api->validate_input();
			$address = $api->get_address();
		} catch ( Address_Not_Found $e ) {
			$this->return_error( $e->getMessage(), 'Address_Not_Found' );
		} catch ( Postcode_Invalid $e ) {
			$this->return_error( $e->getMessage(), 'Postcode_Invalid' );
		} catch ( Number_Invalid $e ) {
			$this->return_error( $e->getMessage(), 'Number_Invalid' );
		} catch ( Connection_Error $e ) {
			$this->return_error( $e->getMessage(), 'Connection_Error' );
		}

		wp_send_json_success( $address );
	}

	public function return_error( $message = '', $error_code = 'undefined' ) {
		wp_send_json_error( compact( 'message', 'error_code' ) );
	}
	

}

endif; // class_exists

return new WPO_WCNLPC_Api();
