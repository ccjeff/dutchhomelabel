<?php
use WPO\WC\Postcode_Checker\Compatibility\WC_Core as WCX;
use WPO\WC\Postcode_Checker\Compatibility\Order as WCX_Order;
use WPO\WC\Postcode_Checker\Compatibility\Product as WCX_Product;

/**
 * Main plugin functions
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'WPO_WCNLPC_Meta_Boxes' ) ) :

class WPO_WCNLPC_Meta_Boxes {
	
	function __construct()	{
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 10, 2 );
		$this->wcnlpc_actions();
	}

	public function add_meta_boxes( $post_type, $post ) {
		if ( $api_data = get_post_meta( $post->ID, '_postcode_api_data', true ) ) {
			add_meta_box(
				'wpo_wcnlpc_api_data',
				__( 'postcode.nl API data', 'wpo_wcnlpc' ),
				array( $this, 'show_api_data'),
				'shop_order',
				'normal',
				'default'
			);
		}
	}

	public function show_api_data ( $order_id ) {
		$order = wc_get_order( $order_id );
		$api_data = WCX_Order::get_meta( $order, '_postcode_api_data' );
		if (empty($api_data)) {
			return;
		}

		$remove_data_url = add_query_arg('wcnlpc_action', 'remove_data');
		printf('<a href="%s" class="button">%s</a>', $remove_data_url, __('Remove data', 'wpo_wcnlpc') );

		echo "<table><tr>";
		foreach ($api_data as $form => $data) {
			switch ($form) {
				case 'billing':
					$title = __('Billing Address', 'wpo_wcnlpc');
					break;
				case 'shipping':
					$title = __('Shipping Address', 'wpo_wcnlpc');
					break;
				default:
					$title = ucfirst($form);
					break;
			}

			echo "<td><h3>{$title}</h3>";
			echo "<table>";
			foreach ($data as $key => $value) {
				if (is_array($value)) {
					$value = implode(', ', $value);
				}
				$title = $key;
				if (empty($value)) {
					$value = '-';
				}
				echo "<tr><th style=\"text-align:left;\">{$title}</th><td>{$value}</td></tr>";
			}
			echo "</table></td>";
		}
		echo "</tr></table>";
	}

	public function wcnlpc_actions() {
		if (!isset($_GET['wcnlpc_action'])) {
			return;
		}

		switch ($_GET['wcnlpc_action']) {
			case 'remove_data':
				$order_id = $_GET['post'];
				$order = wc_get_order($order_id);
				WCX_Order::delete_meta( $order, '_postcode_api_data' );
				break;
		}

		$action_done = remove_query_arg( 'wcnlpc_action' );
		wp_safe_redirect( $action_done );
	}
}

endif; // class_exists

return new WPO_WCNLPC_Meta_Boxes();