<?php
namespace WPO\WC\Postcode_Checker;

use WPO\WC\Postcode_Checker\Compatibility\WC_Core as WCX;
use WPO\WC\Postcode_Checker\Compatibility\Order as WCX_Order;
use WPO\WC\Postcode_Checker\Compatibility\Product as WCX_Product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'WPO\\WC\\Postcode_Checker\\WC_NLPostcode_Fields' ) ) :

class WC_NLPostcode_Fields {

	/**
	 * Construct.
	 */
			
	public function __construct() {
		// Load styles & scripts
		add_action( 'wp_enqueue_scripts', array( &$this, 'add_styles_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_scripts_styles' ) );

		// set later priority for woocommerce_billing_fields / woocommerce_shipping_fields
		// when Checkout Field Editor is active
		if ( function_exists('thwcfd_is_locale_field') || function_exists('wc_checkout_fields_modify_billing_fields') || defined('THWCFD_VERSION') || defined('THWCFE_VERSION') ) {
			$field_priority = 1001;
		} else {
			$field_priority = 9;
		}

		// Add street name & house number checkout fields.
		if ( version_compare( WOOCOMMERCE_VERSION, '2.0' ) >= 0 ) {
			// WC 2.0 or newer is used, the filter got a $coutry parameter, yay!
			add_filter( 'woocommerce_billing_fields', array( &$this, 'nl_billing_fields' ), apply_filters( 'nl_checkout_fields_priority', $field_priority, 'billing' ), 2 );
			add_filter( 'woocommerce_shipping_fields', array( &$this, 'nl_shipping_fields' ), apply_filters( 'nl_checkout_fields_priority', $field_priority, 'shipping' ), 2 );
		} else {
			// Backwards compatibility
			add_filter( 'woocommerce_billing_fields', array( &$this, 'nl_billing_fields' ) );
			add_filter( 'woocommerce_shipping_fields', array( &$this, 'nl_shipping_fields' ) );
		}

		// Hide state field for countries without states (backwards compatible fix for bug #4223)
		if ( version_compare( WOOCOMMERCE_VERSION, '2.2.3', '<' ) ) {
			add_filter( 'woocommerce_countries_allowed_country_states', array( &$this, 'hide_states' ) );
		}

		// Localize checkout fields (limit custom checkout fields to NL)
		$locale_priority = defined('THWCFD_VERSION') || defined('THWCFE_VERSION') ? 1001 : 1;
		add_filter( 'woocommerce_country_locale_field_selectors', array( &$this, 'country_locale_field_selectors' ) );
		add_filter( 'woocommerce_default_address_fields', array( &$this, 'default_address_fields' ) );
		add_filter( 'woocommerce_get_country_locale', array( &$this, 'woocommerce_locale_nl' ), $locale_priority, 1);

		// Load custom order data.
		add_filter( 'woocommerce_load_order_data', array( &$this, 'load_order_data' ) );

		// Custom shop_order details.
		add_filter( 'woocommerce_admin_billing_fields', array( &$this, 'admin_billing_fields' ) );
		add_filter( 'woocommerce_admin_shipping_fields', array( &$this, 'admin_shipping_fields' ) );
		if ( version_compare( WOOCOMMERCE_VERSION, '3.0', '<' ) ) {
			add_filter( 'woocommerce_found_customer_details', array( $this, 'customer_details_ajax_legacy' ) );
		} else {
			add_filter( 'woocommerce_ajax_get_customer_details', array( $this, 'customer_details_ajax' ), 10, 3 );			
		}
		add_action( 'save_post', array( &$this,'save_custom_fields' ) );

		// add to user profile page
		add_filter( 'woocommerce_customer_meta_fields', array( &$this, 'user_profile_fields' ) );

		// Processing checkout
		add_filter( 'woocommerce_validate_postcode', array( &$this, 'validate_postcode' ), 10, 3 );
		add_filter( 'woocommerce_checkout_posted_data', array( &$this, 'merge_street_number_suffix_checkout_posted' ), 9, 1 );

		add_action('woocommerce_checkout_update_order_meta', array( &$this, 'merge_street_number_suffix_checkout' ), 20, 2 );

		add_filter('woocommerce_format_postcode', array( &$this, 'format_postcode' ), 10, 2 );
		add_filter('woocommerce_process_checkout_field_billing_postcode', array( &$this, 'process_postcode' ), 99, 1 );
		add_filter('woocommerce_process_checkout_field_shipping_postcode', array( &$this, 'process_postcode' ), 99, 1 );

		// Save the order data in WooCommerce 2.2 or later.
		if ( version_compare( WOOCOMMERCE_VERSION, '2.2' ) >= 0 ) {
			add_action( 'woocommerce_checkout_update_order_meta', array( &$this, 'save_order_data' ), 10, 2 );
		}

		// Remove placeholder values (IE8 & 9)
		add_action('woocommerce_checkout_update_order_meta', array( &$this, 'remove_placeholders' ), 10, 2 );

		// Compatibility
		add_filter( 'nl_custom_address_field_class', array( &$this, 'theme_compatibility' ), 10, 3 );
		
		// Fix confusing required field translations
		add_filter( 'woocommerce_checkout_required_field_notice', array( &$this, 'required_field_notices' ), 10, 2 );

		// remove private data
		add_action( 'woocommerce_privacy_remove_order_personal_data_meta', array( $this, 'remove_order_personal_data_meta' ), 10, 1 );
		// export private data
		add_action( 'woocommerce_privacy_export_order_personal_data_meta', array( $this, 'export_order_personal_data_meta' ), 10, 1 );

		$this->load_woocommerce_filters();
	}

	public function postcode_field_countries() {
		return apply_filters( 'wpo_wcnlpc_postcode_field_countries', array( 'NL' ) );
	}

	public function load_woocommerce_filters() {
		// Custom address format.
		if ( version_compare( WOOCOMMERCE_VERSION, '2.0.6', '>=' ) ) {
			// add_filter( 'woocommerce_localisation_address_formats', array( $this, 'localisation_address_formats' ), 9 );
			add_filter( 'woocommerce_formatted_address_replacements', array( $this, 'formatted_address_replacements' ), 1, 2 );
			add_filter( 'woocommerce_order_formatted_billing_address', array( $this, 'order_formatted_address' ), 1, 2 );
			add_filter( 'woocommerce_order_formatted_shipping_address', array( $this, 'order_formatted_address' ), 1, 2 );
			add_filter( 'woocommerce_my_account_my_address_formatted_address', array( $this, 'user_address_add_data' ), 1, 3 );

			// Deprecated in WC2.1
			add_filter( 'woocommerce_user_column_billing_address', array( $this, 'user_column_address' ), 1, 2 );
			add_filter( 'woocommerce_user_column_shipping_address', array( $this, 'user_column_address' ), 1, 2 );
		}
		
	}

	/**
	 * Load styles & scripts.
	 */
	public function add_styles_scripts(){
		$is_multiple_address_page = !empty( array_filter( array(
			wc_get_page_id( 'account_addresses' ),
			wc_get_page_id( 'multiple_addresses' ),
		), 'is_page' ) );
		if ( is_checkout() || is_account_page() || $is_multiple_address_page ) {
			if ( version_compare( WOOCOMMERCE_VERSION, '2.1', '<=' ) ) {
				// Backwards compatibility for https://github.com/woothemes/woocommerce/issues/4239
				wp_register_script( 'wcnlpc-nl-checkout', WPO_WCNLPC()->plugin_url() . '/assets/js/nl-checkout.js', array( 'wc-checkout' ) );
				wp_enqueue_script( 'wcnlpc-nl-checkout' );
			}

			if ( is_account_page() ) {
				// Disable regular address fields for NL on account page - Fixed in WC 2.1 but not on init...
				wp_register_script( 'wcnlpc-nl-account-page', WPO_WCNLPC()->plugin_url() . '/assets/js/nl-account-page.js', array( 'jquery' ) );
				wp_enqueue_script( 'wcnlpc-nl-account-page' );
				wp_localize_script(
					'wcnlpc-nl-account-page',
					'wcnlpc_nl_account_page',
					array(
						'postcode_field_countries'	=> apply_filters( 'wpo_wcnlpc_postcode_field_countries', array( 'NL' ) ),
					)
				);
			}

			wp_enqueue_style( 'wcnlpc-nl-checkout', WPO_WCNLPC()->plugin_url() . '/assets/css/nl-checkout.css' );
		}
	}

	/**
	 * Load admin styles & scripts.
	 */
	public function admin_scripts_styles ( $hook ) {
		global $post_type;
		if ( $post_type == 'shop_order' ) {
			wp_enqueue_style(
				'wpo-wcnlpc',
				WPO_WCNLPC()->plugin_url() . '/assets/css/wcnlpc-admin.css'
			);
		}
	}

	/**
	 * Hide default Dutch address fields
	 * @param  array $locale woocommerce country locale field settings
	 * @return array $locale
	 */
	public function woocommerce_locale_nl( $locale ) {
		foreach ($this->postcode_field_countries() as $country_code ) {
			$locale[$country_code]['address_1'] = array(
				'required'  => false,
				'hidden'	=> true,
			);

			$locale[$country_code]['address_2'] = array(
				'hidden'	=> true,
			);
			
			$locale[$country_code]['state'] = array(
				'hidden'	=> true,
				'required'	=> false,
			);

			$locale[$country_code]['street_name'] = array(
				'required'  => true,
				'hidden'	=> false,
			);

			$locale[$country_code]['house_number'] = array(
				'required'  => true,
				'hidden'	=> false,
			);

			$locale[$country_code]['house_number_suffix'] = array(
				'required'  => false,
				'hidden'	=> false,
			);

		}

		return $locale;
	}

	public function nl_billing_fields( $fields, $country = '' ) {
		return $this->nl_checkout_fields( $fields, $country, 'billing');
	}

	public function nl_shipping_fields( $fields, $country = '' ) {
		return $this->nl_checkout_fields( $fields, $country, 'shipping');
	}

	/**
	 * New checkout billing/shipping fields
	 * @param  array $fields Default fields.
	 * @return array $fields New fields.
	 */
	public function nl_checkout_fields( $fields, $country, $form ) {
		if (isset($fields['_country'])) {
			// some weird bug on the my account page
			$form = '';
		}

		// Set required to true if country is NL
		$required = in_array( $country, $this->postcode_field_countries() )?true:false;

		// Add Street name
		$fields[$form.'_street_name'] = array(
			'label'			=> __( 'Street name', 'wpo_wcnlpc' ),
			'placeholder'	=> __( 'Street name', 'wpo_wcnlpc' ),
			'class'			=> apply_filters( 'nl_custom_address_field_class', array( 'form-row-first' ), $form, 'street_name' ),
			'required'		=> $required, // Only required for NL
		);

		// Add house number
		$fields[$form.'_house_number'] = array(
			'label'				=> __( 'Nr.', 'wpo_wcnlpc' ),
			// 'placeholder'	=> __( 'Nr.', 'wpo_wcnlpc' ),
			'class'				=> apply_filters( 'nl_custom_address_field_class', array( 'form-row-quart-first' ), $form, 'house_number' ),
			'required'			=> $required, // Only required for NL
			'type'				=> 'number',
			'custom_attributes'	=> array( 'pattern' => '[0-9]*' ),
		);

		// Add house number Suffix
		$fields[$form.'_house_number_suffix'] = array(
			'label'			=> _x( 'Suffix', 'abbreviated string', 'wpo_wcnlpc' ),
			// 'placeholder'	=> _x( 'Suffix', 'abbreviated string', 'wpo_wcnlpc' ),
			'class'			=> apply_filters( 'nl_custom_address_field_class', array( 'form-row-quart' ), $form, 'house_number_suffix' ),
			'required'		=> false,
		);

		if ( isset($fields[$form.'_address_1']) && isset($fields[$form.'_address_1']['priority']) ) {
			$base_priority = intval( $fields[$form.'_address_1']['priority'] );
			$fields[$form.'_street_name']['priority'] = $base_priority + 1;
			$fields[$form.'_house_number']['priority'] = $base_priority + 2;
			$fields[$form.'_house_number_suffix']['priority'] = $base_priority + 3;
		}


		// put street, number, suffix before _address_1
		$fields = $this->array_move_keys(
			$fields,
			array( $form.'_street_name', $form.'_house_number', $form.'_house_number_suffix' ),
			$form.'_address_1'
		);
			
		return $fields;
	}

	/**
	 * Helper function to move array elements (one or more) to a position before a specific key
	 * @param  array  $array         Main array to modify
	 * @param  mixed  $keys          Single key or array of keys of element(s) to move
	 * @param  string $reference_key key to put elements before or after
	 * @param  string $postion       before or after
	 * @return array                 reordered array
	 */
	public function array_move_keys ( $array, $keys, $reference_key, $position = 'before' ) {
		// cast $key as array
		$keys = (array) $keys;

		if (!isset($array[$reference_key])) {
			return $array;
		}

		$move = array();
		foreach ($keys as $key) {
			if (!isset($array[$key])) {
				continue;
			}
			$move[$key] = $array[$key];
			unset ($array[$key]);
		}

		if ($position == 'before') {
			$move_to_pos = array_search($reference_key, array_keys($array));
		} else { // after
			$move_to_pos = array_search($reference_key, array_keys($array)) + 1;
		}

		$new_array =
			array_slice($array, 0, $move_to_pos, true)
			+ $move
			+ array_slice($array, $move_to_pos, NULL, true);

		return $new_array;
	}

	/**
	 * Hide state field for countries without states (backwards compatible fix for WooCommerce bug #4223)
	 * @param  array $allowed_states states per country
	 * @return array                 
	 */
	public function hide_states($allowed_states) {

		$hidden_states = array(
			'AF' => array(),
			'AT' => array(),
			'AX' => array(),
			'BE' => array(),
			'BH' => array(),
			'BI' => array(),
			'CZ' => array(),
			'DE' => array(),
			'DK' => array(),
			'EE' => array(),
			'FI' => array(),
			'FR' => array(),
			'GP' => array(),
			'GF' => array(),
			'IS' => array(),
			'IL' => array(),
			'IM' => array(),
			'KR' => array(),
			'KW' => array(),
			'LB' => array(),
			'LU' => array(),
			'MQ' => array(),
			'MT' => array(),
			'NL' => array(),
			'NO' => array(),
			'PL' => array(),
			'PT' => array(),
			'RE' => array(),
			'RS' => array(),
			'SG' => array(),
			'SK' => array(),
			'SI' => array(),
			'LK' => array(),
			'SE' => array(),
			'VN' => array(),
			'YT' => array(),
		);
		$states = $hidden_states + $allowed_states;
			
		return $states;
	}

	/**
	 * Localize checkout fields live
	 * @param  array $locale_fields list of fields filtered by locale
	 * @return array $locale_fields with custom fields added
	 */
	public function country_locale_field_selectors( $locale_fields ) {
		$custom_locale_fields = array(
			'street_name'  => '#billing_street_name_field, #shipping_street_name_field',
			'house_number'  => '#billing_house_number_field, #shipping_house_number_field',
			'house_number_suffix'  => '#billing_house_number_suffix_field, #shipping_house_number_suffix_field',
		);

		$locale_fields = array_merge( $locale_fields, $custom_locale_fields );

		return $locale_fields;
	}

	/**
	 * Make NL checkout fields hidden by default
	 * @param  array $fields default checkout fields
	 * @return array $fields default + custom checkoud fields
	 */
	public function default_address_fields( $fields ) {
		$custom_fields = array(
			'street_name' => array(
				'hidden'	=> true,
				'required'	=> false,
				'class'		=> WPO_WCNLPC()->checkout->get_checkout_field_classes( '', 'street_name' ),
			),
			'house_number' => array(
				'hidden'	=> true,
				'required'	=> false,
				'class'		=> WPO_WCNLPC()->checkout->get_checkout_field_classes( '', 'house_number' ),
			),
			'house_number_suffix' => array(
				'hidden'	=> true,
				'required'	=> false,
				'class'		=> WPO_WCNLPC()->checkout->get_checkout_field_classes( '', 'house_number_suffix' ),
			),
		);

		$fields = array_merge( $fields,$custom_fields );

		return $fields;
	}

	/**
	 * Load order custom data.
	 *
	 * @param  array $data Default WC_Order data.
	 * @return array       Custom WC_Order data.
	 */
	public function load_order_data( $data ) {

		// Billing
		$data['billing_street_name']			= '';
		$data['billing_house_number']			= '';
		$data['billing_house_number_suffix']	= '';		

		// Shipping
		$data['shipping_street_name']			= '';
		$data['shipping_house_number']			= '';
		$data['shipping_house_number_suffix']	= '';

		return $data;
	}

	/**
	 * Custom billing admin edit fields.
	 *
	 * @param  array $fields Default WC_Order data.
	 * @return array		 Custom WC_Order data.
	 */
	public function admin_billing_fields( $fields ) {

		$fields['street_name'] = array(
			'label' => __( 'Street name', 'wpo_wcnlpc' ),
			'show'  => true
		);

		$fields['house_number'] = array(
			'label' => __( 'Number', 'wpo_wcnlpc' ),
			'show'  => true
		);

		$fields['house_number_suffix'] = array(
			'label' => _x( 'Suffix', 'abbreviated string', 'wpo_wcnlpc' ),
			'show'  => true
		);

		return $fields;
	}

	/**
	 * Custom shipping admin edit fields.
	 *
	 * @param  array $fields Default WC_Order data.
	 * @return array		 Custom WC_Order data.
	 */
	public function admin_shipping_fields( $fields ) {

		$fields['street_name'] = array(
			'label' => __( 'Street name', 'wpo_wcnlpc' ),
			'show'  => true
		);

		$fields['house_number'] = array(
			'label' => __( 'Number', 'wpo_wcnlpc' ),
			'show'  => true
		);

		$fields['house_number_suffix'] = array(
			'label' => _x( 'Suffix', 'abbreviated string', 'wpo_wcnlpc' ),
			'show'  => true
		);

		return $fields;
	}

	/**
	 * Custom user profile edit fields.
	 */
	public function user_profile_fields ( $meta_fields ) {
		$myparcel_billing_fields = array(
			'billing_street_name' => array(
				'label'       => __( 'Street name', 'wpo_wcnlpc' ),
				'description' => ''
			),
			'billing_house_number' => array(
				'label'       => __( 'Number', 'wpo_wcnlpc' ),
				'description' => ''
			),
			'billing_house_number_suffix' => array(
				'label'       => _x( 'Suffix', 'full string', 'wpo_wcnlpc' ),
				'description' => ''
			),
		);
		$myparcel_shipping_fields = array(
			'shipping_street_name' => array(
				'label'       => __( 'Street name', 'wpo_wcnlpc' ),
				'description' => ''
			),
			'shipping_house_number' => array(
				'label'       => __( 'Number', 'wpo_wcnlpc' ),
				'description' => ''
			),
			'shipping_house_number_suffix' => array(
				'label'       => _x( 'Suffix', 'full string', 'wpo_wcnlpc' ),
				'description' => ''
			),
		);

		// add myparcel fields to billing section
		$billing_fields = array_merge($meta_fields['billing']['fields'], $myparcel_billing_fields);
		$billing_fields = $this->array_move_keys( $billing_fields, array( 'billing_street_name', 'billing_house_number', 'billing_house_number_suffix' ), 'billing_address_2', 'after' );
		$meta_fields['billing']['fields'] = $billing_fields;

		// add myparcel fields to shipping section
		$shipping_fields = array_merge($meta_fields['shipping']['fields'], $myparcel_shipping_fields);
		$shipping_fields = $this->array_move_keys( $shipping_fields, array( 'shipping_street_name', 'shipping_house_number', 'shipping_house_number_suffix' ), 'shipping_address_2', 'after' );
		$meta_fields['shipping']['fields'] = $shipping_fields;
		
		return $meta_fields;
	}

	/**
	 * Add custom fields in customer details ajax.
	 * called when clicking the "Load billing/shipping address" button on Edit Order view
	 *
	 * @return void
	 */
	public function customer_details_ajax_legacy( $customer_data ) {
		$user_id = (int) trim( stripslashes( $_POST['user_id'] ) );
		$type_to_load = esc_attr( trim( stripslashes( $_POST['type_to_load'] ) ) );

		$custom_data = array(
			$type_to_load . '_street_name' => get_user_meta( $user_id, $type_to_load . '_street_name', true ),
			$type_to_load . '_house_number' => get_user_meta( $user_id, $type_to_load . '_house_number', true ),
			$type_to_load . '_house_number_suffix' => get_user_meta( $user_id, $type_to_load . '_house_number_suffix', true ),
		);

		return array_merge( $customer_data, $custom_data );
	}

	public function customer_details_ajax( $customer_data, $customer, $user_id ) {
		$address_types = array( 'billing', 'shipping' );
		$extra_data = array('street_name','house_number','house_number_suffix');
		foreach ($address_types as $type) {
			foreach ($extra_data as $field_name) {
				$customer_data[$type][$field_name] = get_user_meta( $user_id, "{$type}_{$field_name}", true );
			}
		}

		return $customer_data;
	}

	/**
	 * Save custom fields from admin.
	 */
	public function save_custom_fields( $post_id ) {
		$post_type = get_post_type( $post_id );
		if ( ( $post_type == 'shop_order' || $post_type == 'shop_order_refund' ) && !empty($_POST) ) {
			$order = WCX::get_order( $post_id );
			$forms = array( 'billing', 'shipping' );
			$address_fields = array( 'street_name', 'house_number', 'house_number_suffix' );

			$posted = $this->clean_post_data( $_POST );

			foreach ($forms as $form) {
				if ( !empty( $posted["_{$form}_street_name"] ) && !empty( $posted["_{$form}_house_number"] ) ) {
					// concatenate street & house number & copy to 'address_1'
					$house_number = $posted["_{$form}_house_number"] . (!empty($posted["_{$form}_house_number_suffix"])?"-" . $posted["_{$form}_house_number_suffix"]:"");
					$address_1 = $posted["_{$form}_street_name"] . " " . $house_number;
					WCX_Order::set_address_prop( $order, "address_1", $form, $address_1 );
				}
				foreach ($address_fields as $address_field) {
					if (isset($posted["_{$form}_{$address_field}"])) {
						WCX_Order::update_meta_data( $order, "_{$form}_{$address_field}", $posted["_{$form}_{$address_field}"] );
					}
				}
			}
		}
		return;
	}

	/**
	 * Posted checkout data: Merge streetname, street number and street suffix into the default 'address_1' field
	 *
	 * @param  string $order_id Order ID of checkout order.
	 * @return void
	 */
	public function merge_street_number_suffix_checkout_posted ( $data ) {
		$addresses = array( 'billing', 'shipping' );
		foreach ($addresses as $address) {
			if ( !empty( $data["{$address}_street_name"] ) && !empty( $data["{$address}_house_number"] ) ) {
				// concatenate street & house number & copy to 'address_1'
				$house_number = $data["{$address}_house_number"] . (!empty($data["{$address}_house_number_suffix"])?"-" . $data["{$address}_house_number_suffix"]:"");
				$data["{$address}_address_1"] = $data["{$address}_street_name"] . " " . $house_number;
			}
		}
		return $data;
	}

	
	/**
	 * Order data (after checkout): Merge streetname, street number and street suffix into the default 'address_1' field
	 *
	 * @param  string $order_id Order ID of checkout order.
	 * @return void
	 */
	public function merge_street_number_suffix_checkout ( $order_id ) {
		$order = WCX::get_order( $order_id );
		if ( version_compare( WOOCOMMERCE_VERSION, '2.1', '<=' ) ) {
			// old versions use 'shiptobilling'
			$ship_to_different_address = isset($_POST['shiptobilling'])?false:true;
		} else {
			// WC2.1
			$ship_to_different_address = isset($_POST['ship_to_different_address'])?true:false;
		}

		// pre-clean the data we may use
		$posted = $this->clean_post_data( $_POST );

		// check if country is NL
		if ( in_array( $posted['billing_country'], $this->postcode_field_countries() ) ) {
			// concatenate street & house number & copy to 'billing_address_1'
			$billing_house_number = $posted['billing_house_number'] . (!empty($posted['billing_house_number_suffix'])?'-' . $posted['billing_house_number_suffix']:'');
			$billing_address_1 = $posted['billing_street_name'] . ' ' . $billing_house_number;
			WCX_Order::set_address_prop( $order, 'address_1', 'billing', $billing_address_1 );

			// check if 'ship to billing address' is checked
			if ( $ship_to_different_address == false && $this->cart_needs_shipping_address() ) {
				// use billing address
				WCX_Order::set_address_prop( $order, 'address_1', 'shipping', $billing_address_1 );
			}
		}

		if ( in_array( $posted['shipping_country'], $this->postcode_field_countries() ) && $ship_to_different_address == true ) {
			// concatenate street & house number & copy to 'shipping_address_1'
			$shipping_house_number = $posted['shipping_house_number'] . (!empty($posted['shipping_house_number_suffix'])?'-' . $posted['shipping_house_number_suffix']:'');
			$shipping_address_1 = $posted['shipping_street_name'] . ' ' . $shipping_house_number;
			WCX_Order::set_address_prop( $order, 'address_1', 'shipping', $shipping_address_1 );
		}
		return;
	}

	/**
	 * clean & unslash posted data
	 *
	 * @return array $posted
	 */
	public function clean_post_data( $posted ) {
		if (!function_exists('wc_clean')) {
			return $posted;
		}
		foreach ($posted as $key => $value) {
			if ( is_string( $value ) ) {
				$posted[$key] = wc_clean( wp_unslash( $value ) );
			}
		}

		return $posted;
	}

	/**
	 * validate NL postcodes
	 *
	 * @return bool $valid
	 */
	public function validate_postcode( $valid, $postcode, $country ) {
		if ( $country == 'NL' ) {
			$valid = (bool) preg_match( '/^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i', trim($postcode) );
		}
		return $valid;
	}

	/**
	 * Process checkout posted postcodes : format NL postcodes
	 *
	 * @return $billing_postcode / $shipping_postcode
	 */
	public function process_postcode ( $value ) {
		$field_key = str_replace( 'woocommerce_process_checkout_field_', '', current_filter() );
		$form = str_replace( '_postcode', '', $field_key );
		$country = isset($_POST["{$form}_country"]) ? $_POST["{$form}_country"] : '';
		if ( $country == 'NL' ) {
			$postcode = $this->format_postcode( $_POST[$field_key], $country );
			$_POST[$field_key] = $postcode;
		} else {
			$postcode = $value;
		}
		return $postcode;
	}

	/**
	 * Format postcodes : remove space, dashes (& other non alphanumeric characters)
	 * Then insert space at 4th position #### XX
	 *
	 * @return $billing_postcode / $shipping_postcode
	 */
	public function format_postcode ( $postcode, $country ) {
		if ( $country == 'NL' ) {
			// clean first (unifying to ####XX without spaces)
			$postcode = strtoupper( preg_replace('/[^a-zA-Z0-9]/', '', $postcode) );
			// insert space at 4th position when postcode is valid
			$postcode_is_valid = $this->validate_postcode( null, $postcode, 'NL' );
			if ( $postcode_is_valid ) {
				$postcode = substr_replace($postcode, ' ', 4, 0);
			}
		}
		return $postcode;
	}

	/**
	 * Remove placeholders from posted checkout data
	 * @param  string $order_id order_id of the new order
	 * @param  array  $posted   Array of posted form data
	 * @return void
	 */
	public function remove_placeholders( $order_id, $posted ) {
		$order = WCX::get_order( $order_id );
		// get default address fields with their placeholders
		$countries = new \WC_Countries;
		$fields = $countries->get_default_address_fields();

		// define order_comments placeholder
		$order_comments_placeholder = _x('Notes about your order, e.g. special notes for delivery.', 'placeholder', 'woocommerce');

		// check if ship to billing is set
		if ( version_compare( WOOCOMMERCE_VERSION, '2.1', '<=' ) ) {
			// old versions use 'shiptobilling'
			$ship_to_different_address = isset($_POST['shiptobilling'])?false:true;
		} else {
			// WC2.1
			$ship_to_different_address = isset($_POST['ship_to_different_address'])?true:false;
		}

		// check the billing & shipping fields
		$field_types = array('billing','shipping');
		$check_fields = array('address_1','address_2','city','state','postcode');
		foreach ($field_types as $field_type) {
			foreach ($check_fields as $check_field) {
				// file_put_contents(ABSPATH.'field_check.txt', $posted[$field_type.'_'.$check_field] .' || '. $fields[$check_field]['placeholder']."\n",FILE_APPEND);
				if ( isset( $posted[$field_type.'_'.$check_field] ) && isset( $fields[$check_field]['placeholder'] ) && $posted[$field_type.'_'.$check_field] == $fields[$check_field]['placeholder'] ) {
					WCX_Order::set_address_prop( $order, $check_field, $field_type, '' );

					// also clear shipping field when ship_to_different_address is false
					if ( $ship_to_different_address == false && $field_type == 'billing') {
						WCX_Order::set_address_prop( $order, $check_field, 'shipping', '' );
					}
				}
			}
		}

		// check the order comments field		
		if ($posted['order_comments'] == $order_comments_placeholder ) {
			wp_update_post( array(
				'ID'			=> $order_id,
				'post_excerpt'	=> '',
				)
			);
		}
		
		return;
	}

	/**
	 * WooCommerce concatenates translations for required field notices that result in
	 * confusing messages, so we translate the full notice to prevent this
	 */
	function required_field_notices( $notice, $field_label ) {
		// concatenate translations
		$billing_nr = sprintf( __( 'Billing %s', 'woocommerce' ), __( 'Nr.', 'wpo_wcnlpc' ) );
		$shipping_nr = sprintf( __( 'Shipping %s', 'woocommerce' ), __( 'Nr.', 'wpo_wcnlpc' ) );
		// not used:
		// $billing_street = sprintf( __( 'Billing %s', 'woocommerce' ), __( 'Street name', 'wpo_wcnlpc' ) );
		// $shipping_street = sprintf( __( 'Shipping %s', 'woocommerce' ), __( 'Street name', 'wpo_wcnlpc' ) );

		switch ( $field_label ) {
			case $billing_nr:
				$notice = __( '<b>Billing Nr.</b> is a required field', 'wpo_wcnlpc' );
				break;
			case $shipping_nr:
				$notice = __( '<b>Shipping Nr.</b> is a required field', 'wpo_wcnlpc' );
				break;
			default:
				break;
		}
		return $notice;
	}

	/**
	 * Custom country address formats.
	 *
	 * @param  array $formats Defaul formats.
	 *
	 * @return array          New NL format.
	 */
	public function localisation_address_formats( $formats ) {
		// default = $postcode_before_city = "{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
		$formats['NL'] = "{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
		return $formats;
	}

	/**
	 * Custom country address format.
	 *
	 * @param  array $replacements Default replacements.
	 * @param  array $args         Arguments to replace.
	 *
	 * @return array               New replacements.
	 */
	public function formatted_address_replacements( $replacements, $args ) {
		extract( $args );

		if (!empty($street_name)) {
			$replacements['{address_1}'] = $street_name.' '.$house_number.$house_number_suffix;
		}
		
		return $replacements;
	}

	/**
	 * Custom order formatted billing/shipping address.
	 *
	 * @param  array $address Default address.
	 * @param  object $order  Order data.
	 *
	 * @return array          New address format.
	 */
	public function order_formatted_address( $address, $order ) {
		if ( !is_array($address) || empty($address) ) {
			return $address;
		}
		$form = str_replace( array('woocommerce_order_formatted_', '_address'), '', current_filter() );

		$address['street_name']			= WCX_Order::get_meta( $order, "_{$form}_street_name", true, 'view' );
		$address['house_number']		= WCX_Order::get_meta( $order, "_{$form}_house_number", true, 'view' );
		$address['house_number_suffix'] = WCX_Order::get_meta( $order, "_{$form}_house_number_suffix", true, 'view' );
		$address['house_number_suffix']	= !empty($address['house_number_suffix'])?'-'.$address['house_number_suffix']:'';

		return $address;
	}

	/**
	 * Custom user column billing address information.
	 * Deprecated in WC2.1
	 *
	 * @param  array $address Default address.
	 * @param  int $user_id   User id.
	 *
	 * @return array          New address format.
	 */
	public function user_column_address( $address, $user_id ) {
		$form = str_replace( array('woocommerce_user_column_', '_address'), '', current_filter() );

		return $this->user_address_add_data( $address, $user_id, $form );
	}

	/**
	 * Custom my address formatted address.
	 *
	 * @param  array $address   Default address.
	 * @param  int $user_id Customer ID.
	 * @param  string $form     Field name (billing or shipping).
	 *
	 * @return array            New address format.
	 */
	public function user_address_add_data( $address, $user_id, $form ) {
		if ( !is_array($address) || empty($address) ) {
			return $address;
		}
		$address['street_name']			= get_user_meta( $user_id, "{$form}_street_name", true );
		$address['house_number']		= get_user_meta( $user_id, "{$form}_house_number", true );
		$address['house_number_suffix']	= (get_user_meta( $user_id, "{$form}_house_number_suffix", true ))?'-'.get_user_meta( $user_id, "{$form}_house_number_suffix", true ):'';

		return $address;
	}

	/**
	 * Get a posted address field after sanitization and validation.
	 *
	 * @param  string $key
	 * @param  string $type billing for shipping
	 *
	 * @return string
	 */
	public function get_posted_address_data( $key, $posted, $type = 'billing' ) {
		if ( 'billing' === $type || ( !$posted['ship_to_different_address'] && $this->cart_needs_shipping_address() ) ) {
			$return = isset( $posted[ 'billing_' . $key ] ) ? $posted[ 'billing_' . $key ] : '';
		} elseif ( 'shipping' === $type && !$this->cart_needs_shipping_address() ) {
			$return = '';
		} else {
			$return = isset( $posted[ 'shipping_' . $key ] ) ? $posted[ 'shipping_' . $key ] : '';
		}

		return $return;
	}

	public function cart_needs_shipping_address() {
		if ( is_object( WC()->cart ) && method_exists( WC()->cart, 'needs_shipping_address' ) && function_exists('wc_ship_to_billing_address_only') ) {
			if ( WC()->cart->needs_shipping_address() || wc_ship_to_billing_address_only() ) {
				$cart_needs_shipping_address = true;
			} else {
				$cart_needs_shipping_address = false;
			}
		} else {
			$cart_needs_shipping_address = true;
		}

		// Parcel Pro compatibility: if pickup point selected, don't touch the shipping address
		if ( !empty( $_POST['parcelpro_afhaalpunt'] ) ) {
			$cart_needs_shipping_address = false;
		}

		return apply_filters( 'wpo_wcnlpc_cart_needs_shipping_address', $cart_needs_shipping_address );
	}

	/**
	 * Save order data.
	 *
	 * @param  int   $order_id
	 * @param  array $posted
	 *
	 * @return void
	 */
	public function save_order_data( $order_id, $posted ) {
		$order = WCX::get_order( $order_id );
		// Billing.
		WCX_Order::update_meta_data( $order, '_billing_street_name', $this->get_posted_address_data( 'street_name', $posted ) );
		WCX_Order::update_meta_data( $order, '_billing_house_number', $this->get_posted_address_data( 'house_number', $posted ) );
		WCX_Order::update_meta_data( $order, '_billing_house_number_suffix', $this->get_posted_address_data( 'house_number_suffix', $posted ) );

		// Shipping.
		WCX_Order::update_meta_data( $order, '_shipping_street_name', $this->get_posted_address_data( 'street_name', $posted, 'shipping' ) );
		WCX_Order::update_meta_data( $order, '_shipping_house_number', $this->get_posted_address_data( 'house_number', $posted, 'shipping' ) );
		WCX_Order::update_meta_data( $order, '_shipping_house_number_suffix', $this->get_posted_address_data( 'house_number_suffix', $posted, 'shipping' ) );
	}

	/**
	 * Theme compatibility
	 */
	public function theme_compatibility( $class, $form = '', $field = '') {
		// Venedor
		if (defined('venedor_version')) {
			$class = array('form-row-wide address-field input-field');
		}

		return $class;
	}

	/**
	 * Remove all address data when requested
	 */
	public function remove_order_personal_data_meta( $meta_to_remove ) {
		$private_address_meta = array(
			'_billing_street_name'			=> 'none', // 'text' will be replaced with '[deleted]', we want removal
			'_billing_house_number'			=> 'none',
			'_billing_house_number_suffix'	=> 'none',
			'_shipping_street_name'			=> 'none',
			'_shipping_house_number'		=> 'none',
			'_shipping_house_number_suffix'	=> 'none',
		);
		return $meta_to_remove + $private_address_meta;
	}

	/**
	 * Export all address data when requested
	 */
	public function export_order_personal_data_meta( $meta_to_export ) {
		$private_address_meta = array(
			'_billing_street_name'			=> __( 'Billing street name', 'wpo_wcnlpc' ),
			'_billing_house_number'			=> __( 'Billing house number', 'wpo_wcnlpc' ),
			'_billing_house_number_suffix'	=> __( 'Billing house number suffix', 'wpo_wcnlpc' ),
			'_shipping_street_name'			=> __( 'Shipping street name', 'wpo_wcnlpc' ),
			'_shipping_house_number'		=> __( 'Shipping house number', 'wpo_wcnlpc' ),
			'_shipping_house_number_suffix'	=> __( 'Shipping house number suffix', 'wpo_wcnlpc' ),
		);
		return $meta_to_export + $private_address_meta;
	}

}

endif; // class_exists

return new WC_NLPostcode_Fields();
