<?php
use WPO\WC\Postcode_Checker\Compatibility\WC_Core as WCX;
use WPO\WC\Postcode_Checker\Compatibility\Order as WCX_Order;
use WPO\WC\Postcode_Checker\Compatibility\Product as WCX_Product;

/**
 * Main plugin functions
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'WPO_WCNLPC_Checkout' ) ) :

class WPO_WCNLPC_Checkout {
	
	function __construct()	{
		// check if enabled
		if ( WPO_WCNLPC()->validation_enabled() === false ) {
			return;
		}
		add_filter( 'woocommerce_billing_fields', array( $this, 'nl_billing_fields' ), 100, 2 );
		add_filter( 'woocommerce_shipping_fields', array( $this, 'nl_shipping_fields' ), 100, 2 );
		add_filter( 'woocommerce_get_country_locale', array( $this, 'unset_postcode_before_city_nl' ), 10, 1 );

		add_filter( 'wpo_wcnlpc_postcode_field_countries', array( $this, 'enable_fields_for_additional_countries' ) );

		// add street, house number and house number suffix fields unless MyParcel plugin is active
		if ( !$this->myparcel_handles_fields() ) {
			include( 'class-wcnlpc-nlpostcode-fields.php' );
		}

		// MyParcel 3 uses street + number fields for BE as well:
		if ( ( function_exists('WooCommerce_MyParcel') && version_compare( WooCommerce_MyParcel()->version, '3.0', '>=' ) ) || ( defined('WC_POSTNL_VERSION') && version_compare( WC_POSTNL_VERSION, '3.1.0', '>=' ) ) ) {
			add_filter('wpo_wcnlpc_postcode_field_countries', array( $this, 'myparcel_postcode_field_countries' ) );
		}

		// MyParcel limits suffix to 4 characters
		if ( function_exists('WooCommerce_MyParcel') || function_exists('WooCommerce_PostNL')) {
			add_filter( 'woocommerce_billing_fields', array( $this, 'myparcel_suffix_limit' ), 99, 2 );
			add_filter( 'woocommerce_shipping_fields', array( $this, 'myparcel_suffix_limit' ), 99, 2 );
		}

		// Klarna Payments compatibility
		add_filter ( 'wc_kp_remove_postcode_spaces', '__return_true' );

		// add_filter( 'woocommerce_checkout_process', array( $this, 'validate_postcode' ), 10, 1 );
		// remove duplicate street name
		add_filter('woocommerce_process_checkout_field_billing_street_name', array( $this, 'clean_billing_street_duplicates' ) );
		add_filter('woocommerce_process_checkout_field_shipping_street_name', array( $this, 'clean_shipping_street_duplicates' ) );

		// store api response in order
		// add_action( 'woocommerce_checkout_before_customer_details', array( $this, 'checkout_api_response_input' ), 10, 1 );
		// add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'save_postcode_data' ), 10, 2 );
	}

	public function myparcel_suffix_limit( $fields, $country = '' ) {
		$form = str_replace( array('woocommerce_', '_fields'), '', current_filter() );
		if (isset($fields[$form.'_house_number_suffix'])) {
			$fields[$form.'_house_number_suffix']['maxlength'] = 4;
		}
		return $fields;
	}

	public function myparcel_handles_fields() {
		// Check if MyParcel is installed at all
		if ( ! ( $this->is_active_plugin ( 'woocommerce-myparcel/woocommerce-myparcel.php', 'WooCommerce_MyParcel_Export' ) || function_exists('WooCommerce_MyParcel') || function_exists('WooCommerce_PostNL')) ) {
			return false;
		}

		$myparcel_version = function_exists('WooCommerce_MyParcel') ? WooCommerce_MyParcel()->version : '1.0';
		if ( version_compare( $myparcel_version, '3.0.5', '>=' ) || ( defined('WC_POSTNL_VERSION') && version_compare( WC_POSTNL_VERSION, '3.1.0', '>=' ) ) ) {
			// we auto disable the split address fields setting in MyParcel, see main plugin file wc-postcode-checker.php plugins_loaded()
			return false;
		} else {
			// for older versions, we let MyParcel handle the checkout fields
			return true;
		}
	}

	public function postcode_field_countries() {
		return apply_filters( 'wpo_wcnlpc_postcode_field_countries', array( 'NL' ) );
	}

	public function enable_fields_for_additional_countries( $countries ) {
		$show_for = get_option( 'woocommerce_wcnlpc_field_visibility', 'nl' );
		if ( $show_for  == 'nl_plus' ) {
			$additional_countries = get_option( 'woocommerce_wcnlpc_field_countries', array() );
			if ( is_array( $additional_countries ) ) {
				$countries = array_unique( array_merge( array_values( $additional_countries ), array_values( $countries ) ) );
			}
		} elseif ( $show_for == 'all') {
			$countries = array_keys( WC()->countries->countries );
		}

		return $countries;
	}

	public function myparcel_postcode_field_countries( $countries) {
		$countries[] = 'BE';
		return $countries;
	}

	public function nl_billing_fields( $fields, $country = '' ) {
		return $this->nl_checkout_fields( $fields, $country, 'billing');
	}

	public function nl_shipping_fields( $fields, $country = '' ) {
		return $this->nl_checkout_fields( $fields, $country, 'shipping');
	}

	/**
	 * Reorder checkout billing/shipping fields
	 *
	 * Some of this is also done by JS in the checkout,
	 * but this serves as a fallback/preparation mechanism
	 * 
	 * @param  array $fields Default fields.
	 * @return array $fields New fields.
	 */
	public function nl_checkout_fields( $fields, $country, $form ) {
		// disable on My Account page
		if ( is_account_page() && get_option( 'woocommerce_wcnlpc_enable_my_account' ) != 'yes' ) {
			return $fields;
		}

		if (isset($_fields['_country'])) {
			// some weird bug on the my account page
			$form = '';
		}

		// put postcode, number, suffix before _address_1
		$fields = $this->array_move_before_key(
			$fields,
			array( $form.'_postcode', $form.'_house_number', $form.'_house_number_suffix' ),
			$form.'_address_1'
		);

		// put street before city
		$fields = $this->array_move_before_key(
			$fields,
			array( $form.'_street_name' ),
			$form.'_city'
		);

		// set priorities
		if ( isset($fields[$form.'_address_1']) && isset($fields[$form.'_address_1']['priority']) ) {
			$address_1_priority = intval( $fields[$form.'_address_1']['priority'] );
			$fields[$form.'_postcode']['priority'] = $address_1_priority - 3;
			$fields[$form.'_house_number']['priority'] = $address_1_priority - 2;
			$fields[$form.'_house_number_suffix']['priority'] = $address_1_priority - 1;
			$city_priority = intval( $fields[$form.'_city']['priority'] );
			$fields[$form.'_street_name']['priority'] = $city_priority - 1;
		}

		// set field classes / layout
		$fields[$form.'_street_name']['class']         = $this->get_checkout_field_classes( $form, 'street_name' );
		$fields[$form.'_postcode']['class']            = $this->get_checkout_field_classes( $form, 'postcode' );
		$fields[$form.'_house_number']['class']        = $this->get_checkout_field_classes( $form, 'house_number' );
		$fields[$form.'_house_number_suffix']['class'] = $this->get_checkout_field_classes( $form, 'house_number_suffix' );

		if ( get_option( 'woocommerce_wcnlpc_full_field_names', 'no' ) == 'yes' ) {
			$fields[$form.'_house_number']['label']        = __( 'House number', 'wpo_wcnlpc' );
			$fields[$form.'_house_number_suffix']['label'] = _x( 'Suffix', 'full string', 'wpo_wcnlpc' );
		}
		
		$fields[$form.'_house_number_suffix']['autocomplete'] = 'house_number_suffix';

		return $fields;
	}

	public function get_checkout_field_classes( $form, $field, $return = 'array' ) {
		$field_classes = array(
			'default'           => array(
				'street_name'         => array( 'form-row-wide' ),
				'postcode'            => array( 'form-row-first' ),
				'house_number'        => array( 'form-row-quart-first' ),
				'house_number_suffix' => array( 'form-row-quart' ),
			),
			'postcode_separate' => array(
				'postcode'            => array( 'form-row-wide' ),
				'house_number'        => array( 'form-row-first' ),
				'house_number_suffix' => array( 'form-row-last' ),
			),
			'all_separate'      => array(
				'postcode'            => array( 'form-row-wide' ),
				'house_number'        => array( 'form-row-wide' ),
				'house_number_suffix' => array( 'form-row-wide' ),
			),
			'one_line'          => array(
				'postcode'            => array( 'form-row-first' ),
				'house_number'        => array( 'form-row-quart-first' ),
				'house_number_suffix' => array( 'form-row-quart' ),
			),
		);

		$checkout_layout = get_option( 'woocommerce_wcnlpc_checkout_layout', 'default' );
		if ( !empty( $field_classes[$checkout_layout][$field] ) ) {
			$classes = $field_classes[$checkout_layout][$field];
		} elseif ( !empty( $field_classes['default'][$field] ) ) {
			$classes = $field_classes['default'][$field];
		} else {
			$classes = array();
		}

		$classes = apply_filters( 'wpo_wcnlpc_checkout_field_classes', $classes, $form, $field );

		if ( $return == 'string' ) {
			return esc_attr( implode( ' ', $classes ) );
		} else {
			return $classes;
		}
	}

	public function unset_postcode_before_city_nl ( $locale ) {
		foreach ( $this->postcode_field_countries() as $country_code ) {
			if ( isset( $locale[$country_code]['postcode_before_city'] ) ) {
				unset( $locale[$country_code]['postcode_before_city'] );
			}
		}


		return $locale;
	}

	public function clean_billing_street_duplicates ( $street ) {
		return $this->clean_street_duplicates( $street, 'billing' );
	}

	public function clean_shipping_street_duplicates ( $street ) {
		return $this->clean_street_duplicates( $street, 'shipping' );
	}

	public function clean_street_duplicates ( $street, $form ) {
		if (!empty($_POST['postcode_api_data'][$form])) {
			$postcode_data = json_decode(stripslashes_deep($_POST['postcode_api_data'][$form]), true);
			if (!empty($postcode_data['street'])) {
				$street = str_ireplace($postcode_data['street'].$postcode_data['street'], $postcode_data['street'], $street);
			}
		}
		return $street;
	}

	public function checkout_api_response_input () {
		?>
		<input type="hidden" name="postcode_api_data[billing]" id="billing_postcode_api_data" value="">
		<input type="hidden" name="postcode_api_data[shipping]" id="shipping_postcode_api_data" value="">
		<?php
	}

	public function save_postcode_data ( $order_id, $posted ) {
		if (!empty($_POST['postcode_api_data'])) {
			/*
			{"street":"Julianastraat",
			"houseNumber":30,
			"houseNumberAddition":"",
			"postcode":"2012ES",
			"city":"Haarlem",
			"municipality":"Haarlem",
			"province":"Noord-Holland",
			"rdX":103242,
			"rdY":487716,
			"latitude":52.37487801,
			"longitude":4.62714526,
			"bagNumberDesignationId":"0392200000029398",
			"bagAddressableObjectId":"0392010000029398",
			"addressType":"building",
			"purposes":["office"],
			"surfaceArea":643,
			"houseNumberAdditions":[""]}
			*/
			$forms = array( 'billing', 'shipping');
			$postcode_api_data = array();
			foreach ($forms as $form) {
				if (empty($_POST['postcode_api_data'][$form])) {
					continue;
				}
				$posted_data = json_decode(stripslashes_deep($_POST['postcode_api_data'][$form]), true);
				$postcode_api_data[$form] = array (
					"street"					=> $this->get_filtered_array_value( $posted_data, "street" ),
					"houseNumber"				=> $this->get_filtered_array_value( $posted_data, "houseNumber" ),
					"houseNumberAddition"		=> $this->get_filtered_array_value( $posted_data, "houseNumberAddition" ),
					"postcode"					=> $this->get_filtered_array_value( $posted_data, "postcode" ),
					"city"						=> $this->get_filtered_array_value( $posted_data, "city" ),
					"municipality"				=> $this->get_filtered_array_value( $posted_data, "municipality" ),
					"province"					=> $this->get_filtered_array_value( $posted_data, "province" ),
					"rdX"						=> $this->get_filtered_array_value( $posted_data, "rdX" ),
					"rdY"						=> $this->get_filtered_array_value( $posted_data, "rdY" ),
					"latitude"					=> $this->get_filtered_array_value( $posted_data, "latitude" ),
					"longitude"					=> $this->get_filtered_array_value( $posted_data, "longitude" ),
					"bagNumberDesignationId"	=> $this->get_filtered_array_value( $posted_data, "bagNumberDesignationId" ),
					"bagAddressableObjectId"	=> $this->get_filtered_array_value( $posted_data, "bagAddressableObjectId" ),
					"addressType"				=> $this->get_filtered_array_value( $posted_data, "addressType" ),
					"purposes"					=> $this->get_filtered_array_value( $posted_data, "purposes" ),
					"surfaceArea"				=> $this->get_filtered_array_value( $posted_data, "surfaceArea" ),
					"houseNumberAdditions"		=> $this->get_filtered_array_value( $posted_data, "houseNumberAdditions" ),
				);
			}

			$order = WCX::get_order( $order_id );
			WCX_Order::update_meta_data( $order, '_postcode_api_data', $postcode_api_data );
		}
	}

	/**
	 * Helper function to move array elements (one or more) to a position before a specific key
	 * @param  array  $array      Main array to modify
	 * @param  mixed  $keys       Single key or array of keys of element(s) to move
	 * @param  atring $before_key key to put elements in front of
	 * @return array              reordered array
	 */
	public function array_move_before_key ( $array, $keys, $before_key ) {
		// cast $key as array
		$keys = (array) $keys;

		if (!isset($array[$before_key])) {
			return $array;
		}

		$move = array();
		foreach ($keys as $key) {
			if (!isset($array[$key])) {
				continue;
			}
			$move[$key] = $array[$key];
			unset ($array[$key]);
		}

		$before_key_pos = array_search($before_key, array_keys($array));
		$new_array =
			array_slice($array, 0, $before_key_pos, true)
			+ $move
			+ array_slice($array, $before_key_pos, NULL, true);

		return $new_array;
	}

	public function get_filtered_array_value($array, $key) {
		if (isset($array[$key])) {
			$value = $array[$key];
			if (is_string($value)) {
				$value = esc_html( $value );
			} elseif (is_array($value)) {
				$value = array_filter( $value, 'esc_html' );
			}
			return $value;
		} else {
			return '';
		}
	}

	public function is_active_plugin ( $plugin_slug, $plugin_class = '' ) {
		$blog_plugins = get_option( 'active_plugins', array() );
		$site_plugins = get_site_option( 'active_sitewide_plugins', array() ); // Multisite

		if ( in_array( $plugin_slug, $blog_plugins ) || isset( $site_plugins[$plugin_slug] ) || ( !empty($plugin_class) && class_exists( $plugin_class ) ) ) {
			return true;
		} else {
			return false;
		}
	}
}

endif; // class_exists

return new WPO_WCNLPC_Checkout();
