<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'WPO_WCNLPC_Assets' ) ) :

class WPO_WCNLPC_Assets {
	
	function __construct()	{
		add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts_styles' ), 10, 1 );
	}

	/**
	 * Load styles & scripts
	 */
	public function load_scripts_styles ( $hook ) {
		$is_multiple_address_page = !empty( array_filter( array(
			wc_get_page_id( 'account_addresses' ),
			wc_get_page_id( 'multiple_addresses' ),
		), 'is_page' ) );

		if ( ( is_checkout() || ( get_option( 'woocommerce_wcnlpc_enable_my_account' ) == 'yes' && is_account_page() ) || $is_multiple_address_page ) && WPO_WCNLPC()->validation_enabled() ) {
			wp_enqueue_script(
				'wpo-wcnlpc',
				WPO_WCNLPC()->plugin_url() . '/assets/js/wc-postcode-checker.js',
				array( 'jquery', 'wc-address-i18n' ),
				WPO_WCNLPC_VERSION
			);

			$street_city_visibility = get_option( 'woocommerce_wcnlpc_checkout_street_city_visibility', 'show' );
			wp_localize_script(
				'wpo-wcnlpc',
				'wpo_wcnlpc',
				array(
					'ajaxurl'                   => admin_url( 'admin-ajax.php' ), // URL to WordPress ajax handling page
					'nonce'                     => wp_create_nonce('wpo_wcnlpc_api'),
					'layout'                    => get_option( 'woocommerce_wcnlpc_checkout_layout', 'one_line' ),
					'myparcel'                  => !class_exists( 'WooCommerce_MyParcel_Export' ) ? true : false,
					'street_city_visibility'    => $street_city_visibility,
					// 'manual'                 => sprintf(' <input type="checkbox" title="%1$s" class="wpo_wcnlpc_manual" value="" class="checkbox"> <label for="wpo_wcnlpc_manual">%1$s</label>', __('Enter address manually', 'wpo_wcnlpc') ),
					'manual'                    => apply_filters( 'woocommerce_postcode_checker_manual_text', __("We couldn't find your combination of postcode and house number. If it does exist, please fill in your street name and city below.", 'wpo_wcnlpc') ),
					'spinner'                   => WPO_WCNLPC()->plugin_url() . '/assets/img/spinner.gif',
					'xhr_timeout'               => apply_filters( 'woocommerce_postcode_checker_xhr_timeout', 8000 ),
					'postcode_field_countries'  => apply_filters( 'wpo_wcnlpc_postcode_field_countries', array( 'NL' ) ),
					'field_classes'             => array(
						'billing' => array(
							'street_name'         => WPO_WCNLPC()->checkout->get_checkout_field_classes( 'billing', 'street_name', 'string' ),
							'postcode'            => WPO_WCNLPC()->checkout->get_checkout_field_classes( 'billing', 'postcode', 'string' ),
							'house_number'        => WPO_WCNLPC()->checkout->get_checkout_field_classes( 'billing', 'house_number', 'string' ),
							'house_number_suffix' => WPO_WCNLPC()->checkout->get_checkout_field_classes( 'billing', 'house_number_suffix', 'string' ),
						),
						'shipping' => array(
							'street_name'         => WPO_WCNLPC()->checkout->get_checkout_field_classes( 'shipping', 'street_name', 'string' ),
							'postcode'            => WPO_WCNLPC()->checkout->get_checkout_field_classes( 'shipping', 'postcode', 'string' ),
							'house_number'        => WPO_WCNLPC()->checkout->get_checkout_field_classes( 'shipping', 'house_number', 'string' ),
							'house_number_suffix' => WPO_WCNLPC()->checkout->get_checkout_field_classes( 'shipping', 'house_number_suffix', 'string' ),
						),
					),
				)
			);

			wp_enqueue_style(
				'wpo-wcnlpc',
				WPO_WCNLPC()->plugin_url() . '/assets/css/wc-postcode-checker.css',
				array(), // deps
				WPO_WCNLPC_VERSION
			);

			$show_spinner = get_option( 'woocommerce_wcnlpc_show_spinner', 'yes' );
			if ($show_spinner == 'yes') {
				wp_add_inline_style( 'wpo-wcnlpc', ".wcnlpc_spinner {
					background-image: url(".WPO_WCNLPC()->plugin_url().'/assets/img/spinner.gif'.") !important;
					background-position: 95% 50% !important;
					background-repeat: no-repeat !important;
				}" );
			}
		}
	}

	public function admin_scripts_styles( $hook ) {
		if ( isset($_GET['page']) && $_GET['page'] == 'wc-settings' && isset($_GET['tab']) && $_GET['tab'] == 'postcode_checker' ) {
			wp_enqueue_script(
				'wpo-wcnlpc-settings',
				WPO_WCNLPC()->plugin_url() . '/assets/js/admin-settings.js',
				array( 'jquery' ),
				WPO_WCNLPC_VERSION
			);

			$img_place = '';
			$images = array(
				'one_line'           => WPO_WCNLPC()->plugin_url() . '/images/one_line.png',
				'postcode_separate'  => WPO_WCNLPC()->plugin_url() . '/images/postcode_separate.png',
				'all_separate'       => WPO_WCNLPC()->plugin_url() . '/images/separate_lines.png',

			);
			foreach ($images as $option_value => $img_url) {
				$img_place .= sprintf("\t$('input[name=woocommerce_wcnlpc_checkout_layout][value=%s]').after('<img src=\"%s\">');\n", $option_value, $img_url);
			}
			wp_add_inline_script( 'wpo-wcnlpc-settings', "jQuery( function( $ ) {\n".$img_place."});" );
		}
	}
}

endif; // class_exists

return new WPO_WCNLPC_Assets();