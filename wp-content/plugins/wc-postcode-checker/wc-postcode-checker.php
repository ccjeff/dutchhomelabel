<?php
/**
 * Plugin Name: WooCommerce NL Postcode Checker
 * Plugin URI: http://www.wpovernight.com
 * Description: Automatically validate dutch postcodes and fill in town and street name
 * Version: 2.5.2
 * Author: Ewout Fernhout
 * Author URI: http://www.wpovernight.com
 * License: GPLv2 or later
 * License URI: http://www.opensource.org/licenses/gpl-license.php
 * Text Domain: wpo_wcnlpc
 * Domain Path: /languages
 * WC requires at least: 2.2.0
 * WC tested up to: 4.6.0
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'WPO_WC_Postcode_Checker' ) ) :

class WPO_WC_Postcode_Checker {

	public $version = '2.5.2';

	protected static $_instance = null;

	protected $autocomplete = null;

	protected $settings = null;

	public $checkout;

	/**
	 * Main Plugin Instance
	 *
	 * Ensures only one instance of plugin is loaded or can be loaded.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->define( 'WPO_WCNLPC_VERSION', $this->version );
		$this->define( 'WPO_WCNLPC_BASENAME', plugin_basename( __FILE__ ) );

		// load the localisation & classes
		add_action( 'plugins_loaded', array( $this, 'translations' ) );
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
		add_action( 'init', array( $this, 'load_classes' ) );

		// Load the updater
		add_action( 'init', array( $this, 'load_updater' ), 0 );

		// run lifecycle methods
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
			add_action( 'wp_loaded', array( $this, 'do_install' ) );
		}
	}

	/**
	 * Define constant if not already set
	 * @param  string $name
	 * @param  string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Run the updater scripts from the WPO Sidekick
	 * @return void
	 */
	public function load_updater() {
		// Init updater data
		$item_name		= 'WooCommerce Postcode Checker';
		$file			= __FILE__;
		$license_slug	= 'wpo_wcnlpc_license';
		$version		= WPO_WCNLPC_VERSION;
		$author			= 'Ewout Fernhout';

		// Check if sidekick is loaded
		if (class_exists('WPO_Updater')) {
			$this->updater = new WPO_Updater( $item_name, $file, $license_slug, $version, $author );
		}
	}

	/**
	 * Load the translation / textdomain files
	 * 
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present
	 */
	public function translations() {
		$locale = apply_filters( 'plugin_locale', get_locale(), 'wpo_wcnlpc' );
		$dir    = trailingslashit( WP_LANG_DIR );

		/**
		 * Frontend/global Locale. Looks in:
		 *
		 * 		- WP_LANG_DIR/wc-postcode-checker/wpo_wcnlpc-LOCALE.mo
		 * 	 	- WP_LANG_DIR/plugins/wpo_wcnlpc-LOCALE.mo
		 * 	 	- wc-postcode-checker/languages/wpo_wcnlpc-LOCALE.mo (which if not found falls back to:)
		 * 	 	- WP_LANG_DIR/plugins/wpo_wcnlpc-LOCALE.mo
		 */
		load_textdomain( 'wpo_wcnlpc', $dir . 'wc-postcode-checker/wpo_wcnlpc-' . $locale . '.mo' );
		load_textdomain( 'wpo_wcnlpc', $dir . 'plugins/wpo_wcnlpc-' . $locale . '.mo' );
		load_plugin_textdomain( 'wpo_wcnlpc', false, dirname( plugin_basename(__FILE__) ) . '/languages' );
	}


	/**
	 * Perform any actions we may want to do as soon as possible
	 */
	public function plugins_loaded() {
		if ( version_compare( PHP_VERSION, '7.1', '>=' ) && extension_loaded('curl') && $this->is_postcodenl_activated() === false ) {
			$this->autoload_postcodenl_autocomplete();
			if ( $this->settings()->autocomplete_enabled() ) {
				$this->autocomplete();
			}
		}

		// If validation is not enabled, we disable the MyParcel & PostNL checkout setting to avoid unnecessary complexity
		$this->maybe_disable_myparcel_field_setting();
	}

	public function maybe_disable_myparcel_field_setting() {
		if ( $this->validation_enabled() === false ) {
			return;
		}

		if ( ( function_exists( 'WCMYPA' ) || function_exists('WooCommerce_MyParcel') || function_exists('WooCommerce_PostNL') ) && version_compare( PHP_VERSION, '5.3', '>=' ) ) {
			// strip option from get_option()
			$checkout_settings_filter = function_exists('WooCommerce_MyParcel') ? 'option_woocommerce_myparcel_checkout_settings' : 'option_woocommerce_postnl_checkout_settings';
			add_filter($checkout_settings_filter, function( $value ) {
				if (is_array($value)) {
					if (function_exists('WCMYPA')) { // 4.0
						$value['use_split_address_fields'] = 0;
					} else {
						unset($value['use_split_address_fields']);
					}
				}
				return $value;
			});
			// hide the option to avoid confusion
			add_action('admin_enqueue_scripts', function(){
				if ( function_exists( 'WCMYPA' ) ) {
					wp_add_inline_script( 'wcmp-admin', 'jQuery(function($) { $("#use_split_address_fields").closest("tr").hide(); });' );
				} else {
					wp_add_inline_style( 'wcmp-admin-styles', '.use_split_address_fields { display: none !important; }' );
				}
			}, 99999 );
		}
	}

	/**
	 * Auto-load in-accessible properties on demand.
	 *
	 * @param mixed $key Key name.
	 * @return mixed
	 */
	public function __get( $key ) {
		if ( in_array( $key, array( 'settings', 'autocomplete' ), true ) ) {
			return $this->$key();
		}
	}

	public function settings() {
		if (empty($this->settings)) {
			$this->settings = include_once( 'includes/class-wcnlpc-settings.php' );
		}
		return $this->settings;
	}

	public function autocomplete() {
		if (empty($this->autocomplete)) {
			$className = '\PostcodeNl\AddressAutocomplete\Main';
			$this->autocomplete = new $className();
		}
		return $this->autocomplete;
	}

	/**
	 * Load the main plugin classes and functions
	 */
	public function includes() {
		// include compatibility classes
		include_once( 'includes/compatibility/abstract-wc-data-compatibility.php' );
		include_once( 'includes/compatibility/class-wc-date-compatibility.php' );
		include_once( 'includes/compatibility/class-wc-core-compatibility.php' );
		include_once( 'includes/compatibility/class-wc-order-compatibility.php' );
		include_once( 'includes/compatibility/class-wc-product-compatibility.php' );

		include_once( 'includes/class-wcnlpc-api.php' );
		include_once( 'includes/class-wcnlpc-meta-boxes.php' );
		$this->checkout = include_once( 'includes/class-wcnlpc-checkout.php' );
		include_once( 'includes/class-wcnlpc-assets.php' );
	}

	public function autoload_postcodenl_autocomplete() {
		spl_autoload_register(static function(string $className) {
			if (strpos($className, 'PostcodeNl\\InternationalAutocomplete\\') === 0) {
				/** @noinspection PhpIncludeInspection */
				require_once plugin_dir_path(__FILE__) . 'libraries/' . str_replace('\\', '/', $className) . '.php';
				return;
			}
			if (strpos($className, 'PostcodeNl\\AddressAutocomplete\\') === 0) {
				/** @noinspection PhpIncludeInspection */
				$baseClassName = str_replace('PostcodeNl\\AddressAutocomplete\\', '', $className );
				require_once plugin_dir_path(__FILE__) . 'includes/autocomplete/' . str_replace('\\', '/', $baseClassName) . '.php';
				return;
			}
		});
	}

	/**
	 * Instantiate classes when woocommerce is activated
	 */
	public function load_classes() {
		if ( $this->is_woocommerce_activated() === false ) {
			add_action( 'admin_notices', array ( $this, 'need_woocommerce' ) );
			return;
		}

		if ( version_compare( PHP_VERSION, '5.3', '<' ) ) {
			add_action( 'admin_notices', array ( $this, 'required_php_version' ) );
			return;
		}

		// all systems ready - GO!
		$this->includes();
	}

	/**
	 * Check if woocommerce is activated
	 */
	public function is_woocommerce_activated() {
		$blog_plugins = get_option( 'active_plugins', array() );
		$site_plugins = get_site_option( 'active_sitewide_plugins', array() );

		if ( in_array( 'woocommerce/woocommerce.php', $blog_plugins ) || isset( $site_plugins['woocommerce/woocommerce.php'] ) ) {
			return true;
		} else {
			return false;
		}
	}

	public function is_postcodenl_activated() {
		if ( class_exists( '\\PostcodeNl\\AddressAutocomplete\\Main' ) ) {
			return true;
		}
		$blog_plugins = get_option( 'active_plugins', array() );
		foreach ($blog_plugins as $plugin) {
			if ( strpos($plugin, 'postcodenl-address-autocomplete.php') !== false ) {
				return true;
			}
		}
		$site_plugins = array_keys( get_site_option( 'active_sitewide_plugins', array() ) );
		foreach ($site_plugins as $plugin) {
			if ( strpos($plugin, 'postcodenl-address-autocomplete.php') !== false ) {
				return true;
			}
		}
		// prevent PostcodeNl from crashing during activation
		if ( isset($_GET['action']) && $_GET['action'] == 'activate' && isset($_GET['plugin']) && strpos($_GET['plugin'], 'postcodenl-address-autocomplete.php') !== false ) {
			return true;
		}
		return false;
	}

	public function validation_enabled() {
		$enabled = get_option( 'woocommerce_wcnlpc_enable' );
		if ($enabled != 'yes') {
			return false;
		}

		// enabled = set, now check api keys
		$api_source = get_option( 'woocommerce_wcnlpc_checkout_api_source', 'kadaster' );
		return $this->api_keys_set( $api_source );
	}

	public function api_keys_set( $api_source ) {
		if (empty($api_source)) {
			$api_source = get_option( 'woocommerce_wcnlpc_checkout_api_source', 'kadaster' );
		}

		switch ($api_source) {
			case 'postcode_nl':
				$key = get_option( 'woocommerce_wcnlpc_api_key' );
				$secret = get_option( 'woocommerce_wcnlpc_api_secret' );
				return !empty($key) && !empty($secret);
				break;
			case 'postcodeapi_nu':
				$key = get_option( 'woocommerce_wcnlpc_api_key_postcodeapi_nu' );
				return !empty($key);
				break;
			default:
			case 'kadaster':
				return true; // no key or secret required
				break;
		}
	}
	
	/**
	 * WooCommerce not active notice.
	 */
	 
	public function need_woocommerce() {
		$error = sprintf( __( 'WooCommerce NL Postcode Checker requires %sWooCommerce%s to be installed & activated!' , 'wpo_wcnlpc' ), '<a href="http://wordpress.org/extend/plugins/woocommerce/">', '</a>' );

		$message = '<div class="error"><p>' . $error . '</p></div>';
	
		echo $message;
	}

	/**
	 * PHP version requirement notice
	 */
	
	public function required_php_version() {
		$error = __( 'WooCommerce NL Postcode Checker requires PHP 5.3 or higher (5.6 or higher recommended).', 'wpo_wcnlpc' );
		$how_to_update = __( 'How to update your PHP version', 'wpo_wcnlpc' );
		$message = sprintf('<div class="error"><p>%s</p><p><a href="%s">%s</a></p></div>', $error, 'http://docs.wpovernight.com/general/how-to-update-your-php-version/', $how_to_update);
	
		echo $message;
	}


	/** Lifecycle methods *******************************************************
	 * Because register_activation_hook only runs when the plugin is manually
	 * activated by the user, we're checking the current version against the
	 * version stored in the database
	****************************************************************************/

	/**
	 * Handles version checking
	 */
	public function do_install() {
		$version_setting = 'wpo_wcnlpc_version';
		$installed_version = get_option( $version_setting );

		// installed version lower than plugin version?
		if ( version_compare( $installed_version, $this->version, '<' ) ) {

			if ( ! $installed_version ) {
				$this->install();
			} else {
				$this->upgrade( $installed_version );
			}

			// new version number
			update_option( $version_setting, $this->version );
		}
	}


	/**
	 * Plugin install method. Perform any installation tasks here
	 */
	protected function install() {
		// stub
	}

	/**
	 * Plugin upgrade method.  Perform any required upgrades here
	 *
	 * @param string $installed_version the currently installed ('old') version
	 */
	protected function upgrade( $installed_version ) {
		if ( version_compare( $installed_version, '2.0', '<' ) ) {
			update_option( 'woocommerce_wcnlpc_checkout_api_source', 'postcode_nl' );
			$key = get_option( 'woocommerce_wcnlpc_api_key' );
			$secret = get_option( 'woocommerce_wcnlpc_api_secret' );
			if ( !empty($key) && !empty($secret) ) {
				update_option( 'woocommerce_wcnlpc_enable', 'yes' );
			}
		}
	}		

	/**
	 * Get the plugin url.
	 * @return string
	 */
	public function plugin_url() {
		return untrailingslashit( plugins_url( '/', __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 * @return string
	 */
	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( __FILE__ ) );
	}

} // class WPO_WC_Postcode_Checker

endif; // class_exists

/**
 * Returns the main instance of WC to prevent the need to use globals.
 *
 * @since  2.1
 * @return WooCommerce
 */
function WPO_WCNLPC() {
	return WPO_WC_Postcode_Checker::instance();
}

WPO_WCNLPC(); // load plugin

/**
 * WPOvernight updater admin notice
 */
if ( ! class_exists( 'WPO_Updater' ) && ! function_exists( 'wpo_updater_notice' ) ) {

	if ( ! empty( $_GET['hide_wpo_updater_notice'] ) ) {
		update_option( 'wpo_updater_notice', 'hide' );
	}

	/**
	 * Display a notice if the "WP Overnight Sidekick" plugin hasn't been installed.
	 * @return void
	 */
	function wpo_updater_notice() {
		$wpo_updater_notice = get_option( 'wpo_updater_notice' );

		$blog_plugins = get_option( 'active_plugins', array() );
		$site_plugins = get_site_option( 'active_sitewide_plugins', array() );
		$plugin = 'wpovernight-sidekick/wpovernight-sidekick.php';

		if ( in_array( $plugin, $blog_plugins ) || isset( $site_plugins[$plugin] ) || $wpo_updater_notice == 'hide' ) {
			return;
		}

		echo '<div class="updated fade"><p>Install the <strong>WP Overnight Sidekick</strong> plugin to receive updates for your WP Overnight plugins - check your order confirmation email for more information. <a href="'.add_query_arg( 'hide_wpo_updater_notice', 'true' ).'">Hide this notice</a></p></div>' . "\n";
	}

	add_action( 'admin_notices', 'wpo_updater_notice' );
}
