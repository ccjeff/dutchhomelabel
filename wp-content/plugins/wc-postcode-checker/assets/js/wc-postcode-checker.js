jQuery( function( $ ) {
	var xhr = null;

	// MyParcel 3.0.5+ compatibility
	if ( typeof wc_myparcel_frontend !== "undefined" && typeof wc_myparcel_frontend.isUsingSplitAddressFields !== "undefined" ) {
		window.myparcel_is_using_split_address_fields = wc_myparcel_frontend.isUsingSplitAddressFields = true;
	}
	// MyParcel 3.0.9+ compatibility
	if ( typeof wcmp_display_settings !== "undefined" && typeof wcmp_display_settings.isUsingSplitAddressFields !== "undefined" ) {
		window.myparcel_is_using_split_address_fields = wcmp_display_settings.isUsingSplitAddressFields = true;
		window.postnl_is_using_split_address_fields = true;
	}

	// provide country fallback if field was removed
	$.each(['billing','shipping'], function(index, form) {
		if ( $('#'+form+'_country').length == 0 && $('.woocommerce-'+form+'-fields').length > 0 ) {
			$('.woocommerce-'+form+'-fields').append('<input id="'+form+'_country" class="country_to_state" value="NL" type="hidden">');
		}
	});

	$( document.body )
		// Handle locale
		.bind( 'country_to_state_changing', function( event, country, wrapper ) {
			var thisform = wrapper;
			if ( $.inArray( country, wpo_wcnlpc.postcode_field_countries ) !== -1 ) {
				if ( country == 'NL' ) {
					// hide & disable street & city according to settings
					if (wpo_wcnlpc.street_city_visibility == 'hide' || wpo_wcnlpc.street_city_visibility == 'readonly' ) {
						thisform.find('#billing_street_name_field, #shipping_street_name_field, #billing_city_field, #shipping_city_field')
							.find('input')
								.attr('readonly', true);
					}
					if (wpo_wcnlpc.street_city_visibility == 'hide') {
						thisform.find('#billing_street_name_field, #shipping_street_name_field, #billing_city_field, #shipping_city_field')
							.hide();
					}
				} else {
					 // Postcode fields are used outside NL (wpo_wcnlpc.postcode_field_countries)
					 // but don't get validated and thus shouldn't be blocked.
					thisform.find('#billing_street_name_field, #shipping_street_name_field, #billing_city_field, #shipping_city_field')
						.show()
						.find('input')
							.attr('readonly', false);

					// remove postcode checker fields!
					thisform.find('.wcnlpc-error, .wcnlpc-address, .wcnlpc-manual').remove();
				}

				var $postcodefield = thisform.find('#billing_postcode_field, #shipping_postcode_field');
				var $cityfield     = thisform.find('#billing_city_field, #shipping_city_field');
				var $statefield    = thisform.find('#billing_state_field, #shipping_state_field');
				var $emailfield    = thisform.find('#billing_email_field');
				var $phonefield    = thisform.find('#billing_phone_field');
				var $address1field = thisform.find('#billing_address_1_field, #shipping_address_1_field');
				var $address2field = thisform.find('#billing_address_2_field, #shipping_address_2_field');
				var $streetfield   = thisform.find('#billing_street_name_field, #shipping_street_name_field');
				var $numberfield   = thisform.find('#billing_house_number_field, #shipping_house_number_field');
				var $suffixfield   = thisform.find('#billing_house_number_suffix_field, #shipping_house_number_suffix_field');


				// determine if this is the billing or shipping form
				if ( thisform.find('#shipping_postcode_field').length ) {
					var form_name = 'shipping';
				} else {
					var form_name = 'billing';
				}

				// make sure we have the correct layout class (can be overriden the via wpo_wcnlpc_checkout_field_classes filter in php)
				$postcodefield.removeClass( 'form-row-first form-row-last' ).addClass( wpo_wcnlpc['field_classes'][form_name]['postcode'] );
				$numberfield.removeClass( 'form-row-first form-row-last form-row-quart-first form-row-quart' ).addClass( wpo_wcnlpc['field_classes'][form_name]['house_number'] );
				$suffixfield.removeClass( 'form-row-first form-row-last form-row-quart-first form-row-quart' ).addClass( wpo_wcnlpc['field_classes'][form_name]['house_number_suffix'] );

				// fix layout for order notes
				$('#order_comments_field').addClass( 'form-row-wide' );
				
				// moving postcode field (again) to override woocommerce i18n postcode_before_city js
				$postcodefield.insertBefore( $address1field );

				// below is a fallback, should be already set like this under normal circumstances
				$numberfield.insertAfter( $postcodefield );
				$suffixfield.insertAfter( $numberfield );
				$streetfield.insertBefore( $cityfield );
				// WC3.0 reorders using the priority attribute, so we set street to one less than the city field 
				// data-priority & data('priority') are ambiguously used by WooCommerce Checkout Field editor, we pick the minimum value...
				if ($cityfield.data('priority')) {
					var city_prio =  Math.min( $cityfield.data('priority'), $cityfield.attr('data-priority') );
					$streetfield.data('priority',city_prio-1).attr("data-priority", city_prio-1);
				}
				// number & suffix come after postcode
				if ($address1field.data('priority')) {
					var address1_prio = $address1field.data('priority');
					// var postcode_prio = Math.min( $postcodefield.data('priority'), $postcodefield.attr('data-priority') );
					$postcodefield.data('priority',address1_prio-3).attr("data-priority", address1_prio-3);
					$numberfield.data('priority',address1_prio-2).attr("data-priority", address1_prio-2);
					$suffixfield.data('priority',address1_prio-1).attr("data-priority", address1_prio-1);
				}

				$streetfield.removeClass( 'form-row-first form-row-last' ).addClass( wpo_wcnlpc['field_classes'][form_name]['street_name'] );
			} else {
				// make sure city is shown and not read only
				thisform.find('#billing_city_field, #shipping_city_field')
					.show()
					.find('input')
						.attr('readonly', false);

				// remove postcode checker fields!
				thisform.find('.wcnlpc-error, .wcnlpc-address, .wcnlpc-manual').remove();
			}
		})

		// Init trigger
		.bind('init_checkout', function() {
			$('#billing_country, #shipping_country, .country_to_state').change();
		})

		// Make sure shipping addresses are correct when enabling
		.on( 'change', '#ship-to-different-address input', function() {
			if ( $( this ).is( ':checked' ) ) {
				$('#shipping_country').change();
			}
		});

		$('#billing_country, #shipping_country, .country_to_state').change();

		$('#billing_postcode_field input, #shipping_postcode_field input, #billing_house_number_field input, #shipping_house_number_field input, #billing_house_number_suffix_field input, #shipping_house_number_suffix_field input')
			.bind('change', getPostcode);

		// Check on form init
		getPostcode( 'billing' );
		// getPostcode( 'shipping' );

		function getPostcode( object ){
			// console.log(this);
			// MyParcel Delivery Options compatibility
			window.myparcel_checkout_updating = true; // prevent MyParcel from fetching data before we're finished

			if (typeof object === 'string') {
				var form = object;
				var form_prefix = form+'_';
				var form_parent = $( '#'+form_prefix+'postcode' ).closest( '.form-row' ).parent();
			} else {
				if (this.id.indexOf("billing") > -1) {
					var form 		= "billing";
					var form_prefix = "billing_";
				} else {
					var form 		= "shipping";
					var form_prefix = "shipping_";
				}
				var form_parent		= $( this ).closest( '.form-row' ).parent();
			}

			var country = $( '#'+form_prefix+'country' ).val();
			if ( country != 'NL' ) {
				return;
			};

			// temporarily disable fields when visibility set to 'Show'
			if (wpo_wcnlpc.street_city_visibility == 'show' || wpo_wcnlpc.street_city_visibility == 'readonly' ) {
				block_fields( '#'+form_prefix+'street_name, #'+form_prefix+'city' );
			}

			var postcode            = $( '#'+form_prefix+'postcode' ).val();
			var house_number        = $( '#'+form_prefix+'house_number' ).val();
			var house_number_suffix = $( '#'+form_prefix+'house_number_suffix' ).val();

			// skip if disabled
			if ( $( form_parent ).hasClass('wpo_wcnlpc_disabled') ) {
				// set postcode & house number to validated
				$( form_parent ).find('#billing_postcode_field, #shipping_postcode_field, #billing_house_number_field, #shipping_house_number_field')
					.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' )
					.addClass( 'woocommerce-validated' );
					unblock_fields( '#'+form_prefix+'street_name, #'+form_prefix+'city' );
				return;
			}

			if ( typeof postcode == 'undefined' || typeof house_number == 'undefined' || ! (postcode.length > 0 && house_number.length > 0) ) {
				unblock_fields( '#'+form_prefix+'street_name, #'+form_prefix+'city' );
				return;
			};

			if ( $( form_parent ).find( '.wcnlpc-error' ).length < 1 ) {
				$( '#'+form_prefix+'postcode' ).closest( '.form-row' ).before( '<div class="wcnlpc-error" style="color: red;"></div>' );
			}

			var data = {
				security:            wpo_wcnlpc.nonce,
				postcode:            postcode,
				house_number:        house_number,
				house_number_suffix: house_number_suffix,
			};

			if ( typeof wpo_wcnlpc.last_data != 'undefined' &&  JSON.stringify(wpo_wcnlpc.last_data) === JSON.stringify(data) ) {
				// we have already sent this exact request: unblock street & city fields (+remove spinner)
				if (wpo_wcnlpc.street_city_visibility == 'show' || wpo_wcnlpc.street_city_visibility == 'readonly' ) {
					unblock_fields( '#'+form_prefix+'street_name, #'+form_prefix+'city' );
				}
				return;
			} else {
				wpo_wcnlpc.last_data = data;
			}
			

			xhr = $.ajax({
				type:		'POST',
				url:		wpo_wcnlpc.ajaxurl+'?action=wpo_wcnlpc_api_request',
				data:		data,
				dataType:	'json',
				timeout:	wpo_wcnlpc.xhr_timeout, // timeout, 8000ms by default 
				beforeSend : function()    {           
					if(xhr != null) {
						xhr.abort();
					}
				},
				success:	function( response ) {
					// we have a result: unblock street & city fields (+remove spinner)
					if (wpo_wcnlpc.street_city_visibility == 'show' || wpo_wcnlpc.street_city_visibility == 'readonly' ) {
						unblock_fields( '#'+form_prefix+'street_name, #'+form_prefix+'city' );
					}
					// disable manual override
					if (wpo_wcnlpc.street_city_visibility == 'hide'  || wpo_wcnlpc.street_city_visibility == 'readonly' ) {
						disable_fields(form_prefix);
					}


					if (!response) {
						return;  // nonce check failed
					}

					// skip if disabled meanwhile
					if ( $( form_parent ).hasClass('wpo_wcnlpc_disabled') ) {
						// set postcode & house number to validated
						$( form_parent ).find('#billing_postcode_field, #shipping_postcode_field, #billing_house_number_field, #shipping_house_number_field')
							.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' )
							.addClass( 'woocommerce-validated' );
						return;
					}

					// console.log(response);
					// store response for later use/backup/debug in order
					// $( '#'+form_prefix+'postcode_api_data' ).val(JSON.stringify(response));

					if ( typeof response.success != 'undefined' && response.success === true ) {
						// remove error and set fields to validated
						$( form_parent ).find( '.wcnlpc-error, .wcnlpc-manual' ).remove();
						$( '#'+form_prefix+'postcode_field, #'+form_prefix+'house_number_field' )
							.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' )
							.addClass( 'woocommerce-validated' );

						$( '#'+form_prefix+'street_name' ).val(response.data.street);
						// trigger change event for validation
						$( '#'+form_prefix+'street_name' ).keydown().change(); // WC doesn't respond to the change event if there was no keydown
						$( '#'+form_prefix+'city' ).val(response.data.city);
						$( '#'+form_prefix+'city' ).keydown().change();

						// backwards compatibility - fill address_1 field
						var address1 = response.data.street+' '+response.data.housenumber;
						if ( house_number_suffix ) {
							var address1 = address1+'-'+house_number_suffix;
						}
						$( '#'+form_prefix+'address_1' ).val(address1);

						// write resulting address if fields hidden
						if (wpo_wcnlpc.street_city_visibility == 'hide') {
							if ( $( form_parent ).find( '.wcnlpc-address' ).length < 1 ) {
								$( '#'+form_prefix+'street_name' ).closest( '.form-row' ).before( '<p class="wcnlpc-address" style="clear:both;"></p>' );
							}
							$( form_parent ).find('.wcnlpc-address').html( address1+', '+response.data.city );
						}

						// MyParcel Delivery Options compatibility
						trigger_myparcel(form_prefix);
						
					} else if ( typeof response.success != 'undefined' && response.success === false ) {
						// console.log(data);
						$( form_parent ).find('.wcnlpc-address').remove();
						$( form_parent ).find('.wcnlpc-error').html('');
						switch(response.data.error_code) {
							case 'Postcode_Invalid':
								$( '#'+form_prefix+'postcode_field' ).removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
								$( '#'+form_prefix+'postcode_field' ).parent().find( '.wcnlpc-error' ).html( response.data.message );
								break;
							case 'Number_Invalid':
								$( '#'+form_prefix+'house_number_field' ).removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
								$( '#'+form_prefix+'postcode_field' ).parent().find( '.wcnlpc-error' ).html( response.data.message );
								break;
							case 'Address_Not_Found':
								$( '#'+form_prefix+'postcode_field' ).removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
								$( '#'+form_prefix+'house_number_field' ).removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
								// $( '#'+form_prefix+'postcode_field' ).parent().find( '.wcnlpc-error' ).html( response.data.message );
								break;
							case 'Connection_Error':
							case 'undefined':
								// No API response. Enable manual but don't mark fields red (we don't know if it's wrong)
								$('#'+form_prefix+'postcode_field, #'+form_prefix+'house_number_field')
									.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' )
									.addClass( 'woocommerce-validated' );
								break;
							default:
								$( '#'+form_prefix+'postcode_field' ).removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
								$( '#'+form_prefix+'house_number_field' ).removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
								$( '#'+form_prefix+'postcode_field' ).parent().find( '.wcnlpc-error' ).html( response.data.message );
								break;
						}

						// MyParcel Delivery Options compatibility
						trigger_myparcel(form_prefix);
						
						// Allow manual override
						if ( response.data.error_code != 'Postcode_Invalid' && response.data.error_code != 'Number_Invalid') {
							if ( $( form_parent ).find( '.wcnlpc-manual' ).length < 1 ) {
								$( '#'+form_prefix+'street_name' ).closest( '.form-row' ).before( '<p class="wcnlpc-manual" style="clear:both"></p>' );
							}
							$( form_parent ).find('.wcnlpc-manual').html( wpo_wcnlpc.manual );
							enable_fields(form_prefix);
						} else {
							$( form_parent ).find( '.wcnlpc-manual' ).remove();
							if (wpo_wcnlpc.street_city_visibility == 'hide'  || wpo_wcnlpc.street_city_visibility == 'readonly' ) {
								disable_fields(form_prefix);
							}
						}
						
					} else {
						// unknown error
						$( form_parent ).find('.wcnlpc-address').remove();
						$( form_parent ).find('.wcnlpc-error').html('');
						$('#'+form_prefix+'postcode_field, #'+form_prefix+'house_number_field')
							.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' )
							.addClass( 'woocommerce-validated' );
						// $( form_row ).removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
						// $( form_parent ).find( '.wcnlpc-error' ).html( 'Ongeldige postcode / huisnummer combinatie' );

					}
					// allow other scripts to hook in
					$('body').trigger('wpo_wcnlpc_fields_updated');
				},
				error:		function( jqXHR, textStatus, errorThrown ) {
					// no result (timeout or error) but still renable street & city fields
					if (wpo_wcnlpc.street_city_visibility == 'show' || wpo_wcnlpc.street_city_visibility == 'readonly' ) {
						unblock_fields( '#'+form_prefix+'street_name, #'+form_prefix+'city' );
					}
					// MyParcel Delivery Options compatibility
					trigger_myparcel(form_prefix);
						
					// enable manual override
					if (wpo_wcnlpc.street_city_visibility == 'hide'  || wpo_wcnlpc.street_city_visibility == 'readonly' ) {
						enable_fields(form_prefix);
					}
				}
			});
		};

		function postcode_error( element ) {

		}

		// block checkout fields
		function block_fields( selectors ) {
			// selectors = selectors.join(',');
			if ( wpo_wcnlpc.street_city_visibility == 'show' ) {
				$( selectors ).attr('readonly', true);
			}
			$( selectors ).addClass('wcnlpc_spinner');
		}
		function unblock_fields( selectors ) {
			// selectors = selectors.join(',');
			if ( wpo_wcnlpc.street_city_visibility == 'show' ) {
				$( selectors ).attr('readonly', false);
			}
			$( selectors ).removeClass('wcnlpc_spinner');
		}

		// Enable manual address entry
		function enable_fields( form_prefix ) {
			// disable postcode checker
			// thisform.addClass('wpo_wcnlpc_disabled');

			// show street name & city fields
			$('#'+form_prefix+'street_name_field, #'+form_prefix+'city_field')
				.show()
				.find('input')
					.val('')
					.attr('readonly', false);
			
			// set postcode & house number to validated
			// $('#'+form_prefix+'postcode_field, #'+form_prefix+'house_number_field')
			// 	.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field' )
			// 	.addClass( 'woocommerce-validated' );
		}

		// Disable manual address entry
		function disable_fields( form_prefix ) {
			// enable postcode checker
			// thisform.removeClass('wpo_wcnlpc_disabled');

			// show street name & city fields
			$('#'+form_prefix+'street_name_field, #'+form_prefix+'city_field')
				.find('input')
					.attr('readonly', true);
			
			if (wpo_wcnlpc.street_city_visibility == 'hide') {
				$('#'+form_prefix+'street_name_field, #'+form_prefix+'city_field')
					.hide();
			}
		}

		function trigger_myparcel(form_prefix) {
			window.myparcel_checkout_updating = false; // update done
			if ( typeof window.update_myparcel_settings !== "undefined" ) {
				window.update_myparcel_settings();
			} else if ( ( typeof wc_myparcel_frontend !== "undefined" && typeof wc_myparcel_frontend.isUsingSplitAddressFields !== "undefined" )
				||  ( typeof wcmp_display_settings !== "undefined" && typeof wcmp_display_settings.isUsingSplitAddressFields !== "undefined" ) ) {
				// MyParcel 3.0.5+
				$( '#'+form_prefix+'address_1' ).trigger("input");
			}
		}
});
