jQuery( function( $ ) {
	$( '#woocommerce_wcnlpc_checkout_api_source' ).change( function (e) {
		var api_source = $(this).val();
		$('input[data-api_source_specific]').each( function(){
			if (typeof api_source != 'undefined' && $(this).data('api_source_specific') == api_source ) {
				$(this).closest('tr').show();
			} else {
				$(this).closest('tr').hide();
			}
		});
	}).change();

	$( 'select#woocommerce_wcnlpc_field_visibility' ).change( function (e) {
		if ( $(this).val() == 'nl_plus' ) {
			$( this ).closest('tr').next().show();
		} else {
			$( this ).closest('tr').next().hide();
		}
	}).change();
});