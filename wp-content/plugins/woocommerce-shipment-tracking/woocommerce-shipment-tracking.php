<?php
/*
	Plugin Name: WooCommerce Shipment Tracking Pro
	Plugin URI: https://www.xadapter.com/product/woocommerce-shipment-tracking-pro/
	Description: Features includes Custom Tracking Message, Multiple Shipping Services including 70+ predefined services, WPML Support and CSV bulk import of tracking data & FTP import.
	Version: 2.2.12
	Author: WooForce
	Author URI: https://www.xadapter.com/vendor/wooforce/
	Copyright: WooForce
        Text Domain: woocommerce-shipment-tracking
	WC requires at least: 2.6.0
	WC tested up to: 3.3
*/


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Plugin activation check woocommerce_shipment_tracking (wst)
 */
if( !function_exists('wf_wst_basic_pre_activation_check') )
{
	function wf_wst_basic_pre_activation_check(){
		//check if basic version is there
		if ( is_plugin_active('woo-shipment-tracking-order-tracking/woocommerce-shipment-tracking.php') ){
			deactivate_plugins( basename( __FILE__ ) );
			wp_die( __("Oops! You tried installing the premium version without deactivating and deleting the basic version. Kindly deactivate and delete WooCommerce Shipment Tracking Basic Woocommerce Extension and then try again.", "woocommerce-shipment-tracking" ), "", array('back_link' => 1 ));
		}
	}
}
register_activation_hook( __FILE__, 'wf_wst_basic_pre_activation_check' );

define( "WF_SHIPMENT_TRACKING_IMP_EXP_ID", "wf_shipment_tracking_csv_im_ex" );
define( "WF_SHIPMENT_TRACKING_CSV_IM_EX", "import_shipment_tracking_csv" );
/**
 * Check if WooCommerce is active
 */
if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && !class_exists('WooCommerce_Shipment_Tracking') ) {

	/**
	 * WooCommerce_Shipment_Tracking class
	 */
	class WooCommerce_Shipment_Tracking {

                public $cron_shipment_import;
		/**
		 * Constructor
		 */
		public function __construct() {
                        define( 'WF_ShipmentTracking_FILE', __FILE__ );
			add_action( 'init', array( $this, 'init' ) );
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'wf_plugin_action_links' ) );
                        add_action( 'init', array( $this, 'catch_save_settings' ), 20 );
                        
                        
                        
                        require_once( 'includes/class-wf-shipment-tracking-import-cron.php' );
                        $this->cron_shipment_import = new WF_ShipmentTracking_ImportCron();
                          //$this->cron_shipment_import->wf_scheduled_import_shipment_tracking();
                        register_activation_hook(__FILE__, array( $this->cron_shipment_import, 'wf_shipment_tracking_new_scheduled_import'));
                        register_deactivation_hook( __FILE__, array( $this->cron_shipment_import, 'clear_wf_shipment_tracking_scheduled_import' ) );
                        
		}

		public function init() {
			if ( ! class_exists( 'wf_order' ) ) {
				include_once 'includes/class-wf-legacy.php';
			}
			include_once ( 'includes/class-wf-tracking-admin.php' );
			include_once ( 'includes/class-wf-tracking-settings.php' );
			include_once ( 'includes/class-wf-shipment-tracking-setup.php' );
                        include_once( 'includes/class-wf-shipment-tracking-admin-screen.php' );
                        
			
			if ( is_admin() ) {
				include_once ( 'includes/wf_api_manager/wf-api-manager-config.php' );
			}
			// Localisation
			load_plugin_textdomain( 'woocommerce-shipment-tracking', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Plugin page links
		 */
		public function wf_plugin_action_links( $links ) {
			$plugin_links = array(
				'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=wf_tracking' ) . '">' . __( 'Settings', 'woocommerce-shipment-tracking' ) . '</a>',
				'<a href="' . admin_url( 'admin.php?import=import_shipment_tracking_csv' ) . '">' . __( 'Import', 'woocommerce-shipment-tracking' ) . '</a>',
				'<a href="https://www.xadapter.com/category/product/woocommerce-shipment-tracking-pro/" target="_blank">' . __('Documentation', 'woocommerce-shipment-tracking') . '</a>',
                '<a href="https://www.xadapter.com/online-support/" target="_blank">' . __('Support', 'woocommerce-shipment-tracking') . '</a>'
			);
			return array_merge( $plugin_links, $links );
		}
		
		static function wf_plugin_path() {
			return untrailingslashit( plugin_dir_path( __FILE__ ) );
		}
		
		static function wf_plugin_url() {
			return untrailingslashit( plugins_url( '/', __FILE__ ) );
		}
                
                public function catch_save_settings() {
			if ( ! empty( $_GET['action'] ) && ! empty( $_GET['page'] ) && $_GET['page'] == 'import_shipment_tracking_csv' ) {
				switch ( $_GET['action'] ) {
					case "settings" :
						include_once( 'includes/settings/class-wf-shipment-tracking-settings.php' );
						WF_Shipment_Tracking_Settings::save_settings( );
					break;
				}
			}
		}
	}
	


	new WooCommerce_Shipment_Tracking();
}
