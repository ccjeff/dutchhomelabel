<?php

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

class WF_ShipmentTracking_ImportCron {

    public $settings;
    public $file_url;
    public $error_message;

    public function __construct() {
        add_filter('cron_schedules', array($this, 'wf_shipment_tracking_auto_import_schedule'));
        add_action('init', array($this, 'wf_shipment_tracking_new_scheduled_import'));
        add_action('wf_shipment_tracking_csv_im_ex_auto_import', array($this, 'wf_scheduled_import_shipment_tracking'));
        $this->settings = get_option('woocommerce_' . WF_SHIPMENT_TRACKING_IMP_EXP_ID . '_settings', null);
        $this->settings_ftp_import = get_option('wf_shipment_tracking_importer_ftp', null);
        $this->imports_enabled = FALSE;
        if (isset($this->settings['shipment_tracking_auto_import']) && $this->settings['shipment_tracking_auto_import'] === 'Enabled')
            $this->imports_enabled = TRUE;
    }

    public function wf_shipment_tracking_auto_import_schedule($schedules) {
        if ($this->imports_enabled) {
            $import_interval = $this->settings['shipment_tracking_auto_import_interval'];
            if ($import_interval) {
                $schedules['import_interval'] = array(
                    'interval' => (int) $import_interval * 60,
                    'display' => sprintf(__('Every %d minutes', 'woocommerce-shipment-tracking'), (int) $import_interval)
                );
            }
        }
        return $schedules;
    }

    public function wf_shipment_tracking_new_scheduled_import() {
        if ($this->imports_enabled) {
            if (!wp_next_scheduled('wf_shipment_tracking_csv_im_ex_auto_import')) {
                $start_time = $this->settings['shipment_tracking_auto_import_start_time'];
                $current_time = current_time('timestamp');
                if ($start_time) {
                    if ($current_time > strtotime('today ' . $start_time, $current_time)) {
                        $start_timestamp = strtotime('tomorrow ' . $start_time, $current_time) - ( get_option('gmt_offset') * HOUR_IN_SECONDS );
                    } else {
                        $start_timestamp = strtotime('today ' . $start_time, $current_time) - ( get_option('gmt_offset') * HOUR_IN_SECONDS );
                    }
                } else {
                    $import_interval = $this->settings['shipment_tracking_auto_import_interval'];
                    $start_timestamp = strtotime("now +{$import_interval} minutes");
                }
                wp_schedule_event($start_timestamp, 'import_interval', 'wf_shipment_tracking_csv_im_ex_auto_import');
            }
        }
    }

    public static function load_wp_importer() {
        // Load Importer API
        require_once ABSPATH . 'wp-admin/includes/import.php';

        if (!class_exists('WP_Importer')) {
            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            if (file_exists($class_wp_importer)) {
                require $class_wp_importer;
            }
        }
    }

    public function wf_scheduled_import_shipment_tracking() {

        //error_log("test run by wp-cron" , 3 , ABSPATH . '/wp-content/uploads/wc-logs/my-cron-log.txt');
        define('WP_LOAD_IMPORTERS', true);
        if (!class_exists('WooCommerce')) :
            require ABSPATH . 'wp-content/plugins/woocommerce/woocommerce.php';
        endif;

        WF_ShipmentTracking_ImportCron::shipment_tracking_cron_importer();
        $GLOBALS['WF_Shipment_Tracking_Importer']->skip_allready_processed = !empty($this->settings['shipment_tracking_auto_import_merge']) ? $this->settings['shipment_tracking_auto_import_merge'] : 1;

        $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('Starting auto import....', 'woocommerce-shipment-tracking'));
        if ($this->handle_ftp_for_autoimport()) {

            $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('Inside FTP', 'woocommerce-shipment-tracking'));
            $GLOBALS['WF_Shipment_Tracking_Importer']->import($this->file_url);
            $GLOBALS['WF_Shipment_Tracking_Importer']->import_end();

            die();
        } else {
            $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('Fetching file failed. Reason:' . $this->error_message, 'woocommerce-shipment-tracking'));
        }
    }

    public function clear_wf_shipment_tracking_scheduled_import() {
        wp_clear_scheduled_hook('wf_shipment_tracking_csv_im_ex_auto_import');
    }

    private function handle_ftp_for_autoimport() {


        
        $enable_ftp_ie = $this->settings_ftp_import['enable_ftp_ie'];
        $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('FTP status'.$enable_ftp_ie, 'woocommerce-shipment-tracking'));
        if (!$enable_ftp_ie)
            return false;
        $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('FTP started', 'woocommerce-shipment-tracking'));

        $ftp_server = $this->settings_ftp_import['ftp_server'];
        $ftp_user = $this->settings_ftp_import['ftp_user'];
        $ftp_password = $this->settings_ftp_import['ftp_password'];
        $use_ftps = $this->settings_ftp_import['use_ftps'];
        $ftp_server_path = $this->settings_ftp_import['ftp_server_path'];


        $local_file = 'wp-content/uploads/temp-import.csv';
        $server_file = $ftp_server_path;


        $ftp_conn = $use_ftps ? ftp_ssl_connect($ftp_server) : ftp_connect($ftp_server);
        $this->error_message = "";
        $success = false;
        if ($ftp_conn == false) {
            $this->error_message = "There is connection problem\n";
            $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('Connection:'.$this->error_message, 'woocommerce-shipment-tracking'));
        }

        if (empty($this->error_message)) {
            if (ftp_login($ftp_conn, $ftp_user, $ftp_password) == false) {
                $this->error_message = "Not able to login \n";
                $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('Login:'.$this->error_message, 'woocommerce-shipment-tracking'));
            }
        }
        if (empty($this->error_message)) {

            ftp_pasv($ftp_conn, true);
            if (ftp_get($ftp_conn, ABSPATH . $local_file, $server_file, FTP_BINARY)) {
                $this->error_message = "";
                $success = true;
            } else {
                $this->error_message = "There was a problem\n";
                $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('FTP get:'.$this->error_message, 'woocommerce-shipment-tracking'));
            }
        }

        ftp_close($ftp_conn);
        if ($success) {
            $this->file_url = ABSPATH . $local_file;
        } else {
            $GLOBALS['WF_Shipment_Tracking_Importer']->log->add('shipment-tracking-import', __('Error:'.$this->error_message, 'woocommerce-shipment-tracking'));
            die($this->error_message);
        }
        return true;
    }

    public static function shipment_tracking_cron_importer() {
        if (!defined('WP_LOAD_IMPORTERS')) {
            return;
        }
        self::load_wp_importer();

        // includes
        require_once 'class-wf-shipment-tracking-importer.php';

        if (!class_exists('WC_Logger')) {
            $class_wc_logger = ABSPATH . 'wp-content/plugins/woocommerce/includes/class-wc-logger.php';
            if (file_exists($class_wc_logger)) {
                require $class_wc_logger;
            }
        }

        $class_wc_logger = ABSPATH . 'wp-includes/pluggable.php';
        require_once($class_wc_logger);
        wp_set_current_user(1); // escape user access check while running cron

        $GLOBALS['WF_Shipment_Tracking_Importer'] = new WF_Shipment_Tracking_Importer();
        $GLOBALS['WF_Shipment_Tracking_Importer']->import_page = 'import_shipment_tracking_csv_cron';
        $GLOBALS['WF_Shipment_Tracking_Importer']->delimiter = ','; // need to give option in settingn , if some queries are coming
    }

}