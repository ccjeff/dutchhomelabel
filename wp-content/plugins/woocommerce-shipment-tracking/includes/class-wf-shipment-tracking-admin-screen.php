<?php

if (!defined('ABSPATH')) {
    exit;
}

class WF_Shipment_Tracking_Admin_Screen {

    /**
     * Constructor
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'admin_menu'));
        add_action('admin_print_styles', array($this, 'admin_scripts'));
        add_action('admin_notices', array($this, 'admin_notices'));
    }

    /**
     * Notices in admin
     */
    public function admin_notices() {
        if (!function_exists('mb_detect_encoding')) {
            echo '<div class="error"><p>' . __('CSV Import requires the function <code>mb_detect_encoding</code> to import CSV files. Please ask your hosting provider to enable this function.', 'woocommerce-shipment-tracking') . '</p></div>';
        }
    }

    /**
     * Admin Menu
     */
    public function admin_menu() {
        $page = add_submenu_page('woocommerce', __('Shipment Tracking Import', 'woocommerce-shipment-tracking'), __('Shipment Tracking Import', 'woocommerce-shipment-tracking'), 'manage_woocommerce', 'import_shipment_tracking_csv', array($this, 'output'));
    }

    /**
     * Admin Scripts
     */
    public function admin_scripts() {

        wp_enqueue_script('woocommerce-shipment-tracking-csv-importer', plugins_url(basename(plugin_dir_path(WF_ShipmentTracking_FILE)) . '/js/shipment-tracking-csv-import.min.js', basename(__FILE__)), '', '1.0.0', 'screen');
        wp_localize_script('woocommerce-shipment-tracking-csv-importer', 'woocommerce_shipment_tracking_csv_cron_params', array('enable_ftp_ie' => '', 'shipment_tracking_auto_import' => 'Disabled'));
    }

    /**
     * Admin Screen output
     */
    public function output() {
        $tab = 'import';
        if (!empty($_GET['tab'])) {
            if ($_GET['tab'] == 'settings') {
                $tab = 'settings';
            }
        }

        include( 'views/html-wf-admin-screen.php' );
    }

    /**
     * Admin Page for settings
     */
    public function admin_settings_page() {
        include( 'views/settings/html-wf-settings-shipment-tracking.php' );
    }

}
new WF_Shipment_Tracking_Admin_Screen();