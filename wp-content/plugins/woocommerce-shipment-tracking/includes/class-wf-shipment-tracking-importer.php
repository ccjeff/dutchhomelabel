<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( class_exists( 'WP_Importer' ) && !class_exists('WF_Shipment_Tracking_Importer') ) {
    class WF_Shipment_Tracking_Importer extends WP_Importer {
        
        var $id;
        var $file_url;
        var $import_page;
        var $delimiter;
        var $posts = array();
        var $imported;
        var $skipped;
        var $upload_dir;
        var $skip_allready_processed;

        /**
         * __construct function.
         */
        public function __construct() {

            $this->log = new WC_Logger();
            $this->import_page = 'import_shipment_tracking_csv';
            $this->skip_allready_processed = TRUE;
            // defining upload directory
            $this->upload_dir = 'wp-content/uploads/';
        }

        /**
         * Registered callback function for the WordPress Importer
         *
         * Manages the three separate stages of the CSV import process
         */
        public function dispatch() {

            $this->header();

			if ( ! empty( $_POST['delimiter'] ) )
				$this->delimiter = stripslashes( trim( $_POST['delimiter'] ) );

			if ( ! $this->delimiter )
                $this->delimiter = ',';

			$step = empty( $_GET['step'] ) ? 0 : (int) $_GET['step'];

			switch ( $step ) {

                case 0:
                    $this->greet();
                    break;

                case 1:
					check_admin_referer( 'import-upload' );
					if ( $this->handle_upload() ) {

						if ( $this->id )
							$files[] = get_attached_file( $this->id );
						else{
							$files = is_array($this->file_url)?$this->file_url:array($this->file_url);
                        }

						add_filter( 'http_request_timeout', array( $this, 'bump_request_timeout' ) );

						if ( function_exists( 'gc_enable' ) )
                            gc_enable();

                        @set_time_limit(0);
                        @ob_flush();
                        @flush();

						foreach($files as $file){
							$this->import( $file );
                        }
                    }
                    break;
            }

            $this->footer();
        }

        /**
         * format_data_from_csv function.
         *
         * @param mixed $data
         * @param string $enc
         * @return string
         */
		public function format_data_from_csv( $data, $enc ) {
			return ( $enc == 'UTF-8' ) ? $data : utf8_encode( $data );
        }

        /**
         * import function.
         *
         * @param mixed $file
         */
		function import( $file ) {
            global $wpdb;

            $this->log->add('shipment-tracking-import', __('Entered into import', 'wf_csv_import_export'));
            $this->imported = $this->skipped = 0;

            if (!is_file($file)) {
                echo '<p><strong>' . __('Unable to proceed:', 'woocommerce-shipment-tracking') . '</strong><br />';
                echo __('The file does not exist. Please go back and check your settings.', 'woocommerce-shipment-tracking') . '</p>';
                $this->log->add('shipment-tracking-import', __('Unable to proceed:File does not exist', 'wf_csv_import_export'));
                $this->footer();
                die();
            }

            if (!class_exists('WF_Tracking_Admin')) {
                include_once ( 'class-wf-tracking-admin.php' );
            }
            $new_rates = array();

            ini_set('auto_detect_line_endings', '1');

            if (( $handle = fopen($file, "r") ) !== FALSE) {
                $this->log->add('shipment-tracking-import', __('Reading file...', 'wf_csv_import_export'));
                $header = fgetcsv($handle, 0, $this->delimiter);
                if (sizeof($header) == 4) {
                    $this->log->add('shipment-tracking-import', __('CSV validation done', 'wf_csv_import_export'));
                    $i = 1;
                    while (( $row = fgetcsv($handle, 0, $this->delimiter) ) !== FALSE) {
                        
                        //list($order_id, $carrier, $trackingnumber, $shippingdate) = $row;
                        $order_id = isset($row[0]) ? trim($row[0]) : '';
						$order_id = apply_filters( 'xa_tracking_importer_order_id', $order_id);					// To support the woocommerce sequential order number plugin snippet required
                        $carrier = isset($row[1]) ? trim($row[1]) : '';
                        $trackingnumber = isset($row[2]) ? trim($row[2]) : '';
                        $shippingdate = isset($row[3]) ? trim($row[3]) : '';
                        $order = wc_get_order($order_id);

                        if ($order && !empty($carrier) && !empty($trackingnumber)) {

                            $csv_processed = get_post_meta($order_id, '_tracking_csv_processed', true);
                            if ($csv_processed!==1 && $this->skip_allready_processed) {
                                // Auto fill tracking info.
                                $this->log->add('shipment-tracking-import', __("Processing order id ----------------- $order_id", 'wf_csv_import_export'));
                                $message = WfTrackingUtil::update_tracking_data($order_id, $trackingnumber, sanitize_title($carrier), WF_Tracking_Admin::SHIPMENT_SOURCE_KEY, WF_Tracking_Admin::SHIPMENT_RESULT_KEY, $shippingdate);
                                update_post_meta($order_id, '_tracking_csv_processed', 1);
                                $order->update_status('completed');
                                echo '<br>' . __('Order #', 'woocommerce-shipment-tracking') . $order_id . __(' updated successfully.', 'woocommerce-shipment-tracking');
                                update_post_meta( $order_id, WF_Tracking_Admin::TRACKING_MESSAGE_KEY, $message );
                                $this->imported++;
                            } else {
                                $this->skipped++;
                            }
                        } else {
                            $this->skipped++;
                        }
                        $i++;
                    }
                } else {
                    $this->log->add('shipment-tracking-import', __('There has been an error:The CSV is invalid', 'wf_csv_import_export'));
                    echo '<p><strong>' . __('Sorry, there has been an error.', 'woocommerce-shipment-tracking') . '</strong><br />';
                    echo __('The CSV is invalid.', 'woocommerce-shipment-tracking') . '</p>';
                    $this->footer();
                    die();
                }

                fclose($handle);
            }

            $this->log->add('shipment-tracking-import', __('Imported:' . $this->imported, 'wf_csv_import_export'));
            $this->log->add('shipment-tracking-import', __('Skipped:' . $this->skipped, 'wf_csv_import_export'));
            // Show Result
            echo '<div class="updated settings-error below-h2"><p>
				' . sprintf(__('Import complete - imported <strong>%s</strong> Shipment Tracking and skipped <strong>%s</strong>.', 'woocommerce-shipment-tracking'), $this->imported, $this->skipped) . '
			</p></div>';

            $this->import_end();
        }

        /**
         * Performs post-import cleanup of files and the cache
         */
        public function import_end() {
			echo '<p>' . __( 'All done!', 'woocommerce-shipment-tracking' ) . '</p>';

			do_action( 'import_end' );
        }

        /**
         * Handles the CSV upload and initial parsing of the file to prepare for
         * displaying author import options
         *
         * @return bool False if error uploading or invalid file, true otherwise
         */
        public function handle_upload() {
			if($this->handle_ftp()){
                return true;
			}
			else if ( empty( $_POST['file_url'] ) ) {

                $file = wp_import_handle_upload();

				if ( isset( $file['error'] ) ) {
					echo '<p><strong>' . __( 'Sorry, there has been an error.', 'woocommerce-shipment-tracking' ) . '</strong><br />';
					echo esc_html( $file['error'] ) . '</p>';
                    return false;
                }

                $this->id = (int) $file['id'];
            } else {

				if ( file_exists( ABSPATH . $_POST['file_url'] ) ) {

					$this->file_url = esc_attr( $_POST['file_url'] );

                } else {

					echo '<p><strong>' . __( 'Sorry, there has been an error.', 'woocommerce-shipment-tracking' ) . '</strong></p>';
                    return false;
                }
            }

            return true;
        }

		private function handle_ftp(){
			$enable_ftp_ie         	= !empty( $_POST['enable_ftp_ie'] ) ? true : false;
			if($enable_ftp_ie == false) return false;

			$ftp_server				= ! empty( $_POST['ftp_server'] ) ? $_POST['ftp_server'] : '';
			$ftp_server_path		= ! empty( $_POST['ftp_server_path'] ) ? $_POST['ftp_server_path'] : '';
			$ftp_user				= ! empty( $_POST['ftp_user'] ) ? $_POST['ftp_user'] : '';
			$ftp_password           = ! empty( $_POST['ftp_password'] ) ? $_POST['ftp_password'] : '';
			$use_ftps         		= ! empty( $_POST['use_ftps'] ) ? true : false;
            $ftp_port               = ! empty( $_POST['ftp_port'] ) ? $_POST['ftp_port'] : '';
            $ftp_timeout            = ! empty( $_POST['ftp_timeout'] ) ? $_POST['ftp_timeout'] : '90';

            $settings = array();
			$settings[ 'ftp_server' ]		= $ftp_server;
			$settings[ 'ftp_user' ]			= $ftp_user;
			$settings[ 'ftp_password' ]		= $ftp_password;
			$settings[ 'use_ftps' ]			= $use_ftps;
			$settings[ 'enable_ftp_ie' ]	= $enable_ftp_ie;
			$settings[ 'ftp_server_path' ]	= $ftp_server_path;


            $local_file = 'wp-content/uploads/woocommerce-shipment-tracking-temp-import.csv';
            $server_file = $ftp_server_path;

	    update_option( 'wf_shipment_tracking_importer_ftp', $settings );
	    if( ! empty($ftp_server) )
	    {
			$ftp_conn = $use_ftps ? @ftp_ssl_connect( $ftp_server, $ftp_port, $ftp_timeout ) : @ftp_connect( $ftp_server, $ftp_port, $ftp_timeout );
	    }
	    else
	    {
		    die( __( 'Please Provide FTP Host IP / Address .', 'woocommerce-shipment-tracking' ) );
	    }


            $error_message = "";
            $success = false;
	    if($ftp_conn == false){
                $error_message = __("Could not Connect to Server : ", 'woocommerce-shipment-tracking').$ftp_server.'<br /><b>'.__( 'Possible Reasons :', 'woocommerce-shipment-tracking' ).'</b><br /><br />'.__( '1. Server/Host address may be wrong . Server - ', 'woocommerce-shipment-tracking' ).$ftp_server.'<br />'.__( '2. Port may be wrong . Port no. - ', 'woocommerce-shipment-tracking' ).$ftp_port.'<br />'.__( '3. Time out.', 'woocommerce-shipment-tracking' );
            }

	    if(empty($error_message)){
		    if( @ftp_login($ftp_conn, $ftp_user, $ftp_password) == false ){
			    $error_message = __( 'Connected to the server but not able to login .', 'woocommerce-shipment-tracking' ).'<br /><b>'.__( 'Possible Reasons - ', 'woocommerce-shipment-tracking' ).'</b><br />'.__( '1. Username may be wrong . User Name - ', 'woocommerce-shipment-tracking' ).$ftp_user.'<br/>'.__( 'Password may be wrong . Password - ', 'woocommerce-shipment-tracking' ).$ftp_password;
                }
            }

			if(empty($error_message)){
				ftp_pasv($ftp_conn, true);
				$folder_pattern	=	"/\/\.\*$/";
				if(preg_match($folder_pattern, $server_file ,$matches)){ // Folder is provided
					$local_file	=	array();
					$folder_path	=	preg_replace($folder_pattern,'',$server_file);
					$file_list = ftp_nlist($ftp_conn, $folder_path);
					foreach($file_list as $file){
						$file_name = basename($file);
						if(preg_match("/\.csv$/", $file)){ // Skip everything except csv
						
							if ( ftp_get($ftp_conn, ABSPATH.$this->upload_dir.$file_name, $folder_path.'/'.$file_name, FTP_BINARY) ) {
								$error_message =  "";
								$success = true;

								$local_file[]	=	ABSPATH.$this->upload_dir.$file_name;
							}
							else {
                                $success = false;
                                $error_message = __( "There was a problem while getting files from ftp server and uploading into wp-contents/uploads/. ", 'woocommerce-shipment-tracking' ).'<br /><b>'.__( '1. Try with or without FTPS .', 'woocommerce-shipment-tracking' ).'</b><br />'.__( '2. Check your FTP file permission .', 'woocommerce-shipment-tracking' ).'<br/>'.__( '3. Check permission of folder wp-contents/uploads/ .', 'woocommerce-shipment-tracking' );
                                break;
                            }
                        }
                    }
				}
				else{ // Single file
					if ( @ftp_get($ftp_conn, ABSPATH.$local_file, $server_file, FTP_BINARY) ) {
						$error_message =  "";
                        $success = true;
						$local_file = ABSPATH.$local_file;
                    } else {
                        $error_message = __( "There was a problem while getting files from ftp server and uploading into wp-contents/uploads/. ", 'woocommerce-shipment-tracking' ).'<br /><b>'.__( '1. Try with or without FTPS .', 'woocommerce-shipment-tracking' ).'</b><br />'.__( '2. Check your FTP file permission .', 'woocommerce-shipment-tracking' ).'<br/>'.__( '3. Check permission of folder wp-contents/uploads/ .', 'woocommerce-shipment-tracking' );
                    }
                }
            }
	    if($ftp_conn)
		ftp_close($ftp_conn);
	    if($success){
		    $this->file_url = $local_file;
	    }else{
		    die($error_message);
            }
            return true;
        }

        /**
         * header function.
         */
        public function header() {
            echo '<div class="wrap"><div class="icon32 icon32-woocommerce-importer" id="icon-woocommerce"><br></div>';
			echo '<h2>' . __( 'Import Shipment Tracking', 'woocommerce-shipment-tracking' ) . '</h2>';
        }

        /**
         * footer function.
         */
        public function footer() {
            echo '</div>';
        }

        /**
         * greet function.
         */
        public function greet() {
			$ftp_settings        = get_option( 'wf_shipment_tracking_importer_ftp');
			$ftp_server          = '';
			$ftp_user            = '';
			$ftp_password        = '';
			$use_ftps            = '';
			$enable_ftp_ie       = '';	
			$ftp_server_path     = '';	

			if( !empty( $ftp_settings ) ){
				$ftp_server         = $ftp_settings[ 'ftp_server' ];
				$ftp_user           = $ftp_settings[ 'ftp_user' ];
				$ftp_password       = $ftp_settings[ 'ftp_password' ];
				$use_ftps           = $ftp_settings[ 'use_ftps' ];
				$enable_ftp_ie      = $ftp_settings[ 'enable_ftp_ie' ];
				$ftp_server_path    = $ftp_settings[ 'ftp_server_path' ];
				$ftp_port           = isset($ftp_settings[ 'ftp_port' ]) ? $ftp_settings[ 'ftp_port' ] : 21;
				$ftp_timeout        = isset($ftp_settings[ 'ftp_timeout' ]) ? $ftp_settings[ 'ftp_timeout' ] : 90;
            }
			
		
            echo '<div class="narrow">';
			echo '<p>' . __( 'Hi there! Upload a CSV file containing Shipping Tracking to import into your shop. Choose a .csv file to upload, then click "Upload file and import".', 'woocommerce-shipment-tracking' ).'</p>';

			echo '<p>' . sprintf( __( 'Shipping Tracking CSV need to be defined with columns in a specific order (5 columns). <a href="%s">Click here to download a sample</a>.', 'woocommerce-shipment-tracking' ), WooCommerce_Shipment_Tracking::wf_plugin_url() . '/sample-data/sample_shipment_tracking.csv' ) . '</p>';

            $action = 'admin.php?import=import_shipment_tracking_csv&step=1';

			$bytes = apply_filters( 'import_upload_size_limit', wp_max_upload_size() );
			$size = size_format( $bytes );
            $upload_dir = wp_upload_dir();
			if ( ! empty( $upload_dir['error'] ) ) :
				?><div class="error"><p><?php _e( 'Before you can upload your import file, you will need to fix the following error:', 'woocommerce-shipment-tracking' ); ?></p>
                    <p><strong><?php echo $upload_dir['error']; ?></strong></p></div><?php
            else :
                ?>
                <form enctype="multipart/form-data" id="import-upload-form" method="post" action="<?php echo esc_attr(wp_nonce_url($action, 'import-upload')); ?>">
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th>
									<label for="upload"><?php _e( 'Choose a file from your computer:', 'woocommerce-shipment-tracking' ); ?></label>
                                </th>
                                <td>
                                    <input type="file" id="upload" name="import" size="25" />
                                    <input type="hidden" name="action" value="save" />
                                    <input type="hidden" name="max_file_size" value="<?php echo $bytes; ?>" />
									<small><?php printf( __('Maximum size: %s', 'woocommerce-shipment-tracking' ), $size ); ?></small>
                                </td>
                            </tr>

                            <tr>
                                <th>
									<label for="ftp"><?php _e( 'OR Provide FTP Path:', 'woocommerce-shipment-tracking' ); ?></label>
                                </th>
                                <td>
                                    <table class="form-table">
                                        <tr>
                                            <th>
												<label for="enable_ftp_ie"><?php _e( 'Enable FTP Import', 'woocommerce-shipment-tracking' ); ?></label>
                                            </th>
                                            <td>
												<input type="checkbox" name="enable_ftp_ie" id="enable_ftp_ie" class="checkbox" <?php checked( $enable_ftp_ie, 1 ); ?> />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
												<label for="ftp_server"><?php _e( 'FTP Server Host/IP', 'woocommerce-shipment-tracking' ); ?></label>
                                            </th>
                                            <td>
                                                <input type="text" name="ftp_server" id="ftp_server" placeholder="<?php _e('XXX.XXX.XXX.XXX', 'woocommerce-shipment-tracking'); ?>" value="<?php echo $ftp_server; ?>" class="input-text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
												<label for="ftp_user"><?php _e( 'FTP User Name', 'woocommerce-shipment-tracking' ); ?></label>
                                            </th>
                                            <td>
                                                <input type="text" name="ftp_user" id="ftp_user" placeholder="<?php _e('', 'woocommerce-shipment-tracking'); ?>" value="<?php echo $ftp_user; ?>" class="input-text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
												<label for="ftp_password"><?php _e( 'FTP Password', 'woocommerce-shipment-tracking' ); ?></label>
                                            </th>
                                            <td>
                                                <input type="password" name="ftp_password" id="ftp_password" placeholder="<?php _e('', 'woocommerce-shipment-tracking'); ?>" value="<?php echo $ftp_password; ?>" class="input-text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
												<label for="ftp_port"><?php _e( 'FTP Port', 'woocommerce-shipment-tracking' ); ?></label>
												<img class="help_tip" style="float:none;" data-tip="<?php _e( 'Default port will be used if left empty.', 'woocommerce-shipment-tracking' ); ?>" src="<?php echo WC()->plugin_url();?>/assets/images/help.png" height="16" width="16" />
                                            </th>
                                            <td>
                                                <input type="text" name="ftp_port" id="ftp_port" placeholder="<?php _e('21', 'woocommerce-shipment-tracking'); ?>" value="<?php if( isset($ftp_port) ) echo $ftp_port; ?>" class="input-text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
												<label for="ftp_timeout"><?php _e( 'FTP Timeout', 'woocommerce-shipment-tracking' ); ?></label>
												<img class="help_tip" style="float:none;" data-tip="<?php _e( 'Default timeout default value will be used if left empty.', 'woocommerce-shipment-tracking' ); ?>" src="<?php echo WC()->plugin_url();?>/assets/images/help.png" height="16" width="16" />
                                            </th>
                                            <td>
                                                <input type="text" name="ftp_timeout" id="ftp_timeout" placeholder="<?php _e('90', 'woocommerce-shipment-tracking'); ?>" value="<?php if( isset($ftp_timeout) ) echo $ftp_timeout; ?>" class="input-text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
												<label for="ftp_server_path"><?php _e( 'Path/CsvFileName', 'woocommerce-shipment-tracking' ); ?></label>
												<img class="help_tip" style="float:none;" data-tip="<?php _e( 'Remote CSV File Path starting from FTP home directory excluding leading slash and File Name in the end.', 'woocommerce-shipment-tracking' ); ?>" src="<?php echo WC()->plugin_url();?>/assets/images/help.png" height="16" width="16" />
                                            </th>
                                            <td>
                                                <input type="text" name="ftp_server_path" id="ftp_server_path" placeholder="<?php _e('Path/StartingFrom/FTP-Directory/FileName.csv', 'woocommerce-shipment-tracking'); ?>" value="<?php echo $ftp_server_path; ?>" class="input-text" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>
												<label for="use_ftps"><?php _e( 'Use FTPS', 'woocommerce-shipment-tracking' ); ?></label>
                                            </th>
                                            <td>
												<input type="checkbox" name="use_ftps" id="use_ftps" class="checkbox" <?php checked( $use_ftps, 1 ); ?> />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
								<th><label><?php _e( 'Delimiter', 'woocommerce-shipment-tracking' ); ?></label><br/></th>
                                <td><input type="text" name="delimiter" placeholder="," size="2" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="submit">
						<input type="submit" class="button" value="<?php esc_attr_e( 'Upload file and import', 'woocommerce-shipment-tracking' ); ?>" />
                    </p>
                </form>
            <?php
            endif;

            echo '</div>';
        }

        /**
         * Added to http_request_timeout filter to force timeout at 60 seconds during import
         *
         * @param  int $val
         * @return int 60
         */
		public function bump_request_timeout( $val ) {
            return 60;
        }
    }
}