<?php

add_shortcode( 'afterpay_price', 'return_afterpay_price' );
add_shortcode( 'afterpay_currency', 'return_afterpay_currency' );
add_shortcode( 'afterpay_img', 'return_afterpay_basic_img' );

/**
 * Return AfterPay Price
 *
 * @access public
 * @return string
 */
function return_afterpay_price() {
    global $afterpay_account_shortcode_price;
    return $afterpay_account_shortcode_price;
}

/**
 * Return AfterPay Currency
 *
 * @access public
 * @return string
 */
function return_afterpay_currency() {
    global $afterpay_account_shortcode_currency;
    return $afterpay_account_shortcode_currency;
}

/**
 * Return AfterPay image
 *
 * @access public
 * @return string
 */
function return_afterpay_basic_img() {
    global $afterpay_shortcode_img;
    return '<img class="afterpay-logo-img" src="' . $afterpay_shortcode_img . '" />';
}