<?php
/*
Plugin Name: WooCommerce AfterPay Gateway
Plugin URI: https://www.afterpay.nl
Description: Extends WooCommerce. Provides a <a href="http://www.afterpay.nl" target="_blank">AfterPay</a> gateway for WooCommerce.
Version: 3.3.0
Author: AfterPay
Author URI: https://www.afterpay.nl
Text Domain: afterpay
Domain Path: /languages
*/

/**
 * Copyright (c) 2011-2017  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @copyright   Copyright (c) 2011-2017 arvato Finance B.V.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
    require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
//woothemes_queue_update( plugin_basename( __FILE__ ), '', '' );


function init_afterpay_gateway() {

    if ( !class_exists( 'WC_Payment_Gateway' ) ) return;
    
    /**
     * Localisation
     */
    load_plugin_textdomain('afterpay', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/');

    // Define AfterPay root Dir
    define('AFTERPAY_DIR', dirname(__FILE__) . '/');
    
    // Define AfterPay lib Dir
    define('AFTERPAY_LIB', dirname(__FILE__) . '/vendor/payintegrator/afterpay/lib/');
    
    /**
     * AfterPay Payment Gateway
     *
     * @class         WC_Gateway_Afterpay
     * @extends        WC_Payment_Gateway
     * @package        WooCommerce/Classes/Payment
     * @author         Willem Fokkens
     */
    class WC_Gateway_Afterpay extends WC_Payment_Gateway {
        
        /**
        * Constructor for the gateway.
        * 
        * @access public
        * @return void
        */
        public function __construct() { 
            global $woocommerce;
        }
    }
    
    // Include AfterPay specific payment classes
    require_once 'class-afterpay-default.php';
    require_once 'class-afterpay-nl-openinvoice.php';
    require_once 'class-afterpay-nl-directdebit.php';
    require_once 'class-afterpay-nl-business.php';
    require_once 'class-afterpay-be-openinvoice.php';
    require_once 'class-afterpay-de-openinvoice.php';
    require_once 'class-afterpay-at-openinvoice.php';
    require_once 'class-afterpay-ch-openinvoice.php';
}

// Actions
add_action('plugins_loaded', 'init_afterpay_gateway', 0);

/**
 * Add the gateway to WooCommerce
 * 
 * @access public
 * @return array
 **/
function add_afterpay_gateway( $methods ) {
    
    $methods[] = 'WC_Gateway_Afterpay_Nl_Openinvoice';
    $methods[] = 'WC_Gateway_Afterpay_Nl_Directdebit';
    $methods[] = 'WC_Gateway_Afterpay_Nl_Business';
    $methods[] = 'WC_Gateway_Afterpay_Be_Openinvoice';
    $methods[] = 'WC_Gateway_Afterpay_De_Openinvoice';
    $methods[] = 'WC_Gateway_Afterpay_At_Openinvoice';
    $methods[] = 'WC_Gateway_Afterpay_Ch_Openinvoice';
    return $methods;
}

// Filters
add_filter('woocommerce_payment_gateways', 'add_afterpay_gateway' );