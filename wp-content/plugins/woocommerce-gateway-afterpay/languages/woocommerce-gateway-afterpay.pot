#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-10-19 10:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: class-afterpay-at-openinvoice.php:27
msgid "AfterPay AT Digital Invoice"
msgstr ""

#: class-afterpay-at-openinvoice.php:107 class-afterpay-nl-business.php:92 
#: class-afterpay-default.php:98 class-afterpay-ch-openinvoice.php:107 
#: class-afterpay-nl-directdebit.php:105 class-afterpay-nl-openinvoice.php:103 
#: class-afterpay-de-openinvoice.php:107 class-afterpay-be-openinvoice.php:105
msgid "Enable/Disable"
msgstr ""

#: class-afterpay-at-openinvoice.php:109
msgid "Enable AfterPay AT Digital Invoice"
msgstr ""

#: class-afterpay-at-openinvoice.php:113 class-afterpay-nl-business.php:98 
#: class-afterpay-default.php:104 class-afterpay-ch-openinvoice.php:113 
#: class-afterpay-nl-directdebit.php:111 class-afterpay-nl-openinvoice.php:109 
#: class-afterpay-de-openinvoice.php:113 class-afterpay-be-openinvoice.php:111
msgid "Title"
msgstr ""

#: class-afterpay-at-openinvoice.php:115 class-afterpay-nl-business.php:100 
#: class-afterpay-default.php:106 class-afterpay-ch-openinvoice.php:115 
#: class-afterpay-nl-directdebit.php:113 class-afterpay-nl-openinvoice.php:111 
#: class-afterpay-de-openinvoice.php:115
msgid "This controls the title which the user sees during checkout."
msgstr ""

#: class-afterpay-at-openinvoice.php:116 class-afterpay-ch-openinvoice.php:116 
#: class-afterpay-de-openinvoice.php:116
msgid "AfterPay - Rechnung"
msgstr ""

#: class-afterpay-at-openinvoice.php:119 class-afterpay-nl-business.php:104 
#: class-afterpay-default.php:110 class-afterpay-ch-openinvoice.php:119 
#: class-afterpay-nl-directdebit.php:117 class-afterpay-nl-openinvoice.php:115 
#: class-afterpay-de-openinvoice.php:119 class-afterpay-be-openinvoice.php:117
msgid "Extra information"
msgstr ""

#: class-afterpay-at-openinvoice.php:121 class-afterpay-nl-business.php:106 
#: class-afterpay-default.php:112 class-afterpay-ch-openinvoice.php:121 
#: class-afterpay-nl-directdebit.php:119 class-afterpay-nl-openinvoice.php:117 
#: class-afterpay-de-openinvoice.php:121 class-afterpay-be-openinvoice.php:119
msgid "Extra information to show to the customer in the checkout"
msgstr ""

#: class-afterpay-at-openinvoice.php:125 class-afterpay-ch-openinvoice.php:125 
#: class-afterpay-de-openinvoice.php:125
msgid "API Key"
msgstr ""

#: class-afterpay-at-openinvoice.php:127 class-afterpay-ch-openinvoice.php:127 
#: class-afterpay-de-openinvoice.php:127
msgid ""
"Please enter your AfterPay API Key; this is needed in order to take payment!"
msgstr ""

#: class-afterpay-at-openinvoice.php:134 class-afterpay-nl-business.php:137 
#: class-afterpay-default.php:143 class-afterpay-ch-openinvoice.php:134 
#: class-afterpay-nl-directdebit.php:150 class-afterpay-nl-openinvoice.php:148 
#: class-afterpay-de-openinvoice.php:134 class-afterpay-be-openinvoice.php:150
msgid "Lower threshold"
msgstr ""

#: class-afterpay-at-openinvoice.php:136 class-afterpay-nl-business.php:139 
#: class-afterpay-default.php:145 class-afterpay-ch-openinvoice.php:136 
#: class-afterpay-nl-directdebit.php:152 class-afterpay-nl-openinvoice.php:150 
#: class-afterpay-de-openinvoice.php:136 class-afterpay-be-openinvoice.php:152
msgid ""
"Disable AfterPay Invoice if Cart Total is lower than the specified value. "
"Leave blank to disable this feature."
msgstr ""

#: class-afterpay-at-openinvoice.php:143 class-afterpay-nl-business.php:146 
#: class-afterpay-default.php:152 class-afterpay-ch-openinvoice.php:143 
#: class-afterpay-nl-directdebit.php:159 class-afterpay-nl-openinvoice.php:157 
#: class-afterpay-de-openinvoice.php:143 class-afterpay-be-openinvoice.php:159
msgid "Upper threshold"
msgstr ""

#: class-afterpay-at-openinvoice.php:145 class-afterpay-nl-business.php:148 
#: class-afterpay-default.php:154 class-afterpay-ch-openinvoice.php:145 
#: class-afterpay-nl-directdebit.php:161 class-afterpay-nl-openinvoice.php:159 
#: class-afterpay-de-openinvoice.php:145 class-afterpay-be-openinvoice.php:161
msgid ""
"Disable AfterPay Invoice if Cart Total is higher than the specified value. "
"Leave blank to disable this feature."
msgstr ""

#: class-afterpay-at-openinvoice.php:152 class-afterpay-nl-business.php:155 
#: class-afterpay-default.php:161 class-afterpay-ch-openinvoice.php:152 
#: class-afterpay-nl-directdebit.php:168 class-afterpay-nl-openinvoice.php:166 
#: class-afterpay-de-openinvoice.php:152 class-afterpay-be-openinvoice.php:168
msgid "Test Mode"
msgstr ""

#: class-afterpay-at-openinvoice.php:154 class-afterpay-nl-business.php:157 
#: class-afterpay-default.php:163 class-afterpay-ch-openinvoice.php:154 
#: class-afterpay-nl-directdebit.php:170 class-afterpay-nl-openinvoice.php:168 
#: class-afterpay-de-openinvoice.php:154 class-afterpay-be-openinvoice.php:170
msgid ""
"Enable AfterPay Test Mode. This will only work if you have a AfterPay test "
"account. For test purchases with a live account."
msgstr ""

#: class-afterpay-at-openinvoice.php:161 class-afterpay-nl-business.php:164 
#: class-afterpay-default.php:170 class-afterpay-ch-openinvoice.php:161 
#: class-afterpay-nl-directdebit.php:177 class-afterpay-nl-openinvoice.php:175 
#: class-afterpay-de-openinvoice.php:161 class-afterpay-be-openinvoice.php:177
msgid "Debug mail"
msgstr ""

#: class-afterpay-at-openinvoice.php:163 class-afterpay-nl-business.php:166 
#: class-afterpay-default.php:172 class-afterpay-ch-openinvoice.php:163 
#: class-afterpay-nl-directdebit.php:179 class-afterpay-nl-openinvoice.php:177 
#: class-afterpay-de-openinvoice.php:163 class-afterpay-be-openinvoice.php:179
msgid ""
"Use debug mail to send the complete AfterPay request to your mail, for debug "
"functionality only. Leave empty to disable."
msgstr ""

#: class-afterpay-at-openinvoice.php:170 class-afterpay-nl-business.php:173 
#: class-afterpay-default.php:179 class-afterpay-ch-openinvoice.php:170 
#: class-afterpay-nl-directdebit.php:186 class-afterpay-nl-openinvoice.php:184 
#: class-afterpay-de-openinvoice.php:170 class-afterpay-be-openinvoice.php:195
msgid "Show phone number"
msgstr ""

#: class-afterpay-at-openinvoice.php:172 class-afterpay-nl-business.php:175 
#: class-afterpay-default.php:181 class-afterpay-ch-openinvoice.php:172 
#: class-afterpay-nl-directdebit.php:188 class-afterpay-nl-openinvoice.php:186 
#: class-afterpay-de-openinvoice.php:172 class-afterpay-be-openinvoice.php:197
msgid ""
"Show phone number field instead of Woocommerce default field. If this field "
"is missing in default checkout"
msgstr ""

#: class-afterpay-at-openinvoice.php:179 class-afterpay-nl-business.php:182 
#: class-afterpay-default.php:197 class-afterpay-ch-openinvoice.php:179 
#: class-afterpay-nl-directdebit.php:204 class-afterpay-nl-openinvoice.php:202 
#: class-afterpay-de-openinvoice.php:179 class-afterpay-be-openinvoice.php:213
msgid "Show terms and conditions"
msgstr ""

#: class-afterpay-at-openinvoice.php:181 class-afterpay-nl-business.php:184 
#: class-afterpay-default.php:199 class-afterpay-ch-openinvoice.php:181 
#: class-afterpay-nl-directdebit.php:206 class-afterpay-nl-openinvoice.php:204 
#: class-afterpay-de-openinvoice.php:181 class-afterpay-be-openinvoice.php:215
msgid "Show terms and conditions of AfterPay"
msgstr ""

#: class-afterpay-at-openinvoice.php:188 class-afterpay-nl-business.php:188 
#: class-afterpay-default.php:203 class-afterpay-ch-openinvoice.php:185 
#: class-afterpay-nl-directdebit.php:210 class-afterpay-nl-openinvoice.php:208 
#: class-afterpay-de-openinvoice.php:185 class-afterpay-be-openinvoice.php:219
msgid "IP restriction"
msgstr ""

#: class-afterpay-at-openinvoice.php:190 class-afterpay-nl-business.php:190 
#: class-afterpay-default.php:205 class-afterpay-ch-openinvoice.php:187 
#: class-afterpay-nl-directdebit.php:212 class-afterpay-nl-openinvoice.php:210 
#: class-afterpay-de-openinvoice.php:187 class-afterpay-be-openinvoice.php:221
msgid ""
"Fill in IP address to only show the payment method for that specific IP "
"address. Leave empty to disable"
msgstr ""

#: class-afterpay-at-openinvoice.php:197 class-afterpay-nl-business.php:197 
#: class-afterpay-default.php:212 class-afterpay-ch-openinvoice.php:194 
#: class-afterpay-nl-directdebit.php:219 class-afterpay-nl-openinvoice.php:217 
#: class-afterpay-de-openinvoice.php:194 class-afterpay-be-openinvoice.php:228
msgid "Enable captures and refunds"
msgstr ""

#: class-afterpay-at-openinvoice.php:199 class-afterpay-nl-business.php:199 
#: class-afterpay-default.php:214 class-afterpay-ch-openinvoice.php:196 
#: class-afterpay-nl-directdebit.php:221 class-afterpay-nl-openinvoice.php:219 
#: class-afterpay-de-openinvoice.php:196 class-afterpay-be-openinvoice.php:230
msgid "Enable capturing and refunding"
msgstr ""

#: class-afterpay-at-openinvoice.php:200 class-afterpay-nl-business.php:200 
#: class-afterpay-default.php:215 class-afterpay-ch-openinvoice.php:197 
#: class-afterpay-nl-directdebit.php:222 class-afterpay-nl-openinvoice.php:220 
#: class-afterpay-de-openinvoice.php:197 class-afterpay-be-openinvoice.php:231
msgid ""
"Important! This functionality can only be used when automatic capturing is "
"disabled in your AfterPay account. Please contact AfterPay before using this "
"functionality."
msgstr ""

#: class-afterpay-at-openinvoice.php:207 class-afterpay-ch-openinvoice.php:204 
#: class-afterpay-de-openinvoice.php:204
msgid "Refund tax percentage"
msgstr ""

#: class-afterpay-at-openinvoice.php:209 class-afterpay-ch-openinvoice.php:206 
#: class-afterpay-de-openinvoice.php:206
msgid "Default percentage calculated on refunds to AfterPay"
msgstr ""

#: class-afterpay-at-openinvoice.php:216 class-afterpay-ch-openinvoice.php:210 
#: class-afterpay-de-openinvoice.php:210
msgid "Compatibility with Germanized"
msgstr ""

#: class-afterpay-at-openinvoice.php:218 class-afterpay-ch-openinvoice.php:212 
#: class-afterpay-de-openinvoice.php:212
msgid "Use the title field from the Germanized plugin"
msgstr ""

#: class-afterpay-at-openinvoice.php:219 class-afterpay-ch-openinvoice.php:213 
#: class-afterpay-de-openinvoice.php:213
msgid ""
"This functionality hides the gender field at AfterPay and uses the title "
"field from the Germanized plugin"
msgstr ""

#: class-afterpay-at-openinvoice.php:238 class-afterpay-nl-business.php:216 
#: class-afterpay-default.php:287 class-afterpay-ch-openinvoice.php:232 
#: class-afterpay-de-openinvoice.php:232
msgid "TEST MODE ENABLED"
msgstr ""

#: class-afterpay-at-openinvoice.php:248 class-afterpay-default.php:363 
#: class-afterpay-ch-openinvoice.php:242 class-afterpay-de-openinvoice.php:242
msgid "Gender"
msgstr ""

#: class-afterpay-at-openinvoice.php:251 class-afterpay-ch-openinvoice.php:245 
#: class-afterpay-de-openinvoice.php:245
msgid "Frau"
msgstr ""

#: class-afterpay-at-openinvoice.php:252 class-afterpay-ch-openinvoice.php:246 
#: class-afterpay-de-openinvoice.php:246
msgid "Herr"
msgstr ""

#: class-afterpay-at-openinvoice.php:261 class-afterpay-nl-business.php:236 
#: class-afterpay-default.php:356 class-afterpay-ch-openinvoice.php:255 
#: class-afterpay-de-openinvoice.php:255
msgid "Phone number"
msgstr ""

#: class-afterpay-at-openinvoice.php:269 class-afterpay-default.php:374 
#: class-afterpay-ch-openinvoice.php:263 class-afterpay-de-openinvoice.php:263
msgid "I accept the"
msgstr ""

#: class-afterpay-at-openinvoice.php:269 class-afterpay-default.php:374 
#: class-afterpay-ch-openinvoice.php:263 class-afterpay-de-openinvoice.php:263
msgid "payment terms"
msgstr ""

#: class-afterpay-at-openinvoice.php:269 class-afterpay-default.php:374 
#: class-afterpay-ch-openinvoice.php:263 class-afterpay-de-openinvoice.php:263
msgid " from AfterPay."
msgstr ""

#: class-afterpay-at-openinvoice.php:288 class-afterpay-nl-business.php:276 
#: class-afterpay-default.php:403 class-afterpay-ch-openinvoice.php:282 
#: class-afterpay-de-openinvoice.php:282
msgid "Phone number is a required field"
msgstr ""

#: class-afterpay-at-openinvoice.php:293 class-afterpay-ch-openinvoice.php:287 
#: class-afterpay-de-openinvoice.php:287
msgid "Gender is a required field"
msgstr ""

#: class-afterpay-at-openinvoice.php:298 class-afterpay-nl-business.php:272 
#: class-afterpay-default.php:408 class-afterpay-ch-openinvoice.php:292 
#: class-afterpay-de-openinvoice.php:292
msgid "Please accept the AfterPay terms."
msgstr ""

#: class-afterpay-at-openinvoice.php:416 class-afterpay-default.php:531 
#: class-afterpay-ch-openinvoice.php:413 class-afterpay-de-openinvoice.php:411
msgid "Shipping"
msgstr ""

#: class-afterpay-at-openinvoice.php:417 class-afterpay-default.php:532 
#: class-afterpay-ch-openinvoice.php:414 class-afterpay-de-openinvoice.php:412
msgid "Shipping on order"
msgstr ""

#: class-afterpay-at-openinvoice.php:434 class-afterpay-default.php:541 
#: class-afterpay-ch-openinvoice.php:431 class-afterpay-de-openinvoice.php:429
msgid "Service Fee"
msgstr ""

#: class-afterpay-at-openinvoice.php:487 class-afterpay-default.php:624 
#: class-afterpay-ch-openinvoice.php:484 class-afterpay-de-openinvoice.php:482
msgid "AfterPay payment completed."
msgstr ""

#: class-afterpay-at-openinvoice.php:521 class-afterpay-default.php:653 
#: class-afterpay-ch-openinvoice.php:518 class-afterpay-de-openinvoice.php:516
msgid "AfterPay capture completed."
msgstr ""

#: class-afterpay-at-openinvoice.php:523 class-afterpay-default.php:655 
#: class-afterpay-ch-openinvoice.php:520 class-afterpay-de-openinvoice.php:518
msgid "Problem with capturing order."
msgstr ""

#: class-afterpay-at-openinvoice.php:542 class-afterpay-default.php:674 
#: class-afterpay-ch-openinvoice.php:539 class-afterpay-de-openinvoice.php:537
msgid "AfterPay payment pending."
msgstr ""

#: class-afterpay-at-openinvoice.php:545 class-afterpay-default.php:677 
#: class-afterpay-ch-openinvoice.php:542 class-afterpay-de-openinvoice.php:540
msgid "Awaiting AfterPay payment"
msgstr ""

#: class-afterpay-at-openinvoice.php:559 class-afterpay-default.php:691 
#: class-afterpay-ch-openinvoice.php:556 class-afterpay-de-openinvoice.php:554
msgid "AfterPay payment denied."
msgstr ""

#: class-afterpay-at-openinvoice.php:599 class-afterpay-at-openinvoice.php:618 
#: class-afterpay-default.php:706 class-afterpay-ch-openinvoice.php:596 
#: class-afterpay-ch-openinvoice.php:613 class-afterpay-de-openinvoice.php:594 
#: class-afterpay-de-openinvoice.php:613
msgid "There is a problem with submitting this order to AfterPay."
msgstr ""

#: class-afterpay-at-openinvoice.php:600 class-afterpay-at-openinvoice.php:619 
#: class-afterpay-default.php:707 class-afterpay-ch-openinvoice.php:599 
#: class-afterpay-ch-openinvoice.php:614 class-afterpay-de-openinvoice.php:595 
#: class-afterpay-de-openinvoice.php:614
msgid ""
"There is a problem with submitting this order to AfterPay, please check the "
"following issues: "
msgstr ""

#: class-afterpay-at-openinvoice.php:631 class-afterpay-default.php:720 
#: class-afterpay-ch-openinvoice.php:631 class-afterpay-de-openinvoice.php:626
msgid "Unknown response from AfterPay."
msgstr ""

#: class-afterpay-at-openinvoice.php:632 class-afterpay-default.php:721 
#: class-afterpay-ch-openinvoice.php:632 class-afterpay-de-openinvoice.php:627
msgid "Unknown response from AfterPay. Please contact our customer service"
msgstr ""

#: class-afterpay-at-openinvoice.php:643 class-afterpay-default.php:733 
#: class-afterpay-ch-openinvoice.php:643 class-afterpay-de-openinvoice.php:638
#, php-format
msgid "%s (Error code: %s)"
msgstr ""

#: class-afterpay-nl-business.php:15
msgid "AfterPay NL Business 2 Business"
msgstr ""

#: class-afterpay-nl-business.php:94
msgid "Enable AfterPay NL Business 2 Business"
msgstr ""

#: class-afterpay-nl-business.php:101
msgid "AfterPay - Veilig achteraf betalen voor bedrijven"
msgstr ""

#: class-afterpay-nl-business.php:110 class-afterpay-default.php:116 
#: class-afterpay-nl-directdebit.php:123 class-afterpay-nl-openinvoice.php:121 
#: class-afterpay-be-openinvoice.php:123
msgid "Merchant ID"
msgstr ""

#: class-afterpay-nl-business.php:112 class-afterpay-default.php:118 
#: class-afterpay-nl-directdebit.php:125 class-afterpay-nl-openinvoice.php:123 
#: class-afterpay-be-openinvoice.php:125
msgid ""
"Please enter your AfterPay Merchant ID; this is needed in order to take "
"payment!"
msgstr ""

#: class-afterpay-nl-business.php:119 class-afterpay-default.php:125 
#: class-afterpay-nl-directdebit.php:132 class-afterpay-nl-openinvoice.php:130 
#: class-afterpay-be-openinvoice.php:132
msgid "Portfolio number"
msgstr ""

#: class-afterpay-nl-business.php:121 class-afterpay-default.php:127 
#: class-afterpay-nl-directdebit.php:134 class-afterpay-nl-openinvoice.php:132 
#: class-afterpay-be-openinvoice.php:134
msgid ""
"Please enter your AfterPay Portfolio Number; this is needed in order to take "
"payment!"
msgstr ""

#: class-afterpay-nl-business.php:128 class-afterpay-default.php:134 
#: class-afterpay-nl-directdebit.php:141 class-afterpay-nl-openinvoice.php:139 
#: class-afterpay-be-openinvoice.php:141
msgid "Portefeuille password"
msgstr ""

#: class-afterpay-nl-business.php:130 class-afterpay-default.php:136 
#: class-afterpay-nl-directdebit.php:143 class-afterpay-nl-openinvoice.php:141 
#: class-afterpay-be-openinvoice.php:143
msgid ""
"Please enter your AfterPay Portfolio Password; this is needed in order to "
"take payment!"
msgstr ""

#: class-afterpay-nl-business.php:221
msgid "Company name"
msgstr ""

#: class-afterpay-nl-business.php:228
msgid "Chamber of Commerce number"
msgstr ""

#: class-afterpay-nl-business.php:245
msgid ""
"I accept the <a href=\"https://www.afterpay."
"nl/nl/klantenservice/betalingsvoorwaarden-b2b/\" target=\"blank\">payment "
"terms</a> from AfterPay."
msgstr ""

#: class-afterpay-nl-business.php:263
msgid "Company name is a required field"
msgstr ""

#: class-afterpay-nl-business.php:268
msgid "Chamber of Commerce number is a required field"
msgstr ""

#: class-afterpay-default.php:100 class-afterpay-nl-openinvoice.php:105
msgid "Enable AfterPay NL Digital Invoice"
msgstr ""

#: class-afterpay-default.php:107 class-afterpay-nl-openinvoice.php:112 
#: class-afterpay-be-openinvoice.php:114
msgid "AfterPay - Veilig achteraf betalen"
msgstr ""

#: class-afterpay-default.php:188 class-afterpay-nl-directdebit.php:195 
#: class-afterpay-nl-openinvoice.php:193 class-afterpay-be-openinvoice.php:204
msgid "Show gender"
msgstr ""

#: class-afterpay-default.php:190 class-afterpay-nl-directdebit.php:197 
#: class-afterpay-nl-openinvoice.php:195 class-afterpay-be-openinvoice.php:206
msgid "Show gender field in AfterPay form in the checkout"
msgstr ""

#: class-afterpay-default.php:234
msgid ""
"Do you want to offer this or other AfterPay payment methods to your "
"customers? Contact your AfterPay account manager or go to: "
msgstr ""

#: class-afterpay-default.php:235 class-afterpay-default.php:236
msgid ""
"https://www.afterpay.nl/en/business-partners-afterpay/contact-corporate/"
msgstr ""

#: class-afterpay-default.php:296
msgid "Date of birth"
msgstr ""

#: class-afterpay-default.php:300
msgid "Day"
msgstr ""

#: class-afterpay-default.php:313
msgid "Month"
msgstr ""

#: class-afterpay-default.php:315
msgid "Jan"
msgstr ""

#: class-afterpay-default.php:316
msgid "Feb"
msgstr ""

#: class-afterpay-default.php:317
msgid "Mar"
msgstr ""

#: class-afterpay-default.php:318
msgid "Apr"
msgstr ""

#: class-afterpay-default.php:319
msgid "May"
msgstr ""

#: class-afterpay-default.php:320
msgid "Jun"
msgstr ""

#: class-afterpay-default.php:321
msgid "Jul"
msgstr ""

#: class-afterpay-default.php:322
msgid "Aug"
msgstr ""

#: class-afterpay-default.php:323
msgid "Sep"
msgstr ""

#: class-afterpay-default.php:324
msgid "Oct"
msgstr ""

#: class-afterpay-default.php:325
msgid "Nov"
msgstr ""

#: class-afterpay-default.php:326
msgid "Dec"
msgstr ""

#: class-afterpay-default.php:330
msgid "Year"
msgstr ""

#: class-afterpay-default.php:349
msgid "Bankaccount"
msgstr ""

#: class-afterpay-default.php:365
msgid "Male"
msgstr ""

#: class-afterpay-default.php:366
msgid "Female"
msgstr ""

#: class-afterpay-default.php:393
msgid "Birthday is a required field"
msgstr ""

#: class-afterpay-default.php:398
msgid "Bankaccount is a required field"
msgstr ""

#: class-afterpay-default.php:746
msgid ""
"Thank you for your order, you will receive a payment invoice for your order "
"from AfterPay."
msgstr ""

#: class-afterpay-ch-openinvoice.php:27
msgid "AfterPay CH Digital Invoice"
msgstr ""

#: class-afterpay-ch-openinvoice.php:109
msgid "Enable AfterPay CH Digital Invoice"
msgstr ""

#: class-afterpay-nl-directdebit.php:27
msgid "AfterPay NL SEPA Direct Debit"
msgstr ""

#: class-afterpay-nl-directdebit.php:107
msgid "Enable AfterPay NL SEPA Direct Debit"
msgstr ""

#: class-afterpay-nl-directdebit.php:114
msgid "AfterPay - Veilig achteraf betalen (eenmalige machtiging)"
msgstr ""

#: class-afterpay-nl-openinvoice.php:27
msgid "AfterPay NL Digital Invoice"
msgstr ""

#: class-afterpay-de-openinvoice.php:27
msgid "AfterPay DE Digital Invoice"
msgstr ""

#: class-afterpay-de-openinvoice.php:109
msgid "Enable AfterPay DE Digital Invoice"
msgstr ""

#: class-afterpay-be-openinvoice.php:26
msgid "AfterPay BE Digital Invoice"
msgstr ""

#: class-afterpay-be-openinvoice.php:107
msgid "Enable AfterPay BE Digital Invoice"
msgstr ""

#: class-afterpay-be-openinvoice.php:113
msgid "U ontvangt per e-mail een betaalverzoek van AfterPay."
msgstr ""

#: class-afterpay-be-openinvoice.php:186
msgid "Order update mail"
msgstr ""

#: class-afterpay-be-openinvoice.php:188
msgid ""
"Use order update mail to send a notification mail when the order is accepted "
"after pending state. Leave empty to disable."
msgstr ""

#: class-afterpay-be-openinvoice.php:289
msgid "The order is accepted by AfterPay and can be processed"
msgstr ""

#: class-afterpay-be-openinvoice.php:306
msgid "The order is rejected by AfterPay and can be cancelled"
msgstr ""

#: class-afterpay-be-openinvoice.php:313
msgid "The order is removed by AfterPay and can be cancelled"
msgstr ""

#: class-afterpay-be-openinvoice.php:319
msgid "The order is still pending by AfterPay"
msgstr ""

#. Name of the plugin
msgid "WooCommerce AfterPay Gateway"
msgstr ""

#. Description of the plugin
msgid ""
"Extends WooCommerce. Provides a <a href=\"http://www.afterpay.nl\" "
"target=\"_blank\">AfterPay</a> gateway for WooCommerce."
msgstr ""

#. URI of the plugin
#. Author URI of the plugin
msgid "https://www.afterpay.nl"
msgstr ""

#. Author of the plugin
msgid "AfterPay"
msgstr ""
