<?php

class WC_Gateway_Afterpay_Nl_Business extends WC_Gateway_Afterpay_Default {

    /**
     * Class for AfterPay Open Invoice payment.
     *
     */
    public function __construct() {
        global $woocommerce;

        parent::__construct();

        $this->id = 'afterpay_business';
        $this->method_title = __('AfterPay NL Business 2 Business', 'afterpay');
        $this->has_fields = true;

        // Load the form fields.
        $this->init_form_fields();
        $this->show_bankaccount = false;
        $this->order_type = 'B2B';

        // Load the settings.
        $this->init_settings();

        // Define user set variables
        $this->enabled = ( isset( $this->settings['enabled'] ) ) ?
            $this->settings['enabled'] : '';
        $this->title = ( isset( $this->settings['title'] ) ) ?
            $this->settings['title'] : '';
        $this->extra_information = ( isset( $this->settings['extra_information'] ) ) ?
            $this->settings['extra_information'] : '';
        $this->merchantid = ( isset( $this->settings['merchantid'] ) ) ?
            $this->settings['merchantid'] : '';
        $this->portfolioid = ( isset( $this->settings['portfolioid'] ) ) ?
            $this->settings['portfolioid'] : '';
        $this->password = ( isset( $this->settings['password'] ) ) ?
            $this->settings['password'] : '';
        $this->lower_threshold = ( isset( $this->settings['lower_threshold'] ) ) ?
            $this->settings['lower_threshold'] : '';
        $this->upper_threshold = ( isset( $this->settings['upper_threshold'] ) ) ?
            $this->settings['upper_threshold'] : '';
        $this->testmode = ( isset( $this->settings['testmode'] ) ) ?
            $this->settings['testmode'] : '';
        $this->debug_mail = ( isset( $this->settings['debug_mail'] ) ) ?
            $this->settings['debug_mail'] : '';
        $this->show_phone = ( isset( $this->settings['show_phone'] ) ) ?
            $this->settings['show_phone'] : '';
        $this->show_termsandconditions = ( isset( $this->settings['show_termsandconditions'] ) ) ?
            $this->settings['show_termsandconditions'] : '';
        $this->use_custom_housenumber_field = ( isset( $this->settings['use_custom_housenumber_field'] ) ) ?
            $this->settings['use_custom_housenumber_field'] : '';
        $this->use_custom_housenumber_addition_field = 
            ( isset( $this->settings['use_custom_housenumber_addition_field'] ) ) ?
            $this->settings['use_custom_housenumber_addition_field'] : '';
        $this->ip_restriction = ( isset( $this->settings['ip_restriction'] ) ) ?
            $this->settings['ip_restriction'] : '';
        $this->capturesandrefunds = ( isset( $this->settings['capturesandrefunds'] ) ) ?
            $this->settings['capturesandrefunds'] : '';

        if( isset( $this->settings['capturesandrefunds'] ) && $this->settings['capturesandrefunds'] == "yes" ) {
            $this->supports = array('refunds');
        }

        // Country and language
        $afterpay_country = 'NL';
        $afterpay_language = 'NL';
        $afterpay_currency = 'EUR';
        $afterpay_invoice_terms = 'https://www.afterpay.nl/algemene-voorwaarden';
        $afterpay_invoice_icon = plugins_url(basename(dirname(__FILE__))."/images/afterpay.png");

        // Apply filters to Country and language
        $this->afterpay_country = apply_filters( 'afterpay_country', $afterpay_country );
        $this->afterpay_language = apply_filters( 'afterpay_language', $afterpay_language );
        $this->afterpay_currency = apply_filters( 'afterpay_currency', $afterpay_currency );
        $this->afterpay_invoice_terms = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
        $this->icon = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );

        // Actions
        /* 1.6.6 */
        add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
 
        /* 2.0.0 */
        add_action(
            'woocommerce_update_options_payment_gateways_' . $this->id,
            array( $this, 'process_admin_options' )
        );
        add_action( 'woocommerce_receipt_afterpay', array(&$this, 'receipt_page') );
    }

    /**
     * Initialise Gateway Settings Form Fields
     */
    function init_form_fields() {
           $this->form_fields = apply_filters('afterpay_nl_business_form_fields', array(
            'enabled' => array(
                'title' => __( 'Enable/Disable', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __( 'Enable AfterPay NL Business 2 Business', 'afterpay' ),
                'default' => 'no'
            ),
            'title' => array(
                'title' => __( 'Title', 'afterpay' ),
                'type' => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'afterpay' ),
                'default' => __( 'AfterPay - Veilig achteraf betalen voor bedrijven', 'afterpay' )
            ),
            'extra_information' => array(
                'title' => __( 'Extra information', 'afterpay' ),
                'type' => 'textarea',
                'description' => __( 'Extra information to show to the customer in the checkout', 'afterpay' ),
                'default' => ''
            ),
            'merchantid' => array(
                'title' => __( 'Merchant ID', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Please enter your AfterPay Merchant ID; this is needed in order to take payment!',
                    'afterpay'
                ),
                'default' => ''
            ),
            'portfolioid' => array(
                'title' => __( 'Portfolio number', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Please enter your AfterPay Portfolio Number; this is needed in order to take payment!',
                    'afterpay'
                ),
                'default' => ''
            ),
            'password' => array(
                'title' => __( 'Portefeuille password', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Please enter your AfterPay Portfolio Password; this is needed in order to take payment!',
                    'afterpay'
                ),
                'default' => ''
            ),
            'lower_threshold' => array(
                'title' => __( 'Lower threshold', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.',
                    'afterpay'
                ),
                'default' => '5'
            ),
            'upper_threshold' => array(
                'title' => __( 'Upper threshold', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.',
                    'afterpay'
                ),
                'default' => ''
            ),
            'testmode' => array(
                'title' => __( 'Test Mode', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __(
                    'Enable AfterPay Test Mode. This will only work if you have a AfterPay test account. For test purchases with a live account.',
                    'afterpay'
                ),
                'default' => 'yes'
            ),
            'debug_mail' => array(
                'title' => __( 'Debug mail', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.',
                    'afterpay'
                ),
                'default' => ''
            ),
            'show_phone' => array(
                'title' => __( 'Show phone number', 'afterpay' ),
                'type' => 'checkbox',
                'description' => __(
                    'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout',
                    'afterpay'
                ),
                'default' => 'no'
            ),
            'show_termsandconditions' => array(
                'title' => __( 'Show terms and conditions', 'afterpay' ),
                'type' => 'checkbox',
                'description' => __( 'Show terms and conditions of AfterPay', 'afterpay' ),
                'default' => 'yes'
            ),
            'use_custom_housenumber_field' => array(
                'title' => __( 'Use custom housenumber field', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in the custom field name used for the housenumber, without billing_ or shipping_',
                    'afterpay'
                ),
                'default' => ''
            ),
            'use_custom_housenumber_addition_field' => array(
                'title' => __( 'Use custom housenumber addition field', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in the custom field name used for the housenumber addition, without billing_ or shipping_',
                    'afterpay'
                ),
                'default' => ''
            ),
            'ip_restriction' => array(
                'title' => __( 'IP restriction', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable',
                    'afterpay'
                ),
                'default' => ''
            ),
            'capturesandrefunds' => array(
                'title' => __( 'Enable captures and refunds', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __( 'Enable capturing and refunding', 'afterpay' ),
                'description' => __(
                    'Important! This functionality can only be used when automatic capturing is disabled in your AfterPay account. Please contact AfterPay before using this functionality.',
                    'afterpay'
                ),
                'default' => 'no'
            )
        ));
    }

    /**
     * Payment form on checkout page
     */    
    function payment_fields() {
           global $woocommerce;
           ?>
           <?php if ( $this->testmode == 'yes' ) : ?>
        <div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php _e('TEST MODE ENABLED', 'afterpay'); ?></div>
        <?php endif; ?>
        <fieldset>

            <p class="form-row form-row-first validate-required">
                <label for="<?php echo $this->id; ?>_companyname"><?php echo __("Company name", 'afterpay') ?><span class="required">*</span></label>
                <input type="input" class="input-text" name="<?php echo $this->id; ?>_companyname" />
            </p>

            <div class="clear"></div>
            
            <p class="form-row form-row-first validate-required">
                <label for="<?php echo $this->id; ?>_cocnumber"><?php echo __("Chamber of Commerce number", 'afterpay') ?><span class="required">*</span></label>
                <input type="input" class="input-text" name="<?php echo $this->id; ?>_cocnumber" />
            </p>

            <?php if ($this->show_phone == 'yes') : ?>
            <div class="clear"></div>

            <p class="form-row form-row-first validate-required validate-phone">
                <label for="<?php echo $this->id; ?>_phone"><?php echo __( 'Phone number' , 'afterpay' ) ?><span class="required">*</span></label>
                <input type="input" class="input-text" name="<?php echo $this->id; ?>_phone" />    
            </p>

            <?php endif; ?>

            <div class="clear"></div>
            <p class="form-row form-row-first validate-required">
                <input type="checkbox" class="input-checkbox" name="<?php echo $this->id; ?>_terms" /><span class="required">*</span>
                <?php echo __("I accept the <a href=\"https://www.afterpay.nl/nl/klantenservice/betalingsvoorwaarden-b2b/\" target=\"blank\">payment terms</a> from AfterPay.", 'afterpay'); ?>
            </p>
            <div class="clear"></div>
        </fieldset>
        <?php
    }

    /**
     * Validate form fields.
     *
     * @access public
     * @return boolean
     */
    public function validate_fields()
    {
        global $woocommerce;
        if ( !$_POST[$this->id . '_companyname'] )
        {
            wc_add_notice( __('Company name is a required field', 'afterpay'), 'error' );
            return false;
        }
        if ( !$_POST[$this->id . '_cocnumber'] )
        {
            wc_add_notice( __('Chamber of Commerce number is a required field', 'afterpay'), 'error' );
            return false;
        }
        if ( !$_POST[$this->id . '_terms'] ) { 
            wc_add_notice( __('Please accept the AfterPay terms.', 'afterpay'), 'error' );
        }
        if ( $this->show_phone == "yes" && !$_POST[$this->id . '_phone'] ) 
        {
            wc_add_notice( __( 'Phone number is a required field' , 'afterpay' ), 'error' );
            return false;
        }
        return true;
    }
}