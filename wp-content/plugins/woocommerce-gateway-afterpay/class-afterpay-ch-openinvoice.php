<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Open invoice payment method for AfterPay Switserland
 *
 * @class         WC_Gateway_Afterpay_At_Openinvoice
 * @extends       WC_Gateway_Afterpay_Default
 * @package       WooCommerce/Classes/Payment
 * @author        AfterPay
 */
class WC_Gateway_Afterpay_Ch_Openinvoice extends WC_Gateway_Afterpay_Default {

    /**
     * Constructor for the gateway.
     *
     * @access public
     * @return void
     */
    public function __construct() {
        global $woocommerce;

        parent::__construct();

        $this->id = 'afterpay_ch_openinvoice';
        $this->method_title = __( 'AfterPay CH Digital Invoice' , 'afterpay' );
        $this->has_fields = true;

        // Load the form fields.
        $this->init_form_fields();
        $this->show_bankaccount = false;
        $this->order_type = 'B2C';

        // Load the settings.
        $this->init_settings();

        // Define user set variables
        $this->enabled = ( isset( $this->settings['enabled'] ) ) ?
            $this->settings['enabled'] : '';
        $this->title = ( isset( $this->settings['title'] ) ) ?
            $this->settings['title'] : '';
        $this->extra_information = ( isset( $this->settings['extra_information'] ) ) ?
            $this->settings['extra_information'] : '';
        $this->merchantid = ( isset( $this->settings['merchantid'] ) ) ?
            $this->settings['merchantid'] : '';
        $this->portfolioid = ( isset( $this->settings['portfolioid'] ) ) ?
            $this->settings['portfolioid'] : '';
        $this->password = ( isset( $this->settings['password'] ) ) ?
            $this->settings['password'] : '';
        $this->lower_threshold = ( isset( $this->settings['lower_threshold'] ) ) ?
            $this->settings['lower_threshold'] : '';
        $this->upper_threshold = ( isset( $this->settings['upper_threshold'] ) ) ?
            $this->settings['upper_threshold'] : '';
        $this->testmode = ( isset( $this->settings['testmode'] ) ) ?
            $this->settings['testmode'] : '';
        $this->debug_mail = ( isset( $this->settings['debug_mail'] ) ) ?
            $this->settings['debug_mail'] : '';
        $this->show_phone = ( isset( $this->settings['show_phone'] ) ) ?
            $this->settings['show_phone'] : '';
        $this->show_termsandconditions = ( isset( $this->settings['show_termsandconditions'] ) ) ? 
            $this->settings['show_termsandconditions'] : '';
        $this->use_custom_housenumber_field = ( isset( $this->settings['use_custom_housenumber_field'] ) ) ?
            $this->settings['use_custom_housenumber_field'] : '';
        $this->use_custom_housenumber_addition_field = 
            ( isset( $this->settings['use_custom_housenumber_addition_field'] ) ) ?
            $this->settings['use_custom_housenumber_addition_field'] : '';
        $this->ip_restriction = ( isset( $this->settings['ip_restriction'] ) ) ?
            $this->settings['ip_restriction'] : '';
        $this->capturesandrefunds = ( isset( $this->settings['capturesandrefunds'] ) ) ?
            $this->settings['capturesandrefunds'] : '';
        $this->refund_tax_percentage = ( isset( $this->settings['refund_tax_percentage'] ) ) ?
            $this->settings['refund_tax_percentage'] : '';
        $this->compatibility_germanized = ( isset( $this->settings['compatibility_germanized'] ) ) ?
            $this->settings['compatibility_germanized'] : '';

        if( isset( $this->settings['capturesandrefunds'] ) && $this->settings['capturesandrefunds'] == "yes" ) {
            $this->supports = array('refunds');
        }

        $afterpay_country = 'CH';
        $afterpay_language = 'DE';
        $afterpay_currency = 'CHF';
        $afterpay_invoice_terms = 'https://www.afterpay.de/agb';
        $afterpay_invoice_icon = plugins_url(basename(dirname(__FILE__))."/images/afterpay.png");

        // Apply filters to Country and language
        $this->afterpay_country = apply_filters( 'afterpay_country', $afterpay_country );
        $this->afterpay_language = apply_filters( 'afterpay_language', $afterpay_language );
        $this->afterpay_currency = apply_filters( 'afterpay_currency', $afterpay_currency );
        $this->afterpay_invoice_terms = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
        $this->icon = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );

        // Actions
        /* 2.0.0 */
        add_action(
            'woocommerce_update_options_payment_gateways_' . $this->id,
            array( $this, 'process_admin_options' )
        );
        add_action( 'woocommerce_receipt_afterpay', array(&$this, 'receipt_page' ) );
    }

    /**
     * Initialise Gateway Settings Form Fields
     * 
     * @access public
     * @return void
     */
    function init_form_fields() {
           $this->form_fields = apply_filters('afterpay_de_openinvoice_form_fields', array(
            'enabled' => array(
                'title' => __( 'Enable/Disable', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __( 'Enable AfterPay CH Digital Invoice', 'afterpay' ),
                'default' => 'no'
            ),
            'title' => array(
                'title' => __( 'Title', 'afterpay' ),
                'type' => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'afterpay' ),
                'default' => __( 'AfterPay - Rechnung', 'afterpay' )
            ),
            'extra_information' => array(
                'title' => __( 'Extra information', 'afterpay' ),
                'type' => 'textarea',
                'description' => __( 'Extra information to show to the customer in the checkout', 'afterpay' ),
                'default' => ''
            ),
            'api_key' => array(
                'title' => __( 'API Key', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Please enter your AfterPay API Key; this is needed in order to take payment!',
                    'afterpay'
                ),
                'default' => ''
            ),
            'lower_threshold' => array(
                'title' => __( 'Lower threshold', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.',
                    'afterpay'
                ),
                'default' => '5'
            ),
            'upper_threshold' => array(
                'title' => __( 'Upper threshold', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.',
                    'afterpay'
                ),
                'default' => ''
            ),    
            'testmode' => array(
                'title' => __( 'Test Mode', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __(
                    'Enable AfterPay Test Mode. This will only work if you have a AfterPay test account. For test purchases with a live account.',
                    'afterpay'
                ),
                'default' => 'yes'
            ),
            'debug_mail' => array(
                'title' => __( 'Debug mail', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.',
                    'afterpay'
                ),
                'default' =>  ''
            ),
            'show_phone' => array(
                'title' => __( 'Show phone number', 'afterpay' ),
                'type' => 'checkbox',
                'description' => __(
                    'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout',
                    'afterpay'
                ),
                'default' => 'no'
            ),
            'show_termsandconditions' => array(
                'title' => __( 'Show terms and conditions', 'afterpay' ),
                'type' => 'checkbox',
                'description' => __( 'Show terms and conditions of AfterPay', 'afterpay' ),
                'default' => 'yes'
            ),
            'use_custom_housenumber_field' => array(
                'title' => __( 'Use custom housenumber field', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in the custom field name used for the housenumber, without billing_ or shipping_',
                    'afterpay'
                ),
                'default' => ''
            ),
            'use_custom_housenumber_addition_field' => array(
                'title' => __( 'Use custom housenumber addition field', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in the custom field name used for the housenumber addition, without billing_ or shipping_',
                    'afterpay'
                ),
                'default' => ''
            ),
            'ip_restriction' => array(
                'title' => __( 'IP restriction', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable',
                    'afterpay'
                ),
                'default' =>  ''
            ),
            'capturesandrefunds' => array(
                'title' => __( 'Enable captures and refunds', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __( 'Enable capturing and refunding', 'afterpay' ),
                'description' => __(
                    'Important! This functionality can only be used when automatic capturing is disabled in your AfterPay account. Please contact AfterPay before using this functionality.',
                    'afterpay'
                ),
                'default' => 'yes'
            ),
            'refund_tax_percentage' => array(
                'title' => __( 'Refund tax percentage', 'afterpay' ),
                'type' => 'text',
                'label' => __( 'Default percentage calculated on refunds to AfterPay', 'afterpay' ),
                'default' => '0'
            ),
            'compatibility_germanized' => array(
                'title' => __( 'Compatibility with Germanized', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __( 'Use the title field from the Germanized plugin', 'afterpay' ),
                'description' => __(
                    'This functionality hides the gender field at AfterPay and uses the title field from the Germanized plugin',
                    'afterpay'
                ),
                'default' => 'no'
            )
        ));
    }

    /**
     * Payment form on checkout page
     * 
     * @acces public
     * @return void
     */
    public function payment_fields() {
        global $woocommerce;

        if ( $this->testmode == 'yes' ) : ?>
        <div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php _e( 'TEST MODE ENABLED', 'afterpay' ); ?></div>
        <?php endif; ?>

        <?php if ( $this->extra_information != '' ) : ?>
        <p> <?php echo $this->extra_information; ?></p>
        <?php endif; ?>

        <fieldset>
            <?php if ($this->compatibility_germanized == 'no') : ?>
            <p class="form-row">
                <label for="<?php echo $this->id; ?>_gender"><?php echo __( 'Gender', 'afterpay' ) ?> <span class="required">*</span></label>
                <span class="gender">
                    <select class="gender_select" name="<?php echo $this->id; ?>_gender">
                        <option value="V" selected><?php echo __( 'Frau', 'afterpay' ) ?></option>
                        <option value="M"><?php echo __( 'Herr', 'afterpay' ) ?></option>
                    </select>
                </span>
            </p>
            <div class="clear"></div>
            <?php endif; ?>
            <?php if ($this->show_phone == 'yes') : ?>
            <div class="clear"></div>
            <p class="form-row form-row-first validate-required validate-phone">
                <label for="<?php echo $this->id; ?>_phone"><?php echo __( 'Phone number' , 'afterpay' ) ?><span class="required">*</span></label>
                <input type="input" class="input-text" name="<?php echo $this->id; ?>_phone" />    
            </p>
            <?php endif; ?>
            <?php if ($this->show_termsandconditions == 'yes') : ?>
            <div class="clear"></div>
            <p class="form-row validate-required">
                <input type="checkbox" class="input-checkbox" name="<?php echo $this->id; ?>_terms" /><span class="required">*</span>
                <?php echo __( "I accept the", 'afterpay'  ) .  " <a href=\"" . $this->afterpay_invoice_terms . "\" target=\"blank\">" . __( "payment terms", 'afterpay'  ) . "</a>" . __( " from AfterPay.", 'afterpay' ); ?>
            </p>
            <?php endif; ?>
        </fieldset>
        <?php
    }

    /**
     * Validate form fields.
     *
     * @access public
     * @return boolean
     */
    public function validate_fields()
    {
        global $woocommerce;

        if ( $this->show_phone == "yes" && !$_POST[$this->id . '_phone'] )
        {
            wc_add_notice( __( 'Phone number is a required field' , 'afterpay' ), 'error' );
            return false;
        }
        if ( $this->compatibility_germanized == "no" && !$_POST[$this->id . '_gender'] )
        {
            wc_add_notice( __( 'Gender is a required field' , 'afterpay' ), 'error' );
            return false;
        }
        if ( $this->show_termsandconditions == "yes" && !$_POST[$this->id . '_terms'] ) 
        {
            wc_add_notice( __( 'Please accept the AfterPay terms.' , 'afterpay' ), 'error' );
            return false;
        }
        return true;
    }

    /**
     * Process the payment and return the result
     * 
     * @access public
     * @param int $order_id
     * @return array
     **/
    public function process_payment( $order_id ) {
        global $woocommerce;

        $_tax = new WC_Tax();

        $order = wc_get_order( $order_id );

        require_once(__DIR__ . '/vendor/autoload.php');

        // Create AfterPay object
        $afterpay = new \Afterpay\Afterpay();
        $afterpay->setRest();

        // Get values from afterpay form on checkout page
        // Set form fields per payment option
        $afterpay_phone = isset( $_POST[$this->id . '_phone'] ) ?
            wc_clean( $_POST[$this->id . '_phone'] ) : $order->get_billing_phone();
        $afterpay_gender = isset( $_POST[$this->id . '_gender'] ) ?
            wc_clean( $_POST[$this->id . '_gender'] ) : '';

        // Get language
        $language = strtoupper(substr(get_locale(), 0, 2));

        // Check if Germanized plugin is used for the gender / title
        $germanized_billing_title = get_post_meta( $order_id, '_billing_title', true );
        if ( $this->compatibility_germanized == "yes" && $germanized_billing_title !== null) {
            switch ($germanized_billing_title) {
                case '1':
                    $afterpay_gender = 'M';
                    break;
                case '2':
                    $afterpay_gender = 'V';
                    break;
            }
        }

        // Split address into House number and House extension
        $afterpay_billing_address = $order->get_billing_address_1();
        $afterpay_billing_address_2 = $order->get_billing_address_2();
        $splitted_address = $this->split_address( $afterpay_billing_address );
        if ( $afterpay_billing_address_2 != '' && $splitted_address[1] == "" ) {
            $afterpay_billing_address = $afterpay_billing_address . ' ' . $afterpay_billing_address_2;
        }
        $splitted_address = $this->split_address( $afterpay_billing_address );
        $afterpay_billing_address = $splitted_address[0];
        $afterpay_billing_house_number = $splitted_address[1];
        $afterpay_billing_house_extension = $splitted_address[2];
        $afterpay_shipping_address = $order->get_shipping_address_1();
        $afterpay_shipping_address_2 = $order->get_shipping_address_2();
        $splitted_address = $this->split_address( $afterpay_shipping_address );
        if ( $afterpay_shipping_address_2 != '' && $splitted_address[1] == "" ) {
            $afterpay_shipping_address = $afterpay_shipping_address . ' ' . $afterpay_shipping_address_2;
        }
        $splitted_address = $this->split_address( $afterpay_shipping_address );
        $afterpay_shipping_address = $splitted_address[0];
        $afterpay_shipping_house_number = $splitted_address[1];
        $afterpay_shipping_house_extension = $splitted_address[2];

        // If special field is being used for housenumber then use that field
        if( $this->settings['use_custom_housenumber_field'] !== '' ) {
            $afterpay_billing_house_number = 
            isset( $_POST['billing_' . $this->settings['use_custom_housenumber_field']] )
            ? wc_clean( $_POST['billing_' . $this->settings['use_custom_housenumber_field']] ) 
            : $afterpay_billing_house_number;

            $afterpay_shipping_house_number = 
            isset( $_POST['shipping_' . $this->settings['use_custom_housenumber_field']] )
            ? wc_clean( $_POST['shipping_' . $this->settings['use_custom_housenumber_field']] ) 
            : $afterpay_shipping_house_number;
        }

        // If special field is being used for housenumber addition then use that field
        if( $this->settings['use_custom_housenumber_addition_field'] !== '' ) {
            $afterpay_billing_house_extension = 
            isset( $_POST['billing_' . $this->settings['use_custom_housenumber_addition_field']] )
            ? wc_clean( $_POST['billing_' . $this->settings['use_custom_housenumber_addition_field']] ) 
            : $afterpay_billing_house_extension;

            $afterpay_shipping_house_extension = 
            isset( $_POST['shipping_' . $this->settings['use_custom_housenumber_addition_field']] )
            ? wc_clean( $_POST['shipping_' . $this->settings['use_custom_housenumber_addition_field']] ) 
            : $afterpay_shipping_house_extension;
        }

        // Test mode or Live mode
        if ( $this->testmode == 'yes' ) {
            $afterpay_mode = 'test';
        } else {
            $afterpay_mode = 'live';
        }

        $authorisation['apiKey'] = $this->settings['api_key'];

        // Create the order

        // Cart Contents
        if (sizeof($order->get_items())>0) { 
            foreach ($order->get_items() as $item) {
                // Get product to retrieve sku or product id
                $_product = $item->get_product();
                // Get SKU or product id
                if ( $_product->get_sku() ) {
                    $sku = $_product->get_sku();
                } else {
                    $sku = $_product->get_id();
                }
                $item_tax_category = $this->get_tax_class( $order->get_line_total( $item, false ), $order->get_line_tax($item) );
                $item_tax_amount = $order->get_line_tax($item);

                // apply_filters to item price so we can filter this if needed
                $afterpay_item_price_including_tax = $order->get_item_total( $item, true );
                $item_price = apply_filters( 'afterpay_item_price_including_tax', $afterpay_item_price_including_tax );
                $item_price = round($item_price * 100, 0);
                $afterpay->create_order_line(
                    $sku,
                    $item['name'],
                    $item['qty'],
                    $item_price,
                    $item_tax_category,
                    $item_tax_amount
                );
            }
        }

        // Shipping
        if ( $order->get_shipping_total() > 0 ) {
            // We manually calculate the shipping tax percentage here
            $calculated_shipping_tax_percentage = ( $order->get_shipping_tax() / $order->get_shipping_total() ) * 100;
            $calculated_shipping_tax_decimal = ( $order->get_shipping_tax() / $order->get_shipping_total() ) + 1;
            $shipping_tax_rate = $this->get_tax_class( $order->get_total_shipping(), $order->get_shipping_tax() );

            // apply_filters to Shipping so we can filter this if needed
            $afterpay_shipping_price_including_tax = $order->get_shipping_total() * $calculated_shipping_tax_decimal;
            $shipping_price = apply_filters( 'afterpay_shipping_price_including_tax', $afterpay_shipping_price_including_tax );
            $shipping_sku = __( 'Shipping', 'afterpay' );
            $shipping_description = __( 'Shipping on order', 'afterpay' );
            $shipping_price = round($shipping_price * 100, 0);
            $shipping_tax = $order->get_shipping_tax();
            $afterpay->create_order_line(
                $shipping_sku,
                $shipping_description,
                1,
                $shipping_price,
                $shipping_tax_rate,
                $shipping_tax
            );
        }

        $fees = $woocommerce->cart->get_fees();

        if(count($fees) > 0) {
            foreach($fees as $fee) {
                $fee_sku = __( 'Service Fee', 'afterpay' );
                $fee_description = $fee->name;
                $fee_price = round(($fee->amount + $fee->tax) * 100);
                $afterpay->create_order_line( $fee_sku, $fee_description, 1, $fee_price, 1, $fee->tax );
            }
        }

        $aporder['billtoaddress']['city'] = $order->get_billing_city();
        $aporder['billtoaddress']['housenumber'] = $afterpay_billing_house_number;
        $aporder['billtoaddress']['housenumberaddition'] = $afterpay_billing_house_extension;
        $aporder['billtoaddress']['isocountrycode'] = $order->get_billing_country();
        $aporder['billtoaddress']['postalcode'] = $order->get_billing_postcode();
        $aporder['billtoaddress']['referenceperson']['email'] = $order->get_billing_email();
        $aporder['billtoaddress']['referenceperson']['gender'] = $afterpay_gender;
        $aporder['billtoaddress']['referenceperson']['firstname'] = $order->get_billing_first_name();
        $aporder['billtoaddress']['referenceperson']['isolanguage'] = $language;
        $aporder['billtoaddress']['referenceperson']['lastname'] = $order->get_billing_last_name();
        $aporder['billtoaddress']['referenceperson']['phonenumber'] = $afterpay_phone;
        $aporder['billtoaddress']['streetname'] =  $afterpay_billing_address;

        // Shipping address
        if ( $order->get_shipping_method() == '' ) {
            // Use billing address if Shipping is disabled in Woocommerce
            $aporder['shiptoaddress'] = $aporder['billtoaddress'];
        } else {
            $aporder['shiptoaddress']['city'] = $order->get_shipping_city();
            $aporder['shiptoaddress']['housenumber'] = $afterpay_shipping_house_number;
            $aporder['shiptoaddress']['housenumberaddition'] = $afterpay_shipping_house_extension;
            $aporder['shiptoaddress']['isocountrycode'] = $order->get_shipping_country();
            $aporder['shiptoaddress']['postalcode'] = $order->get_shipping_postcode();
            $aporder['shiptoaddress']['referenceperson']['email'] = $order->get_billing_email();
            $aporder['shiptoaddress']['referenceperson']['gender'] = $afterpay_gender;
            $aporder['shiptoaddress']['referenceperson']['firstname'] = $order->get_shipping_first_name();
            $aporder['shiptoaddress']['referenceperson']['isolanguage'] = $language;
            $aporder['shiptoaddress']['referenceperson']['lastname'] = $order->get_shipping_last_name();
            $aporder['shiptoaddress']['referenceperson']['phonenumber'] = $afterpay_phone;
            $aporder['shiptoaddress']['streetname'] = $afterpay_shipping_address;
        }

        $aporder['ordernumber'] = filter_var( $order->get_order_number(), FILTER_SANITIZE_NUMBER_INT );
        $aporder['currency'] = strtoupper( $order->get_currency() );
        $aporder['ipaddress'] = $this->get_client_ip();

        try {
            // Transmit all the specified data, from the steps above, to afterpay.
            $afterpay->set_order( $aporder, $this->order_type);
            $afterpay->do_request( $authorisation, $afterpay_mode );
            $this->send_debug_mail( $afterpay );

            // Retreive response
            if(isset($afterpay->order_result->return->statusCode)) {
                switch( $afterpay->order_result->return->statusCode ) {
                    case 'A':
                        $order->add_order_note( __('AfterPay payment completed.', 'afterpay') );

                        // If capturing is enabled, capture the full order
                        if( isset( $this->settings['capturesandrefunds'] )
                            && $this->settings['capturesandrefunds'] == "yes" ) {

                            // Capture payment
                            // Create AfterPay object
                            $afterpay_capture = new \Afterpay\Afterpay();
                            $afterpay_capture->setRest();
                            $afterpay_capture->set_ordermanagement('capture_full');

                            // Set up the additional information
                            $capture_details['invoicenumber'] = filter_var(
                                $order->get_order_number(),
                                FILTER_SANITIZE_NUMBER_INT
                            );
                            $capture_details['ordernumber'] = filter_var(
                                $order->get_order_number(),
                                FILTER_SANITIZE_NUMBER_INT
                            );

                            // Add order total in cents
                            $authorised_order = $afterpay->client->getOrder();
                            $capture_details['totalamount'] = $authorised_order['order']['totalGrossAmount'] * 100;

                            // Create the order object for order management (OM)
                            $afterpay_capture->set_order( $capture_details, 'OM' );
                            $afterpay_capture->do_request( $authorisation, $afterpay_mode );

                            $this->send_debug_mail( $afterpay_capture );

                            if( isset($afterpay_capture->order_result->return->resultId) ) {
                                if($afterpay_capture->order_result->return->resultId == 0) {
                                    $order->add_order_note( __('AfterPay capture completed.', 'afterpay') );
                                } else {
                                    $order->add_order_note( __('Problem with capturing order.', 'afterpay') );
                                }
                            }
                        }

                        // Payment complete
                        $order->payment_complete();

                        // Remove cart
                        $woocommerce->cart->empty_cart();

                        // Return thank you redirect
                        return array(
                            'result'     => 'success',
                            'redirect'    => $this->get_return_url( $order )
                        );

                        break;
                    case 'P':
                        $order->add_order_note( __('AfterPay payment pending.', 'afterpay') );

                        // Payment complete
                        $order->update_status('on-hold', __( 'Awaiting AfterPay payment', 'afterpay' ));

                        // Remove cart
                        $woocommerce->cart->empty_cart();

                        // Return thank you redirect
                        return array(
                            'result'     => 'success',
                            'redirect'    => $afterpay->order_result->return->extrafields->valueField
                        );

                        break;
                    case 'W':
                        //Order is denied, store it in a database.
                        $order->add_order_note( __('AfterPay payment denied.', 'afterpay') );
                        $message = isset( $afterpay->order_result->return->messages->message ) ?
                            $afterpay->order_result->return->messages->message : '';

                        // Check if description failure isset, else set rejectCode to return default rejection message
                        if( isset($afterpay->order_result->return->riskCheckMessages )
                            && isset($afterpay->order_result->return->riskCheckMessages[0]->customerFacingMessage)
                            && isset($afterpay->order_result->return->riskCheckMessages[0]->code)) {
                            // Get the rejection message from REST
                            $message = $afterpay->order_result->return->riskCheckMessages[0]->customerFacingMessage;

                            // If the rejection is an address correction, show the address correction
                            if(
                                in_array(
                                    $afterpay->order_result->return->riskCheckMessages[0]->code,
                                    array('200.103', '200.104')
                                )
                                && isset($afterpay->order_result->return->customer->addressList[0])
                                && is_object($afterpay->order_result->return->customer->addressList[0]))
                                {
                                    $newAddress = $afterpay->order_result->return->customer->addressList[0];
                                    $message = 'Leider ist uns die eingegebene Rechnungsadresse nicht bekannt, jedoch wurde eine mögliche, abweichende Rechnungsadresse ermittelt. Sollte sie korrekt sein, bitten wir, den Bestellvorgang mit dieser Adresse noch einmal durchzuführen. Andernfalls ist eine der alternativen Zahlmethoden zu wählen.';
                                    $message .= "<br/><br/>";
                                    $message .= $newAddress->street . ' ' . $newAddress->streetNumber . "<br/>";
                                    $message .= $newAddress->postalCode . " ";
                                    $message .= $newAddress->postalPlace . "<br/>";
                                    $message .= $newAddress->countryCode;
                            }
                        }
                        $order->add_order_note( __($message, 'afterpay'));
                        wc_add_notice( __($message, 'afterpay'), 'error' );

                        return;
                        break;
                }
            } else {

                // Check for business errors
                if ( $afterpay->order_result->return->resultId == '1' ) {
                    //Unknown response, store it in a database
                    $order->add_order_note( __('There is a problem with submitting this order to AfterPay.', 'afterpay' ) );
                    $validationmsg = '';
                    if(isset($afterpay->order_result->return->messages) && !is_object($afterpay->order_result->return->messages)) {
                        $validationmsg = __( 'There is a problem with submitting this order to AfterPay, please check the following issues: ', 'afterpay' );
                        $validationmsg .= '<ul>';
                        foreach ( $afterpay->order_result->return->messages as $value ) {
                            $validationmsg .= '<li style="list-style: inherit">' . __( $value->description, 'afterpay' ) . '</li>';
                            $order->add_order_note( __( $value->description, 'afterpay' ) );
                        }
                        $validationmsg .= '</ul>';
                    } else {
                        $validationmsg = '<li style="list-style: inherit">' . __( $afterpay->order_result->return->message, 'afterpay' ) . '</li>';	
                    }
                    wc_add_notice($validationmsg, 'error');
                }
                elseif ( $afterpay->order_result->return->resultId == '2' ) {
                    //Unknown response, store it in a database
                    $order->add_order_note( __( 'There is a problem with submitting this order to AfterPay.', 'afterpay' ) );
                    $validationmsg = __( 'There is a problem with submitting this order to AfterPay, please check the following issues: ', 'afterpay' );
                    $validationmsg .= '<ul>';
                    if( isset( $afterpay->order_result->return->messages ) 
                        && !is_object( $afterpay->order_result->return->messages )) {
                        foreach ( $afterpay->order_result->return->messages as $value ) {
                            $validationmsg .= '<li style="list-style: inherit">' . __( $value->description, 'afterpay' ) . '</li>';
                            $order->add_order_note( __( $value->description, 'afterpay' ) );
                        }
                    }
                    elseif( isset( $afterpay->order_result->return->failures->failure ) ) {
                        $validationmsg .= '<li style="list-style: inherit">' . 
                            __( $afterpay->order_result->return->failures->failure, 'afterpay' ) . '</li>';
                    }
                    $validationmsg .= '</ul>';
                    wc_add_notice($validationmsg, 'error');
                } else {
                   //Unknown response, store it in a database.
                    $order->add_order_note( __( 'Unknown response from AfterPay.', 'afterpay' ) );
                    wc_add_notice( __( 'Unknown response from AfterPay. Please contact our customer service', 'afterpay' ), 'error' );
                    // Cancel order to make new order possible
                    $order->cancel_order();
                }

                return;
            }
        }

        catch( Exception $e ) {
            //The purchase was denied or something went wrong, print the message:
            wc_add_notice( sprintf(__( '%s (Error code: %s)', 'afterpay' ), $e->getMessage(), $e->getCode() ), 'error' );
            return;
        }
    }

    /**
     * Process refunds.
     * WooCommerce 2.2 or later.
     *
     * @param  int $order_id
     * @param  float $amount
     * @param  string $reason
     * @return bool|WP_Error
     */
    public function process_refund( $order_id, $amount = null, $reason = '' ) {

        try {
            // Load AfterPay Library
            require_once( __DIR__ . '/vendor/autoload.php' );

            // Create AfterPay object
            $afterpay = new \Afterpay\Afterpay();
            $afterpay->setRest();

            // Set order management action to partial refund
            $afterpay->set_ordermanagement('refund_partial');

            // Check the refund id
            if( metadata_exists( 'post', $order_id, '_afterpay_refund_id' ) ) {
                $refund_id = get_post_meta( $order_id, '_afterpay_refund_id', true ) + 1;
            } else {
                $refund_id = 1;
            }

            // Set up the additional information
            $aporder['invoicenumber'] = $order_id;
            $aporder['ordernumber'] = $order_id;
            $aporder['creditinvoicenumber'] = 'REFUND-' . $order_id . '-' . $refund_id;

            // Set refund line
            $sku = 'REFUND';
            $name = 'REFUND';

            // If a reason has been set, use it in  the name/description
            if ($reason != '') $name = $name . ': ' . $reason;
            $qty = 1;
            $price = round($amount * 100, 0) * -1;
            $tax_category = 1; // 1 = high, 2 = low, 3, zero, 4 no tax
            $tax_percentage = $this->refund_tax_percentage;
            $tax_amount = $this->calculateVatAmount($amount, $tax_percentage);
            $afterpay->create_order_line(
                $sku,
                $name,
                $qty,
                $price,
                $tax_category,
                $tax_amount
            );

            // Create the order object for order management (OM)
            $afterpay->set_order( $aporder, 'OM' );

            // Test mode or Live mode
            if ( $this->testmode == 'yes' ) {
                $afterpay_mode = 'test';
            } else {
                $afterpay_mode = 'live';
            }

            // Set up the AfterPay credentials and sent the request
            $authorisation['apiKey'] = $this->settings['api_key'];

            $afterpay->do_request( $authorisation, $afterpay_mode );

            $this->send_debug_mail( $afterpay );

            if ( $afterpay->order_result->return->resultId == '0' ) {
                if($refund_id == 1) {
                    add_post_meta( $order_id, '_afterpay_refund_id', 1, true );
                } else {
                    update_post_meta( $order_id, '_afterpay_refund_id', $refund_id );
                }
                return true;
            } else {
                return new WP_Error( 'afterpay_refund_error', $afterpay->order_result->return->messages[0]->description );
            }
        } catch ( Exception $e ) {
            return new WP_Error( 'afterpay_refund_error', $e->getMessage() );
        }
        return false;
    }
}