<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * AfterPay Standard Payment Gateway
 *
 * Provides an common default for AfterPay Payment methods
 *
 * @class         WC_Gateway_Afterpay_Default
 * @extends       WC_Gateway_Afterpay
 * @package       WooCommerce/Classes/Payment
 * @author        AfterPay
 */
class WC_Gateway_Afterpay_Default extends WC_Gateway_Afterpay {

    /**
     * Constructor for the gateway.
     *
     * @access public
     * @return void
     */
    public function __construct() {
        global $woocommerce;

        parent::__construct();

        $this->has_fields = true;

        // Load the form fields.
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        // Define user set variables
        $this->enabled = ( isset( $this->settings['enabled'] ) ) ?
            $this->settings['enabled'] : '';
        $this->title = ( isset( $this->settings['title'] ) ) ?
            $this->settings['title'] : '';
        $this->extra_information = ( isset( $this->settings['extra_information'] ) ) ?
            $this->settings['extra_information'] : '';
        $this->merchantid = ( isset( $this->settings['merchantid'] ) ) ?
            $this->settings['merchantid'] : '';
        $this->portfolioid = ( isset( $this->settings['portfolioid'] ) ) ?
            $this->settings['portfolioid'] : '';
        $this->password = ( isset( $this->settings['password'] ) ) ?
            $this->settings['password'] : '';
        $this->lower_threshold = ( isset( $this->settings['lower_threshold'] ) ) ?
            $this->settings['lower_threshold'] : '';
        $this->upper_threshold = ( isset( $this->settings['upper_threshold'] ) ) ?
            $this->settings['upper_threshold'] : '';
        $this->testmode = ( isset( $this->settings['testmode'] ) ) ?
            $this->settings['testmode'] : '';
        $this->debug_mail = ( isset( $this->settings['debug_mail'] ) ) ?
            $this->settings['debug_mail'] : '';
        $this->show_phone = ( isset( $this->settings['show_phone'] ) ) ?
            $this->settings['show_phone'] : '';
        $this->show_gender = ( isset( $this->settings['show_gender'] ) ) ?
            $this->settings['show_gender'] : '';
        $this->show_termsandconditions = ( isset( $this->settings['show_termsandconditions'] ) ) ?
            $this->settings['show_termsandconditions'] : '';
        $this->use_custom_housenumber_field = ( isset( $this->settings['use_custom_housenumber_field'] ) ) ?
            $this->settings['use_custom_housenumber_field'] : '';
        $this->use_custom_housenumber_addition_field = 
            ( isset( $this->settings['use_custom_housenumber_addition_field'] ) ) ?
            $this->settings['use_custom_housenumber_addition_field'] : '';
        $this->ip_restriction = ( isset( $this->settings['ip_restriction'] ) ) ?
            $this->settings['ip_restriction'] : '';
        $this->capturesandrefunds = ( isset( $this->settings['capturesandrefunds'] ) ) ?
            $this->settings['capturesandrefunds'] : '';

        $afterpay_country = 'NL';
        $afterpay_language = 'NL';
        $afterpay_currency = 'EUR';
        $afterpay_invoice_terms = 'https://www.afterpay.nl/algemene-voorwaarden';
        $afterpay_invoice_icon = plugins_url( basename( dirname( __FILE__ ) ) . "/images/afterpay.png" );

        // Apply filters to Country and language
        $this->afterpay_country = apply_filters( 'afterpay_country', $afterpay_country );
        $this->afterpay_language = apply_filters( 'afterpay_language', $afterpay_language );
        $this->afterpay_currency = apply_filters( 'afterpay_currency', $afterpay_currency );
        $this->afterpay_invoice_terms = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
        $this->icon = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );

        // Actions
        add_action(
            'woocommerce_update_options_payment_gateways_' . $this->id,
            array( $this, 'process_admin_options' )
        );
        add_action( 'woocommerce_receipt_afterpay', array(&$this, 'receipt_page') );
    }

    /**
     * Initialise Gateway Settings Form Fields
     * 
     * @access public
     * @return void
     */
    public function init_form_fields() {
           $this->form_fields = apply_filters('afterpay_invoice_form_fields', array(
            'enabled' => array(
                'title' => __( 'Enable/Disable', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __( 'Enable AfterPay NL Digital Invoice', 'afterpay' ),
                'default' => 'yes'
            ),
            'title' => array(
                'title' => __( 'Title', 'afterpay' ),
                'type' => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'afterpay' ),
                'default' => __( 'AfterPay - Veilig achteraf betalen', 'afterpay' )
            ),
            'extra_description' => array(
                'title' => __( 'Extra information', 'afterpay' ),
                'type' => 'textarea',
                'description' => __( 'Extra information to show to the customer in the checkout', 'afterpay' ),
                'default' => ''
            ),
            'merchantid' => array(
                'title' => __( 'Merchant ID', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Please enter your AfterPay Merchant ID; this is needed in order to take payment!',
                    'afterpay'
                ),
                'default' => ''
            ),
            'portfolioid' => array(
                'title' => __( 'Portfolio number', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Please enter your AfterPay Portfolio Number; this is needed in order to take payment!',
                    'afterpay'
                ),
                'default' => ''
            ),
            'password' => array(
                'title' => __( 'Portefeuille password', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Please enter your AfterPay Portfolio Password; this is needed in order to take payment!',
                    'afterpay'
                ),
                'default' => ''
            ),
            'lower_threshold' => array(
                'title' => __( 'Lower threshold', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.',
                    'afterpay'
                ),
                'default' => ''
            ),
            'upper_threshold' => array(
                'title' => __( 'Upper threshold', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.',
                    'afterpay'
                ),
                'default' => ''
            ),
            'testmode' => array(
                'title' => __( 'Test Mode', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __(
                    'Enable AfterPay Test Mode. This will only work if you have a AfterPay test account. For test purchases with a live account.',
                    'afterpay'
                ),
                'default' => 'no'
            ),
            'debug_mail' => array(
                'title' => __( 'Debug mail', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.',
                    'afterpay'
                ),
                'default' => ''
            ),
            'show_phone' => array(
                'title' => __( 'Show phone number', 'afterpay' ),
                'type' => 'checkbox',
                'description' => __(
                    'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout',
                    'afterpay'
                ),
                'default' => 'no'
            ),
            'show_gender' => array(
                'title' => __( 'Show gender', 'afterpay' ),
                'type' => 'checkbox',
                'description' => __(
                    'Show gender field in AfterPay form in the checkout',
                    'afterpay'
                ),
                'default' => 'no'
            ),
            'show_termsandconditions' => array(
                'title' => __( 'Show terms and conditions', 'afterpay' ),
                'type' => 'checkbox',
                'description' => __('Show terms and conditions of AfterPay', 'afterpay' ),
                'default' => 'yes'
            ),
            'use_custom_housenumber_field' => array(
                'title' => __( 'Use custom housenumber field', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in the custom field name used for the housenumber, without billing_ or shipping_',
                    'afterpay'
                ),
                'default' => ''
            ),
            'use_custom_housenumber_addition_field' => array(
                'title' => __( 'Use custom housenumber addition field', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in the custom field name used for the housenumber addition, without billing_ or shipping_',
                    'afterpay'
                ),
                'default' => ''
            ),
            'ip_restriction' => array(
                'title' => __( 'IP restriction', 'afterpay' ),
                'type' => 'text',
                'description' => __(
                    'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable',
                    'afterpay'
                ),
                'default' => ''
            ),
            'capturesandrefunds' => array(
                'title' => __( 'Enable captures and refunds', 'afterpay' ),
                'type' => 'checkbox',
                'label' => __( 'Enable capturing and refunding', 'afterpay' ),
                'description' => __(
                    'Important! This functionality can only be used when automatic capturing is disabled in your AfterPay account. Please contact AfterPay before using this functionality.',
                    'afterpay'
                ),
                'default' => 'yes'
            )
        ));
    }

    /**
     * Show the admin options
     * 
     * @access public
     * @return void
     */
    public function admin_options() {
        ?>
        <h3><?php echo $this->method_title ?></h3>
        <p style="font-weight: bold">
            <?php _e('Do you want to offer this or other AfterPay payment methods to your customers? Contact your AfterPay account manager or go to: ', 'afterpay'); ?>
            <a href="<?php _e('https://www.afterpay.nl/en/business-partners-afterpay/contact-corporate/', 'afterpay')  ?>" target="_blank" style="color: #95d18c">
                <?php _e('https://www.afterpay.nl/en/business-partners-afterpay/contact-corporate/', 'afterpay')  ?>
            </a>
        </p>
        <table class="form-table">
            <?php $this->generate_settings_html(); ?>
        </table>
        <?php
    }
    
    /**
     * Check if this gateway is enabled and available in the user's country
     * 
     * @access public
     * @return boolean
     */    
    public function is_available() {
        global $woocommerce;
        if ( $this->enabled == "yes" ) :
            // Cart totals check - Lower threshold
            if ( !is_admin() && $this->lower_threshold !== '' ) {
                if ( $woocommerce->cart->subtotal < $this->lower_threshold ) return false;
            }
            // Cart totals check - Upper threshold
            if ( !is_admin() && $this->upper_threshold !== '' ) {
                if ( $woocommerce->cart->subtotal > $this->upper_threshold ) return false;
            }
            // Only activate the payment gateway if the customers country is the same as the filtered shop country
            if( !is_admin() ) {
                if ( $woocommerce->customer->get_billing_country() != $this->afterpay_country ) return false;
            }
            // Check if variable with ip's contains the ip of the client
            if ( !is_admin() && $this->ip_restriction !== '') {
                if (strpos($this->ip_restriction, $this->get_client_ip()) === false) {
                    return false;
                }
            }
            return true;
        endif;
        return false;
    }

    /**
     * Payment form on checkout page
     * 
     * @acces public
     * @return void
     */
    public function payment_fields() {
           global $woocommerce;
           ?>
           <?php if ( $this->testmode == 'yes' ) : ?>
        <div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php _e( 'TEST MODE ENABLED', 'afterpay' ); ?></div>
        <?php endif; ?>

        <?php if ( $this->extra_information != '' ) : ?>
        <p> <?php echo $this->extra_information; ?></p>
        <?php endif; ?>

        <fieldset>
            <p class="form-row">
                <label for="<?php echo $this->id; ?>_pno"><?php echo __( 'Date of birth', 'afterpay' ) ?> <span class="required">*</span></label>
                <span class="dob">
                    <select class="dob_select dob_day" name="<?php echo $this->id; ?>_date_of_birth_day">
                        <option value="">
                        <?php echo __( 'Day', 'afterpay' ) ?>
                        </option>
                        <?php
                            $day = 1;
                            while ( $day <= 31 ) {
                                $day_pad = str_pad( $day, 2, '0', STR_PAD_LEFT );
                                echo '<option value="' . $day_pad . '">' . $day_pad . '</option>';
                                $day++;
                            }
                        ?>
                    </select>
                    <select class="dob_select dob_month" name="<?php echo $this->id; ?>_date_of_birth_month">
                        <option value="">
                        <?php echo __( 'Month' , 'afterpay' ) ?>
                        </option>
                        <option value="01"><?php echo __( 'Jan', 'afterpay' ) ?></option>
                        <option value="02"><?php echo __( 'Feb', 'afterpay' ) ?></option>
                        <option value="03"><?php echo __( 'Mar', 'afterpay' ) ?></option>
                        <option value="04"><?php echo __( 'Apr', 'afterpay' ) ?></option>
                        <option value="05"><?php echo __( 'May', 'afterpay' ) ?></option>
                        <option value="06"><?php echo __( 'Jun', 'afterpay' ) ?></option>
                        <option value="07"><?php echo __( 'Jul', 'afterpay' ) ?></option>
                        <option value="08"><?php echo __( 'Aug', 'afterpay' ) ?></option>
                        <option value="09"><?php echo __( 'Sep', 'afterpay' ) ?></option>
                        <option value="10"><?php echo __( 'Oct', 'afterpay' ) ?></option>
                        <option value="11"><?php echo __( 'Nov', 'afterpay' ) ?></option>
                        <option value="12"><?php echo __( 'Dec', 'afterpay' ) ?></option>
                    </select>
                    <select class="dob_select dob_year" name="<?php echo $this->id; ?>_date_of_birth_year">
                        <option value="">
                        <?php echo __( 'Year', 'afterpay' ); ?>
                        </option>
                        <?php
                            // Select current date and deduct 18 years because of the date limit of using AfterPay
                            $year = date("Y") - 18;
                            // Select the oldest year (current year minus 100 years)
                            $lowestyear = $year - 82;
                            while ( $year >= $lowestyear ) {
                                echo '<option value="' . $year . '">' . $year . '</option>';
                                $year--;
                            }
                        ?>
                    </select>
                </span>
            </p>
            <div class="clear"></div>
            <?php if ($this->show_bankaccount) : ?>
            <div class="clear"></div>
            <p class="form-row validate-required">
                <label for="afterpay_bankaccount"><?php echo __( 'Bankaccount' , 'afterpay' ) ?><span class="required">*</span></label>
                <input type="input" class="input-text" name="<?php echo $this->id; ?>_bankaccount" />    
            </p>
            <?php endif; ?>
            <?php if ($this->show_phone == 'yes') : ?>
            <div class="clear"></div>
            <p class="form-row form-row-first validate-required validate-phone">
                <label for="<?php echo $this->id; ?>_phone"><?php echo __( 'Phone number' , 'afterpay' ) ?><span class="required">*</span></label>
                <input type="input" class="input-text" name="<?php echo $this->id; ?>_phone" />    
            </p>
            <?php endif; ?>
            <?php if ($this->show_gender == 'yes') : ?>
            <div class="clear"></div>
            <p class="form-row validate-required">
                <label for="<?php echo $this->id; ?>_gender"><?php echo __( 'Gender' , 'afterpay' ) ?><span class="required">*</span></label>
                <select name="<?php echo $this->id; ?>_gender">
                        <option value="M"><?php echo __( 'Male', 'afterpay' ) ?></option>
                        <option value="V" selected><?php echo __( 'Female', 'afterpay' ) ?></option>
                </select>
            </p>
            <?php endif; ?>
            <?php if ($this->show_termsandconditions == 'yes') : ?>
            <div class="clear"></div>
            <p class="form-row validate-required">
                <input type="checkbox" class="input-checkbox" name="<?php echo $this->id; ?>_terms" /><span class="required">*</span>
                <?php echo __( "I accept the", 'afterpay'  ) .  " <a href=\"" . $this->afterpay_invoice_terms . "\" target=\"blank\">" . __( "payment terms", 'afterpay'  ) . "</a>" . __( " from AfterPay.", 'afterpay' ); ?>
            </p>
            <?php endif; ?>
            <div class="clear"></div>
        </fieldset>
        <?php
    }

    /**
     * Validate form fields.
     *
     * @access public
     * @return boolean
     */
    public function validate_fields()
    {
        global $woocommerce;
        if ( !$_POST[$this->id . '_date_of_birth_day'] || !$_POST[$this->id . '_date_of_birth_month'] || !$_POST[$this->id . '_date_of_birth_year'] )
        {    
            wc_add_notice( __( 'Birthday is a required field', 'afterpay' ), 'error' );
            return false;
        }
        if ( $this->show_bankaccount && !$_POST[$this->id . '_bankaccount'] ) 
        {
            wc_add_notice( __( 'Bankaccount is a required field' , 'afterpay' ), 'error' );
            return false;
        }
        if ( $this->show_phone == "yes" && !$_POST[$this->id . '_phone'] ) 
        {
            wc_add_notice( __( 'Phone number is a required field' , 'afterpay' ), 'error' );
            return false;
        }
        if ( $this->show_termsandconditions == "yes" && !$_POST[$this->id . '_terms'] ) 
        {
            wc_add_notice( __( 'Please accept the AfterPay terms.' , 'afterpay' ), 'error' );
            return false;
        }
        return true;
    }

    /**
     * Process the payment and return the result
     * 
     * @access public
     * @param int $order_id
     * @return array
     **/
    public function process_payment( $order_id ) {
        global $woocommerce;
        $_tax = new WC_Tax();
        $order = wc_get_order( $order_id );
        require_once(__DIR__ . '/vendor/autoload.php');

        // Create AfterPay object
        $afterpay = new \Afterpay\Afterpay();

        // Get values from afterpay form on checkout page
        // Set form fields per payment option
        // Collect the dob
        $afterpay_pno_day = isset( $_POST[$this->id . '_date_of_birth_day'] )
            ? wc_clean( $_POST[$this->id . '_date_of_birth_day'] ) : '';
        if ( $afterpay_pno_day == '' && $this->order_type != 'B2B') {
            wc_add_notice( __('Please enter a birthday'), 'error' );
        }
        $afterpay_pno_month = isset( $_POST[$this->id . '_date_of_birth_month'] )
            ? wc_clean( $_POST[$this->id . '_date_of_birth_month'] ) : '';
        $afterpay_pno_year = isset( $_POST[$this->id . '_date_of_birth_year'] )
            ? wc_clean( $_POST[$this->id . '_date_of_birth_year'] ) : '';

        if($this->order_type == 'B2B') {
            $afterpay_pno = '1970-01-01T00:00:00';
        } else {
            $afterpay_pno = $afterpay_pno_year . '-' . $afterpay_pno_month . '-' . $afterpay_pno_day . 'T00:00:00';
        }

        $afterpay_bankacount = isset( $_POST[$this->id . '_bankaccount'] )
            ? wc_clean( $_POST[$this->id . '_bankaccount'] ) : '';

        $afterpay_phone = isset( $_POST[$this->id . '_phone'] )
            ? wc_clean( $_POST[$this->id . '_phone'] ) : $order->get_billing_phone();

        if($this->order_type == 'B2B') {
            $afterpay_cocnumber = isset( $_POST[$this->id . '_cocnumber'] )
                ? wc_clean( $_POST[$this->id . '_cocnumber'] ) : '';
            $afterpay_companyname = isset( $_POST[$this->id . '_companyname'] )
                ? wc_clean( $_POST[$this->id . '_companyname'] ) : '';
        }

        // Split address into House number and House extension for NL customers
        $afterpay_billing_address = $order->get_billing_address_1();
        $afterpay_billing_address_2 = $order->get_billing_address_2();
        $splitted_address = $this->split_address( $afterpay_billing_address );
        if ( $afterpay_billing_address_2 != '' && $splitted_address[1] == "" ) {
            $afterpay_billing_address = $afterpay_billing_address . ' ' . $afterpay_billing_address_2;
        }
        $splitted_address = $this->split_address( $afterpay_billing_address );
        $afterpay_billing_address = $splitted_address[0];
        $afterpay_billing_house_number = $splitted_address[1];
        $afterpay_billing_house_extension = $splitted_address[2];
        $afterpay_shipping_address = $order->get_shipping_address_1();
        $afterpay_shipping_address_2 = $order->get_shipping_address_2();
        $splitted_address = $this->split_address( $afterpay_shipping_address );
        if ( $afterpay_shipping_address_2 != '' && $splitted_address[1] == "" ) {
            $afterpay_shipping_address = $afterpay_shipping_address . ' ' . $afterpay_shipping_address_2;
        }
        $splitted_address = $this->split_address( $afterpay_shipping_address );
        $afterpay_shipping_address = $splitted_address[0];
        $afterpay_shipping_house_number = $splitted_address[1];
        $afterpay_shipping_house_extension = $splitted_address[2];

        // If special field is being used for housenumber then use that field
        if( $this->settings['use_custom_housenumber_field'] !== '' ) {
            $afterpay_billing_house_number = 
            isset( $_POST['billing_' . $this->settings['use_custom_housenumber_field']] )
            ? wc_clean( $_POST['billing_' . $this->settings['use_custom_housenumber_field']] ) 
            : $afterpay_billing_house_number;

            $afterpay_shipping_house_number = 
            isset( $_POST['shipping_' . $this->settings['use_custom_housenumber_field']] )
            ? wc_clean( $_POST['shipping_' . $this->settings['use_custom_housenumber_field']] ) 
            : $afterpay_shipping_house_number;
        }

        // If special field is being used for housenumber addition then use that field
        if( $this->settings['use_custom_housenumber_addition_field'] !== '' ) {
            $afterpay_billing_house_extension = 
            isset( $_POST['billing_' . $this->settings['use_custom_housenumber_addition_field']] )
            ? wc_clean( $_POST['billing_' . $this->settings['use_custom_housenumber_addition_field']] ) 
            : $afterpay_billing_house_extension;

            $afterpay_shipping_house_extension = 
            isset( $_POST['shipping_' . $this->settings['use_custom_housenumber_addition_field']] )
            ? wc_clean( $_POST['shipping_' . $this->settings['use_custom_housenumber_addition_field']] ) 
            : $afterpay_shipping_house_extension;
        }

        // Store afterpay specific form values in order as post meta
        update_post_meta( $order_id, 'afterpay_pno', $afterpay_pno );

        // Test mode or Live mode
        if ( $this->testmode == 'yes' ) {
            $afterpay_mode = 'test';
        } else {
            $afterpay_mode = 'live';
        }

        $authorisation['merchantid'] = $this->settings['merchantid'];
        $authorisation['portfolioid'] = $this->settings['portfolioid'];
        $authorisation['password'] = $this->settings['password'];

        // Create the order

        // Cart Contents
        if (sizeof($order->get_items())>0) { 
            foreach ($order->get_items() as $item) {
                // Get product to retrieve sku or product id
                $_product = $item->get_product();
                // Get SKU or product id
                if ( $_product->get_sku() ) {
                    $sku = $_product->get_sku();
                } else {
                    $sku = $_product->get_id();
                }
                $item_tax_category = $this->get_tax_class( $order->get_line_total( $item, false ), $order->get_line_tax($item) );

                // apply_filters to item price so we can filter this if needed
                $afterpay_item_price_including_tax = $order->get_item_total( $item, true );
                $item_price = apply_filters( 'afterpay_item_price_including_tax', $afterpay_item_price_including_tax );
                $item_price = round($item_price * 100, 0);
                $afterpay->create_order_line( $sku, $item['name'], $item['qty'], $item_price, $item_tax_category );
            }
        }

        // Shipping
        if ( $order->get_shipping_total() > 0 ) {
            // We manually calculate the shipping tax percentage here
            $calculated_shipping_tax_percentage = ( $order->get_shipping_tax() / $order->get_shipping_total() ) * 100;
            $calculated_shipping_tax_decimal = ( $order->get_shipping_tax() / $order->get_shipping_total() ) + 1;
            $shipping_tax_rate = $this->get_tax_class( $order->get_total_shipping(), $order->get_shipping_tax() );

            // apply_filters to Shipping so we can filter this if needed
            $afterpay_shipping_price_including_tax = $order->get_shipping_total() * $calculated_shipping_tax_decimal;
            $shipping_price = apply_filters( 'afterpay_shipping_price_including_tax', $afterpay_shipping_price_including_tax );
            $shipping_sku = __( 'Shipping', 'afterpay' );
            $shipping_description = __( 'Shipping on order', 'afterpay' );
            $shipping_price = round($shipping_price * 100, 0);
            $afterpay->create_order_line( $shipping_sku, $shipping_description, 1, $shipping_price, $shipping_tax_rate );    
        }

        $fees = $woocommerce->cart->get_fees();

        if(count($fees) > 0) {
            foreach($fees as $fee) {
                $fee_sku = __( 'Service Fee', 'afterpay' );
                $fee_description = $fee->name;
                $fee_price = round(($fee->amount + $fee->tax) * 100);
                $afterpay->create_order_line( $fee_sku, $fee_description, 1, $fee_price, 1 );
            }
        }

        // Check value for gender
        $afterpay_gender = '';
        $payment_methods_with_gender = array(
            'afterpay_openinvoice',
            'afterpay_directdebit',
            'afterpay_belgium'
        );

        if( in_array( $this->id, $payment_methods_with_gender ) ) {
            $afterpay_gender = isset( $_POST[$this->id . '_gender'] )
            ? wc_clean( $_POST[$this->id . '_gender'] ) : '';
        }

        $aporder['billtoaddress']['city'] = utf8_decode ( $order->get_billing_city() );
        $aporder['billtoaddress']['housenumber'] = utf8_decode ( $afterpay_billing_house_number );
        $aporder['billtoaddress']['housenumberaddition'] = utf8_decode ( $afterpay_billing_house_extension );
        $aporder['billtoaddress']['isocountrycode'] = $order->get_billing_country();
        $aporder['billtoaddress']['postalcode'] = utf8_decode ( $order->get_billing_postcode() );
        $aporder['billtoaddress']['referenceperson']['dob'] = $afterpay_pno;
        $aporder['billtoaddress']['referenceperson']['email'] = $order->get_billing_email();
        $aporder['billtoaddress']['referenceperson']['gender'] = $afterpay_gender;
        $aporder['billtoaddress']['referenceperson']['initials'] = utf8_decode( substr( $order->get_billing_first_name(),0,1 ) );
        $aporder['billtoaddress']['referenceperson']['isolanguage'] = 'NL';
        $aporder['billtoaddress']['referenceperson']['lastname'] = utf8_decode ( $order->get_billing_last_name() );
        $aporder['billtoaddress']['referenceperson']['phonenumber'] = $afterpay_phone;
        $aporder['billtoaddress']['streetname'] =  utf8_decode( $afterpay_billing_address );

        // Shipping address
        if ( $order->get_shipping_method() == '' ) {
            // Use billing address if Shipping is disabled in Woocommerce
            $aporder['shiptoaddress'] = $aporder['billtoaddress'];
        } else {
            $aporder['shiptoaddress']['city'] = utf8_decode( $order->get_shipping_city() );
            $aporder['shiptoaddress']['housenumber'] = utf8_decode( $afterpay_shipping_house_number );
            $aporder['shiptoaddress']['housenumberaddition'] = utf8_decode( $afterpay_shipping_house_extension );
            $aporder['shiptoaddress']['isocountrycode'] = $order->get_shipping_country();
            $aporder['shiptoaddress']['postalcode'] = utf8_decode( $order->get_shipping_postcode() );
            $aporder['shiptoaddress']['referenceperson']['dob'] = $afterpay_pno;
            $aporder['shiptoaddress']['referenceperson']['email'] = $order->get_billing_email();
            $aporder['shiptoaddress']['referenceperson']['gender'] = $afterpay_gender;
            $aporder['shiptoaddress']['referenceperson']['initials'] = utf8_decode( substr( $order->get_shipping_first_name(),0,1 ) );
            $aporder['shiptoaddress']['referenceperson']['isolanguage'] = 'NL';
            $aporder['shiptoaddress']['referenceperson']['lastname'] = utf8_decode ( $order->get_shipping_last_name() );
            $aporder['shiptoaddress']['referenceperson']['phonenumber'] = $afterpay_phone;
            $aporder['shiptoaddress']['streetname'] = utf8_decode( $afterpay_shipping_address );
        }

        $aporder['ordernumber'] = filter_var($order->get_order_number(), FILTER_SANITIZE_NUMBER_INT);
        $aporder['bankaccountnumber'] = $afterpay_bankacount;
        $aporder['currency'] = 'EUR';
        $aporder['ipaddress'] = $this->get_client_ip();

        if($this->order_type == 'B2B') {
            $aporder['company']['cocnumber'] = $afterpay_cocnumber;
            $aporder['company']['companyname'] = $afterpay_companyname;
            $aporder['person']['dob'] = $afterpay_pno;
            $aporder['person']['emailaddress'] = $order->get_billing_email();
            $aporder['person']['initials'] = utf8_decode( substr( $order->get_billing_first_name(),0,1 ) );
            $aporder['person']['isolanguage'] = 'NL';
            $aporder['person']['lastname'] = utf8_decode ( $order->get_billing_last_name() );
            $aporder['person']['phonenumber1'] = $afterpay_phone;
            $aporder['billtoaddress']['isolanguage'] = 'NL';
            $aporder['shiptoaddress']['isolanguage'] = 'NL';
        }

        try {
            // Transmit all the specified data, from the steps above, to afterpay.
            $afterpay->set_order( $aporder, $this->order_type);
            $afterpay->do_request( $authorisation, $afterpay_mode );

            $this->send_debug_mail( $afterpay );

            // Retreive response
            if(isset($afterpay->order_result->return->statusCode)) {
                switch( $afterpay->order_result->return->statusCode ) {
                    case 'A':
                        $order->add_order_note( __('AfterPay payment completed.', 'afterpay') );

                        // If capturing is enabled, capture the full order
                        if( isset( $this->settings['capturesandrefunds'] )
                            && $this->settings['capturesandrefunds'] == "yes" ) {

                            // Capture payment
                            // Create AfterPay object
                            $afterpay_capture = new \Afterpay\Afterpay();
                            $afterpay_capture->set_ordermanagement('capture_full');

                            // Set up the additional information
                            $capture_details['invoicenumber'] = filter_var(
                                $order->get_order_number(),
                                FILTER_SANITIZE_NUMBER_INT
                            );
                            $capture_details['ordernumber'] = filter_var(
                                $order->get_order_number(),
                                FILTER_SANITIZE_NUMBER_INT
                            );
                            $capture_details['billtoaddress']
                                ['isocountrycode'] = $aporder['billtoaddress']['isocountrycode'];

                            // Create the order object for order management (OM)
                            $afterpay_capture->set_order( $capture_details, 'OM' );
                            $afterpay_capture->do_request( $authorisation, $afterpay_mode );

                            if( isset($afterpay_capture->order_result->return->resultId) ) {
                                if($afterpay_capture->order_result->return->resultId == 0) {
                                    $order->add_order_note( __('AfterPay capture completed.', 'afterpay') );
                                } else {
                                    $order->add_order_note( __('Problem with capturing order.', 'afterpay') );
                                }
                            }
                        }

                        // Payment complete
                        $order->payment_complete();

                        // Remove cart
                        $woocommerce->cart->empty_cart();

                        // Return thank you redirect
                        return array(
                            'result' => 'success',
                            'redirect' => $this->get_return_url( $order )
                        );

                        break;
                    case 'P':
                        $order->add_order_note( __('AfterPay payment pending.', 'afterpay') );

                        // Payment complete
                        $order->update_status('on-hold', __( 'Awaiting AfterPay payment', 'afterpay' ));

                        // Remove cart
                        $woocommerce->cart->empty_cart();

                        // Return thank you redirect
                        return array(
                            'result'     => 'success',
                            'redirect'    => $afterpay->order_result->return->extrafields->valueField
                        );

                        break;
                    case 'W':
                        //Order is denied, store it in a database.
                        $order->add_order_note( __('AfterPay payment denied.', 'afterpay') );
                        $order->add_order_note( __($afterpay->order_result->return->messages->message, 'afterpay'));
                        wc_add_notice( __($afterpay->order_result->return->messages->description, 'afterpay'), 'error' );

                        // Cancel order to make new order possible
                        $order->cancel_order();

                        return;
                        break;
                }
            } else {

                // Check for validation errors
                if ( $afterpay->order_result->return->resultId == '2' ) {
                    //Unknown response, store it in a database
                    $order->add_order_note( __( 'There is a problem with submitting this order to AfterPay.', 'afterpay' ) );
                    $validationmsg = __( 'There is a problem with submitting this order to AfterPay, please check the following issues: ', 'afterpay' );
                    $validationmsg .= '<ul>';
                    if(!is_object($afterpay->order_result->return->messages)) {
                        foreach ( $afterpay->order_result->return->messages as $value ) {
                            $validationmsg .= '<li style="list-style: inherit">' . __( $value->description, 'afterpay' ) . '</li>';
                            $order->add_order_note( __( $value->description, 'afterpay' ) );
                        }
                    }
                    $validationmsg .= '</ul>';

                    wc_add_notice($validationmsg, 'error');
                } else {
                   //Unknown response, store it in a database.
                    $order->add_order_note( __( 'Unknown response from AfterPay.', 'afterpay' ) );
                    wc_add_notice( __( 'Unknown response from AfterPay. Please contact our customer service', 'afterpay' ), 'error' );

                    // Cancel order to make new order possible
                    $order->cancel_order();
                }

                return;
            }
        }

        catch( Exception $e ) {
            //The purchase was denied or something went wrong, print the message:
            wc_add_notice( sprintf(__( '%s (Error code: %s)', 'afterpay' ), utf8_encode( $e->getMessage() ), $e->getCode() ), 'error' );
            return;
        }
    }

    /**
     * Function to show a specific message on the succes page
     * 
     * @access public
     * @param int $order_id
     * @return void
     **/
    public function receipt_page( $order ) {
        echo '<p>' . __( 'Thank you for your order, you will receive a payment invoice for your order from AfterPay.', 'afterpay' ).'</p>';
    }

    /**
     * Function to send AfterPay debug email using the AfterPay Library debuglog function
     * 
     * @access public
     * @param object $afterpay
     * @return void
     **/
    public function send_debug_mail( $afterpay )
    {
        if ( $this->debug_mail != '' ) {
            wp_mail( $this->debug_mail, 'DEBUG MAIL WOOCOMMERCE AFTERPAY', $afterpay->client->getDebugLog() );
        }
    }

    /**
     * Function to get the the IP address of the client
     * 
     * @access public
     * @return string $ipaddress
     **/
    function get_client_ip(){
        if ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $_SERVER ) ) {
            $ipaddress = explode( ",", $_SERVER["HTTP_X_FORWARDED_FOR"] );
            return trim( $ipaddress[0] );
        } else if ( array_key_exists( 'REMOTE_ADDR', $_SERVER ) ) { 
            return $_SERVER["REMOTE_ADDR"];
        } else if ( array_key_exists('HTTP_CLIENT_IP', $_SERVER ) ) {
            return $_SERVER["HTTP_CLIENT_IP"]; 
        } 

        return '';
    }

    /**
     * Function to get the AfterPay NL Tax Category based on full amount and tax amount
     * 
     * @access public
     * @param float $total_amount
     * @param float $tax_amount
     * @return int $item_tax_category
     **/
    public function get_tax_class( $total_amount, $tax_amount ) {

        // We manually calculate the tax percentage here
        if ($tax_amount > 0) {
            // Calculate tax percentage
            $item_tax_percentage = number_format( ( $tax_amount / $total_amount ) * 100, 2, '.', '');
        } else {
            $item_tax_percentage = 0.00;
        }

        if ($item_tax_percentage > 7) {
            $item_tax_category = 1;
        } elseif ($item_tax_percentage > 0) {
            $item_tax_category = 2;
        } else {
            $item_tax_category = 3;
        }
        return $item_tax_category;
    }

    /**
     * Function to spit the address in array containing address, house number and house number extension
     * 
     * @access public
     * @param string $address
     * @return array $address
     **/
    public function split_address( $address ) {
        // Get everything up to the first number with a regex
        $hasMatch = preg_match( '/^[^0-9]*/', $address, $match );

        // If no matching is possible, return the supplied string as the street
        if ( !$hasMatch ) {
            return array($address, "", "");
        }

        // Remove the street from the address.
        $address = str_replace( $match[0], "", $address );
        $street = trim( $match[0] );

        // Nothing left to split, return
        if ( strlen( $address == 0 ) ) {
            return array($street, "", "");
        }
        // Split the address to an array
        $addrArray = preg_split("/([\s,--,\/,\\,|,,,+,_]+)|(?<=\d)(?=[a-z])|(?<=[a-z])(?=\d)/", $address);

        // Shift the first element off the array, that is the house number
        $housenumber = array_shift( $addrArray );

        // If the array is empty now, there is no extension.
        if ( count( $addrArray ) == 0 ) {
            return array( $street, $housenumber, "" );
        }

        // Join together the remaining pieces as the extension.
        $extension = implode( " ", $addrArray );

        return array( $street, $housenumber, $extension );
    }

    /**
     * Process refunds.
     * WooCommerce 2.2 or later.
     *
     * @param  int $order_id
     * @param  float $amount
     * @param  string $reason
     * @return bool|WP_Error
     */
    public function process_refund( $order_id, $amount = null, $reason = '' ) {

        try {
            // Load AfterPay Library
            require_once( __DIR__ . '/vendor/autoload.php' );

            // Create AfterPay object
            $afterpay = new \Afterpay\Afterpay();

            // Set order management action to partial refund
            $afterpay->set_ordermanagement('refund_partial');

            // Check the refund id
            if( metadata_exists( 'post', $order_id, '_afterpay_refund_id' ) ) {
                $refund_id = get_post_meta( $order_id, '_afterpay_refund_id', true ) + 1;
            } else {
                $refund_id = 1;
            }

            // Set up the additional information
            $aporder['invoicenumber'] = $order_id;
            $aporder['ordernumber'] = $order_id;
            $aporder['creditinvoicenumber'] = 'REFUND-' . $order_id . '-' . $refund_id;

            // Set refund line
            $sku = 'REFUND';
            $name = 'REFUND';

            // If a reason has been set, use it in  the name/description
            if ($reason != '') $name = $name . ': ' . $reason;
            $qty = 1;
            $price = round($amount * 100, 0) * -1;
            $tax_category = 1; // 1 = high, 2 = low, 3, zero, 4 no tax
            $afterpay->create_order_line( $sku, $name, $qty, $price, $tax_category );

            // Create the order object for order management (OM)
            $afterpay->set_order( $aporder, 'OM' );

            // Test mode or Live mode
            if ( $this->testmode == 'yes' ) {
                $afterpay_mode = 'test';
            } else {
                $afterpay_mode = 'live';
            }

            // Set up the AfterPay credentials and sent the request
            $authorisation['merchantid'] = $this->settings['merchantid'];
            $authorisation['portfolioid'] = $this->settings['portfolioid'];
            $authorisation['password'] = $this->settings['password'];

            $afterpay->do_request( $authorisation, $afterpay_mode );

            if ( $afterpay->order_result->return->statusCode == 'A' ) {
                if($refund_id == 1) {
                    add_post_meta( $order_id, '_afterpay_refund_id', 1, true );
                } else {
                    update_post_meta( $order_id, '_afterpay_refund_id', $refund_id );
                }
                return true;
            } else {
                return new WP_Error( 'afterpay_refund_error', $afterpay->order_result->return->messages[0]->description );
            }
        } catch ( Exception $e ) {
            return new WP_Error( 'afterpay_refund_error', $e->getMessage() );
        }
        return false;
    }

    /**
     * Calculate vat amount based on totalamount and vat percentage
     *
     * @param int $priceInclVat
     * @param int $vatPercentage
     *
     * @return float $vatAmount
     */
    public function calculateVatAmount($priceInclVat, $vatPercentage) {
        $vatAmount = 0;
        $priceExclVat = ( $priceInclVat / ( $vatPercentage + 100 ) ) * 100;
        $vatAmount = $priceInclVat - $priceExclVat;
        return round($vatAmount,2);
    }
}