jQuery(function(){
	function field_is_required( field, is_required ) {
		if ( is_required ) {
			field.find( 'label .optional' ).remove();
			field.addClass( 'validate-required' );

			if ( field.find( 'label .required' ).length === 0 ) {
				field.find( 'label' ).append(
					'&nbsp;<abbr class="required" title="' +
					wc_address_i18n_params.i18n_required_text +
					'">*</abbr>'
				);
			}
		} else {
			field.find( 'label .required' ).remove();
			field.removeClass( 'validate-required woocommerce-invalid woocommerce-invalid-required-field' );

			if ( field.find( 'label .optional' ).length === 0 ) {
				field.find( 'label' ).append( '&nbsp;<span class="optional">(' + wc_address_i18n_params.i18n_optional_text + ')</span>' );
			}
		}
	}

	jQuery( 'form.checkout, form#order_review').on( 'change', '#billing_country, input[name="billing_postcode"]', function() {
		var country         = jQuery( '#billing_country' ).val();
		var postcode        = jQuery( 'input[name="billing_postcode"]' ).val();
		var check_countries = wc_eu_vat_params.eu_countries;
		var b2b_enabled     = wc_eu_vat_params.b2b_required;

		field_is_required( jQuery( '#woocommerce_eu_vat_number_field' ), false );

		if ( country && jQuery.inArray( country, check_countries ) >= 0 ) {
			if ( 'yes' === b2b_enabled ) {
				field_is_required( jQuery( '#woocommerce_eu_vat_number_field' ), true );
			}

			if ( 'GB' === country ) {
				if ( ! jQuery( '#woocommerce_eu_vat_number_ni_notice' ).length ) {
					// Show VAT notice if country is GB.
					jQuery( '#woocommerce_eu_vat_number_field label' ).after( '<small id="woocommerce_eu_vat_number_ni_notice" style="font-size: 13px;">' + wc_eu_vat_params.uk_ni_notice +'</small>' );
				}

				if ( ! postcode.match(/^(bt).*$/i) ) {
					// Hide VAT field and make it optional if not Northern Ireland.
					jQuery( '#woocommerce_eu_vat_number_field' ).fadeOut( function( e ) {
						field_is_required( jQuery( '#woocommerce_eu_vat_number_field' ), false );
						jQuery( '#woocommerce_eu_vat_number' ).val( '' );
					} );
				} else {
					// Display VAT field.
					jQuery( '#woocommerce_eu_vat_number_field' ).fadeIn();
				}
			} else {
				// Hide VAT notice for other countries.
				jQuery( '#woocommerce_eu_vat_number_ni_notice' ).remove();
				// Show VAT field for other european countries.
				jQuery( '#woocommerce_eu_vat_number_field' ).fadeIn();
			}
		} else {
			jQuery( '#woocommerce_eu_vat_number_field' ).fadeOut();
		}
	});
	jQuery( '#billing_country, input[name="billing_postcode"]' ).trigger( 'change' );

	/* Validate EU VAT Number field only on change event */
	jQuery( 'form.checkout, form#order_review' ).on( 'change', '#woocommerce_eu_vat_number', function() {
		jQuery( 'body' ).trigger( 'update_checkout' );
	} );
});
