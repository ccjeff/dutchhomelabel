<?php
/*
	Plugin Name: Add rich snippets to product cat
	Description: Show up structured data to Woocommerce product categories.
	Author: Team Nijhuis
	Version: 1.0
*/

// Rich snippets to product categories
function add_rich_snippets_to_product_cat(){ 

    $cat_object         = get_queried_object();
    $thumb_id           = get_woocommerce_term_meta( $cat_object->term_id, 'thumbnail_id', true );
    $term_img           = wp_get_attachment_url(  $thumb_id );
    $meta               = get_option( 'wpseo_taxonomy_meta' );
    $yoast_desc         = $meta['product_cat'][$cat_object->term_id]['wpseo_desc'];
    $lowest_price       = wpq_get_min_price_per_product_cat( $cat_object->term_id );
    $highest_price      = wpq_get_max_price_per_product_cat( $cat_object->term_id );

    $script = '
        <script type="application/ld+json">{
            "@context": "http://schema.org/",
            "@type": "Product",
            "name": "'. $cat_object->name .'",
            "image":["'. $term_img .'"],
            "sku" : "'. $cat_object->term_id .'",
            "description": "'.  $yoast_desc  .'",
            "mpn": "0",
            "brand": {
                "@type": "Thing",
                "name": "Light & Living"
            },
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "4.7",
                "reviewCount": "49"
            },
            "offers": {
                "@type": "AggregateOffer",
                "lowPrice": "'. $lowest_price .'",
                "highPrice": "'. $highest_price .'",
                "priceCurrency": "'. get_woocommerce_currency() .'",
                "availability": "http://schema.org/InStock",
                "offerCount" : "'. $cat_object->count .'"
            },
            "review": {
                "@type": "Review",
                "author": "Dutch Home Label",
                "itemReviewed": "'. $cat_object->name .'"
            }
        }</script>
    ';
    echo $script;
}
add_action( 'woocommerce_after_shop_loop', 'add_rich_snippets_to_product_cat', 30 );

//woocommerce get lowest price in category
function wpq_get_min_price_per_product_cat( $term_id ) {

  global $wpdb;

  $sql = "
    SELECT  MIN( meta_value+0 ) as minprice
    FROM {$wpdb->posts} 
    INNER JOIN {$wpdb->term_relationships} ON ({$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id)
    INNER JOIN {$wpdb->postmeta} ON ({$wpdb->posts}.ID = {$wpdb->postmeta}.post_id) 
    WHERE  
      ( {$wpdb->term_relationships}.term_taxonomy_id IN (%d) ) 
    AND {$wpdb->posts}.post_type = 'product' 
    AND {$wpdb->posts}.post_status = 'publish' 
    AND {$wpdb->postmeta}.meta_key = '_price'
  ";

  return $wpdb->get_var( $wpdb->prepare( $sql, $term_id ) );

}

//woocommerce get highest price in category
function wpq_get_max_price_per_product_cat( $term_id ) {

  global $wpdb;

  $sql = "
    SELECT  MAX( meta_value+0 ) as maxprice
    FROM {$wpdb->posts} 
    INNER JOIN {$wpdb->term_relationships} ON ({$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id)
    INNER JOIN {$wpdb->postmeta} ON ({$wpdb->posts}.ID = {$wpdb->postmeta}.post_id) 
    WHERE  
      ( {$wpdb->term_relationships}.term_taxonomy_id IN (%d) ) 
    AND {$wpdb->posts}.post_type = 'product' 
    AND {$wpdb->posts}.post_status = 'publish' 
    AND {$wpdb->postmeta}.meta_key = '_price'
  ";

  return $wpdb->get_var( $wpdb->prepare( $sql, $term_id ) );

}