<?php
/**
 * @since      4.1.4
 * Description: Conversios Onboarding page, It's call while active the plugin
 */
if ( ! class_exists( 'Conversios_Dashboard' ) ) {
	class Conversios_Dashboard {
		protected $screen;
    protected $TVC_Admin_Helper;
    protected $CustomApi;
    protected $subscription_id;
    protected $ga_traking_type;
    protected $ga3_property_id;
    protected $ga3_ua_analytic_account_id;
    protected $ga3_view_id;
    protected $ga_currency;
    protected $ga_currency_symbols;
    protected $ga4_measurement_id;
    protected $ga4_property_id;
    protected $subscription_data;
    protected $plan_id=1;
    protected $is_need_to_update_api_data_wp_db = false;
    protected $pro_plan_site;
    protected $report_data;
		public function __construct( ){
      $this->TVC_Admin_Helper = new TVC_Admin_Helper();
      $this->CustomApi = new CustomApi();
      $this->subscription_id = $this->TVC_Admin_Helper->get_subscriptionId();
      $this->subscription_data = $this->TVC_Admin_Helper->get_user_subscription_data();
      $this->pro_plan_site = $this->TVC_Admin_Helper->get_pro_plan_site().'?utm_source=EE+Plugin+User+Interface&utm_medium=dashboard&utm_campaign=Upsell+at+Conversios';
      if(isset($this->subscription_data->plan_id) && !in_array($this->subscription_data->plan_id, array("1"))){
        $this->plan_id = $this->subscription_data->plan_id;
      }

      if( $this->subscription_id != "" ){
        $this->ga_traking_type = $this->subscription_data->tracking_option; // UA,GA4,BOTH
        $this->ga3_property_id = $this->subscription_data->property_id; // GA3
        $this->ga3_ua_analytic_account_id = $this->subscription_data->ua_analytic_account_id;
        $this->set_ga3_view_id_and_ga3_currency();
        $this->ga4_measurement_id = $this->subscription_data->measurement_id; //GA4 ID
      }else{
        wp_redirect("admin.php?page=conversios_onboarding");
        exit;
      }     
      
			$this->includes();
			$this->screen = get_current_screen();
      $this->init();
      $this->load_html();
		}

    public function includes() {
      if (!class_exists('CustomApi.php')) {
        require_once(ENHANCAD_PLUGIN_DIR . 'includes/setup/CustomApi.php');
      }   
    }

		public function init(){
      
    }
    
    public function set_ga3_view_id_and_ga3_currency(){
      //$this->view_id = get_option('ee_ga_view_id');
      if(isset($this->subscription_data->view_id) && isset($this->subscription_data->analytics_currency) && $this->subscription_data->view_id!="" && $this->subscription_data->analytics_currency!=""){
        $this->ga3_view_id = $this->subscription_data->view_id;
        $this->ga_currency = $this->subscription_data->analytics_currency;
        $this->ga_currency_symbols = $this->TVC_Admin_Helper->get_currency_symbols($this->ga_currency);
        
      }else{
        $data = array(
          "subscription_id"=>$this->subscription_id,
          "property_id"=>$this->ga3_property_id,
          "ua_analytic_account_id"=>$this->ga3_ua_analytic_account_id
        );
        $api_rs = $this->CustomApi->get_analytics_viewid_currency($data);        
        if (isset($api_rs->error) && $api_rs->error == '') {
          if(isset($api_rs->data) && $api_rs->data != ""){
            $data = json_decode($api_rs->data);
            $this->ga3_view_id = $data->view_id;
            $this->ga_currency =$data->analytics_currency;
            $this->ga_currency_symbols = $this->TVC_Admin_Helper->get_currency_symbols($this->ga_currency);
            $this->is_need_to_update_api_data_wp_db = true;
          }
        }
      }
    }
    public function load_html(){
      do_action('conversios_start_html_'.$_GET['page']);
      $this->current_html();
      $this->current_js();
      do_action('conversios_end_html_'.$_GET['page']);
    }    
		
    /**
     * Page custom js code
     *
     * @since    4.1.4
     */
    public function current_js(){
    ?>
    <script>
    $( document ).ready(function() {
      /**
        * daterage script
        **/
      var plan_id = '<?php echo $this->plan_id; ?>';
      //$(function() {
        var start = moment().subtract(30, 'days');
        var end = moment();
        function cb(start, end) {
            var start_date = start.format('DD/MM/YYYY') || 0,
            end_date = end.format('DD/MM/YYYY') || 0;
            $('span.daterangearea').html(start_date + ' - ' + end_date);

            /*var date_range = $.trim($(".report_range_val").text()).split('-');

            var start_date = $.trim(date_range[0].replace(/\//g,"-")) || 0,
            end_date = $.trim(date_range[1].replace(/\//g,"-")) || 0;*/
            var data = {
              action:'get_google_analytics_reports',      
              subscription_id:'<?php echo $this->subscription_id; ?>',
              plan_id:plan_id,
              ga_traking_type:'<?php echo $this->ga_traking_type; ?>',
              view_id :'<?php echo $this->ga3_view_id; ?>',
              ga4_property_id:'<?php echo $this->ga4_property_id; ?>',
              ga_currency :'<?php echo $this->ga_currency; ?>',
              plugin_url:'<?php echo ENHANCAD_PLUGIN_URL; ?>',
              start_date :$.trim(start_date.replace(/\//g,"-")),
              end_date :$.trim(end_date.replace(/\//g,"-")),
              conversios_nonce:'<?php echo wp_create_nonce( 'conversios_nonce' ); ?>'
            };
            // Call API
            tvc_helper.get_google_analytics_reports(data);
        }
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        cb(start, end);
      //});
      //upgrds plan popup
      $(".upgrdsbrs-btn").on( "click", function() {
          $('.upgradsbscrptn-pp').addClass('showpopup');
          $('body').addClass('scrlnone');
      });
      $('body').click(function(evt){ 
        if($(evt.target).closest('.upgrdsbrs-btn, .upgradsbscrptnpp-cntr').length){
          return;
        }        
        $('.upgradsbscrptn-pp').removeClass('showpopup');
        $('body').removeClass('scrlnone');
      });
      $(".clsbtntrgr").on( "click", function() {
          $(this).closest('.pp-modal').removeClass('showpopup');
          $('body').removeClass('scrlnone');
      });
      //upcoming_featur popup
       $(".upcoming-featur-btn").on( "click", function() {
          $('.upcoming_featur-btn-pp').addClass('showpopup');
          $('body').addClass('scrlnone');
      });
      $('body').click(function(evt){   
        if($(evt.target).closest('.upcoming-featur-btn, .upgradsbscrptnpp-cntr').length){
          return;
        }        
        $('.upcoming_featur-btn-pp').removeClass('showpopup');
        $('body').removeClass('scrlnone');
      });
      /**
        * Custom js code for API call
        **/
      
      //var start_date = moment().subtract(5, 'months').format('YYYY-MM-DD');
      //var end_date = moment().subtract(1, 'days').format('YYYY-MM-DD');
      

      $(".prmoclsbtn").on( "click", function() {
        $(this).parents('.promobandtop').fadeOut()
      });
      
      /**
        * click chart
        **/
      /*var click_ctx = document.getElementById('clickChart').getContext('2d');
      var clcikgradientFill = click_ctx.createLinearGradient(0, 0, 0, 500);
      clcikgradientFill.addColorStop(0.4, 'rgba(153, 170, 255, 0.9)');
      clcikgradientFill.addColorStop(0.85, 'rgba(255, 255, 255, 0.7)');
      var myChart = new Chart(click_ctx, {

          type: 'line',
          data: {
              labels: ['01/07/2021', '02/07/2021', '03/07/2021', '04/07/2021', '05/07/2021', '06/07/2021', '07/07/2021', '08/07/2021', '09/07/2021', '10/07/2021', '11/07/2021', '12/07/2021'],
              datasets: [{
                  borderColor: '#002BFC',
                  pointBorderColor: '#002BFC',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointRadius: 2,
                  fill: true,
                  backgroundColor: clcikgradientFill,
                  borderWidth: 1,
                  data: [0,2500,2800,3000,4000,3200,4500,4400,3800,3400,3400,3950,2800,6000]
              }]
          },
          options: {
              animation: {
                  easing: "easeInOutBack"
              },
              plugins:{
                  legend:false
              },
              scales: {
                    y:{
                      fontColor: "#ffffff",
                      fontStyle: "normal",
                      beginAtZero: true,
                      maxTicksLimit: 5,
                      padding: 30,
                      grid:{
                        borderWidth:0,
                      },
                      ticks: {
                        stepSize: 1000,
                        callback: function(value) {
                           var ranges = [
                              { divider: 1e6, suffix: 'M' },
                              { divider: 1e3, suffix: 'k' }
                           ];
                           function formatNumber(n) {
                              for (var i = 0; i < ranges.length; i++) {
                                 if (n >= ranges[i].divider) {
                                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                                 }
                              }
                              return n;
                           }
                           return '' + formatNumber(value);
                        }
                     }
                  },
                  x:{
                      padding: 10,
                      fontColor: "#ffffff",
                      fontStyle: "normal",
                      grid: {
                        display:false
                      }
                  }
                  
              },
          }
      });
      /**
        * cost chart
        **/
      /*var ctx = document.getElementById('myChart').getContext("2d");
      var costgradientFill = ctx.createLinearGradient(0, 0, 0, 500);
      costgradientFill.addColorStop(0.4, 'rgba(110, 245, 197, 0.9)');
      costgradientFill.addColorStop(0.85, 'rgba(255, 255, 255, 0.7)');

      var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: ['01/07/2021', '02/07/2021', '03/07/2021', '04/07/2021', '05/07/2021', '06/07/2021', '07/07/2021', '08/07/2021', '09/07/2021', '10/07/2021', '11/07/2021', '12/07/2021','13/07/2021',],
              datasets: [{
                  borderColor: '#14AE77',
                  pointBorderColor: '#14AE77',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointRadius: 2,
                  fill: true,
                  backgroundColor: costgradientFill,
                  borderWidth: 1,
                  data: [0,2500,2800,3000,4000,3200,4500,4400,3800,3400,3400,3950,2800,6000]
              }]
          },
          options: {
              animation: {
                  easing: "easeInOutBack"
              },
              plugins:{
                  legend:false
              },
              scales: {
                  y:{
                      fontColor: "#ffffff",
                      fontStyle: "normal",
                      beginAtZero: true,
                      maxTicksLimit: 5,
                      padding: 30,
                      grid:{
                        borderWidth:0,
                      },
                      ticks: {
                        stepSize: 1000,
                        callback: function(value) {
                           var ranges = [
                              { divider: 1e6, suffix: 'M' },
                              { divider: 1e3, suffix: 'k' }
                           ];
                           function formatNumber(n) {
                              for (var i = 0; i < ranges.length; i++) {
                                 if (n >= ranges[i].divider) {
                                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                                 }
                              }
                              return n;
                           }
                           return '' + formatNumber(value);
                        }
                     }
                  },
                  x:{
                      padding: 10,
                      fontColor: "#ffffff",
                      fontStyle: "normal",
                      grid: {
                        display:false
                      }
                  }
                  
              },
          }
      });
      /**
        * Conversions chart
        **/
      /*var conversions_ctx = document.getElementById('conversionsChart').getContext('2d');
      var conversionsgradientFill = click_ctx.createLinearGradient(0, 0, 0, 500);
      conversionsgradientFill.addColorStop(0.4, 'rgba(255, 229, 139, 0.9)');
      conversionsgradientFill.addColorStop(0.85, 'rgba(255, 255, 255, 0.7)');
      var myChart = new Chart(conversions_ctx, {

          type: 'line',
          data: {
              labels: ['01/07/2021', '02/07/2021', '03/07/2021', '04/07/2021', '05/07/2021', '06/07/2021', '07/07/2021', '08/07/2021', '09/07/2021', '10/07/2021', '11/07/2021', '12/07/2021'],
              datasets: [{
                  borderColor: '#C0980E',
                  pointBorderColor: '#C0980E',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointRadius: 2,
                  fill: true,
                  backgroundColor: conversionsgradientFill,
                  borderWidth: 1,
                  data: [0,2500,2800,3000,4000,3200,4500,4400,3800,3400,3400,3950,2800,6000]
              }]
          },
          options: {
              animation: {
                  easing: "easeInOutBack"
              },
              plugins:{
                  legend:false
              },
              scales: {
                    y:{
                      fontColor: "#ffffff",
                      fontStyle: "normal",
                      beginAtZero: true,
                      maxTicksLimit: 5,
                      padding: 30,
                      grid:{
                        borderWidth:0,
                      },
                      ticks: {
                        stepSize: 1000,
                        callback: function(value) {
                           var ranges = [
                              { divider: 1e6, suffix: 'M' },
                              { divider: 1e3, suffix: 'k' }
                           ];
                           function formatNumber(n) {
                              for (var i = 0; i < ranges.length; i++) {
                                 if (n >= ranges[i].divider) {
                                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                                 }
                              }
                              return n;
                           }
                           return '' + formatNumber(value);
                        }
                     }
                  },
                  x:{
                      padding: 10,
                      fontColor: "#ffffff",
                      fontStyle: "normal",
                      grid: {
                        display:false
                      }
                  }
                  
              },
          }
      });
      /**
        * Sales chart
        **/
      /*var sales_ctx = document.getElementById('salesChart').getContext('2d');
      var salesgradientFill = click_ctx.createLinearGradient(0, 0, 0, 500);
      salesgradientFill.addColorStop(0.4, 'rgba(107, 232, 255, 0.9)');
      salesgradientFill.addColorStop(0.85, 'rgba(255, 255, 255, 0.75)');

      var myChart = new Chart(sales_ctx, {

          type: 'line',
          data: {
              labels: ['01/07/2021', '02/07/2021', '03/07/2021', '04/07/2021', '05/07/2021', '06/07/2021', '07/07/2021', '08/07/2021', '09/07/2021', '10/07/2021', '11/07/2021', '12/07/2021'],
              datasets: [{
                  borderColor: '#159EB8',
                  pointBorderColor: '#159EB8',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointRadius: 2,
                  fill: true,
                  backgroundColor: salesgradientFill,
                  borderWidth: 1,
                  data: [0,2500,2800,3000,4000,3200,4500,4400,3800,3400,3400,3950,2800,6000]
              }]
          },
          options: {
              animation: {
                  easing: "easeInOutBack"
              },
              plugins:{
                  legend:false
              },
              scales: {
                  y:{
                      fontColor: "#ffffff",
                      fontStyle: "normal",
                      beginAtZero: true,
                      maxTicksLimit: 5,
                      padding: 30,
                      grid:{
                        borderWidth:0,
                      },
                      ticks: {
                        stepSize: 1000,
                        callback: function(value) {
                           var ranges = [
                              { divider: 1e6, suffix: 'M' },
                              { divider: 1e3, suffix: 'k' }
                           ];
                           function formatNumber(n) {
                              for (var i = 0; i < ranges.length; i++) {
                                 if (n >= ranges[i].divider) {
                                    return (n / ranges[i].divider).toString() + ranges[i].suffix;
                                 }
                              }
                              return n;
                           }
                           return '' + formatNumber(value);
                        }
                     }

                  },
                  x:{
                      padding: 10,
                      fontColor: "#ffffff",
                      fontStyle: "normal",
                      grid: {
                        display:false
                      }
                  }
              },
          }
      });
      /**
        * End chart
        * table responcive
        **/
        $('.mbl-table').basictable({
          breakpoint: 768
        });
        

    });
    </script>
    <?php
    }
    protected function add_upgrdsbrs_btn_calss($featur_name){
      if($this->plan_id == 1){
        return "upgrdsbrs-btn";
      }else if($featur_name != ""){
        $upcoming_featur  = array('download_pdf','schedule_email');
        if(in_array($featur_name, $upcoming_featur)){
          return "upcoming-featur-btn";
        }
      }
    }

    /**
     * Main html code
     *
     * @since    4.1.4
     */
	  public function current_html(){
	  ?>
    <div class="dashbrdpage-wrap">
      <div class="dflex align-items-center mt24 dshbrdtoparea">
        <div class="dashtp-left">
          <button class="dashtpleft-btn <?php echo $this->add_upgrdsbrs_btn_calss('download_pdf'); ?>"><img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/download-icon.png'; ?>" alt="" />Download PDF</button>
          <button class="dashtpleft-btn <?php echo $this->add_upgrdsbrs_btn_calss('schedule_email'); ?>"><img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/clock-icon.png'; ?>" alt="" />Schedule Email</button>
        </div>
        <div class="dashtp-right">

          <?php /*
          <div class="dshtprightselect">
              <select>
                  <option>All</option>
                  <option>All</option>
                  <option>All</option>
              </select>
          </div>*/ ?>
          <?php if($this->plan_id != 1){?>
          <div id="reportrange" class="dshtpdaterange" >
              <div class="dateclndicn">
                  <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/claendar-icon.png'; ?>" alt="" />
              </div> 
              <span class="daterangearea report_range_val"></span>
              <div class="careticn"><img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/caret-down.png'; ?>" alt="" /></div>
          </div>
        <?php } else{ ?>
          <div class="dshtpdaterange <?php echo $this->add_upgrdsbrs_btn_calss('download_pdf'); ?>">
              <div class="dateclndicn">
                  <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/claendar-icon.png'; ?>" alt="" />
              </div> 
              <span class="daterangearea report_range_val"></span>
              <div class="careticn"><img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/caret-down.png'; ?>" alt="" /></div>
          </div>
        <?php }?>

        </div>
      </div>
      <?php if($this->ga_traking_type == "GA4"){?>
      <div class="temp_note"><p>The reporting dashboard feature is only available for Google Analytics 3 properties currently. We are working on Google Analytics 4 dashboard, we will update you once it is live.</p></div>
    <?php } ?>
      <!--- dashboard summary section start -->
      <div class="wht-rnd-shdwbx mt24 dashsmry-wrap">
        <div class="dashsmry-item">
          <div class="dashsmrybx" id="s1_transactionsPerSession">
            <div class="dshsmrycattxt dash-smry-title">Conversion Rate</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/green-up.png'; ?>" alt="" />
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx" id="s1_transactionRevenue">
            <div class="dshsmrycattxt dash-smry-title">Revenue </div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx mblsmry3bx" id="s1_transactions">
            <div class="dshsmrycattxt dash-smry-title">Total Transactions </div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx mblsmry3bx" id="s1_revenuePerTransaction">
            <div class="dshsmrycattxt dash-smry-title">Avg. Order Value</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx mblsmry3bx flwdthmblbx" id="s1_productAddsToCart">
            <div class="dshsmrycattxt dash-smry-title">Added to Cart</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
        </div>
        <div class="dashsmry-item">
          <div class="dashsmrybx" id="s1_productRemovesFromCart">
            <div class="dshsmrycattxt dash-smry-title">Removed from Cart</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx" id="s1_sessions">
            <div class="dshsmrycattxt dash-smry-title">Sessions</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx" id="s1_users">
            <div class="dshsmrycattxt dash-smry-title">Total Users</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx" id="s1_newUsers">
            <div class="dshsmrycattxt dash-smry-title">New Users</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx" id="s1_productDetailViews">
            <div class="dshsmrycattxt dash-smry-title">Product Views</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          
        </div>
        <?php /*
        <div class="dashsmry-item">
          <div class="dashsmrybx" id="s1_transactionShipping">
            <div class="dshsmrycattxt dash-smry-title">Shipping</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>
          <div class="dashsmrybx" id="s1_transactionTax">
            <div class="dshsmrycattxt dash-smry-title">TAX</div>
            <div class="dshsmrylrgtxt dash-smry-value">-</div>
            <div class="updownsmry dash-smry-compare-val">
                %
            </div>
            <div class="dshsmryprdtxt">From Previous Period</div>
          </div>

        </div> */?>
      </div>
      <!--- dashboard summary section end -->

      <!--- dashboard ecommerce cahrt section start -->
      <div class="mt24 dshchrtwrp ecomfunnchart">
        <div class="row">
          <?php if($this->plan_id != 1){?>
          <div class="col50">
            <div class="chartbx ecomfunnchrtbx ecom-funn-chrt-bx">
              <div class="chartcntnbx">
                <h5>Ecommerce Conversion Funnel</h5>
                <div class="chartarea">
                   <canvas id="ecomfunchart" width="400" height="300"></canvas>
                </div>
                <hr>
                <div class="ecomchartinfo">
                  <div class="ecomchrtinfoflex">
                    <div class="ecomchartinfoitem">
                        <div class="ecomchartinfolabel">Sessions</div>
                        <div class="chartpercarrow conversion_s1"></div>
                    </div>
                    <div class="ecomchartinfoitem">
                        <div class="ecomchartinfolabel">Product View</div>
                        <div class="chartpercarrow conversion_s2"></div>
                    </div>
                    <div class="ecomchartinfoitem">
                        <div class="ecomchartinfolabel">Add to Cart</div>
                        <div class="chartpercarrow conversion_s3"></div>
                    </div>
                    <div class="ecomchartinfoitem">
                        <div class="ecomchartinfolabel">Checkouts</div>
                        <div class="chartpercarrow conversion_s4"></div>
                    </div>
                    <div class="ecomchartinfoitem">
                        <div class="ecomchartinfolabel">Order Confirmation</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php }else{ ?>
            <div class="col50">
              <div class="chartbx ecomfunnchrtbx">
                <div class="chartcntnbx prochrtftr">
                  <h5>Ecommerce Conversion Funnel</h5>
                  <div class="chartarea">
                     <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/ecom-chart.jpg'; ?>" alt="" />
                  </div>
                </div>
                <div class="prochrtovrbox">
                  <div class="prochrtcntn">
                    <div class="prochrttop">
                      <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/lock-orange.png'; ?>" alt="" />
                      Locked 
                    </div>
                    <h5>Conversion Funnel</h5>
                    <p>This Report will help you visualize drop offs at each stage of your shopping funnel starting from home page to product page, cart page to checkout page and to final order confirmation page. Find out the major drop offs at each stage and take informed data driven decisions to increase the conversions and better marketing ROI.</p>
                    <a class="blueupgrdbtn" href="<?php echo $this->pro_plan_site; ?>" target="_blank">Upgrade Now</a>
                  </div>
                </div>
              </div>
            </div>
          <?php }?>

          <?php if($this->plan_id != 1){?>
            <div class="col50">
              <div class="chartbx ecomfunnchrtbx ecom-checkout-funn-chrt-bx">
                <div class="chartcntnbx">
                  <h5>Ecommerce Checkout Funnel</h5>
                  <div class="chartarea">
                     <canvas id="ecomcheckoutfunchart" width="400" height="300"></canvas>
                  </div>
                  <hr>
                <div class="ecomchartinfo ecomcheckoutfunchartinfo">
                  <div class="ecomchrtinfoflex">
                    <div class="ecomchartinfoitem">
                      <div class="ecomchartinfolabel">Checkout Step 1</div>
                      <div class="chartpercarrow checkoutfunn_s1"></div>
                    </div>
                    <div class="ecomchartinfoitem">
                      <div class="ecomchartinfolabel">Checkout Step 2</div>
                      <div class="chartpercarrow checkoutfunn_s2"></div>
                    </div>
                    <div class="ecomchartinfoitem">
                      <div class="ecomchartinfolabel">Checkout Step 3</div>
                      <div class="chartpercarrow checkoutfunn_s3"></div>
                    </div>
                    <div class="ecomchartinfoitem">
                      <div class="ecomchartinfolabel">Purchase</div>
                    </div>
                    
                  </div>
                </div>

                </div>
              </div>
            </div>
          <?php }else{ ?>
            <div class="col50">
              <div class="chartbx ecomfunnchrtbx">
                <div class="chartcntnbx prochrtftr">
                  <h5>Checkout Funnel</h5>
                  <div class="chartarea">
                     <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/ecom-chart.jpg'; ?>" alt="" />
                  </div>
                </div>
                <div class="prochrtovrbox">
                  <div class="prochrtcntn">
                    <div class="prochrttop">
                      <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/lock-orange.png'; ?>" alt="" />
                      Locked 
                    </div>
                    <h5>Checkout Funnel</h5>
                    <p>This Report will help you in finding out the performance of your checkout page and leakages at each checkout step. Identify the small areas of improvements and fix them for smooth customer experience on your ecommerce site.</p>
                    <a class="blueupgrdbtn" href="<?php echo $this->pro_plan_site; ?>" target="_blank">Upgrade Now</a>
                  </div>
                </div>
              </div>
            </div>
          <?php  } ?>
          
        </div>
      </div>
      <!--- dashboard ecommerce cahrt section over -->

      <!--- Product Performance section start -->
      <?php if($this->plan_id != 1){?>
      <div class="mt24 whiteroundedbx dshreport-sec">
        <div class="row dsh-reprttop">
          <div class="dshrprttp-left">
            <h4>Product Performance Report</h4>
            <a href="#" class="viewallbtn <?php echo $this->add_upgrdsbrs_btn_calss('download_pdf'); ?>">View all <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/blue-right-arrow.png'; ?>" alt="" /></a>
          </div>
        </div>
        <div class="dashtablewrp product_performance_report" id="product_performance_report">
          <table class="dshreporttble mbl-table" >
              <thead>
                  <tr>
                      <th class="prdnm-cell">Product Name</th>
                      <th>Views</th>
                      <th>Added to Cart</th>
                      <th>Orders</th>
                      <th>Qty</th>
                      <th>Revenue (<?php echo $this->ga_currency_symbols; ?>)</th>
                      <th>Avg Price (<?php echo $this->ga_currency_symbols; ?>)</th>
                      <th>Refund Amount (<?php echo $this->ga_currency_symbols; ?>)</th>
                      <th>Cart to details (%)</th>
                      <th>Buy to details (%)</th>
                </tr>
              </thead>
              <tbody>                                    
              </tbody>
          </table>
        </div>
      </div>
      <?php }else{ ?>
        <div class="mt24 whiteroundedbx dshreport-sec">
          <div class="row dsh-reprttop">
            <div class="col">
              <div class="chartbx ecomfunnchrtbx">
                <div class="chartcntnbx prochrtftr">
                  <h5>Product Performance Report</h5>
                  <div class="chartarea">
                     <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/table-data.jpg'; ?>" alt="" />
                  </div>
                </div>
                <div class="prochrtovrbox">
                  <div class="prochrtcntn">
                    <div class="prochrttop">
                      <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/lock-orange.png'; ?>" alt="" />
                      Locked 
                    </div>
                    <h5>Product Performance Report</h5>
                    <p>This report will help you understand how products in your store are performing and based on it you can take informed merchandising decision to further increase your revenue.</p>
                    <a class="blueupgrdbtn" href="<?php echo $this->pro_plan_site; ?>" target="_blank">Upgrade Now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php  } ?>
      <!--- Product Performance section over -->

      <!--- Source Performance Report section start -->
      <?php if($this->plan_id != 1){?>
      <div class="mt24 whiteroundedbx dshreport-sec">
          <div class="row dsh-reprttop">
              <div class="dshrprttp-left">
                <h4>Source/Medium Performance Report</h4>
                <a href="" class="viewallbtn <?php echo $this->add_upgrdsbrs_btn_calss('download_pdf'); ?>">View all <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/blue-right-arrow.png'; ?>" alt="" /></a>
              </div>
          </div>
          <div class="dashtablewrp medium_performance_report" id="medium_performance_report">
              <table class="dshreporttble mbl-table" >
                  <thead>
                      <tr>
                          <th class="prdnm-cell">Source/Medium</th>
                          <th>Conversion (%)</th>
                          <th>Revenue (<?php echo $this->ga_currency_symbols; ?>)</th>
                          <th>Total transactions</th>
                          <th>Avg Order value (<?php echo $this->ga_currency_symbols; ?>)</th>
                          <th>Added to carts</th>
                          <th>removed from cart</th>
                          <th>Product views</th>
                          <th>Users</th>
                          <th>Sessions</th>
                    </tr>
                  </thead>
                  <tbody>
                      
                  </tbody>
              </table>
          </div>
      </div>
      <?php }else{ ?>
        <div class="mt24 whiteroundedbx dshreport-sec">
          <div class="row dsh-reprttop">
            <div class="col">
              <div class="chartbx ecomfunnchrtbx">
                <div class="chartcntnbx prochrtftr">
                  <h5>Source/Medium Performance Report</h5>
                  <div class="chartarea">
                     <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/table-data.jpg'; ?>" alt="" />
                  </div>
                </div>
                <div class="prochrtovrbox">
                  <div class="prochrtcntn">
                    <div class="prochrttop">
                      <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/lock-orange.png'; ?>" alt="" />
                      Locked 
                    </div>
                    <h5>Source/Medium Performance Report</h5>
                    <p>Find out the performance of each of your traffic channels. You can access which campaigns or channels are attributing sales, add to carts, product views etc. You can also see your shopping and checkout behavior funnels for each of the channels and take informed decisions of managing your ad spends for each channel.</p>
                    <a class="blueupgrdbtn" href="<?php echo $this->pro_plan_site; ?>" target="_blank">Upgrade Now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php  } ?>
      <!--- Source Performance Report section over -->


      <!--- Shopping and Google Ads Performance section start -->
      <?php /*
      <div class="mt24 whiteroundedbx ggladsperfom-sec">
          <h4>Shopping and Google Ads Performance</h4>
          <div class="row">
            <div class="col50">
              <div class="chartbx ggladschrtbx">
                <div class="chartcntnbx">
                  <h5>Clicks</h5>
                  <div class="chartarea">
                     <canvas id="clickChart" width="400" height="300" class="chartcntainer"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="col50">
              <div class="chartbx ggladschrtbx">
                <div class="chartcntnbx">
                  <h5>Cost</h5>
                  <div class="chartarea">
                     <canvas id="myChart" width="400" height="300" class="chartcntainer"></canvas>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col50">
              <div class="chartbx ggladschrtbx">
                <div class="chartcntnbx">
                  <h5>Conversions</h5>
                  <div class="chartarea">
                     <canvas id="conversionsChart" width="400" height="300" class="chartcntainer"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="col50">
              <div class="chartbx ggladschrtbx">
                <div class="chartcntnbx">
                  <h5>Sales</h5>
                  <div class="chartarea">
                     <canvas id="salesChart" width="400" height="300" class="chartcntainer"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div> */ ?>
      <!--- Shopping and Google Ads Performance section start -->
      <!-- UPGRADE SUBSCRIPTION -->
      <div id="upgradsbscrptn" class="pp-modal whitepopup upgradsbscrptn-pp">
        <div class="sycnprdct-ppcnt">
          <div class="ppwhitebg pp-content upgradsbscrptnpp-cntr">
            <div class="ppclsbtn absltpsclsbtn clsbtntrgr">
              <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/close-white.png'; ?>" alt="">
            </div>
            <div class="upgradsbscrptnpp-hdr">
              <h5>Upgrade to Pro..!!</h5>
            </div>
            <div class="ppmodal-body">
              <p>This feature is only available in the paid plan. Please upgrade to get the full range of reports and more.</p>
              <div class="ppupgrdbtnwrap">
                <a class="blueupgrdbtn" href="<?php echo $this->pro_plan_site; ?>" target="_blank">Upgrade Now</a>
              </div>
            </div>              
          </div>
        </div>
      </div>
      <!--  Upcoming featur -->
      <div id="upcoming_featur" class="pp-modal whitepopup upcoming_featur-btn-pp">
        <div class="sycnprdct-ppcnt">
          <div class="ppwhitebg pp-content upgradsbscrptnpp-cntr">
            <div class="ppclsbtn absltpsclsbtn clsbtntrgr">
              <img src="<?php echo ENHANCAD_PLUGIN_URL.'/admin/images/close-white.png'; ?>" alt="">
            </div>
            <div class="upgradsbscrptnpp-hdr">
              <h5>Upcoming..!!</h5>
            </div>
            <div class="ppmodal-body">
              <p>We are currently working on this feature and we will reach out to you once this is live.</p>
              <p>We aim to give you a capability to schedule the reports directly in your inbox whenever you want.</p>              
            </div>              
          </div>
        </div>
      </div>

    </div>
    <?php	  	
	  }
	}
}
new Conversios_Dashboard();