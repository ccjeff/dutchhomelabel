<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once(dirname(__FILE__) . "/../system/http/http-functions.php");

class CloudInvoiceRequestor {
    
    function addtaxes($creds) {
        $jsonstring = $this->getJson('addtaxes', $creds->getCloudinvoiceapikey(), $creds->getInvoicesystem(), $this->retrieveShoptaxes());
        $http = new CloudInvoiceHTTP();
        return $http->send_msg($jsonstring, $creds);
    }
    
    function addpaymentmethods($creds) {
        $jsonstring = $this->getJson('addpaymentmethods', $creds->getCloudinvoiceapikey(), $creds->getInvoicesystem(), $this->retrievePaymentmethods());
        $http = new CloudInvoiceHTTP();
        return $http->send_msg($jsonstring, $creds);
    }
    
    private function retrieveShoptaxes() {
        global $wpdb;
        
        $etr = array();
        $query = "SELECT * FROM {$wpdb->prefix}woocommerce_tax_rates as tax_rates";
        $taxrates = $wpdb->get_results( $wpdb->prepare($query, 0));

        foreach ($taxrates as $taxrate) {    
            $export_taxrates = array();
            $export_taxrates['shoptaxid'] = $taxrate->tax_rate_id;
            $ctry = $taxrate->tax_rate_country;
            if ($ctry == '') {
               $export_taxrates['shoptaxname'] = $taxrate->tax_rate_name;
            } else {
               $export_taxrates['shoptaxname'] = $taxrate->tax_rate_name . ' - ' . $ctry; 
            }
            $export_taxrates['shopcountry'] = $ctry;
            if (strlen($taxrate->tax_rate) > 0) {
                $export_taxrates['shoppercentage'] = number_format((float)$taxrate->tax_rate, 2, '.','');
            } else {
                $export_taxrates['shoppercentage'] = '0';
            }
            $export_taxrates['shoptaxclass'] = $taxrate->tax_rate_class;
            $etr[] = $export_taxrates;
        }
        
        return $etr;
    }
    
    private function retrievePaymentmethods() {
        global $woocommerce;
        
        $epm = array();
        
        foreach ($woocommerce->payment_gateways->payment_gateways as $pgw) {
            if ($pgw->enabled == 'yes') {
                $pm = array();
                $pm['shoppm'] = $pgw->id;
                $pm['shoppmname'] = $pgw->title;
            
                $epm[] = $pm;
            }
        }

        return $epm;
    }
    
    private function getJson($action, $cikey, $invoicesystem, $array) {
       return json_encode(array(
           'action' => $action,
           'cloudinvoiceapikey' => $cikey,
           'invoicesystem' => $invoicesystem,
           'webshopurl' => get_site_url(),
           'details' => $array
       ));
    }
}
