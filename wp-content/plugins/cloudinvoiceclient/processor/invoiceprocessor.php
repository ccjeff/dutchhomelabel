<?php

/**
 * Used to create and review invoices
 *
 * @author sophie
 */
include_once(dirname(__FILE__) . "/../controller/credentials-controller.php");
include_once(dirname(__FILE__) . "/../system/http/http-functions.php");
include_once(dirname(__FILE__) . "/../system/utils/htmlobjectcreator.php");

class WO_InvoiceProcessor {
    
    function displayInvoicebox($order) {
        //Retrieve the credentials to determine the action based on the invoicesystem
        $credcontroller = new CredentialsController();
        $creds = $credcontroller->get_credentials();
        
        if ($creds->getInvoicesystem() == 'moneybird' || $creds->getInvoicesystem() == 'moneybird2') {
           //$string = $this->getInvoicedisplayMoneyBird($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'factuursturen') {
           //$string = $this->getInvoicedisplayFactuursturen($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'reeleezee') {
           //$string = $this->getInvoicedisplayReeleezee($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'twinfieldtrx') {
           //$string = $this->getInvoicedisplayTwinfieldtrx($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'twinfieldfacturen') {
           //$string = $this->getInvoicedisplayTwinfieldinv($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'exactonline') {
           //$string = $this->getInvoicedisplayExactonline($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'asperion') {
           //$string = $this->getInvoicedisplayAsperion($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'yuki') {
           //$string = $this->getInvoicedisplayYuki($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'imuis') {
           //$string = $this->getInvoicedisplayMuis($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'cash') {
           //$string = $this->getInvoicedisplayCash($creds, $order);
        }
        
        if ($creds->getInvoicesystem() == 'visma') {
           //$string = $this->getInvoicedisplayCash($creds, $order);
        }
        
        $string = '';
        return $string;
    }
    
    private function getInvoicedisplayMoneyBird($creds, $order) {
        
        $jsonstring = json_encode(array(
                              'credentials' => $creds->createArray(),
                              'action' => 'manread_invoice',
                              'orderid' => $order->id
                      ));
   
        $invoice = json_decode($this->processInvoice($jsonstring, $creds, $order->id, 'manread_invoice'), true);
                
        $string = '';
        $string .= '<script>var orderidx = ' . $order->id . '</script>';
        $htmlobject = new HTMLObjectCreator();
        
        $string .= '<div class=\'cloudinvoicebox\'>';
        if ($invoice['httpcode'] == '400') {
            $string .= '<label>'. __('Geen factuur beschikbaar', 'cloudinvoice') . '</label>';
            $string .= $htmlobject->createCheckBox('create', __('Creeer de factuur', 'cloudinvoice'), 'mbcloudinvoice_create');
        } else {
            $string .= '<label>'. __('Factuur aangemaakt', 'cloudinvoice') . '</label>';
            $string .= '<p style=\'font-weight: 300; margin-bottom: 10px;\'>' . __('Naam klant', 'cloudinvoice') . ' ' . $invoice['clientname'] . ', ' . __('bedrag', 'cloudinvoice') . ' ' . $invoice['totalincl'] . ' ' . __('en factuurdatum', 'cloudinvoice') . ' ' . $invoice['invoicedate'];
            if ($invoice['stateinvoice'] != 'draft') {
                $string .= '<br><a href=\'' . $invoice['link'] . '\' target=\'_blank\'>' . __('Bekijk de factuur', 'cloudinvoice') . '</a></p>';
            }
        }
         
        if ($invoice['httpcode'] == 400 || $invoice['stateinvoice'] == 'draft') {
                $string .= $htmlobject->createCheckBox('send', __('Email de factuur naar de klant', 'cloudinvoice'), 'mbcloudinvoice_send');
        } else {
            if ($invoice['send-method'] == 'email') {
                  $string .= '<label>'. __('De factuur is verzonden naar de klant', 'cloudinvoice') . '</label>';
            }
            if ($invoice['send-method'] == 'hand') {
                $string .= '<label>'. __('De factuur is opgeslagen en moet naar de klant gestuurd worden', 'cloudinvoice') . '</label>';
            }
            if ($invoice['send-method'] == 'post') {
                $string .= '<label>'. __('De factuur is via de post naar de klant verstuurd', 'cloudinvoice') . '</label>';
            }
        }
        
        if ($invoice['httpcode'] == 400 || $invoice['stateinvoice'] != 'paid') {
            $string .= $htmlobject->createCheckBox('pay', __('Creeer een betaling', 'cloudinvoice'), 'mbcloudinvoice_createpayment');
            $string .= $htmlobject->createButton('mbcloudinvoice_processinvoice', __('Verwerk de factuur', 'cloudinvoice'));
            $string .= "<div id='spinwheel1' style='display: none;'>";
            $string .= "<img src='". WP_PLUGIN_URL . '/' . dirname( plugin_basename(__FILE__) ) . "/images/spinningwheel.gif' /></div>";
        } else {
            $string .= '<label>'. sprintf(__('De factuur %s is betaald', 'cloudinvoice'), $invoice['invoicenumber']) . '</label>';
        }
       
        $string .= '</div>';
        $string .= '<div class=\'error\' id=\'stf_error\'></div>';
        $string .= '<div class=\'success\' id=\'stf_success\'></div>';
        
        return $string;
    }
    
    private function getInvoicedisplayFactuursturen($creds, $order) {
        $jsonstring = json_encode(array(
                              'credentials' => $creds->createArray(),
                              'action' => 'manread_invoice',
                              'orderid' => $order->id
                      ));
   
        $invoice = json_decode($this->processInvoice($jsonstring, $creds, $order->id, 'manread_invoice'), true);
        
        $string = '';
        $string .= '<script>var orderidx = ' . $order->id . '</script>';
        $htmlobject = new HTMLObjectCreator();
        
        $string .= '<div class=\'cloudinvoicebox\'>';
        if ($invoice['httpcode'] == '400') {
            $string .= '<div class=\'fs_createbox\'>';
            $string .= '<label>'. __('Geen factuur beschikbaar', 'cloudinvoice') . '</label>';
            $string .= $htmlobject->createRadio('concept', __('Creeer een concept-factuur', 'cloudinvoice'), 'fscloudinvoice_concept');
            $string .= $htmlobject->createRadio('create', __('Creeer een factuur', 'cloudinvoice'), 'fscloudinvoice_create');
            $string .= $htmlobject->createRadio('send', __('Creeer een factuur en mail hem naar de klant', 'cloudinvoice'), 'fscloudinvoice_send');
            $string .= $htmlobject->createRadio('pay', __('Creeer een factuur en betaling', 'cloudinvoice'), 'fscloudinvoice_createpayment');
            $string .= $htmlobject->createRadio('payandsend', __('Creeer een betaling en mail de factuur naar de klant', 'cloudinvoice'), 'fscloudinvoice_payandsend');
            $string .= $htmlobject->createButton('fscloudinvoice_processinvoice', __('Process invoice', 'cloudinvoice'));
            $string .= "<div id='spinwheel1' style='display: none;'>";
            $string .= "<img src='". WP_PLUGIN_URL . '/' . dirname( plugin_basename(__FILE__) ) . "/images/spinningwheel.gif' /></div>";
        } else {
            if ($invoice['invoiceid'] != 'Created') {
                      $string .= '<label>'. __('Factuur is aangemaakt', 'cloudinvoice') . '</label>';
                      $string .= '<p style=\'font-weight: 300; margin-bottom: 10px;\'>' . __('Naam klant', 'cloudinvoice') . ' ' . $invoice['clientname'] . ', ' . __('bedrag', 'cloudinvoice') . ' ' . $invoice['totalincl'] . ', ' . __('factuurdatum', 'cloudinvoice') . ' ' . $invoice['invoicedate'];
                      $string .= ', '. sprintf(__('factuurnummer %s', 'cloudinvoice'), $invoice['invoicenumber']) . '</p>';
                      $string .= '<p><a href=\'' . $invoice['link'] . '\' target=\'_blank\'>' . __('Bekijk de factuur', 'cloudinvoice') . '</a></p>';
            } else {
                    $string .= '<label>'. __('Concept-factuur gecreeerd', 'cloudinvoice') . '</label>';
            }
              
        }
            
        $string .= '</div>';
        $string .= '<div class=\'error\' id=\'stf_error\'></div>';
        $string .= '<div class=\'success\' id=\'stf_success\'></div>';
                
        return $string;
    }
    
    private function getInvoicedisplayReeleezee($creds, $order) {
        $jsonstring = json_encode(array(
                              'credentials' => $creds->createArray(),
                              'action' => 'manread_invoice',
                              'orderid' => $order->id
                      ));
   
        $invoice = json_decode($this->processInvoice($jsonstring, $creds, $order->id, 'manread_invoice'), true);
        
        $string = '';
        $string .= '<script>var orderidx = ' . $order->id . '</script>';
        $htmlobject = new HTMLObjectCreator();
        
        $string .= '<div class=\'cloudinvoicebox\'>';
        if ($invoice['httpcode'] == 400 || $invoice['resultcode'] == '400') {
            $string .= '<div class=\'rl_createbox\'>';
            $string .= '<label>'. __('Geen factuur beschikbaar', 'cloudinvoice') . '</label>';
            $string .= $htmlobject->createButton('rlcloudinvoice_processinvoice', __('Maak de factuur aan', 'cloudinvoice'));
            $string .= "<div id='spinwheel1' style='display: none;'>";
            $string .= "<img src='". WP_PLUGIN_URL . '/' . dirname( plugin_basename(__FILE__) ) . "/images/spinningwheel.gif' /></div>";
            $string .= '</div>';
        } 
        if ($invoice['httpcode'] == 205 || $invoice['resultcode'] == '205') {
            $string .= $invoice['message'];
            $string .= '</div>';
        } 
        if ($invoice['resultcode'] == '200') {
            $string .= '<label>'. __('Er is in Reeleezee een factuur voor deze bestelling aangemaakt.', 'cloudinvoice') . '</label>';
            $string .= '<p style=\'font-weight: 300; margin-bottom: 10px;\'>De gegevens van de factuur in Reeleezee zijn: ' . __('Totaalbedrag', 'cloudinvoice') . ' ' . $invoice['totalincl'] . ', ' . __('factuurdatum', 'cloudinvoice') . ' ' . $invoice['invoicedate'];
            $string .= ', '. sprintf(__('factuurnummer %s', 'cloudinvoice'), $invoice['invoicenumber']) . '</p>';
        }
            
        $string .= '</div>';
        $string .= '<div class=\'error\' id=\'stf_error\'></div>';
        $string .= '<div class=\'success\' id=\'stf_success\'></div>';
                
        return $string;
    }
    
    private function getInvoicedisplayTwinfieldtrx($creds, $order) {
        $string = '';
        return $string;
    }
    
    private function getInvoicedisplayTwinfieldinv($creds, $order) {
        $jsonstring = json_encode(array(
                              'credentials' => $creds->createArray(),
                              'action' => 'manread_invoice',
                              'orderid' => $order->id
                      ));
   
        $invoice = json_decode($this->processInvoice($jsonstring, $creds, $order->id, 'manread_invoice'), true);
        
        $string = '';
        $string .= '<script>var orderidx = ' . $order->id . '</script>';
        $htmlobject = new HTMLObjectCreator();
        
        $string .= '<div class=\'cloudinvoicebox\'>';
        if ($invoice['httpcode'] == '400') {
            $string .= '<div class=\'tw_createbox\'>';
            $string .= '<label>'. __('Geen factuur beschikbaar', 'cloudinvoice') . '</label>';
            $string .= $htmlobject->createCheckbox('create', __('Create invoice', 'cloudinvoice'), 'twcloudinvoice_create');
            $string .= $htmlobject->createButton('twcloudinvoice_processinvoice', __('Maak de factuur aan', 'cloudinvoice'));
            $string .= '</div>';
        } else {
            if ($invoice['invoiceid'] != 'Created') {
                      $string .= '<label>'. __('Factuur aangemaakt', 'cloudinvoice') . '</label>';
                      $string .= '<p style=\'font-weight: 300; margin-bottom: 10px;\'>' . __('Naam klant', 'cloudinvoice') . ' ' . $invoice['clientname'] . ', ' . __('bedrag', 'cloudinvoice') . ' ' . $invoice['totalincl'] . ', ' . __('factuurdatum', 'cloudinvoice') . ' ' . $invoice['invoicedate'];
                      $string .= ', '. sprintf(__('factuurnummer %s', 'cloudinvoice'), $invoice['invoicenumber']) . '</p>';
                      $string .= '<p><a href=\'' . $invoice['link'] . '\' target=\'_blank\'>' . __('Bekijk de factuur', 'cloudinvoice') . '</a></p>';
            } else {
                    $string .= '<label>'. __('Concept-factuur aangemaakt', 'cloudinvoice') . '</label>';
            }
              
        }
            
        $string .= '</div>';
        $string .= '<div class=\'error\' id=\'stf_error\'></div>';
        $string .= '<div class=\'success\' id=\'stf_success\'></div>';
                
        return $string;
    }
    
    private function getInvoicedisplayAsperion($creds, $order) {
        $string = '';
        return $string;
    }
    
    private function getInvoicedisplayYuki($creds, $order) {
        $string = '';
        return $string;
    }
    
    private function getInvoicedisplayExactonline($creds, $order) {
        $jsonstring = json_encode(array(
                  'credentials' => $creds->createArray(),
                  'action' => 'manread_invoice',
                  'orderid' => $order->id
          ));
   
        $invoice = json_decode($this->processInvoice($jsonstring, $creds, $order->id, 'manread_invoice'), true);

        $string = '';
        $string .= '<script>var orderidx = ' . $order->id . '</script>';
        $htmlobject = new HTMLObjectCreator();
        
        $string .= '<div class=\'cloudinvoicebox\'>';
        if ($invoice['httpcode'] == '400') {
            $string .= '<div class=\'tw_createbox\'>';
            $string .= '<label>'. __('Geen factuur beschikbaar', 'cloudinvoice') . '</label>';
            $string .= $htmlobject->createCheckbox('create', __('Create invoice', 'cloudinvoice'), 'twcloudinvoice_create');
            $string .= $htmlobject->createButton('twcloudinvoice_processinvoice', __('Maak de factuur aan', 'cloudinvoice'));
            $string .= '</div>';
        } else {
            if ($invoice['invoiceid'] != 'Created') {
                      $string .= '<label>'. __('Factuur aangemaakt', 'cloudinvoice') . '</label>';
                      $string .= '<p style=\'font-weight: 300; margin-bottom: 10px;\'>' . __('Naam klant', 'cloudinvoice') . ' ' . $invoice['clientname'] . ', ' . __('bedrag', 'cloudinvoice') . ' ' . $invoice['totalincl'] . ', ' . __('factuurdatum', 'cloudinvoice') . ' ' . $invoice['invoicedate'];
                      $string .= ', '. sprintf(__('factuurnummer %s', 'cloudinvoice'), $invoice['invoicenumber']) . '</p>';
                      $string .= '<p><a href=\'' . $invoice['link'] . '\' target=\'_blank\'>' . __('Bekijk de factuur', 'cloudinvoice') . '</a></p>';
            } else {
                    $string .= '<label>'. __('Concept-factuur aangemaakt', 'cloudinvoice') . '</label>';
            }
              
        }
            
        $string .= '</div>';
        $string .= '<div class=\'error\' id=\'stf_error\'></div>';
        $string .= '<div class=\'success\' id=\'stf_success\'></div>';
                
        return $string;
    }
    
    private function getInvoicedisplayMuis($creds, $order) {
        $string = '';
        return $string;
    }
    
    private function getInvoicedisplayCash($creds, $order) {
        $string = '';
        return $string;
    }
    
    private function getInvoicedisplayVisma($creds, $order) {
        $string = '';
        return $string;
    }
    
    function processInvoice($jsonstring, $creds) {
        if ($creds->getInvoicesystem() == 'moneybird' || 
                $creds->getInvoicesystem() == 'moneybird2' ||
                $creds->getInvoicesystem() == 'reeleezee' ||
                $creds->getInvoicesystem() == 'factuursturen' ||
                $creds->getInvoicesystem() == 'twinfieldfacturen' ||
                $creds->getInvoicesystem() == 'twinfieldtransacties' ||
                $creds->getInvoicesystem() == 'exactonline') {
            $http = new CloudInvoiceHTTP();
            $result = $http->send_msg($jsonstring, $creds);
            
            return $result;
        }
    }
    
}
