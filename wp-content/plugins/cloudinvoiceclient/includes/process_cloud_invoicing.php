<?php
include_once(dirname(__FILE__) . "/../system/utils/constants.php");
include_once(dirname(__FILE__) . "/../model/credentials.php");
include_once(dirname(__FILE__) . "/../controller/credentials-controller.php");
include_once(dirname(__FILE__) . "/../processor/platformrequestor.php");
include_once(dirname(__FILE__) . "/../processor/invoiceprocessor.php");
    
function stf_manorder() {
   $orderid = $_POST['orderid'];
   $invoiceactions = explode(';', $_POST['invoiceactions']);
   $credcontroller = new CredentialsController();
   $creds = $credcontroller->get_credentials();
   
   if(!$orderid){
       return;
   }
   
   $jsonstring = json_encode(array(
                         'credentials' => $creds->createArray(),
                         'action' => 'manorder',
                         'orderid' => $orderid,
                         'invoiceactions' => $invoiceactions
                  ));
   
   $invoiceprocessor = new WO_InvoiceProcessor();
   $result = $invoiceprocessor->processInvoice($jsonstring, $creds);
   
   echo $result;
   die();
}