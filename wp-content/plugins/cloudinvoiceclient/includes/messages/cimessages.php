<?php

/* Dealing with errors and messages from the factuursturen plugin */
final class CIMessageHandler {
    
    const SUCCESS = 'success';
    const ERROR = 'error';
    const BLANK = '';
    const CREATEINVOICE = 'createinvoice';
    const CREATECONNECTION = 'createinvoice';
    const SYNCPRODUCTS = 'syncproducts';
    const OPTIONSTATUS = 'cloudinvoice_status';
    const OPTIONMESSAGE = 'cloudinvoice_message';
    
    private $status;
    private $message;
    
    function __construct() {
        $this->setStatus('');
        $this->setMessage('');
    }
    function getStatus() {
        return $this->status;
    }
    
    function setStatus($str) {
        $this->status = $str;
    }
    
    function getMessage() {
        return $this->message;
    }
    
    function setMessage($str) {
        $this->message = $str;
    }
    
    function displaySuccess() {
        $htmlstring = '<div class="updated">' . $this->getMessage() . '</div>';
        return $htmlstring;
    }
    
    function displayError() {
        $htmlstring = '<div class="error">' . $this->getMessage() . '</div>';
        return $htmlstring;
    }
    
    function displayResult() {
        $this->setMessage(get_option(CIMessageHandler::OPTIONMESSAGE));
        $this->setStatus(get_option(CIMessageHandler::OPTIONSTATUS));
        $resultm = '';
        if ($this->getStatus() == CIMessageHandler::ERROR) {
           $resultm = $this->displayError();
        }
        if ($this->getStatus() == CIMessageHandler::SUCCESS) {
           $resultm = $this->displaySuccess();
        }
        if ($this->getStatus() == CIMessageHandler::BLANK) {}
        $this->clean();
        return $resultm;
    }
    
    function clean() {
       $stat1 = delete_option(CIMessageHandler::OPTIONMESSAGE);
       $stat2 = delete_option(CIMessageHandler::OPTIONSTATUS);
    }
    
    function processHttpResponse($httpcode, $body, $invs, $action) {
        if ($action == CIMessageHandler::CREATEINVOICE && ($invs == 'factuursturen' || $invs == 'exact')) {
            if (((int)$httpcode > 199) && ((int)$httpcode < 300)) {
                update_option(CIMessageHandler::OPTIONSTATUS, CIMessageHandler::SUCCESS);
                update_option(CIMessageHandler::OPTIONMESSAGE, 'The invoice with invoicenr ' . $body . ' is successfully created');
            } else {
                update_option(CIMessageHandler::OPTIONSTATUS, CIMessageHandler::ERROR);
                update_option(CIMessageHandler::OPTIONMESSAGE, 'There was a problem processing the invoice. The http return code is ' . $httpcode . ' and the message is ' .$body);
            }
        }
        if ($action == CIMessageHandler::CREATEINVOICE && $invs == 'moneybird') {
            if ((int)$httpcode == 0 || (int)$httpcode == 2) {
                update_option(CIMessageHandler::OPTIONSTATUS, CIMessageHandler::SUCCESS);
                if ((int)$httpcode == 0) {
                    update_option(CIMessageHandler::OPTIONMESSAGE, 'The invoice with invoicenr ' . $body . ' is successfully created');
                }
                if ((int)$httpcode == 2) {
                    update_option(CIMessageHandler::OPTIONMESSAGE, 'The connection was interrupted. Please check within MoneyBird that the invoice was created');
                }
            } else {
                update_option(CIMessageHandler::OPTIONSTATUS, CIMessageHandler::ERROR);
                update_option(CIMessageHandler::OPTIONMESSAGE, 'There was a problem processing the invoice. The code is ' . $httpcode . '. The message is ' . $body . '.');
            }
        }
        if ($action == CIMessageHandler::SYNCPRODUCTS && $invs == 'exact') {
            if (((int)$httpcode > 199) && ((int)$httpcode < 300)) {
                update_option(CIMessageHandler::OPTIONSTATUS, CIMessageHandler::SUCCESS);
                update_option(CIMessageHandler::OPTIONMESSAGE, 'The products are successfully synchronized with the invoicesystem');
            } else {
                update_option(CIMessageHandler::OPTIONSTATUS, CIMessageHandler::ERROR);
                update_option(CIMessageHandler::OPTIONMESSAGE, 'There was a problem processing this request. The http return code is ' . $httpcode . ' and the message is ' .$body);
            }
        }
    }
}