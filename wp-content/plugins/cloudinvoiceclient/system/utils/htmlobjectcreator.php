<?php

class HTMLObjectCreator {

    function createCheckbox($strname, $strvalue, $strid){
        $checkbox = '<div class=\'cloudinvoicecheckbox\'><input type=\'checkbox\' name=\'' . $strname . '\' id=\'' . $strid . '\'>' . $strvalue . '</input></div>';
        return $checkbox;
    }
    
    function createRadio($strname, $strvalue, $strid){
        $checkbox = '<div class=\'cloudinvoiceradio\'><input type=\'radio\' name=\'' . $strname . '\' id=\'' . $strid . '\'>' . $strvalue . '</input></div>';
        return $checkbox;
    }
    
    function createButton($strid, $strvalue){
        $button = '<div class=\'cloudinvoicebutton\'><a href=\'#\' id=\'' . $strid . '\'>' . $strvalue . '</a></div>';
        return $button;
    }
    
}