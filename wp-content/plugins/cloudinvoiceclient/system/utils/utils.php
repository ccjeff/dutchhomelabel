<?php

include_once(dirname(__FILE__) . "/constants.php");

class CIUtils {
    function elog($v){
        error_log(var_export($v, true), 0);
    }
    
    function xml($string) {
        return str_replace('&','en',str_replace('|', '-', html_entity_decode($string,ENT_QUOTES,'UTF-8')));
    }
    
    function readFormInput($string) {
        return htmlspecialchars(strip_tags(trim($string)));
    }
    
    function getWooCommerceVersion() {
        // If get_plugins() isn't available, require it
	if ( ! function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
       
        $plugin_data = get_plugin_data( ABSPATH . 'wp-content/plugins/woocommerce/woocommerce.php');
        $version = $plugin_data['Version'];
        return $version;
    } 

    function getOptions($invoicesystem) {
       $constants = new CI_Constants();
       
       $optionstring = '';
       if ($invoicesystem == '') {
           $optionstring .= '<option value=\'\'>Select system</option>';
       } else {
           $optionstring .= '<option value=\'' . $invoicesystem . '\'>' . $constants->getInvoicesystemname($invoicesystem) . '</option>';
       }

       if ($invoicesystem != 'reeleezee') {
           $optionstring .= '<option value=\'reeleezee\'>Reeleezee</option>';
       }

       if ($invoicesystem != 'exactonline') {
           $optionstring .= '<option value=\'exactonline\'>Exact Online</option>';
       }
       
       if ($invoicesystem != 'eboekhouden') {
           $optionstring .= '<option value=\'eboekhouden\'>e-Boekhouden</option>';
       }
       
       if ($invoicesystem != 'twinfieldtransacties') {
           $optionstring .= '<option value=\'twinfieldtransacties\'>Twinfield - Verkoopadministratie</option>';
       } 
       if ($invoicesystem != 'twinfieldfacturen') {
           $optionstring .= '<option value=\'twinfieldfacturen\'>Twinfield - Facturen</option>';
       } 
       
       if ($invoicesystem != 'eboekhouden') {
           $optionstring .= '<option value=\'eboekhouden\'>e-Boekhouden.nl</option>';
       }

       if ($invoicesystem != 'snelstart') {
           $optionstring .= '<option value=\'octopus\'>SnelStart</option>';
       }
                             
       if ($invoicesystem != 'moneybird2') {
           $optionstring .= '<option value=\'moneybird2\'>Moneybird</option>';
       }

       if ($invoicesystem != 'factuursturen') {
           $optionstring .= '<option value=\'factuursturen\'>Factuursturen</option>';
       }

       if ($invoicesystem != 'octopus') {
           $optionstring .= '<option value=\'octopus\'>Octopus</option>';
       }
                             
       if ($invoicesystem != 'offective') {
           $optionstring .= '<option value=\'octopus\'>Offective</option>';
       }
                             
       if ($invoicesystem != 'imuis') {
           $optionstring .= '<option value=\'imuis\'>Muis</option>';
       }
                             
       if ($invoicesystem != 'visma') {
           $optionstring .= '<option value=\'visma\'>Visma</option>';
       }
                             
       if ($invoicesystem != 'cash') {
           $optionstring .= '<option value=\'cash\'>Cash</option>';
       }
                             
       if ($invoicesystem != 'asperion') {
           $optionstring .= '<option value=\'asperion\'>Asperion</option>';
       }
                             
       return $optionstring;
    }
    
    function getDefaultactions($creds) {
       $constants = new CI_Constants();
       
       $optionstring = '';
       $defaultaction = $creds->getDefaultaction();
       error_log('defaultaction '. $defaultaction);
       
       if ($defaultaction == '') {
           $optionstring .= '<option value=\'\'>Niets</option>';
       } else {
           $optionstring .= '<option value=\'' . $defaultaction . '\'>' . $constants->getInvoiceactions($defaultaction) . '</option>';
           $optionstring .= '<option value=\'\'>Niets</option>';
       }

       if ($defaultaction != 'conceptinvoice' && $creds->getInvoicesystem() == 'factuursturen') {
           $optionstring .= '<option value=\'conceptinvoice\'>Creeer een concept-factuur</option>';
       }
       
       if ($defaultaction != 'createinvoice') {
           $optionstring .= '<option value=\'createinvoice\'>Creeer een factuur</option>';
       }

       if ($defaultaction != 'createinvoice;emailinvoice;' && $creds->getInvoicesystem() == 'factuursturen') {
           $optionstring .= '<option value=\'createinvoice;emailinvoice;\'>Creeer een factuur en email deze naar de klant</option>';
       }
       
       if ($defaultaction != 'createinvoice;payinvoice') {
           $optionstring .= '<option value=\'createinvoice;payinvoice;\'>Creeer een factuur en betaling</option>';
       }
       
       if ($defaultaction != 'createinvoice;payinvoice;emailinvoice;' && $creds->getInvoicesystem() == 'factuursturen') {
           $optionstring .= '<option value=\'createinvoice;payinvoice;emailinvoice;\'>Creeer een factuur met betaling en email deze naar de klant</option>';
       }
       
       return $optionstring;
    }
   
}
