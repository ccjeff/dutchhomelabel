<?php

class CI_Constants {

    function getInvoicesystemname($invoicesystem){
        if ($invoicesystem == 'reeleezee') {
            return 'Reeleezee';
        }
        if ($invoicesystem == 'moneybird') {
            return 'MoneyBird';
        }
        if ($invoicesystem == 'moneybird2') {
            return 'MoneyBird - Nieuw';
        }
        if ($invoicesystem == 'factuursturen') {
            return 'Factuursturen';
        }
        if ($invoicesystem == 'yuki') {
            return 'Yuki';
        }
        if ($invoicesystem == 'twinfieldtransacties') {
            return 'Twinfield - Verkoopadministratie';
        }
        if ($invoicesystem == 'twinfieldfacturen') {
            return 'Twinfield - Facturen';
        }
        if ($invoicesystem == 'exactonline') {
            return 'Exact Online';
        }
        if ($invoicesystem == 'eboekhouden') {
            return 'e-Boekhouden';
        }
        if ($invoicesystem == 'imuis') {
            return 'Muis';
        }
        if ($invoicesystem == 'visma') {
            return 'Visma';
        }
        if ($invoicesystem == 'asperion') {
            return 'Asperion';
        }     
        if ($invoicesystem == 'cash') {
            return 'Cash';
        }
        if ($invoicesystem == 'octopus') {
            return 'Octopus';
        }
        if ($invoicesystem == 'offective') {
            return 'Offective';
        }
        if ($invoicesystem == 'snelstart') {
            return 'SnelStart';
        }
    }
    
    function getInvoiceactions($action){
        if ($action == '') {
           return 'Niets';
        }
        
        if ($action == 'conceptinvoice') {
           return 'Creeer een concept-factuur';
        }
        if ($action == 'createinvoice') {
           return 'Creeer een factuur';
        }
        if ($action == 'createinvoice;payinvoice;') {
           return 'Creeer een factuur en betaling';
        }
        if ($action == 'createinvoice;emailinvoice;') {
           return 'Creeer een factuur en email deze naar de klant';
        }
        if ($action == 'createinvoice;payinvoice;emailinvoice;') {
           return 'Creeer een factuur met betaling en email deze naar de klant';
        }
    }
}
