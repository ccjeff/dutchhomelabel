<?php

class CloudInvoiceHTTP {
    
     /* send function via curl
     * @param jsonstring - data,
     *               url - url of sponizaservlet.
     * */
    function send_msg($jsonstring, $cred){
        
        $url = $cred->getCloudinvoiceurl();
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "jsonstring=$jsonstring");
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $resp = curl_exec($ch);
        
        //echo '<p>foutmelding ' . curl_errno ( $ch ) . "<br>";
        //echo '<p>foutboodschap ' . curl_error ( $ch ) . "<br>";
        //echo '<p>' . htmlentities($xml_string);
        //echo 'test ' . $resp;
        //print_r(curl_getinfo ( $ch ));
        //echo 'httpcode ' . curl_getinfo ( $ch, CURLINFO_HTTP_CODE ) . "<br>" . $url;
        
        curl_close($ch);
        return $resp;
    }

}

