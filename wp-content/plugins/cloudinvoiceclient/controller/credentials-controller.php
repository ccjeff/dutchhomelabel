<?php

class CredentialsController {
    
    function get_credentials(){
        include_once(dirname(__FILE__) . "/../model/credentials.php");
        $credsdata = new Credentials();
        $credsdata->merge();

        return $credsdata;
    }
    
    /**
    * Processes the Plugin-Options Post requests.
    * The settings.php file is used to find the name and fields defined in
    * the database.
    */
    function update_credentials($redirect = true){
 
        if (!current_user_can('manage_options')){
            wp_die('You are not allowed to be on this page.');
        }

        /* get the supported fields */
        include_once(dirname(__FILE__) . "/../model/credentials.php");
        
        $accreds = new Credentials();
        
        if (isset($_POST['invoice_system'])) {
           $accreds->setInvoicesystem(sanitize_text_field($_POST['invoice_system']));
        }
        
        if (isset($_POST['cloudinvoiceurl'])) {
           $accreds->setCloudinvoiceurl(sanitize_text_field($_POST['cloudinvoiceurl']));
        }
        
        if (isset($_POST['cloudinvoiceapikey'])) {
           $accreds->setCloudinvoiceapikey(sanitize_text_field($_POST['cloudinvoiceapikey']));
        }
        
        
        
        $accreds->update_credentials();
        /* redirect to previous page */
        if ($redirect) {
            wp_redirect($_SERVER['HTTP_REFERER']);
        }
    }
    
}
