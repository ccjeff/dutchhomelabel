<?php

function cloudinvoiceclient_settings() {

   include_once(dirname(__FILE__) . "/../controller/credentials-controller.php");
   include_once(dirname(__FILE__) . "/../system/utils/utils.php");
   
   $credcontroller = new CredentialsController();
   $creds = $credcontroller->get_credentials();
   $utils = new CIUtils();
   
?>
            
<div class="cloudinvoice_wrap">
<h2>Installatie koppeling WooCommerce</h2>
 <p>De installatie van de koppeling bestaat uit de volgende stappen:
    <br>1. Vul eerst de onderstaande gegevens in en klik op <strong>Opslaan</strong>. De BTW tarieven en betaalmethoden uit je webwinkel worden nu gesynchroniseerd
           met ons platform. 
    <br>2. Na de synchronisatie kun je dit scherm sluiten. Je gaat of verder met de installatie-link die je in je email hebt ontvangen, of je
           gaat verder met stap 2 als je een koppeling met Exact Online legt.
 </p>
 
 <div class="ci_install_step_1">
     <label class='cli_h1'>Stap 1</label>
     <form method='post' action='admin-post.php'>
        <input type='hidden' name='action' value='install'/>
        <div> 
            <label class='cli_h3 onethird'>Boekhoudsysteem</label>
            <div class='twothird'>
                <select id="stf_invoicesystem" name="invoice_system">
                <?php
                    echo $utils->getOptions($creds->getInvoicesystem());
                ?>
                </select>
            </div>
        </div>
        <div>
            <label class='cli_h3 onethird'>Toegangscode - zie email</label>
            <div class='twothird'>
                <input type='text' id='stf_cloudinvoiceapikey' name='cloudinvoiceapikey' value='<?php echo esc_html($creds->getCloudinvoiceapikey()) ?>' />
            </div>
        </div>
        <div>
            <label class='cli_h3 onethird'>URL cloudinvoice platform</label>
            <div class='twothird'>
                <input type='text' id='stf_servleturl' name='cloudinvoiceurl' value='<?php echo esc_html($creds->getCloudinvoiceurl()) ?>' />
            </div>
        </div>      
        <div>
            <input type='submit' id='stf_settings_submit' name='Submit' class='button-primary' value='Opslaan' />
        </div>
    </form>
 </div>
<?php }
