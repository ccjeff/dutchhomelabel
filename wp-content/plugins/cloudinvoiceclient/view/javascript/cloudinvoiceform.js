jQuery(document).ready(function($) {

     $('#mbcloudinvoice_createpayment').on('click', function() {
         if ($('#mbcloudinvoice_create').length) {
             $('#mbcloudinvoice_create').prop('checked', true);
         }
     });
     
     $('#mbcloudinvoice_send').on('click', function() {
         if ($('#mbcloudinvoice_create').length) {
             $('#mbcloudinvoice_create').prop('checked', true);
         }
     });
     
     $('#mbcloudinvoice_processinvoice').on('click', function() {

         $('#spinwheel1').show();

         var invoiceactions = '';
         if ($('#mbcloudinvoice_create').prop('checked')) {
             invoiceactions = invoiceactions + 'createinvoice;';
         }
         if ($('#mbcloudinvoice_createpayment').prop('checked')) {
             invoiceactions = invoiceactions + 'payinvoice;';
         }
         if ($('#mbcloudinvoice_send').prop('checked')) {
             invoiceactions = invoiceactions + 'sendinvoice;';
         }
         
         //var orderid = $('#orderid').val();
         var orderid = orderidx;

         jQuery.ajax({url: my_ajax_script.ajaxurl,
                      type: 'POST',
                      dataType: 'json',
                      data: ({action: 'stf_manorder', invoiceactions: invoiceactions, orderid: orderid}),
                      success: function(data) {
                                   //alert('terug op het clientplatform ' + JSON.stringify(data));
                                   if (data.error && data.error.length > 0) {
                                       $("#stf_success").text('');
                                       $("#stf_success").css('display', 'none');
                                       $("#stf_error").html('Foutmelding: ' + data.error);
                                       $("#stf_error").css('display', 'block');
                                   } else {
                                       $("#stf_error").text('');
                                       $("#stf_error").css('display', 'none');
                                       
                                       var success_string = 'De actie is succesvol uitgevoerd. <br>De gegevens van de factuur zijn: klantnaam ' + data.clientname + '. ';
                                       success_string += '<br>factuur bedrag ' + data.totalincl + ' en factuurdatum ' + data.invoicedate + '. ';
                                       if (!data.invoicenumber || data.invoicenumber.length == 0) {
                                           success_string += '<br>Deze factuur heeft nog geen factuurnummer. ';
                                       } else {
                                           success_string += 'Het factuurnummer is ' + data.invoicenumber + '. ';
                                       }
                                       
                                       if (data.stateinvoice != 'draft') {
                                           success_string += '<br><a href=\'' + data.link + '\' target=\'_blank\'>Bekijk de factuur</a>.';
                                       }
                                       
                                       $("#stf_success").html(success_string);
                                       $("#stf_success").css('display', 'inline-block');
                                  }
                       },
                       error: function(xhr, status) {
                              //alert('foutmelding ' + JSON.stringify(xhr) + ' status ' + status);
                      }
         }).always( function() {$('#spinwheel1').hide();});
     }); 
     
     $('#fscloudinvoice_processinvoice').on('click', function() {
         $('#spinwheel1').show();

         var invoiceactions = '';
         if ($('#fscloudinvoice_concept').prop('checked')) {
             invoiceactions = invoiceactions + 'conceptinvoice;';
         }
         if ($('#fscloudinvoice_create').prop('checked')) {
             invoiceactions = invoiceactions + 'createinvoice;';
         }
         if ($('#fscloudinvoice_createpayment').prop('checked')) {
             invoiceactions = invoiceactions + 'createinvoice;payinvoice;';
         }
         if ($('#fscloudinvoice_send').prop('checked')) {
             invoiceactions = invoiceactions + 'createinvoice;emailinvoice;';
         }
         if ($('#fscloudinvoice_payandsend').prop('checked')) {
             invoiceactions = invoiceactions + 'createinvoice;payinvoice;emailinvoice;';
         }
         
         //var orderid = $('#orderid').val();
         var orderid = orderidx;

         jQuery.ajax({url: my_ajax_script.ajaxurl,
                      type: 'POST',
                      dataType: 'json',
                      data: ({action: 'stf_manorder', invoiceactions: invoiceactions, orderid: orderid}),
                      success: function(data) {
                                   if (data.error && data.error.length > 0) {
                                       $("#stf_success").text('');
                                       $("#stf_success").css('display', 'none');
                                       $("#stf_error").html('Foutmelding: ' + data.error);
                                       $("#stf_error").css('display', 'block');
                                   } else {
                                       $("#stf_error").text('');
                                       $("#stf_error").css('display', 'none');
                                       
                                       var success_string = '';
                                       if (data.invoicenumber == 'Created') {
                                           success_string += 'Er is een conceptfactuur voor deze bestelling aangemaakt. ';
                                       } else {
                                           if (data.invoicenumber == '') {
                                               success_string += 'Er is een netwerkprobleem opgetreden bij het aanmaken van de factuur. Dit kan komen doordat internet traag of niet beschikbaar was. Controleer of de factuur is aangemaakt in Factuursturen. Zo niet, ververs dan het scherm en probeer het nog eens. ';
                                           } else {
                                               success_string += 'De actie is succesvol uitgevoerd. <br>De gegevens van de factuur zijn: klantnaam ' + data.clientname + '. ';
                                               success_string += '<br>factuur bedrag ' + data.totalincl + ' en factuurdatum ' + data.invoicedate + '. ';
                                               success_string += 'Het factuurnummer is ' + data.invoicenumber + '. ';
                                               success_string += '<br><a href=\'' + data.link + '\' target=\'_blank\'>Bekijk de factuur</a>.';
                                           }
                                       }
                                       
                                       $('#cloudinvoicebox').remove();
                                       
                                       $("#stf_success").html(success_string);
                                       $("#stf_success").css('display', 'inline-block');
                                  }
                       },
                       error: function(xhr, status) {
                              //alert('foutmelding ' + JSON.stringify(xhr) + ' status ' + status);
                      }
         }).always( function() {$('#spinwheel1').hide();});
     });
     
     $('#rlcloudinvoice_processinvoice').on('click', function() {

         $('#spinwheel1').css('display','inline-block');
         var invoiceactions = 'createinvoice;';
         
         //var orderid = $('#orderid').val();
         var orderid = orderidx;

         jQuery.ajax({url: my_ajax_script.ajaxurl,
                      type: 'POST',
                      dataType: 'json',
                      data: ({action: 'stf_manorder', invoiceactions: invoiceactions, orderid: orderid}),
                      success: function(data) {
                                   if (data.error && data.error.length > 0) {
                                       $("#stf_success").text('');
                                       $("#stf_success").css('display', 'none');
                                       $("#stf_error").html('Foutmelding: ' + data.error);
                                       $("#stf_error").css('display', 'block');
                                   } else {
                                       $("#stf_error").text('');
                                       $("#stf_error").css('display', 'none');
                                       
                                       var success_string = '';
                                       if (data.invoicenumber == 'Created') {
                                           success_string += 'Er is een conceptfactuur voor deze bestelling aangemaakt. ';
                                       } else {
                                           if (data.invoicenumber == '') {
                                               success_string += 'De boekingsbon is aangemaakt in Reeleezee';
                                           } else {
                                               success_string += 'De actie is succesvol uitgevoerd. <br>De gegevens van de factuur zijn: ';
                                               success_string += '<br>Factuur bedrag ' + data.totalincl + ' en factuurdatum ' + data.invoicedate + '. ';
                                               success_string += 'Het factuurnummer is ' + data.invoicenumber + '. ';
                                           }
                                       }
                                       
                                       $('#cloudinvoicebox').remove();
                                       
                                       $("#stf_success").html(success_string);
                                       $("#stf_success").css('display', 'inline-block');
                                  }
                       },
                       error: function(xhr, status) {
                              //alert('foutmelding ' + JSON.stringify(xhr) + ' status ' + status);
                      }
         }).always( function() {$('#spinwheel1').hide();});
     });
     
     $('#twcloudinvoice_processinvoice').on('click', function() {
         $('#spinwheel1').show();
         var invoiceactions = '';
         if ($('#twcloudinvoice_create').prop('checked')) {
             invoiceactions = invoiceactions + 'createinvoice;';
         }
         
         //var orderid = $('#orderid').val();
         var orderid = orderidx;

         jQuery.ajax({url: my_ajax_script.ajaxurl,
                      type: 'POST',
                      dataType: 'json',
                      data: ({action: 'stf_manorder', invoiceactions: invoiceactions, orderid: orderid}),
                      success: function(data) {
                                   //alert('terug op het clientplatform ' + JSON.stringify(data));
                                   if (data.error && data.error.length > 0) {
                                       $("#stf_success").text('');
                                       $("#stf_success").css('display', 'none');
                                       $("#stf_error").html('Foutmelding: ' + data.error);
                                       $("#stf_error").css('display', 'block');
                                   } else {
                                       $("#stf_error").text('');
                                       $("#stf_error").css('display', 'none');
                                       
                                       var success_string = 'De actie is succesvol uitgevoerd. <br>De gegevens van de factuur zijn: klantnaam ' + data.clientname + '. ';
                                       success_string += '<br>factuur bedrag ' + data.totalincl + ' en de factuurdatum ' + data.invoicedate + '. ';
                                       if (!data.invoicenumber || data.invoicenumber.length == 0) {
                                           success_string += '<br>Deze factuur heeft nog geen factuurnummer. ';
                                       } else {
                                           success_string += 'Het factuurnummer is ' + data.invoicenumber + '. ';
                                       }
                                       
                                       $('#cloudinvoicebox').remove();
                                       
                                       $("#stf_success").html(success_string);
                                       $("#stf_success").css('display', 'inline-block');
                                  }
                       },
                       error: function(xhr, status) {
                              //alert('foutmelding ' + JSON.stringify(xhr) + ' status ' + status);
                      }
         }).always( function() {$('#spinwheel1').hide();});

     });
     
     
});
