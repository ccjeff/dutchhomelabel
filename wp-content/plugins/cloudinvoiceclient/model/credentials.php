<?php

class Credentials {

    private $cloudinvoiceurl;
    private $cloudinvoiceapikey;
    private $invoicesystem;
    
    function getCloudinvoiceurl() {
        return $this->cloudinvoiceurl;
    }
    
    function setCloudinvoiceurl($string) {
        $this->cloudinvoiceurl = $string;
    }
    
    function getCloudinvoiceapikey() {
        return $this->cloudinvoiceapikey;
    }
    
    function setCloudinvoiceapikey($string) {
        $this->cloudinvoiceapikey = $string;
    }
    
    function getInvoicesystem() {
        return $this->invoicesystem;
    }
    
    function setInvoicesystem($string) {
        $this->invoicesystem = $string;
    }
    
    function merge() {
        $row = $this->get_credentials();
        $this->setInvoicesystem($row->invoice_system);
        $this->setCloudinvoiceurl($row->cloudinvoiceurl);
        $this->setCloudinvoiceapikey($row->cloudinvoiceapikey);
    }
    
    function get_credentials() {
        global $wpdb;

        $query = "SELECT * FROM {$wpdb->prefix}cloudinvoiceclient";
        $options = $wpdb->get_row( $wpdb->prepare($query,0));  
   
        return $options;
    }
    
    function update_credentials() {
        global $wpdb;
        
        $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix . "cloudinvoiceclient");
        $wpdb->insert($wpdb->prefix . "cloudinvoiceclient", $this->createInsertvalues());
    }
    
    function createInsertvalues() {
       return array(
             'invoice_system' => $this->getInvoicesystem(),
             'cloudinvoiceurl' => $this->getCloudinvoiceurl(),
             'cloudinvoiceapikey' => $this->getCloudinvoiceapikey(),
       ); 
    }
    
    function createArray() {
       return array(
             'invoicesystem' => $this->getInvoicesystem(),
             'cloudinvoiceurl' => $this->getCloudinvoiceurl(),
             'cloudinvoiceapikey' => $this->getCloudinvoiceapikey(),
       ); 
    }
}
