<?php
        
class ShopTaxes {
    
    function getShoptaxes() {
        global $wpdb;
        
        $etr = array();
        $taxrates = $wpdb->get_results( $wpdb->prepare("
                    SELECT * 
                    FROM {$wpdb->prefix}woocommerce_tax_rates as tax_rates", 0));
        
        foreach ($taxrates as $taxrate) {    
            $export_taxrates = array();
            $export_taxrates['shoptaxid'] = $taxrate->tax_rate_id;
            $ctry = $taxrate->tax_rate_country;
            if ($ctry == '') {
               $export_taxrates['shoptaxname'] = $taxrate->tax_rate_name;
            } else {
               $export_taxrates['shoptaxname'] = $taxrate->tax_rate_name . ' - ' . $ctry; 
            }
            $export_taxrates['shopcountry'] = $ctry;
            if (strlen($taxrate->tax_rate) > 0) {
                $export_taxrates['shoppercentage'] = number_format((float)$taxrate->tax_rate, 2, '.','');
            } else {
                $export_taxrates['shoppercentage'] = '0';
            }
            $etr[] = $export_taxrates;
        }
        
        return $etr;
    }
    
}