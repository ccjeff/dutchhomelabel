��          �      |      �     �            2   0      c     �     �     �     �  .   �               #     <     I     \  �   l          '     3     D  4  Q     �     �  $   �  5   �     �          9      J     k  ,   �     �     �  $   �               9  �   B     �                          
                                               	                                       Choose invoicesystem Clientname invoice Conceptinvoice created Create a paid invoice and email it to the customer Create a payment for the invoice Create concept invoice Create invoice Create invoice and payment Create payment Create the invoice and mail it to the customer History invoice Invoice created Invoice has been paid %s Invoice sent No invoice created Process invoice Spam in the <a href="%s">spam folder</a> older than 1 day is deleted automatically. Spam in the <a href="%1$s">spam folder</a> older than %2$d days is deleted automatically. View invoice invoicedate invoicenumber %s total amount PO-Revision-Date: 2015-03-12 13:09+0100
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/0.1
Project-Id-Version: Development
POT-Creation-Date: 
Last-Translator: Sophie <info@sponiza.nl>
Language-Team: 
 Kies je factuursysteem Tav Er is een concept-factuur aangemaakt Maak een betaalde factuur en email deze naar de klant Markeer de factuur als betaald Maak een concept factuur aan. Maak een factuur Maak een factuur en betaling aan Maak een betaalde factuur aan Maak een factuur en email deze naar de klant Audittrail factuur Factuur is aangemaakt Factuur is betaald, factuurnummer %s Factuur is verstuurd Geen factuurinformatie aanwezig Voer uit Spam in de <a href="%s">spam map</a> ouder dan 1 dag wordt automatisch verwijderd. Spam in de <a href="%1$s">spam map</a> ouder dan %2$d dagen wordt automatisch verwijderd. Bekijk de factuur factuurdatum factuurnummer %s totaalbedrag incl BTW 