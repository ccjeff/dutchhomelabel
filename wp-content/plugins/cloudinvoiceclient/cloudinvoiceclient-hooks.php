<?php

    add_action( 'wp_router_generate_routes', 'bl_add_routes', 20);

    /* add to admin_menu */
    add_action('admin_menu', 'cloudinvoiceclient');

    add_action('admin_post_install', 'update_cloudinvoiceclient_install' );

    function update_cloudinvoiceclient_install() {
        include_once(dirname(__FILE__) . "/controller/credentials-controller.php");
        include_once(dirname(__FILE__) . "/processor/platformrequestor.php");
        
        $credcontroller = new CredentialsController();
        $credcontroller->update_credentials();
        $credcontroller->get_credentials();
        
        $pfrequestor = new CloudInvoiceRequestor();
        $pfrequestor->addtaxes($credcontroller->get_credentials());
        $pfrequestor->addpaymentmethods($credcontroller->get_credentials());
    }

    function cloudinvoiceclient(){

        $hook_suffix = add_options_page("WooCommerce CloudInvoice Options",
            "WooCommerce CloudInvoice Settings", 'manage_options',
            'cloudinvoiceclient-plugin-page', 'cloudinvoiceclient_settings' );

        /* Use the hook suffix to compose the hook and register an action executed
         * when plugin's options page is loaded */
        add_action('load-' . $hook_suffix, 'cloudinvoiceclient_load_function');
    }
    
    /**
    * Current admin page is the options page for our plugin, so do not display
    * the notice. (remove the action responsible for this)
    */
    function cloudinvoiceclient_load_function() {
        
    }

    /* Update information */
    function cloudinvoiceclient_admin_notices() {
    }
