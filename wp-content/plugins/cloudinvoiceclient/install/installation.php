<?php
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

class CloudInvoiceInstallDB {
    function processDatabase() {
        global $wpdb;
        /* included to set activation admin notice */
        $table_name = $wpdb->prefix . "cloudinvoiceclient";
        
        $sql = "CREATE TABLE $table_name (
                          id mediumint(9) NOT NULL AUTO_INCREMENT,
                          invoice_system VARCHAR(128) DEFAULT '' NOT NULL,
                          cloudinvoiceurl VARCHAR(256) DEFAULT 'https://interface.cloudinvoice.company/api/v1/servlet/woocommerce/processorder.php' NOT NULL,
                          cloudinvoiceapikey VARCHAR(256) DEFAULT '' NOT NULL,
                          UNIQUE KEY id (id)
        );";

	dbDelta( $sql );
        
        if (!$wpdb->get_var("SELECT COUNT(*) FROM $table_name WHERE api_name = 'cloudinvoiceclient'")) {
            $wpdb->insert($table_name, array('api_key' => ''));
        }
   }
   
   function removeDatabase() {
      global $wpdb;
      $table = $wpdb->prefix . 'cloudinvoiceclient';
      $wpdb->query("DROP TABLE IF EXISTS $table");
   }
}
