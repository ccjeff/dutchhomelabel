<?php

/* 
 * Description and functions shortcodes
*/

require_once("includes/process_cloud_invoicing.php");

function sponiza_header() {
    wp_enqueue_style('cloudinvoicestyle',plugins_url('/cloudinvoiceclient/view/css/settings-cloudinvoice-style.css'));
    wp_enqueue_script('cloudinvoiceform',plugins_url('/cloudinvoiceclient/view/javascript/cloudinvoiceform.js'), array('jquery'), true);
    wp_localize_script('cloudinvoiceform', 'my_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('admin_enqueue_scripts','sponiza_header');
add_action("wp_ajax_stf_manorder", "stf_manorder");
