<?php

/* Plugin Name: cloudinvoiceclient
* Plugin URI:
* Description: Sends invoice from woocommerce orders to an invoicing tool of choice
* Version: 2.4.1
* Author: Sophie Fischer
* Author URI: http://www.sponiza.nl
* License: GPLv2 or later
* Textdomain: cloudinvoice
*/

/**
 * Check if WooCommerce is active
 **/
if (in_array('woocommerce' . DIRECTORY_SEPARATOR.
    'woocommerce.php',  apply_filters('active_plugins',get_option('active_plugins')))) {
    
    include_once("view/settings.php"); 
    include_once("system/utils/utils.php");
    include_once("system/utils/constants.php");
    include_once("cloudinvoiceclient-hooks.php");
    include_once("functions.php");
     
    function cloudinvoiceclient_activation() {
       
        add_action('admin_notices', 'cloudinvoiceclient_admin_notices');
        
        include_once(dirname(__FILE__) . "/install/installation.php");
        $db = new CloudInvoiceInstallDB();
        $db->processDatabase();

        /* add action */
        add_action('wp_head', 'call_send_invoice');
    }

    function cloudinvoice_invoicedetails($order){
        include_once(dirname(__FILE__) . "/processor/invoiceprocessor.php");
        $invoiceprocessor = new WO_InvoiceProcessor();
        echo $invoiceprocessor->displayInvoicebox($order);
    }

    function cloudinvoiceclient_deactivation() {
        include_once(dirname(__FILE__) . "/install/installation.php");
        $db = new CloudInvoiceInstallDB();
        $db->removeDatabase();
    }

    function language_init() {
        $path = dirname(plugin_basename( __FILE__ )) . '/languages/';
        $loaded = load_plugin_textdomain('cloudinvoice', false, $path);
        if (!$loaded && false) {
            //echo '<div class="error">Sample Localization: ' . __('Could not load the localization file: ' . $path, 'cloudinvoice') . '</div>';
            return;
        } 
    }
    
    add_action('init', 'language_init');
    
    register_activation_hook(__FILE__, 'cloudinvoiceclient_activation');
    register_uninstall_hook(__FILE__, 'cloudinvoiceclient_deactivation');
    //add_action('woocommerce_admin_order_data_after_order_details', 'cloudinvoice_invoicedetails' );
    
}
