<?php
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 */
define('AUTH_KEY',         'YSHhLSd7Qf3VUWJpqeBRD1xX5LIiYnthQLoigrqcTVDnGCQGeSy8z0YCHtwtQ6Cs');
define('SECURE_AUTH_KEY',  'uWy7aGHvtd9oNwUJhgVKKYuzSNufPwg4UQRXMgFF4XqRbmVHGGVS5YM9XNexnqij');
define('LOGGED_IN_KEY',    'V0xz4IrGwfhEzTGAJ6IIAhNduWGXQAGLXBCmAHy9j7B8EzTW80j4ioug2MEgotbn');
define('NONCE_KEY',        'F9d3SNBCRIwBxC198nsLCsNjmf80XUpg9YowWSxBLAzno9vTSH4oeDgqS2Nhy05u');
define('AUTH_SALT',        'b3Q0wdKp0quqp9L5p0P36r49HhNPhHNpJ7ayshKjbr3H0gQq0Fuy85GNsIowNBqG');
define('SECURE_AUTH_SALT', 'cWDbMSX6XQNt24diGCIW1DzbKoIWG0PP7X4MDRh1VaRYXg7uIEFgKFIQURptModJ');
define('LOGGED_IN_SALT',   '3smCyy8vhB3qehMGQ06VhKmhSw5h20D98M6wte9Lx54HbCowqKPmHXdiIvVAq4LU');
define('NONCE_SALT',       'UX9ojV0JCIG88EQvVYpNdBjW2M59IQuXQ8D9RGezuht5DAJvpHmauPz9pnGYET0W');
